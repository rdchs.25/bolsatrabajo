<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'InicioController@menu')->name('inicio.menu');

/*Login*/
Auth::routes();


Route::get('/home', 'HomeController@index')->name('welcome');
Route::get('/prueba', function () {
    return view('prueba');
});
Route::get('/mensajeregister', function () {
    return view('mensajeregister');
});
/*Categoria menu*/
Route::resource('categoriamenu', 'CategoriamenuController');
Route::post('categoriamenu/buscar', 'CategoriamenuController@buscar')->name('categoriamenu.buscar');
Route::get('categoriamenu/eliminar/{id}/{listarluego}', 'CategoriamenuController@eliminar')->name('categoriamenu.eliminar');

/*Opción menu*/
Route::resource('opcionmenu', 'OpcionmenuController');
Route::get('opcionmenu/eliminar/{id}/{listarluego}', 'OpcionmenuController@eliminar')->name('opcionmenu.eliminar');
Route::post('opcionmenu/buscar', 'OpcionmenuController@buscar')->name('opcionmenu.buscar');

/*Tipo de Usuario*/
Route::resource('tipousuario', 'TipousuarioController', array('except' => array('show')));
Route::post('tipousuario/buscar', 'TipousuarioController@buscar')->name('tipousuario.buscar');
Route::get('tipousuario/obtenerpermisos/{listar}/{id}', 'TipousuarioController@obtenerpermisos')->name('tipousuario.obtenerpermisos');
Route::post('tipousuario/guardarpermiso/{id}/{permiso_id}/{accion}', 'TipousuarioController@guardarpermisos')->name('tipousuario.guardarpermisos');
Route::get('tipousuario/eliminar/{id}/{listarluego}', 'TipousuarioController@eliminar')->name('tipousuario.eliminar');

/*Universidad*/

Route::resource('universidad', 'UniversidadController');
Route::post('universidad/buscar', 'UniversidadController@buscar')->name('universidad.buscar');
Route::get('universidad/eliminar/{id}/{listarluego}', 'UniversidadController@eliminar')->name('universidad.eliminar');

/*Colegio*/

Route::resource('colegio', 'ColegioController');
Route::post('colegio/buscar', 'ColegioController@buscar')->name('colegio.buscar');
Route::get('colegio/eliminar/{id}/{listarluego}', 'ColegioController@eliminar')->name('colegio.eliminar');

/*Empleador*/

Route::resource('empleador', 'EmpleadorController');
Route::post('empleador/buscar', 'EmpleadorController@buscar')->name('empleador.buscar');
Route::get('empleador/eliminar/{id}/{listarluego}', 'EmpleadorController@eliminar')->name('empleador.eliminar');

// /*Empleador*/
// Route::post('empleador/buscar', 'EmpleadorController@buscar')->name('empleador.buscar');
// Route::get('empleador/eliminar/{id}/{listarluego}', 'EmpleadorController@eliminar')->name('empleador.eliminar');
// Route::resource('empleador','EmpleadorController', array('except' => array('show')));
/*Empresa*/

Route::resource('empresa', 'EmpresaController');
Route::post('empresa/buscar', 'EmpresaController@buscar')->name('empresa.buscar');
Route::get('empresa/eliminar/{id}/{listarluego}', 'EmpresaController@eliminar')->name('empresa.eliminar');
/*Empresa*/

// Route::post('empresa/buscar', 'EmpresaController@buscar')->name('empresa.buscar');
// Route::get('empresa/eliminar/{id}/{listarluego}', 'EmpresaController@eliminar')->name('empresa.eliminar');
// Route::resource('empresa','EmpresaController', array('except' => array('show')));
// Route::post('/upload', 'EmpresaController@subirarchivo');
/*Persona*/

Route::resource('persona', 'PersonaController');
Route::post('persona/buscar', 'PersonaController@buscar')->name('persona.buscar');
Route::get('persona/eliminar/{id}/{listarluego}', 'PersonaController@eliminar')->name('persona.eliminar');

Route::get('persona/createconocimientoinformatico/{listarluego}', 'PersonaController@createconocimientoinformatico')->name('persona.createconocimientoinformatico');
Route::post('persona/editarconocimientoinformatico/{listarluego}', 'PersonaController@editarconocimientoinformatico')->name('persona.editarconocimientoinformatico');
Route::get('persona/mostrarconocimientoinformatico/{persona_id}', 'PersonaController@mostrarconocimientoinformatico')->name('persona.mostrarconocimientoinformatico');

Route::get('persona/createformacionlaboral/{listarluego}', 'PersonaController@createformacionlaboral')->name('persona.createformacionlaboral');
Route::post('persona/editarformacionlaboral/{listarluego}', 'PersonaController@editarformacionlaboral')->name('persona.editarformacionlaboral');
Route::get('persona/mostrarformacionlaboral/{persona_id}', 'PersonaController@mostrarformacionlaboral')->name('persona.mostrarformacionlaboral');

Route::get('persona/createformacionacademica/{listarluego}', 'PersonaController@createformacionacademica')->name('persona.createformacionacademica');
Route::post('persona/editarformacionacademica/{listarluego}', 'PersonaController@editarformacionacademica')->name('persona.editarformacionacademica');
Route::get('persona/mostrarformacionacademica/{persona_id}', 'PersonaController@mostrarformacionacademica')->name('persona.mostrarformacionacademica');

Route::get('persona/createformacioncomplementaria/{listarluego}', 'PersonaController@createformacioncomplementaria')->name('persona.createformacioncomplementaria');
Route::post('persona/editarformacioncomplementaria/{listarluego}', 'PersonaController@editarformacioncomplementaria')->name('persona.editarformacioncomplementaria');
Route::get('persona/mostrarformacioncomplementaria/{persona_id}', 'PersonaController@mostrarformacioncomplementaria')->name('persona.mostrarformacioncomplementaria');


/*Ubigeo*/
Route::get('provincia/cboprovincia/{id?}', array('as' => 'provincia.cboprovincia', 'uses' => 'ProvinciaController@cboprovincia'));
Route::get('distrito/cbodistrito/{id?}', array('as' => 'distrito.cbodistrito', 'uses' => 'DistritoController@cbodistrito'));

/*Discapacidad*/
Route::get('discapacidad/cbodiscapacidad/{id?}', array('as' => 'discapacidad.cbodiscapacidad', 'uses' => 'DiscapacidadController@cbodiscapacidad'));


/*Usuario*/

Route::resource('usuario', 'UsuarioController');
Route::post('usuario/buscar', 'UsuarioController@buscar')->name('usuario.buscar');
Route::get('usuario/eliminar/{id}/{listarluego}', 'UsuarioController@eliminar')->name('usuario.eliminar');
Route::get('usuario/autocompletarpersona/{querysearch}', 'UsuarioController@autocompletarpersona')->name('usuario.autocompletarpersona');
Route::get('usuario/activarusuario/{id}/{listarluego}', 'UsuarioController@activarusuario')->name('usuario.activarusuario');
Route::post('usuario/activarcuenta/{id}', 'UsuarioController@activarcuenta')->name('usuario.activarcuenta');


/*Pais*/
Route::post('pais/buscar', 'PaisController@buscar')->name('pais.buscar');
Route::get('pais/eliminar/{id}/{listarluego}', 'PaisController@eliminar')->name('pais.eliminar');
Route::resource('pais','PaisController', array('except' => array('show')));

/*Departamento*/
Route::post('departamento/buscar', 'DepartamentoController@buscar')->name('departamento.buscar');
Route::get('departamento/eliminar/{id}/{listarluego}', 'DepartamentoController@eliminar')->name('departamento.eliminar');
Route::resource('departamento','DepartamentoController', array('except' => array('show')));


/*Provincia*/
Route::post('provincia/buscar', 'ProvinciaController@buscar')->name('provincia.buscar');
Route::get('provincia/eliminar/{id}/{listarluego}', 'ProvinciaController@eliminar')->name('provincia.eliminar');
Route::resource('provincia','ProvinciaController', array('except' => array('show')));


/*Distrito*/
Route::post('distrito/buscar', 'DistritoController@buscar')->name('distrito.buscar');
Route::get('distrito/eliminar/{id}/{listarluego}', 'DistritoController@eliminar')->name('distrito.eliminar');
Route::resource('distrito','DistritoController', array('except' => array('show')));

Route::get('provincia/cboprovincia/{id?}', array('as' => 'provincia.cboprovincia', 'uses' => 'ProvinciaController@cboprovincia'));
Route::get('distrito/cbodistrito/{id?}', array('as' => 'distrito.cbodistrito', 'uses' => 'DistritoController@cbodistrito'));

/*Sector de Empresa*/
Route::post('sectorempresa/buscar', 'SectorempresaController@buscar')->name('sectorempresa.buscar');
Route::get('sectorempresa/eliminar/{id}/{listarluego}', 'SectorempresaController@eliminar')->name('sectorempresa.eliminar');
Route::resource('sectorempresa','SectorempresaController', array('except' => array('show')));

/*Escuela*/
Route::post('escuela/buscar', 'EscuelaController@buscar')->name('escuela.buscar');
Route::get('escuela/eliminar/{id}/{listarluego}', 'EscuelaController@eliminar')->name('escuela.eliminar');
Route::resource('escuela','EscuelaController', array('except' => array('show')));

/*Tipo de Documento*/
Route::post('tipodocumento/buscar', 'TipodocumentoController@buscar')->name('tipodocumento.buscar');
Route::get('tipodocumento/eliminar/{id}/{listarluego}', 'TipodocumentoController@eliminar')->name('tipodocumento.eliminar');
Route::resource('tipodocumento','TipodocumentoController', array('except' => array('show')));

/*Tipo de Discapacidad*/
Route::post('tipodiscapacidad/buscar', 'TipodiscapacidadController@buscar')->name('tipodiscapacidad.buscar');
Route::get('tipodiscapacidad/eliminar/{id}/{listarluego}', 'TipodiscapacidadController@eliminar')->name('tipodiscapacidad.eliminar');
Route::resource('tipodiscapacidad','TipodiscapacidadController', array('except' => array('show')));


/*Tipo de Contrato*/
Route::post('tipocontrato/buscar', 'TipocontratoController@buscar')->name('tipocontrato.buscar');
Route::get('tipocontrato/eliminar/{id}/{listarluego}', 'TipocontratoController@eliminar')->name('tipocontrato.eliminar');
Route::resource('tipocontrato','TipocontratoController', array('except' => array('show')));

/*Tipo de Documento Anexo*/
Route::post('tipodocumentoanexo/buscar', 'TipodocumentoanexoController@buscar')->name('tipodocumentoanexo.buscar');
Route::get('tipodocumentoanexo/eliminar/{id}/{listarluego}', 'TipodocumentoanexoController@eliminar')->name('tipodocumentoanexo.eliminar');
Route::resource('tipodocumentoanexo','TipodocumentoanexoController', array('except' => array('show')));

/*Idioma*/
Route::post('idioma/buscar', 'IdiomaController@buscar')->name('idioma.buscar');
Route::get('idioma/eliminar/{id}/{listarluego}', 'IdiomaController@eliminar')->name('idioma.eliminar');
Route::resource('idioma','IdiomaController', array('except' => array('show')));

/*Nivel de Estudio*/
Route::post('nivelestudio/buscar', 'NivelestudioController@buscar')->name('nivelestudio.buscar');
Route::get('nivelestudio/eliminar/{id}/{listarluego}', 'NivelestudioController@eliminar')->name('nivelestudio.eliminar');
Route::resource('nivelestudio','NivelestudioController', array('except' => array('show')));

/*Nivel de Estudio*/
Route::post('cargolaboral/buscar', 'CargolaboralController@buscar')->name('cargolaboral.buscar');
Route::get('cargolaboral/eliminar/{id}/{listarluego}', 'CargolaboralController@eliminar')->name('cargolaboral.eliminar');
Route::resource('cargolaboral','CargolaboralController', array('except' => array('show')));

/*Área Laboral*/
Route::post('arealaboral/buscar', 'ArealaboralController@buscar')->name('arealaboral.buscar');
Route::get('arealaboral/eliminar/{id}/{listarluego}', 'ArealaboralController@eliminar')->name('arealaboral.eliminar');
Route::resource('arealaboral','ArealaboralController', array('except' => array('show')));

/*Jornada Laboral*/
Route::post('jornadalaboral/buscar', 'JornadalaboralController@buscar')->name('jornadalaboral.buscar');
Route::get('jornadalaboral/eliminar/{id}/{listarluego}', 'JornadalaboralController@eliminar')->name('jornadalaboral.eliminar');
Route::resource('jornadalaboral','JornadalaboralController', array('except' => array('show')));


/*Discapacidad*/
Route::post('discapacidad/buscar', 'DiscapacidadController@buscar')->name('discapacidad.buscar');
Route::get('discapacidad/eliminar/{id}/{listarluego}', 'DiscapacidadController@eliminar')->name('discapacidad.eliminar');
Route::resource('discapacidad','DiscapacidadController', array('except' => array('show')));

/*Oferta Laboral*/
Route::post('ofertalaboral/buscar', 'OfertalaboralController@buscar')->name('ofertalaboral.buscar');
Route::get('ofertalaboral/eliminar/{id}/{listarluego}', 'OfertalaboralController@eliminar')->name('ofertalaboral.eliminar');
Route::resource('ofertalaboral','OfertalaboralController', array('except' => array('show')));
Route::get('ofertalaboral/cbodiscapacidad/{id?}', array('as' => 'discapacidad.cbodiscapacidad', 'uses' => 'DiscapacidadController@cbodiscapacidad'));
Route::get('ofertalaboral/{id}/verpostulantes', 'OfertalaboralController@verpostulantes')->name('ofertalaboral.verpostulantes');
Route::get('ofertalaboral/{id}/verofertalaboral', 'OfertalaboralController@verofertalaboral')->name('ofertalaboral.verofertalaboral');



/*DELETE*/
Route::get('eliminaridiomarequerido/{id}','OfertalaboralController@eliminaridiomarequerido');
//Route::get('eliminararrayidiomarequerido/{id}','OfertalaboralController@eliminararrayidiomarequerido');

Route::get('eliminarsoftwarerequerido/{id}','OfertalaboralController@eliminarsoftwarerequerido');
Route::get('eliminardiscapacidadrequerida/{id}','OfertalaboralController@eliminardiscapacidadrequerida');
Route::get('eliminarnivelprocesorequerida/{id}','OfertalaboralController@eliminarnivelprocesorequerida');


/*Nivelproceso*/
Route::post('nivelproceso/buscar', 'NivelprocesoController@buscar')->name('nivelproceso.buscar');
Route::get('nivelproceso/eliminar/{id}/{listarluego}', 'NivelprocesoController@eliminar')->name('nivelproceso.eliminar');
Route::resource('nivelproceso','NivelprocesoController', array('except' => array('show')));

/*Alertalaboral*/
Route::post('alertalaboral/buscar', 'AlertalaboralController@buscar')->name('alertalaboral.buscar');
Route::get('alertalaboral/eliminar/{id}/{listarluego}', 'AlertalaboralController@eliminar')->name('alertalaboral.eliminar');
Route::resource('alertalaboral','AlertalaboralController', array('except' => array('show')));

/*Alertalaboralcargo*/
Route::post('alertalaboralcargo/buscar', 'AlertalaboralcargoController@buscar')->name('alertalaboralcargo.buscar');
Route::get('alertalaboralcargo/eliminar/{id}/{listarluego}', 'AlertalaboralcargoController@eliminar')->name('alertalaboralcargo.eliminar');
Route::resource('alertalaboralcargo','AlertalaboralcargoController', array('except' => array('show')));

/*Alertalaboraldepartamento*/
Route::post('alertalaboraldepartamento/buscar', 'AlertalaboraldepartamentoController@buscar')->name('alertalaboraldepartamento.buscar');
Route::get('alertalaboraldepartamento/eliminar/{id}/{listarluego}', 'AlertalaboraldepartamentoController@eliminar')->name('alertalaboraldepartamento.eliminar');
Route::resource('alertalaboraldepartamento','AlertalaboraldepartamentoController', array('except' => array('show')));

/*Ofertalaboralpersona*/
Route::post('ofertalaboralpersona/buscar', 'OfertalaboralpersonaController@buscar')->name('ofertalaboralpersona.buscar');
Route::get('ofertalaboralpersona/eliminar/{id}/{listarluego}', 'OfertalaboralpersonaController@eliminar')->name('ofertalaboralpersona.eliminar');
Route::resource('ofertalaboralpersona','OfertalaboralpersonaController', array('except' => array('show')));