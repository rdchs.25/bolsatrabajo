<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePreferencialaboralTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('preferencialaboral', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('salario')->unsigned();
            $table->string('moneda');
            $table->boolean('disponibilidadextranjero')->default(false);
            $table->boolean('mostrarpreferenciasalario')->default(false);
            $table->boolean('mostrarcv')->default(false);
            $table->boolean('disponibilidadviajar')->default(false);
            $table->boolean('cambioresidencia')->default(false);
            $table->integer('sectorempresa_id')->unsigned();
            $table->integer('arealaboral_id')->unsigned();
            $table->integer('cargolaboral_id')->unsigned();
            $table->integer('departamento_id')->unsigned();
            $table->integer('jornadalaboral_id')->unsigned();
            $table->integer('tipocontrato_id')->unsigned();
            $table->integer('persona_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('sectorempresa_id')->references('id')->on('sectorempresa')->onDelete('restrict')->OnUpdate('restrict');
            $table->foreign('arealaboral_id')->references('id')->on('arealaboral')->onDelete('restrict')->OnUpdate('restrict');
            $table->foreign('cargolaboral_id')->references('id')->on('cargolaboral')->onDelete('restrict')->OnUpdate('restrict');
            $table->foreign('departamento_id')->references('id')->on('departamento')->onDelete('restrict')->OnUpdate('restrict');
            $table->foreign('jornadalaboral_id')->references('id')->on('jornadalaboral')->onDelete('restrict')->OnUpdate('restrict');
            $table->foreign('tipocontrato_id')->references('id')->on('tipocontrato')->onDelete('restrict')->OnUpdate('restrict');
            $table->foreign('persona_id')->references('id')->on('persona')->onDelete('restrict')->OnUpdate('restrict');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('preferencialaboral');
    }
}
