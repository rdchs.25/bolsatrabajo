<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConocimientoinformaticorequeridoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conocimientoinformaticorequerido', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre', 200)->unique();
            $table->string('nivel', 100)->nullable();
            $table->integer('ofertalaboral_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('ofertalaboral_id')->references('id')->on('ofertalaboral')->onDelete('restrict')->onUpdate('restrict');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conocimientoinformaticorequerido');
    }
}
