<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpresaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresa', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombrecomercial', 255);
            $table->char('ruc', 11);
            $table->string('razonsocial', 255);
            $table->string('telefono',9);
            $table->string('celular',20);
            $table->string('direccion', 255);
            $table->string('web', 150);
            $table->string('email', 100);
            $table->string('logo')->default('default.jpg');
            $table->text('descripcion');
            $table->integer('distrito_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('distrito_id')->references('id')->on('distrito')->onDelete('restrict')->onUpdate('restrict');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresa');
    }
}
