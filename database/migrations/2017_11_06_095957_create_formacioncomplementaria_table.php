<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormacioncomplementariaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formacioncomplementaria', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tipoformacion',50);
            $table->string('titulo',100);
            $table->string('institucion',150);
            $table->integer('cantidadhoras');
            $table->string('anioinicio');
            $table->string('mesinicio');
            $table->string('aniotermino')->nullable();
            $table->string('mestermino')->nullable();
            $table->boolean('estudiandoactualmente')->default(false);
            $table->integer('persona_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('persona_id')->references('id')->on('persona')->onDelete('restrict')->OnUpdate('restrict');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('formacioncomplementaria');
    }
}
