<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiscapacidadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discapacidad', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->integer('tipodiscapacidad_id')->unsigned();
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('tipodiscapacidad_id')->references('id')->on('tipodiscapacidad');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discapacidad');
    }
}
