<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpleadorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empleador', function (Blueprint $table) {
            $table->increments('id');
            $table->char('ruc',11);
            $table->string('razonsocial', 255);
            $table->string('nombrecomercial', 255);
            $table->string('telefono',9);
            $table->string('celular',20);
            $table->string('direccion', 255);
            $table->string('web', 255);
            $table->string('email', 255);
            $table->integer('nrotrabajadores')->unsigned();
            $table->text('descripcion');
            $table->boolean('extranjero');
            $table->boolean('norecibiremail');
            $table->boolean('aceptoterminos')->default(true);
            $table->boolean('datosverificados')->default(false);
            $table->string('logo')->default('default.jpg');
            $table->integer('distrito_id')->unsigned()->nullable();
            $table->integer('pais_id')->unsigned()->nullable();
            $table->integer('sectorempresa_id')->unsigned();
            $table->string('nombrecontacto');
            $table->string('telefonocontacto');
            $table->integer('celularcontacto')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('distrito_id')->references('id')->on('distrito')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('pais_id')->references('id')->on('pais')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('sectorempresa_id')->references('id')->on('sectorempresa')->onDelete('restrict')->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empleador');
    }
}
