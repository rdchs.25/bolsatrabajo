<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormacionlaboralTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formacionlaboral', function (Blueprint $table) {
            $table->increments('id');
            $table->char('situacionlaboral', 20)->nullable();
            $table->char('anioexperiencia', 25)->nullable();
            $table->date('fechainicio');
            $table->date('fechatermino')->nullable();
            $table->char('nropersonalcargo', 10);
            $table->string('empresa');
            $table->integer('salario')->unsigned();
            $table->char('moneda', 20);
            $table->text('descripcionlaboral')->nullable();
            $table->boolean('trabajaactualmente')->default(false);
            $table->boolean('mostrarsalario')->default(false);
            $table->integer('tipocontrato_id')->unsigned();
            $table->integer('sectorempresa_id')->unsigned();
            $table->integer('arealaboral_id')->unsigned();
            $table->integer('cargolaboral_id')->unsigned();
            $table->integer('persona_id')->unsigned();
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('tipocontrato_id')->references('id')->on('tipocontrato')->onDelete('restrict')->OnUpdate('restrict');
            $table->foreign('sectorempresa_id')->references('id')->on('sectorempresa')->onDelete('restrict')->OnUpdate('restrict');
            $table->foreign('arealaboral_id')->references('id')->on('arealaboral')->onDelete('restrict')->OnUpdate('restrict');
            $table->foreign('cargolaboral_id')->references('id')->on('cargolaboral')->onDelete('restrict')->OnUpdate('restrict');
            $table->foreign('persona_id')->references('id')->on('persona')->onDelete('restrict')->OnUpdate('restrict');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('formacionlaboral');
    }
}
