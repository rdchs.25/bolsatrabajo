<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIdiomaofertalaboralTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('idioma_ofertalaboral', function (Blueprint $table) {
            $table->increments('id');
            $table->string('lectura')->nullable();
            $table->string('escritura')->nullable();
            $table->string('conversacion')->nullable();
            $table->integer('idioma_id')->unsigned();
            $table->integer('ofertalaboral_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('ofertalaboral_id')->references('id')->on('ofertalaboral')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('idioma_id')->references('id')->on('idioma')->onDelete('restrict')->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('idioma_ofertalaboral');
    }
}
