<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableOfertalaboral extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ofertalaboral', function (Blueprint $table) {
            $table->string('duracioncontrato', 200)->nullable()->change();
            $table->date('fechafincontrato')->nullable()->change();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ofertalaboral', function (Blueprint $table) {
            $table->string('duracioncontrato', 200);
            $table->date('fechafincontrato');
        });
    }
}
