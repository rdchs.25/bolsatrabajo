<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDiscapacidadPersonaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discapacidad_persona', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('discapacidad_id')->unsigned();
            $table->integer('persona_id')->unsigned();
            $table->foreign('discapacidad_id')->references('id')->on('discapacidad')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('persona_id')->references('id')->on('persona')->onDelete('restrict')->onUpdate('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discapacidad_persona');
    }
}
