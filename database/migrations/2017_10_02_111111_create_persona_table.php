<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('persona', function (Blueprint $table) {
            $table->increments('id');
            $table->char('codigouniversitario', 50)->nullable();
            $table->string('nombres', 255)->nullable();
            $table->string('apellidopaterno', 100)->nullable();
            $table->string('apellidomaterno', 100)->nullable();
            $table->char('nrodocumento', 20)->nullable();
            $table->char('sexo', 1)->nullable();
            $table->string('direccion', 255)->nullable();
            $table->string('telefono', 9)->nullable();
            $table->string('celular', 20)->nullable();
            $table->string('email', 100)->nullable();
            $table->string('email2', 100)->nullable();
            $table->date('fechanacimiento')->nullable();
            $table->string('estadocivil', '7')->nullable();
            $table->string('twitter', '100')->nullable();
            $table->string('facebook', '100')->nullable();
            $table->string('linkedin', '100')->nullable();
            $table->text('descripcion')->nullable();
            $table->string('foto')->default('default.jpg');
            $table->string('cv')->nullable();
            $table->boolean('contactoempresa')->default(false);
            $table->boolean('contacto')->default(false);
            $table->boolean('privacidadalta')->default(true);
            $table->boolean('privacidadbaja')->default(false);
            $table->boolean('discapacidad')->default(false);
            $table->boolean('dispositivoespecial')->default(false);
            $table->boolean('protesis')->default(false);
            $table->boolean('noofertasempleo')->default(false);
            $table->boolean('noofertacurso')->default(false);
            $table->boolean('noayudar')->default(false);
            $table->boolean('nonewsletter')->default(false);
            $table->boolean('nocompartircv')->default(false);
            $table->boolean('nocomunicacioncomercial')->default(false);
            $table->boolean('aceptoterminos')->default(true);
            $table->integer('distrito_id')->unsigned()->nullable();
            $table->integer('pais_id')->unsigned()->nullable();
            $table->integer('tipodocumento_id')->unsigned()->nullable();
            $table->integer('cargolaboral_id')->unsigned()->nullable();
            $table->integer('escuela_id')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('distrito_id')->references('id')->on('distrito')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('pais_id')->references('id')->on('pais')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('tipodocumento_id')->references('id')->on('tipodocumento')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('cargolaboral_id')->references('id')->on('cargolaboral')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('escuela_id')->references('id')->on('escuela')->onDelete('restrict')->onUpdate('restrict');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('persona');
    }
}
