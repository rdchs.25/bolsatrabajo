
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiscapacidadofertalaboralTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discapacidad_ofertalaboral', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('discapacidad_id')->unsigned();
            $table->foreign('discapacidad_id')->references('id')->on('discapacidad');
            $table->integer('ofertalaboral_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('ofertalaboral_id')->references('id')->on('ofertalaboral')->onDelete('restrict')->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discapacidadofertalaboral');
    }
}
