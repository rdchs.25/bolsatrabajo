<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConocimientoinformaticoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conocimientoinformatico', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre', 100);
            $table->char('nivel', 12);
            $table->integer('anioexperiencia')->unsigned();
            $table->text('descripcionsoftware')->nullable();
            $table->integer('persona_id')->unsigned();
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('persona_id')->references('id')->on('persona')->onDelete('restrict')->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conocimientoinformatico');
    }
}
