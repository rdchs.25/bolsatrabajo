<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOfertalaboralpersonaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ofertalaboralpersona', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('persona_id')->unsigned();
            $table->integer('nivelproceso_id')->unsigned();
            $table->integer('ofertalaboral_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('persona_id')->references('id')->on('persona')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('nivelproceso_id')->references('id')->on('nivelproceso')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('ofertalaboral_id')->references('id')->on('ofertalaboral')->onDelete('restrict')->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ofertalaboralpersona');
    }
}
