<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIdiomaPersonaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('idioma_persona', function (Blueprint $table) {
            $table->increments('id');
            $table->char('lectura', 10)->nullable();
            $table->char('escritura', 10)->nullable();
            $table->char('conversacion', 10)->nullable();
            $table->boolean('nativo');
            $table->integer('idioma_id')->unsigned();
            $table->integer('persona_id')->unsigned();
            $table->foreign('idioma_id')->references('id')->on('idioma')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('persona_id')->references('id')->on('persona')->onDelete('restrict')->onUpdate('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('idioma_persona');
    }
}
