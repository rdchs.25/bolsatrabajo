<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormacionacademicaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formacionacademica', function (Blueprint $table) {
            $table->increments('id');
            $table->string('anioinicio');
            $table->string('mesinicio');
            $table->string('aniotermino')->nullable();
            $table->string('mestermino')->nullable();
            $table->string('situacion');
            $table->string('semestre')->nullable();
            $table->string('especialidad')->nullable();
            $table->string('tipoestudio')->nullable();
            $table->boolean('estudiandoactualmente')->default(false);
            $table->integer('nivelestudio_id')->unsigned();
            $table->integer('colegio_id')->unsigned()->nullable();
            $table->integer('universidad_id')->unsigned()->nullable();
            $table->integer('persona_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('nivelestudio_id')->references('id')->on('nivelestudio')->onDelete('restrict')->OnUpdate('restrict');
            $table->foreign('colegio_id')->references('id')->on('colegio')->onDelete('restrict')->OnUpdate('restrict');
            $table->foreign('universidad_id')->references('id')->on('universidad')->onDelete('restrict')->OnUpdate('restrict');
            $table->foreign('persona_id')->references('id')->on('persona')->onDelete('restrict')->OnUpdate('restrict');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('formacionacademica');
    }
}
