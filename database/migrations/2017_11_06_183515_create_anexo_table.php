<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnexoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('anexo', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion');
            $table->string('direccionarchivo');
            $table->integer('tipodocumento_id')->unsigned();
            $table->integer('persona_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('tipodocumento_id')->references('id')->on('tipodocumento')->onDelete('restrict')->OnUpdate('restrict');
            $table->foreign('persona_id')->references('id')->on('persona')->onDelete('restrict')->OnUpdate('restrict');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('anexo');
    }
}
