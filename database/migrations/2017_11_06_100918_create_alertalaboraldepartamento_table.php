<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlertalaboraldepartamentoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alertalaboraldepartamento', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('alertalaboral_id')->unsigned();
            $table->integer('departamento_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('alertalaboral_id')->references('id')->on('alertalaboral')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('departamento_id')->references('id')->on('departamento')->onDelete('restrict')->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alertalaboraldepartamento');
    }
}
