<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermisoTipousuarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permiso_tipousuario', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('permiso_id')->unsigned();
            $table->integer('tipousuario_id')->unsigned();
            $table->timestamps();
            $table->foreign('permiso_id')->references('id')->on('permiso')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('tipousuario_id')->references('id')->on('tipousuario')->onDelete('restrict')->onUpdate('restrict');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permiso_tipousuario');
    }
}
