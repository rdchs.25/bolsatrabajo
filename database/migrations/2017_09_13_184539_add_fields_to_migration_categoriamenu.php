<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToMigrationCategoriamenu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categoriamenu', function (Blueprint $table) {
            $table->string('icono',255)->after('orden');
            $table->integer('categoriamenu_id')->unsigned()->nullable();
            $table->foreign('categoriamenu_id')->references('id')->on('categoriamenu')->onDelete('restrict')->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categoriamenu', function (Blueprint $table) {
            $table->dropColumn('icono');
        });
    }
}
