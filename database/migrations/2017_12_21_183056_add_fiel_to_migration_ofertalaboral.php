<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFielToMigrationOfertalaboral extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ofertalaboral', function (Blueprint $table) {
            $table->integer('tipocontrato_id')->unsigned()->nullable()->after('jornadalaboral_id');;
            $table->foreign('tipocontrato_id')->references('id')->on('tipocontrato')->onDelete('restrict')->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ofertalaboral', function (Blueprint $table) {
            $table->dropColumn('tipocontrato_id');
        });
    }
}
