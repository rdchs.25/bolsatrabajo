<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpcionmenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opcionmenu', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre', 100);
            $table->string('link', 120);
            $table->integer('orden')->unsigned();
            $table->string('icono', 100)->default('fa fa-bank');
            $table->integer('categoriamenu_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('categoriamenu_id')->references('id')->on('categoriamenu')->onDelete('restrict')->onUpdate('restrict');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('opcionmenu');
    }
}
