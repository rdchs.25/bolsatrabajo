<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlertalaboralTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alertalaboral', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('arealaboral_id')->unsigned();
            $table->integer('escuela_id')->unsigned();
            $table->integer('persona_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('arealaboral_id')->references('id')->on('arealaboral')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('escuela_id')->references('id')->on('escuela')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('persona_id')->references('id')->on('persona')->onDelete('restrict')->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alertalaboral');
    }
}