<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlertalaboralcargoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alertalaboralcargo', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('alertalaboral_id')->unsigned();
            $table->integer('cargolaboral_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('alertalaboral_id')->references('id')->on('alertalaboral')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('cargolaboral_id')->references('id')->on('cargolaboral')->onDelete('restrict')->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alertalaboralcargo');
    }
}
