<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOfertalaboralTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ofertalaboral', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo', 400);
            $table->integer('nrovacante');
            $table->string('descripcion', 400);
            $table->string('duracioncontrato', 200);
            $table->string('horario', 200);
            $table->float('salario');
            $table->string('monedapago', 100);
            $table->boolean('mostrarsalario')->nullable();
            $table->string('comentariosalario', 400)->nullable();
            $table->string('experiencialaboral', 100);
            $table->integer('anioexperiencia')->nullable();
            $table->string('estudiominimo', 100);
            $table->boolean('requiereidioma')->nullable();
            $table->boolean('requieresoftware')->nullable();
            $table->string('descripcionrequisitos', 200);
            $table->boolean('vehiculopropio')->nullable();
            $table->boolean('tienelicencia')->nullable();
            $table->string('tipolicencia', 100)->nullable();
            $table->boolean('ofertaextranjero')->nullable();
            $table->string('direccionextranjero', 200)->nullable();
            $table->boolean('admitediscapacidad')->nullable();
            $table->integer('vigenciaoferta');
            $table->string('observacionoferta', 400)->nullable();
            $table->date('fechainiciocontrato');
            $table->date('fechafincontrato');
            $table->boolean('cambioresidencia')->nullable();
            $table->boolean('disponibilidadviajar')->nullable();
            $table->date('fechapublicacion');
            $table->boolean('publicar')->nullable();
            $table->integer('cargolaboral_id')->unsigned();
            $table->integer('arealaboral_id')->unsigned();
            $table->integer('pais_id')->unsigned()->nullable();
            $table->integer('jornadalaboral_id')->unsigned();
            $table->integer('nivelestudio_id')->unsigned();
            $table->integer('empleador_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('cargolaboral_id')->references('id')->on('cargolaboral')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('arealaboral_id')->references('id')->on('arealaboral')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('pais_id')->references('id')->on('pais')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('jornadalaboral_id')->references('id')->on('jornadalaboral')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('nivelestudio_id')->references('id')->on('nivelestudio')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('empleador_id')->references('id')->on('empleador')->onDelete('restrict')->onUpdate('restrict');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ofertalaboral');
    }
}
