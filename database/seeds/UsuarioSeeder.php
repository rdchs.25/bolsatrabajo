<?php

use Illuminate\Database\Seeder;

class UsuarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = new DateTime;
        $tipousuario_id = DB::table('tipousuario')->where('nombre', '=', 'Administrador')->first()->id;
        $usuario_id = DB::table('usuario')->insert(array(
            'nombre' => 'Administrador Principal Aplicación',
            'email' => 'administrador@udl.edu.pe',
            'usuario' => 'admin',
            'password' => Hash::make('123456'),
            'superusuario' => true,
            'estado' => 'Activado',
            'persona_id' => 1,
            'created_at' => $now,
            'updated_at' => $now
        ));
        DB::table('tipousuario_usuario')->insert(array(
            'tipousuario_id' => $tipousuario_id,
            'usuario_id' => $usuario_id,
            'created_at' => $now,
            'updated_at' => $now
        ));
    }
}
