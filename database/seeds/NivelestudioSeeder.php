<?php

use Illuminate\Database\Seeder;

class NivelestudioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = new DateTime;
        DB::table('nivelestudio')->insert(array(
            array('id' => '1',
                'nombre' => 'Secundaria',
                'created_at' => $now,
                'updated_at' => $now
            ),
            array('id' => '2',
                'nombre' => 'Técnico',
                'created_at' => $now,
                'updated_at' => $now
            ),
            array('id' => '3',
                'nombre' => 'Universitario',
                'created_at' => $now,
                'updated_at' => $now
            ),
            array('id' => '4',
                'nombre' => 'Especialización',
                'created_at' => $now,
                'updated_at' => $now
            ),
            array('id' => '5',
                'nombre' => 'Maestría',
                'created_at' => $now,
                'updated_at' => $now
            ),
            array('id' => '6',
                'nombre' => 'Doctorado',
                'created_at' => $now,
                'updated_at' => $now
            ),
        ));
    }
}
