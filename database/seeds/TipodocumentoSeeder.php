<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TipodocumentoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = new DateTime;

        DB::table('tipodocumento')->insert(array(
            array('id' => '1',
                'nombre' => 'DNI',
                'created_at' => $now,
                'updated_at' => $now
            ),
            array('id' => '2',
                'nombre' => 'Carnet Extranjeria',
                'created_at' => $now,
                'updated_at' => $now
            ),
        ));
    }
}
