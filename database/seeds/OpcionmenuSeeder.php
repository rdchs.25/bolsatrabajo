<?php

use Illuminate\Database\Seeder;

class OpcionmenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = new DateTime;
        $permisos = array('list', 'read', 'add', 'change', 'delete');

        /*Módulo Usuarios*/

        $categoriamenu_id = DB::table('categoriamenu')->where('nombre', '=', 'Usuarios')->first()->id;

        $datos = array(
            array(
                'nombre' => 'Categoría Menú',
                'link' => 'categoriamenu'
            ),
            array(
                'nombre' => 'Opción de Menú',
                'link' => 'opcionmenu'
            ),
            array(
                'nombre' => 'Tipos de usuario',
                'link' => 'tipousuario'
            ),
            array(
                'nombre' => 'Usuario',
                'link' => 'usuario'
            )
        );

        for ($i = 0; $i < count($datos); $i++) {

            DB::table('opcionmenu')->insert(array(
                'nombre' => $datos[$i]['nombre'],
                'link' => $datos[$i]['link'] . '.index',
                'orden' => $i + 1,
                'categoriamenu_id' => $categoriamenu_id,
                'created_at' => $now,
                'updated_at' => $now
            ));
            $opcionmenu_id = DB::table('opcionmenu')->where('nombre', '=', $datos[$i]['nombre'])->first()->id;

            /*Crear los permisos*/
            foreach ($permisos as $clave => $valor) {
                DB::table('permiso')->insert(array(
                    'nombre' => 'can ' . $valor,
                    'codigo' => strtolower($valor . '_' . $datos[$i]['link']),
                    'opcionmenu_id' => $opcionmenu_id,
                    'created_at' => $now,
                    'updated_at' => $now
                ));
            }

        }


        /*Módulo Mantenimientos*/
        $categoriamenu_id = DB::table('categoriamenu')->where('nombre', '=', 'Mantenimientos')->first()->id;

        $datos = array(
            array(
                'nombre' => 'Alerta Laboral',
                'link' => 'alertalaboral'
            ),
            array(
                'nombre' => 'Alerta Laboral cargo',
                'link' => 'alertalaboralcargo'
            ),
            array(
                'nombre' => 'Alerta Laboral Departamento',
                'link' => 'alertalaboraldepartamento'
            ),
            array(
                'nombre' => 'Área Laboral',
                'link' => 'arealaboral'
            ),
            array(
                'nombre' => 'Cargo Laboral',
                'link' => 'cargolaboral'
            ),
            array(
                'nombre' => 'Colegio',
                'link' => 'colegio'
            ),
            array(
                'nombre' => 'Departamento',
                'link' => 'departamento'
            ),
            array(
                'nombre' => 'Provincia',
                'link' => 'provincia'
            ),
            array(
                'nombre' => 'Distrito',
                'link' => 'distrito'
            ),
            array(
                'nombre' => 'Discapacidad',
                'link' => 'discapacidad'
            ),
            array(
                'nombre' => 'Empresa',
                'link' => 'empresa'
            ),
            array(
                'nombre' => 'Escuela',
                'link' => 'escuela'
            ),
            array(
                'nombre' => 'Idioma',
                'link' => 'idioma'
            ),
            array(
                'nombre' => 'Jornada Laboral',
                'link' => 'jornadalaboral'
            ),
            array(
                'nombre' => 'Nivel Estudio',
                'link' => 'nivelestudio'
            ),
            array(
                'nombre' => 'Nivel Proceso',
                'link' => 'nivelproceso'
            ),
            array(
                'nombre' => 'País',
                'link' => 'pais'
            ),
            array(
                'nombre' => 'Sector Empresa',
                'link' => 'sectorempresa'
            ),
            array(
                'nombre' => 'Tipo Contrato',
                'link' => 'tipocontrato'
            ),
            array(
                'nombre' => 'Tipo Discapacidad',
                'link' => 'tipodiscapacidad'
            ),
            array(
                'nombre' => 'Tipo Documento',
                'link' => 'tipodocumento'
            ),
            array(
                'nombre' => 'Tipo Documento Anexo',
                'link' => 'tipodocumentoanexo'
            ),
            array(
                'nombre' => 'Universidad',
                'link' => 'universidad'
            ),
        );

        for ($i = 0; $i < count($datos); $i++) {
            DB::table('opcionmenu')->insert(array(
                'nombre' => $datos[$i]['nombre'],
                'link' => $datos[$i]['link'] . '.index',
                'orden' => $i + 1,
                'categoriamenu_id' => $categoriamenu_id,
                'created_at' => $now,
                'updated_at' => $now
            ));

            /*Crear los permisos*/
            $opcionmenu_id = DB::table('opcionmenu')->where('nombre', '=', $datos[$i]['nombre'])->first()->id;

            foreach ($permisos as $clave => $valor) {
                DB::table('permiso')->insert(array(
                    'nombre' => 'can ' . $valor,
                    'codigo' => strtolower($valor . '_' . $datos[$i]['link']),
                    'opcionmenu_id' => $opcionmenu_id,
                    'created_at' => $now,
                    'updated_at' => $now
                ));
            }
            $opcionmenu_id = 0;

        }

        /*Módulo Mantenimientos*/

        $categoriamenu_id = DB::table('categoriamenu')->where('nombre', '=', 'Administración')->first()->id;

        $datos = array(
            array(
                'nombre' => 'Persona',
                'link' => 'persona'
            ),
            array(
                'nombre' => 'Oferta Laboral',
                'link' => 'ofertalaboral'
            ),
            array(
                'nombre' => 'Oferta Laboral Persona',
                'link' => 'ofertalaboralpersona'
            ),

        );

        for ($i = 0; $i < count($datos); $i++) {
            DB::table('opcionmenu')->insert(array(
                'nombre' => $datos[$i]['nombre'],
                'link' => $datos[$i]['link'] . '.index',
                'orden' => $i + 1,
                'categoriamenu_id' => $categoriamenu_id,
                'created_at' => $now,
                'updated_at' => $now
            ));

            /*Crear los permisos*/
            $opcionmenu_id = DB::table('opcionmenu')->where('nombre', '=', $datos[$i]['nombre'])->first()->id;

            foreach ($permisos as $clave => $valor) {
                DB::table('permiso')->insert(array(
                    'nombre' => 'can ' . $valor,
                    'codigo' => strtolower($valor . '_' . $datos[$i]['link']),
                    'opcionmenu_id' => $opcionmenu_id,
                    'created_at' => $now,
                    'updated_at' => $now
                ));

            }

            $opcionmenu_id = 0;
        }


    }
}
