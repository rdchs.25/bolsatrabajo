<?php

use Illuminate\Database\Seeder;


class PaisSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = new DateTime;

        DB::table('pais')->insert(array(
            array('id' => '1',
                'nombre' => 'Perú',
                'created_at' => $now,
                'updated_at' => $now
            ),
            array('id' => '2',
                'nombre' => 'EE.UU',
                'created_at' => $now,
                'updated_at' => $now
            ),
        ));
    }
}
