<?php

use Illuminate\Database\Seeder;

class TipousuarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = new DateTime;

        DB::table('tipousuario')->insert(array(
            array('id' => '1',
                'nombre' => 'Administrador',
                'mostrar' => true,
                'created_at' => $now,
                'updated_at' => $now
            ),
            array('id' => '2',
                'nombre' => 'Alumno',
                'mostrar' => true,
                'created_at' => $now,
                'updated_at' => $now
            ),
            array('id' => '3',
                'nombre' => 'Empleador',
                'mostrar' => true,
                'created_at' => $now,
                'updated_at' => $now
            ),
        ));
    }
}
