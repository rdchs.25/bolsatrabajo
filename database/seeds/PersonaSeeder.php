<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class PersonaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = new DateTime;

        DB::table('persona')->insert(array(
            array(
                'codigouniversitario' => '020114550L',
                'nombres' => 'Administrador General',
                'apellidopaterno' => 'Aplicacion',
                'apellidomaterno' => 'Ichamba',
                'created_at' => $now,
                'updated_at' => $now
            )
        ));
    }
}
