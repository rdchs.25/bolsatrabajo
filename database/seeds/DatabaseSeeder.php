<?php

use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            NivelestudioSeeder::class,
            TipodocumentoSeeder::class,
            CategoriamenuSeeder::class,
            OpcionmenuSeeder::class,
            TipousuarioSeeder::class,
            PersonaSeeder::class,
            UsuarioSeeder::class,
            PaisSeeder::class,
        ]);
    }
}
