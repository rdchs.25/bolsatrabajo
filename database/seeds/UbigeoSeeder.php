<?php

use Illuminate\Database\Seeder;

class UbigeoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = new DateTime;

        $departamento_id = DB::table('departamento')->insert(array(
            array('id' => '1',
                'nombre' => 'Lambayeque',
                'created_at' => $now,
                'updated_at' => $now
            ),
        ));
        $provincia_id = DB::table('provincia')->insert(array(
            array('id' => '1',
                'nombre' => 'Chiclayo',
                'departamento_id' => $departamento_id,
                'created_at' => $now,
                'updated_at' => $now
            ),
        ));
        DB::table('distrito')->insert(array(
            array('id' => '1',
                'nombre' => 'Chiclayo',
                'departamento_id' => $provincia_id,
                'created_at' => $now,
                'updated_at' => $now
            ),
        ));
    }
}
