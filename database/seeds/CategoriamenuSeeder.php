<?php

use Illuminate\Database\Seeder;

class CategoriamenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = new DateTime;

        DB::table('categoriamenu')->insert(array(

                array(
                    'nombre' => 'Mantenimientos',
                    'orden' => 1,
                    'icono' => 'fa fa-bank',
                    'created_at' => $now,
                    'updated_at' => $now
                ),
                array(
                    'nombre' => 'Administración',
                    'orden' => 2,
                    'icono' => 'fa fa-bank',
                    'created_at' => $now,
                    'updated_at' => $now
                ),
                array(
                    'nombre' => 'Usuarios',
                    'orden' => 3,
                    'icono' => 'fa fa-bank',
                    'created_at' => $now,
                    'updated_at' => $now
                )
            )
        );
    }
}
