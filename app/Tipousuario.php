<?php

namespace bolsatrabajo;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Tipousuario extends Model
{
    use SoftDeletes;

    protected $table = 'tipousuario';
    protected $dates = ['deleted_at'];

    /**
     * Método para listar
     * @param  model $query modelo
     * @param  string $name nombre
     * @return sql        sql
     */
    public function scopelistar($query, $nombre)
    {
        return $query->where(function ($subquery) use ($nombre) {
            if (!is_null($nombre)) {
                $subquery->where('nombre', 'LIKE', '%' . $nombre . '%');
            }
        })
            ->orderBy('nombre', 'ASC');
    }


    /**
     * Método que hace una relación de muchos a muchos, y que devuelve todas las opciones de menu de un tipo de usuario
     * @return sql sql
     */
    public function opcionmenu()
    {
        return $this->belongsToMany('bolsatrabajo\Opcionmenu', 'permiso', 'tipousuario_id', 'opcionmenu_id');
    }


    /**
     * Método que retorna los usuarios con el tipo de usuario indicado
     * @return sql sql
     */
    public function users()
    {
        return $this->hasMany('bolsatrabajo\Users');
    }

    /**
     * Método de que retorna todos los permisos para el tpo de usuario indicado
     * @return sql sql
     */
    public function permisos()
    {
        return $this->belongsToMany('bolsatrabajo\Permiso');
    }


}
