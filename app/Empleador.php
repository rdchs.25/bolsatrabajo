<?php

namespace bolsatrabajo;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Empleador extends Model
{
    use SoftDeletes;

    protected $table = 'empleador';
    protected $dates = ['deleted_at'];

    public function distrito()
    {
        return $this->belongsTo('bolsatrabajo\Distrito');
    }

    public function pais()
    {
        return $this->belongsTo('bolsatrabajo\Pais');
    }

    public function sectorempresa()
    {
        return $this->belongsTo('bolsatrabajo\Sectorempresa');
    }

    public function usuario()
    {
        return $this->hasMany('bolsatrabajo\User');
    }


    public function scopelistar($query, $querysearch,$distrito_id , $pais_id, $sectorempresa_id)
    {
        return $query->where(function ($subquery) use ($querysearch) {
            if (!is_null($querysearch)) {
                $subquery->orwhere('ruc', 'LIKE', '%' . $querysearch . '%')
                    ->orwhere('razonsocial', 'LIKE', '%' . $querysearch . '%')
                    ->orwhere('nombrecomercial', 'LIKE', '%' . $querysearch . '%')
                    ->orwhere('direccion', 'LIKE', '%' . $querysearch . '%');
            }
        })
            ->where(function ($subquery) use ($pais_id, $distrito_id, $sectorempresa_id) {
                if (!is_null($pais_id)) {
                    $subquery->where('pais_id', '=', $pais_id);
                }
                if (!is_null($distrito_id)) {
                    $subquery->where('distrito_id', '=', $distrito_id);
                }
                if (!is_null($sectorempresa_id)) {
                    $subquery->where('sectorempresa_id', '=', $sectorempresa_id);
                }
            })
            ->orderBy('ruc', 'ASC');
    }
}
