<?php

namespace bolsatrabajo;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Anexo extends Model
{
    use SoftDeletes;
    protected $table = 'anexo';
    protected $dates = ['deleted_at'];
}
