<?php

namespace bolsatrabajo\Http\Controllers;

use bolsatrabajo\Nivelproceso;
use bolsatrabajo\Ofertalaboral;
use Illuminate\Http\Request;
use bolsatrabajo\Librerias\Libreria;
use Illuminate\Support\Facades\DB;
use Validator;

class NivelprocesoController extends Controller
{
    protected $folderview = 'app.nivelproceso';
    protected $tituloAdmin = 'Nivel Proceso';
    protected $tituloRegistrar = 'Registrar Nivel del Proceso';
    protected $tituloModificar = 'Modificar Nivel del Proceso';
    protected $tituloEliminar = 'Eliminar Nivel del Proceso';
    protected $rutas = array('create' => 'nivelproceso.create',
        'edit' => 'nivelproceso.edit',
        'delete' => 'nivelproceso.eliminar',
        'search' => 'nivelproceso.buscar',
        'index' => 'nivelproceso.index',
    );

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function buscar(Request $request)
    {
        $pagina = $request->input('page');
        $filas = $request->input('filas');
        $entidad = 'Nivelproceso';
        $name = Libreria::getParam($request->input('name'));
        $resultado = DB::table('nivelproceso')
            ->join('ofertalaboral', 'ofertalaboral.id', '=', 'nivelproceso.ofertalaboral_id')
            ->where('nivelproceso.nombre', 'like','%'.$name.'%')
            ->whereNull('nivelproceso.deleted_at')
            ->select('nivelproceso.*','ofertalaboral.id as ofertalaboral_id',
                'ofertalaboral.titulo as ofertalaboral_titulo');
//        $resultado = Nivelproceso::listar($name);
        $lista = $resultado->get();
        $cabecera = array();
        $cabecera[] = array('valor' => '#', 'numero' => '1');
        $cabecera[] = array('valor' => 'Nombre', 'numero' => '1');
        $cabecera[] = array('valor' => 'Prioridad', 'numero' => '1');
        $cabecera[] = array('valor' => 'Código', 'numero' => '1');
        $cabecera[] = array('valor' => 'Ofertal Laboral', 'numero' => '1');
        $cabecera[] = array('valor' => 'Operaciones', 'numero' => '2');

        $titulo_modificar = $this->tituloModificar;
        $titulo_eliminar = $this->tituloEliminar;
        $ruta = $this->rutas;
        if (count($lista) > 0) {
            $clsLibreria = new Libreria();
            $paramPaginacion = $clsLibreria->generarPaginacion($lista, $pagina, $filas, $entidad);
            $paginacion = $paramPaginacion['cadenapaginacion'];
            $inicio = $paramPaginacion['inicio'];
            $fin = $paramPaginacion['fin'];
            $paginaactual = $paramPaginacion['nuevapagina'];
            $lista = $resultado->paginate($filas);
            $request->replace(array('page' => $paginaactual));
            return view($this->folderview . '.list')->with(compact('lista', 'paginacion', 'inicio', 'fin', 'entidad', 'cabecera', 'titulo_modificar', 'titulo_eliminar', 'ruta'));
        }
        return view($this->folderview . '.list')->with(compact('lista', 'entidad'));
    }

    public function index()
    {
        $entidad = 'Nivelproceso';
        $title = $this->tituloAdmin;
        $titulo_registrar = $this->tituloRegistrar;
        $ruta = $this->rutas;
        return view($this->folderview . '.admin')->with(compact('entidad', 'title', 'titulo_registrar', 'ruta'));
    }

    public function create(Request $request)
    {
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $entidad = 'Nivelproceso';
        $cboOfertalaboral = ['' => 'Seleccione una Oferta Laboral'] + Ofertalaboral::pluck('titulo', 'id')->all();
        $nivelproceso = null;
        $formData = array('nivelproceso.store');
        $formData = array('route' => $formData, 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Registrar';
        return view($this->folderview . '.mant')->with(compact('nivelproceso', 'formData', 'entidad', 'boton', 'cboOfertalaboral','listar'));
    }

    public function store(Request $request)
    {
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $mensajes = array(
            'ofertalaboral_id.required' => 'Debe seleccionar una Oferta Laboral',
            'nombre.required' => 'Debe ingresar un nombre',
            'prioridad.required' => 'Debe ingresar una prioridad'
        );
        $reglas = array(
            'ofertalaboral_id' => 'required',
            'nombre' => 'required|max:200',
            'prioridad' => 'required|integer'
        );

        $validacion = Validator::make($request->all(), $reglas, $mensajes);
        if ($validacion->fails()) {
            return $validacion->messages()->toJson();
        }
        $error = DB::transaction(function () use ($request) {
            $nivelproceso= new Nivelproceso();
            $nivelproceso->nombre = $request->input('nombre');
            $nivelproceso->prioridad = $request->input('prioridad');
            $nivelproceso->ofertalaboral_id = $request->input('ofertalaboral_id');
            $nivelproceso->save();
        });
        return is_null($error) ? "OK" : $error;
    }

    public function edit($id, Request $request)
    {
        $existe = Libreria::verificarExistencia($id, 'nivelproceso');
        if ($existe !== true) {
            return $existe;
        }
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $cboOfertalaboral = array('' => 'Seleccione') + Ofertalaboral::pluck('titulo', 'id')->all();
        $nivelproceso = Nivelproceso::find($id);
        $entidad = 'Nivelproceso';
        $formData = array('nivelproceso.update', $id);
        $formData = array('route' => $formData, 'method' => 'PUT', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Modificar';
        return view($this->folderview . '.mant')->with(compact('nivelproceso', 'formData', 'entidad', 'boton', 'listar','cboOfertalaboral'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \bolsatrabajo\Provincia  $provincia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $existe = Libreria::verificarExistencia($id, 'nivelproceso');
        if ($existe !== true) {
            return $existe;
        }
        $reglas = array('nombre' => 'required|max:200','ofertalaboral_id' => 'required','prioridad' => 'required|integer');
        $mensajes = array();
        $validacion = Validator::make($request->all(), $reglas, $mensajes);
        if ($validacion->fails()) {
            return $validacion->messages()->toJson();
        }
        $error = DB::transaction(function () use ($request, $id) {
            $nivelproceso = Nivelproceso::find($id);
            $nivelproceso->nombre = $request->input('nombre');
            $nivelproceso->prioridad = $request->input('prioridad');
            $nivelproceso->ofertalaboral_id = $request->input('ofertalaboral_id');
            $nivelproceso->save();
        });
        return is_null($error) ? "OK" : $error;
    }

    public function destroy($id)
    {
        $existe = Libreria::verificarExistencia($id, 'nivelproceso');
        if ($existe !== true) {
            return $existe;
        }
        $error = DB::transaction(function () use ($id) {
            $nivelproceso = Nivelproceso::find($id);
            $nivelproceso->delete();
        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Función para confirmar la eliminación de un registrlo
     * @param  integer $id id del registro a intentar eliminar
     * @param  string $listarLuego consultar si luego de eliminar se listará
     * @return html              se retorna html, con la ventana de confirmar eliminar
     */
    public function eliminar($id, $listarLuego)
    {
        $existe = Libreria::verificarExistencia($id, 'nivelproceso');
        if ($existe !== true) {
            return $existe;
        }
        $listar = "NO";
        if (!is_null(Libreria::obtenerParametro($listarLuego))) {
            $listar = $listarLuego;
        }
        $modelo = Nivelproceso::find($id);
        $entidad = 'Nivelproceso';
        $formData = array('route' => array('nivelproceso.destroy', $id), 'method' => 'DELETE', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Eliminar';
        return view('app.confirmarEliminar')->with(compact('modelo', 'formData', 'entidad', 'boton', 'listar'));
    }

}
