<?php

namespace bolsatrabajo\Http\Controllers;

use bolsatrabajo\Categoriamenu;
use bolsatrabajo\Librerias\Libreria;
use bolsatrabajo\Permission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Validator;


class CategoriamenuController extends Controller
{
    protected $folderview = 'app.categoriamenu';
    protected $tituloAdmin = 'Categoría Menú';
    protected $tituloRegistrar = 'Registrar Categoría Menú';
    protected $tituloModificar = 'Modificar Categoría Menú';
    protected $tituloEliminar = 'Eliminar Categoría Menú';
    protected $rutas = array('create' => 'categoriamenu.create',
        'edit' => 'categoriamenu.edit',
        'delete' => 'categoriamenu.eliminar',
        'search' => 'categoriamenu.buscar',
        'index' => 'categoriamenu.index',
    );

    protected $nuevo = 'Nueva Categoría Menú';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $entidad = 'Categoriamenu';
        $title = $this->tituloAdmin;
        $titulo_registrar = $this->tituloRegistrar;
        $ruta = $this->rutas;
        return view($this->folderview . '.admin')->with(compact('entidad', 'title', 'titulo_registrar', 'ruta'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create(Request $request)
    {
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $entidad = 'Categoriamenu';
        $cboCategoriamenu = ['' => 'Seleccione una categoría'] + Categoriamenu::pluck('nombre', 'id')->all();
        $categoriamenu = null;
        $formData = array('categoriamenu.store');
        $formData = array('route' => $formData, 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Registrar';
        return view($this->folderview . '.mant')->with(compact('categoriamenu', 'formData', 'entidad', 'cboCategoriamenu', 'boton', 'listar'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validación de los datos

//        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $validacion = Validator::make($request->all(),
            array(
                'nombre' => 'required|string|unique:categoriamenu',
                'orden' => 'required|integer',
                'icono' => 'required|string',
                'categoriamenu_id' => 'nullable|integer|exists:categoriamenu,id,deleted_at,NULL',

            )
        );
        if ($validacion->fails()) {
            return $validacion->messages()->toJson();
        }
        $error = DB::transaction(function () use ($request) {
            $categoriamenu = new Categoriamenu();
            $categoriamenu->nombre = $request->input('nombre');
            $categoriamenu->orden = $request->input('orden');
            $categoriamenu->icono = $request->input('icono');
            $categoriamenu->categoriamenu_id = Libreria::obtenerParametro($request->input('categoriamenu_id'));
            $categoriamenu->save();
        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Display the specified resource.
     *
     * @param  \bolsatrabajo\Categoriamenu $categoriamenu
     * @return \Illuminate\Http\Response
     */
    public function show(Categoriamenu $categoriamenu)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \bolsatrabajo\Categoriamenu $categoriamenu
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $existe = Libreria::verificarExistencia($id, 'categoriamenu');
        if ($existe !== true) {
            return $existe;
        }
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $categoriamenu = Categoriamenu::find($id);
        $entidad = 'Categoriamenu';
        $cboCategoriamenu = ['' => 'Seleccione una categoría'] + Categoriamenu::where('id', '<>', $id)->pluck('nombre', 'id')->all();
        $formData = array('categoriamenu.update', $id);
        $formData = array('route' => $formData, 'method' => 'PUT', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Modificar';
        return view($this->folderview . '.mant')->with(compact('categoriamenu', 'formData', 'entidad', 'cboCategoriamenu', 'boton', 'listar'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \bolsatrabajo\Categoriamenu $categoriamenu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $existe = Libreria::verificarExistencia($id, 'categoriamenu');
        if ($existe !== true) {
            return $existe;
        }
        $validacion = Validator::make($request->all(),
            array(
                'nombre' => [
                    'required',
                    Rule::unique('categoriamenu')->ignore($id),
                ],
                'orden' => 'required|integer',
                'icono' => 'required|string',
            )
        );
        if ($validacion->fails()) {
            return $validacion->messages()->toJson();
        }
        $error = DB::transaction(function () use ($request, $id) {
            $categoriamenu = Categoriamenu::find($id);
            $categoriamenu->nombre = $request->input('nombre');
            $categoriamenu->orden = $request->input('orden');
            $categoriamenu->icono = $request->input('icono');
            $categoriamenu->categoriamenu_id = Libreria::obtenerParametro($request->input('categoriamenu_id'));
            $categoriamenu->save();

        });
        return is_null($error) ? "OK" : $error;
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \bolsatrabajo\Categoriamenu $categoriamenu
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $existe = Libreria::verificarExistencia($id, 'categoriamenu');
        if ($existe !== true) {
            return $existe;
        }
        $error = DB::transaction(function () use ($id) {
            $categoriamenu = Categoriamenu::find($id);
            $categoriamenu->delete();
        });
        return is_null($error) ? "OK" : $error;
    }


    /**
     * Mostrar el resultado de búsquedas
     *
     * @return Response
     */
    public function buscar(Request $request)
    {
        $pagina = $request->input('page');
        $filas = $request->input('filas');
        $entidad = 'Categoriamenu';
        $querysearch = Libreria::getParam($request->input('querysearch'));
        $resultado = Categoriamenu::listar($querysearch);
        $lista = $resultado->get();
        $cabecera = array();
        $cabecera[] = array('valor' => '#', 'numero' => '1');
        $cabecera[] = array('valor' => 'Nombre', 'numero' => '1');
        $cabecera[] = array('valor' => 'Orden', 'numero' => '1');
        $cabecera[] = array('valor' => 'Icono', 'numero' => '1');
        $cabecera[] = array('valor' => 'Categoría', 'numero' => '1');
        $cabecera[] = array('valor' => 'Operaciones', 'numero' => '2');

        $titulo_modificar = $this->tituloModificar;
        $titulo_eliminar = $this->tituloEliminar;
        $ruta = $this->rutas;

        if (count($lista) > 0) {
            $clsLibreria = new Libreria();
            $paramPaginacion = $clsLibreria->generarPaginacion($lista, $pagina, $filas, $entidad);
            $paginacion = $paramPaginacion['cadenapaginacion'];
            $inicio = $paramPaginacion['inicio'];
            $fin = $paramPaginacion['fin'];
            $paginaactual = $paramPaginacion['nuevapagina'];
            $lista = $resultado->paginate($filas);
            $request->replace(array('page' => $paginaactual));
            return view($this->folderview . '.list')->with(compact('lista', 'paginacion', 'inicio', 'fin', 'entidad', 'cabecera', 'titulo_modificar', 'titulo_eliminar', 'ruta'));
        }

        return view($this->folderview . '.list')->with(compact('lista', 'entidad'));
    }

    /**
     * Función para confirmar la eliminación de un registrlo
     * @param  integer $id id del registro a intentar eliminar
     * @param  string $listarLuego consultar si luego de eliminar se listará
     * @return html              se retorna html, con la ventana de confirmar eliminar
     */
    public function eliminar($id, $listarLuego)
    {
        $existe = Libreria::verificarExistencia($id, 'categoriamenu');
        if ($existe !== true) {
            return $existe;
        }
        $listar = "NO";

        if (!is_null(Libreria::obtenerParametro($listarLuego))) {
            $listar = $listarLuego;
        }
        /**
         * Mostrar los hijos
         * Parametros $id objeto a eliminar,array con las tablas foraneas, entidad o nombre de la tabla
         */

        $categoria = new Categoriamenu();
        $data = $this->arboleliminado($id, $categoria->cascadadelete(), 'categoriamenu');

        $modelo = Categoriamenu::find($id);
        $entidad = 'Categoriamenu';
        $formData = array('route' => array('categoriamenu.destroy', $id), 'method' => 'DELETE', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Si, Estoy Seguro';
        return view('app.confirmarEliminar')->with(compact('modelo', 'formData', 'entidad', 'data', 'boton', 'listar'));
    }


    /**
     *método para mostrar el árbol de los permisos
     * @param [type] $idtipousuario [description]
     * @return arbol de permisos
     */

    public function arboleliminado($id, $tables, $entidad)
    {
        $cadenacategoria = '<ul>';
        foreach ($tables as $clave => $valor) {
            $nombremostrar = 'id';

            $objeliminar = DB::table($valor)->where($entidad . '_id', '=', $id)->whereNull('deleted_at')->get();
            $model_name = "bolsatrabajo\\" . ucwords(str_replace('_', '', $valor));
            $model = app($model_name);
            if (method_exists($model_name, 'mostrar')) {
                $nombremostrar = $model->mostrar();
            }
            if ($objeliminar->count()) {
                foreach ($objeliminar as $eliminar) {
                    $hijos = $this->buscarhijoseliminar($eliminar->id, $valor);
                    $cadenacategoria .= '<li>' . $eliminar->$nombremostrar . '</li>';
                    if ($hijos != '') {
                        $cadenacategoria .= $hijos;
                    }

                }
            }
        }

        return $cadenacategoria . '</u>';
    }

    public function buscarhijoseliminar($id, $table)
    {

        $model_name = "bolsatrabajo\\" . ucwords(str_replace('_', '', $table));
        $model = app($model_name);

        if (!method_exists($model_name, 'cascadadelete')) {
            return '';
        }
        $nombremostrar = 'id';
        $cadenahijos = '';
        $modeltable = $model->cascadadelete();

        foreach ($modeltable as $clave => $valor) {

            $objeliminar = DB::table($valor)->where($table . '_id', '=', $id)->get();
            if ($objeliminar->count()) {

                $model_name_hijo = "bolsatrabajo\\" . ucwords(str_replace('_', '', $valor));
                $modelmostrar = app($model_name_hijo);

                if (method_exists($model_name_hijo, 'mostrar')) {
                    if (strpos($valor, '_') === false) {
                        $nombremostrar = $modelmostrar->mostrar();
                    }
                }

                $cadenahijos .= '<ul>';
                foreach ($objeliminar as $eliminar) {
                    $hijos = $this->buscarhijoseliminar($eliminar->id, $valor);

                    if (strpos($valor, '_') > 0) {
                        if (method_exists($model_name_hijo, 'mostrar')) {
                            $nombremostrar = $modelmostrar->mostrar($eliminar->id);
                        }
                        $cadenahijos .= '<li>' . $nombremostrar . '</li>';
                    } else {
                        $cadenahijos .= '<li>' . $eliminar->$nombremostrar . '</li>';
                    }
                    if ($hijos != '') {
                        $cadenahijos .= $hijos;
                    }
                }
                $cadenahijos .= '</ul>';
            }
        }

        return $cadenahijos;
    }


}
