<?php

namespace bolsatrabajo\Http\Controllers;

use bolsatrabajo\Http\Requests;
use bolsatrabajo\Librerias\Libreria;
use bolsatrabajo\Tipodocumentoanexo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;


class TipodocumentoanexoController extends Controller
{
    protected $folderview = 'app.tipodocumentoanexo';
    protected $tituloAdmin = 'Tipo Documento Anexo';
    protected $tituloRegistrar = 'Registrar Tipo de Documento Anexo';
    protected $tituloModificar = 'Modificar Tipo de Documento Anexo';
    protected $tituloEliminar = 'Eliminar Tipo de Documento Anexo';
    protected $rutas = array('create' => 'tipodocumentoanexo.create',
        'edit' => 'tipodocumentoanexo.edit',
        'delete' => 'tipodocumentoanexo.eliminar',
        'search' => 'tipodocumentoanexo.buscar',
        'index' => 'tipodocumentoanexo.index',
    );

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function buscar(Request $request)
    {
        $pagina = $request->input('page');
        $filas = $request->input('filas');
        $entidad = 'Tipodocumentoanexo';
        $name = Libreria::getParam($request->input('name'));
        $resultado = Tipodocumentoanexo::listar($name);
        $lista = $resultado->get();
        $cabecera = array();
        $cabecera[] = array('valor' => '#', 'numero' => '1');
        $cabecera[] = array('valor' => 'Nombre', 'numero' => '1');
        $cabecera[] = array('valor' => 'Operaciones', 'numero' => '2');

        $titulo_modificar = $this->tituloModificar;
        $titulo_eliminar = $this->tituloEliminar;
        $ruta = $this->rutas;
        if (count($lista) > 0) {
            $clsLibreria = new Libreria();
            $paramPaginacion = $clsLibreria->generarPaginacion($lista, $pagina, $filas, $entidad);
            $paginacion = $paramPaginacion['cadenapaginacion'];
            $inicio = $paramPaginacion['inicio'];
            $fin = $paramPaginacion['fin'];
            $paginaactual = $paramPaginacion['nuevapagina'];
            $lista = $resultado->paginate($filas);
            $request->replace(array('page' => $paginaactual));
            return view($this->folderview . '.list')->with(compact('lista', 'paginacion', 'inicio', 'fin', 'entidad', 'cabecera', 'titulo_modificar', 'titulo_eliminar', 'ruta'));
        }
        return view($this->folderview . '.list')->with(compact('lista', 'entidad'));
    }

    public function index()
    {
        $entidad = 'Tipodocumentoanexo';
        $title = $this->tituloAdmin;
        $titulo_registrar = $this->tituloRegistrar;
        $ruta = $this->rutas;
        return view($this->folderview . '.admin')->with(compact('entidad', 'title', 'titulo_registrar', 'ruta'));
    }

    public function create(Request $request)
    {
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $entidad = 'Tipodocumentoanexo';
        $tipodocumentoanexo= null;
        $formData = array('tipodocumentoanexo.store');
        $formData = array('route' => $formData, 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Registrar';
        return view($this->folderview . '.mant')->with(compact('tipodocumentoanexo', 'formData', 'entidad', 'boton', 'listar'));
    }

    public function store(Request $request)
    {
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $reglas = array('nombre' => 'required|max:60');
        $mensajes = array();
        $validacion = Validator::make($request->all(), $reglas, $mensajes);
        if ($validacion->fails()) {
            return $validacion->messages()->toJson();
        }
        $error = DB::transaction(function () use ($request) {
            $tipodocumentoanexo = new Tipodocumentoanexo();
            $tipodocumentoanexo->nombre = $request->input('nombre');
            $tipodocumentoanexo->save();
        });
        return is_null($error) ? "OK" : $error;
    }

    public function edit($id, Request $request)
    {
        $existe = Libreria::verificarExistencia($id, 'tipodocumentoanexo');
        if ($existe !== true) {
            return $existe;
        }
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $tipodocumentoanexo = Tipodocumentoanexo::find($id);
        $entidad = 'Tipodocumentoanexo';
        $formData = array('tipodocumentoanexo.update', $id);
        $formData = array('route' => $formData, 'method' => 'PUT', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Modificar';
        return view($this->folderview . '.mant')->with(compact('tipodocumentoanexo', 'formData', 'entidad', 'boton', 'listar'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \bolsatrabajo\Departamento  $departamento
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $existe = Libreria::verificarExistencia($id, 'tipodocumentoanexo');
        if ($existe !== true) {
            return $existe;
        }
        $reglas = array('nombre' => 'required|max:60');
        $mensajes = array();
        $validacion = Validator::make($request->all(), $reglas, $mensajes);
        if ($validacion->fails()) {
            return $validacion->messages()->toJson();
        }
        $error = DB::transaction(function () use ($request, $id) {
            $tipodocumentoanexo = Tipodocumentoanexo::find($id);
            $tipodocumentoanexo->nombre = $request->input('nombre');
            $tipodocumentoanexo->save();
        });
        return is_null($error) ? "OK" : $error;
    }

    public function destroy($id)
    {
        $existe = Libreria::verificarExistencia($id, 'tipodocumentoanexo');
        if ($existe !== true) {
            return $existe;
        }
        $error = DB::transaction(function () use ($id) {
            $tipodocumentoanexo = Tipodocumentoanexo::find($id);
            $tipodocumentoanexo->delete();
        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Función para confirmar la eliminación de un registrlo
     * @param  integer $id id del registro a intentar eliminar
     * @param  string $listarLuego consultar si luego de eliminar se listará
     * @return html              se retorna html, con la ventana de confirmar eliminar
     */
    public function eliminar($id, $listarLuego)
    {
        $existe = Libreria::verificarExistencia($id, 'tipodocumentoanexo');
        if ($existe !== true) {
            return $existe;
        }
        $listar = "NO";
        if (!is_null(Libreria::obtenerParametro($listarLuego))) {
            $listar = $listarLuego;
        }
        $modelo = Tipodocumentoanexo::find($id);
        $entidad = 'Tipodocumentoanexo';
        $formData = array('route' => array('tipodocumentoanexo.destroy', $id), 'method' => 'DELETE', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Eliminar';
        return view('app.confirmarEliminar')->with(compact('modelo', 'formData', 'entidad', 'boton', 'listar'));
    }


}
