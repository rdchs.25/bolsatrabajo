<?php

namespace bolsatrabajo\Http\Controllers;

use bolsatrabajo\Departamento;
use bolsatrabajo\Distrito;
use bolsatrabajo\Empleador;
use bolsatrabajo\Librerias\Libreria;
use bolsatrabajo\Pais;
use bolsatrabajo\Provincia;
use bolsatrabajo\Sectorempresa;
use bolsatrabajo\Tipousuariousuario;
use bolsatrabajo\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Image;
use Input;
use Validator;


class EmpleadorController extends Controller
{
    protected $folderview = 'app.empleador';
    protected $tituloAdmin = 'Empleador';
    protected $tituloRegistrar = 'Registrar Empleador';
    protected $tituloModificar = 'Modificar Empleador';
    protected $tituloEliminar = 'Eliminar Empleador';
    protected $rutas = array('create' => 'empleador.create',
        'edit' => 'empleador.edit',
        'delete' => 'empleador.eliminar',
        'search' => 'empleador.buscar',
        'index' => 'empleador.index'
    );

//    public function __construct()
//    {
//        $this->middleware('auth');
//    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        if (!Libreria::verificarExistenciaPermiso('list_empleador')) {
//            return view('app.401');
//        }
        $entidad = 'Empleador';
        $title = $this->tituloAdmin;
        $titulo_registrar = $this->tituloRegistrar;
        $ruta = $this->rutas;
        $cboDistrito = ['' => 'Todos'] + Distrito::pluck('nombre', 'id')->all();
        $cboPais = ['' => 'Todos'] + Pais::pluck('nombre', 'id')->all();
        $cboSectorempresa = ['' => 'Todos'] + Sectorempresa::pluck('nombre', 'id')->all();

        return view($this->folderview . '.admin')->with(compact('entidad', 'cboDistrito', 'cboPais', 'cboSectorempresa', 'title', 'titulo_registrar', 'ruta'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
//        if (!Libreria::verificarExistenciaPermiso('add_empleador')) {
//            return view('app.401');
//        }
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $entidad = 'Empleador';
        $cboSectorempresa = ['' => 'Seleccione Sector Empresa'] + Sectorempresa::orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        $cboPais = ['' => 'Seleccione un País'] + Pais::orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        $cboDepartamento = ['' => 'Seleccione'] + Departamento::orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        $cboProvincia = ['' => 'Seleccione'];
        $cboDistrito = ['' => 'Seleccione'];
        $empleador = null;
        $formData = array('empleador.store');
        $formData = array('route' => $formData, 'files' => true, 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off', 'files' => true);
        $boton = 'Registrar';
        return view($this->folderview . '.mant')->with(compact('empleador', 'formData', 'entidad', 'cboDepartamento', 'cboProvincia', 'cboDistrito', 'cboPais', 'cboSectorempresa', 'boton', 'listar'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        if (!Libreria::verificarExistenciaPermiso('add_empleador')) {
//            return view('app.401');
//        }

        $mensajes = array(
            'ruc.required' => 'Debe ingresar el ruc',
            'razonsocial.required' => 'Debe ingresar razón social',
            'nombrecomercial.required' => 'Debe ingresar nombre comercial',
            'direccion.required' => 'Debe ingresar nombre comercial',
            'web.required' => 'Debe ingresar página web',
            'email.required' => 'Debe ingresar página web',
            'telefono.required' => 'Debe ingresar teléfono',
            'celular.required' => 'Debe ingresar celular',
            'nrotrabajadores.required' => 'Debe ingresar Nro. de trabajadores',
            'nrotrabajadores.min' => 'El de Nro. trabajadores no puede ser cero',
            'sectorempresa_id.required' => 'Debe ingresar página web',
            'descripcion.required' => 'Debe ingresar descripción de la empresa',
            'logo.required' => 'Debe seleccionar un logo ',
            'aceptoterminos.required' => 'Debe aceptar los términos',
            'usuario' => 'Debe ingresar usuario',
            'password' => 'Debe ingresar password',
            'nombrecontacto.required' => 'Debe ingresar nombre contacto',
            'telefonocontacto.required' => 'Debe ingresar teléfono contacto',
            'celularcontacto.required' => 'Debe ingresar célular contacto',
            'departamento_id.required' => 'Debe seleccionar un departamento',
            'provincia_id.required' => 'Debe seleccionar una provincia',
            'distrito_id.required' => 'Debe seleccionar un distrito',
            'pais_id.required' => 'Debe seleccionar su país',


        );       

        $reglas = array(
            'ruc' => 'unique:empleador,ruc,NULL,id,deleted_at,NULL|regex:/^[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$/',
            'razonsocial' => 'required|unique:empleador',
            'nombrecomercial' => 'required|unique:empleador',
            'direccion' => 'required',
            'web' => 'required',
            'email' => 'required|email',
            'telefono' => 'required|integer',
            'celular' => 'required|integer',
            'nrotrabajadores' => 'required|integer|min:1',
            'sectorempresa_id' => 'required|integer|exists:sectorempresa,id',
            'descripcion' => 'required',
            'logo' => 'required|image',
            'aceptoterminos' => 'required',
            'usuario' => 'required|string|unique:usuario',
            'password' => 'required|string|min:6|confirmed',
            'nombrecontacto' => 'required|unique:empleador',
            'telefonocontacto' => 'required|unique:empleador',
            'celularcontacto' => 'required|unique:empleador',
            'departamento_id' => 'required|integer|exists:departamento,id',
            'provincia_id' => 'required:departamento_id|integer|exists:provincia,id',
            'distrito_id' => 'required:provincia_id|integer|exists:distrito,id',
            'pais_id' => 'required|integer|exists:pais,id',
            );
        if($request->input('extranjero')){
            unset($mensajes["departamento_id.required"]);
            unset($reglas["departamento_id"]);

            unset($mensajes["provincia_id.required"]);
            unset($reglas["provincia_id"]);

            unset($mensajes["departamento_id.required"]);
            unset($reglas["distrito_id"]);
        }else{
            unset($mensajes["pais_id.required"]);
            unset($reglas["pais_id"]);
        }

        $validacion = Validator::make($request->all(), $reglas, $mensajes);

        $validacion = Validator::make($request->all(), $reglas, $mensajes);

        if ($validacion->fails()) {
            return $validacion->messages()->toJson();
        }

        $error = DB::transaction(function () use ($request) {
            if ($request->hasFile('logo')) {
                $extension = $request->file('logo')->getClientOriginalExtension();
                $ruta = "/public/".$request->logo->storeAs('logo', 'logo_' . $request->input('ruc') . '.' . $extension);
            }
            $empleador = new Empleador();
            $empleador->ruc = $request->input('ruc');
            $empleador->razonsocial = $request->input('razonsocial');
            $empleador->nombrecomercial = $request->input('nombrecomercial');
            $empleador->direccion = $request->input('direccion');
            $empleador->web = $request->input('web');
            $empleador->email = $request->input('email');
            $empleador->telefono = $request->input('telefono');
            $empleador->celular = $request->input('celular');
            $empleador->nrotrabajadores = $request->input('nrotrabajadores');
            $empleador->descripcion = $request->input('descripcion');
            $empleador->sectorempresa_id = $request->input('sectorempresa_id');
            $empleador->nombrecontacto = $request->input('nombrecontacto');
            $empleador->telefonocontacto = $request->input('telefonocontacto');
            $empleador->celularcontacto = $request->input('celularcontacto');
            $empleador->logo = $ruta;
            if ($request->input('extranjero')) {
                $empleador->extranjero = true;
                $empleador->pais_id = $request->input('pais_id');
            } else {
                $empleador->extranjero = false;
                $empleador->distrito_id = $request->input('distrito_id');
            }
            if ($request->input('norecibiremail')) {
                $empleador->norecibiremail = true;
            } else {
                $empleador->norecibiremail = false;

            }
            $empleador->save();

            /*creacion del usuario*/
            $usuario = new User();
            $usuario->nombre = $request->input('razonsocial');
            $usuario->email = $request->input('email');
            $usuario->usuario = $request->input('usuario');
            $usuario->password = bcrypt($request->input('password'));
            $usuario->empleador_id = $empleador->id;
            $usuario->save();

            /*creacion tipo usuario*/
            $tipousuario_usuario = new Tipousuariousuario();
            $tipousuario_usuario->usuario_id = $usuario->id;
            $tipousuario_usuario->tipousuario_id = 3;
            $tipousuario_usuario->save();

        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {

//        if (!Libreria::verificarExistenciaPermiso('change_empleador')) {
//            return view('app.401');
//        }
        $existe = Libreria::verificarExistencia($id, 'empleador');
        if ($existe !== true) {
            return $existe;
        }
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $empleador = Empleador::find($id);
        $usuario = User::where('empleador_id', $id)->first();
        $entidad = 'Empleador';
        if ($empleador->distrito_id) {
            $distrito = Distrito::where('id', '=', $empleador->distrito_id)->first();
            $provincia = Provincia::where('id', '=', $distrito->provincia_id)->first();
            $departamento = Departamento::where('id', '=', $provincia->departamento_id)->first();
            $cboDepartamento = array('' => 'Seleccione') + Departamento::orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
            $cboProvincia = array('' => 'Seleccione') + Provincia::where('departamento_id', '=', $departamento->id)->orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
            $cboDistrito = array('' => 'Seleccione') + Distrito::where('provincia_id', '=', $provincia->id)->orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        } else {
            $cboDepartamento = ['' => 'Seleccione'] + Departamento::orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
            $cboProvincia = ['' => 'Seleccione'];
            $cboDistrito = ['' => 'Seleccione'];
        }
        $cboSectorempresa = ['' => 'Seleccione Sector Empresa'] + Sectorempresa::orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        $cboPais = ['' => 'Seleccione un País'] + Pais::orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();

        $formData = array('empleador.update', $id);
        $formData = array('route' => $formData, 'method' => 'PUT', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Modificar';
        return view($this->folderview . '.mant')->with(compact('empleador', 'usuario', 'formData', 'entidad', 'boton', 'departamento', 'provincia', 'distrito', 'cboDepartamento', 'cboProvincia', 'cboDistrito', 'cboSectorempresa', 'cboPais', 'listar'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        if (!Libreria::verificarExistenciaPermiso('change_empleador')) {
//            return view('app.401');
//        }
        $existe = Libreria::verificarExistencia($id, 'empleador');
        if ($existe !== true) {
            return $existe;
        }
        $request->merge(array_map('trim', $request->all()));

        $mensajes = array(
            'ruc.required' => 'Debe ingresar el ruc',
            'razonsocial.required' => 'Debe ingresar razón social',
            'nombrecomercial.required' => 'Debe ingresar nombre comercial',
            'direccion.required' => 'Debe ingresar nombre comercial',
            'web.required' => 'Debe ingresar página web',
            'email.required' => 'Debe ingresar página web',
            'telefono.required' => 'Debe ingresar teléfono',
            'celular.required' => 'Debe ingresar celular',
            'nrotrabajadores.required' => 'Debe ingresar página web',
            'nrotrabajadores.min' => 'El de Nro. trabajadores no puede ser cero',
            'sectorempresa_id.required' => 'Debe ingresar página web',
            'descripcion.required' => 'Debe ingresar descripción de la empresa',
            'aceptoterminos.required' => 'Debe aceptar los términos',
            'usuario' => 'Debe ingresar usuario',
            'password' => 'Debe ingresar password',
            'nombrecontacto.required' => 'Debe ingresar nombre contacto',
            'telefonocontacto.required' => 'Debe ingresar teléfono contacto',
            'celularcontacto.required' => 'Debe ingresar célular contacto',
            'departamento_id.required' => 'Debe seleccionar un departamento',
            'provincia_id.required' => 'Debe seleccionar una provincia',
            'distrito_id.required' => 'Debe seleccionar un distrito',
            'pais_id' => 'required|integer|exists:pais,id',

        );
         $reglas = array(
                'ruc' => [
                    'required',
                    Rule::unique('empleador')->ignore($id),
                ],
                'razonsocial' => [
                    'required',
                    Rule::unique('empleador')->ignore($id),
                ],
                'nombrecomercial' => [
                    'required',
                    Rule::unique('empleador')->ignore($id),
                ],
                'nombrecontacto' => [
                    'required',
                    Rule::unique('empleador')->ignore($id),
                ],
                'direccion' => 'required',
                'web' => 'required',
                'email' => 'required|email',
                'telefono' => 'required|integer',
                'celular' => 'required|integer',
                'nrotrabajadores' => 'required|integer|min:1',
                'sectorempresa_id' => 'required|integer|exists:sectorempresa,id',
                'descripcion' => 'required',
                'aceptoterminos' => 'required',
                'usuario' => 'required',
                'password' => 'required|string|min:6|confirmed',
                'telefonocontacto' => 'required',
                'celularcontacto' => 'required',
                'departamento_id' => 'required|integer|exists:departamento,id',
                'provincia_id' => 'required:departamento_id|integer|exists:provincia,id',
                'distrito_id' => 'required:provincia_id|integer|exists:distrito,id',
                'pais_id' => 'required:pais_id|integer|exists:pais,id',


            );

        if($request->input('password_confirmation') == "" && $request->input('password') == ""){
			unset($mensajes["password.required"]);
            unset($reglas["password"]);
        }
        if($request->input('extranjero')){
            unset($mensajes["departamento_id.required"]);
            unset($reglas["departamento_id"]);

            unset($mensajes["provincia_id.required"]);
            unset($reglas["provincia_id"]);

            unset($mensajes["departamento_id.required"]);
            unset($reglas["distrito_id"]);
        }else{
            unset($mensajes["pais_id.required"]);
            unset($reglas["pais_id"]);
        }

        $validacion = Validator::make($request->all(), $reglas, $mensajes);
        if ($validacion->fails()) {
            return $validacion->messages()->toJson();
        }

        $error = DB::transaction(function () use ($request, $id) {
            if ($request->hasFile('logo')) {
                $extension = $request->file('logo')->getClientOriginalExtension();
                $ruta = "/public/".$request->logo->storeAs('logo', 'logo_' . $request->input('ruc') . '.' . $extension);
            }
            $empleador = Empleador::find($id);
            $empleador->ruc = $request->input('ruc');
            $empleador->razonsocial = $request->input('razonsocial');
            $empleador->nombrecomercial = $request->input('nombrecomercial');
            $empleador->direccion = $request->input('direccion');
            $empleador->web = $request->input('web');
            $empleador->email = $request->input('email');
            $empleador->telefono = $request->input('telefono');
            $empleador->celular = $request->input('celular');
            $empleador->nrotrabajadores = $request->input('nrotrabajadores');
            $empleador->descripcion = $request->input('descripcion');
            $empleador->sectorempresa_id = $request->input('sectorempresa_id');
            $empleador->nombrecontacto = $request->input('nombrecontacto');
            $empleador->telefonocontacto = $request->input('telefonocontacto');
            $empleador->celularcontacto = $request->input('celularcontacto');
            if($request->hasFile('logo')){
                $empleador->logo = $ruta;

            }

            if ($request->input('extranjero')) {
                $empleador->extranjero = true;
                $empleador->pais_id = $request->input('pais_id');
            } else {
                $empleador->extranjero = false;
                $empleador->distrito_id = $request->input('distrito_id');
            }
            if ($request->input('norecibiremail')) {
                $empleador->norecibiremail = true;
            } else {
                $empleador->norecibiremail = false;

            }
            $empleador->save();

            /*creacion del usuario*/
            $usuario = User::where('empleador_id', $id)->first();
            $usuario->nombre = $request->input('razonsocial');
            $usuario->email = $request->input('email');
            $usuario->usuario = $request->input('usuario');
            if($request->input('password_confirmation') != "" && $request->input('password') != ""){
            	$usuario->password = bcrypt($request->input('password'));

        	}
            $usuario->save();

        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

//        if (!Libreria::verificarExistenciaPermiso('delete_empleador')) {
//            return view('app.401');
//        }
        $existe = Libreria::verificarExistencia($id, 'empleador');
        if ($existe !== true) {
            return $existe;
        }
        $error = DB::transaction(function () use ($id) {
            $empleador = Empleador::find($id);
            $empleador->delete();

        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Mostrar el resultado de búsquedas
     *
     * @return Response
     */
    public function buscar(Request $request)
    {
        $change = Libreria::verificarExistenciaPermiso('change_empleador');
        $delete = Libreria::verificarExistenciaPermiso('delete_empleador');

        if (!Libreria::verificarExistenciaPermiso('list_empleador')) {
            return view('app.401');
        }

        $pagina = $request->input('page');
        $filas = $request->input('filas');
        $entidad = 'Empleador';
        $querysearch = Libreria::getParam($request->input('querysearch'));
        $distrito_id = Libreria::getParam($request->input('distrito_id'));
        $pais_id = Libreria::getParam($request->input('pais_id'));
        $sectorempresa_id = Libreria::getParam($request->input('sectorempresa_id'));
        $resultado = Empleador::listar($querysearch, $distrito_id, $pais_id, $sectorempresa_id);
        $lista = $resultado->get();
        $cabecera = array();
        $cabecera[] = array('valor' => '#', 'numero' => '1');
        $cabecera[] = array('valor' => 'RUC', 'numero' => '1');
        $cabecera[] = array('valor' => 'Razón Social', 'numero' => '1');
        $cabecera[] = array('valor' => 'Teléfono/Celular', 'numero' => '1');
        $cabecera[] = array('valor' => 'Dirección', 'numero' => '1');
        $cabecera[] = array('valor' => 'Ubigeo', 'numero' => '1');
        $cabecera[] = array('valor' => 'Logo', 'numero' => '1');
        $cabecera[] = array('valor' => 'Operaciones', 'numero' => '2');

        $titulo_modificar = $this->tituloModificar;
        $titulo_eliminar = $this->tituloEliminar;
        $ruta = $this->rutas;
        if (count($lista) > 0) {
            $clsLibreria = new Libreria();
            $paramPaginacion = $clsLibreria->generarPaginacion($lista, $pagina, $filas, $entidad);
            $paginacion = $paramPaginacion['cadenapaginacion'];
            $inicio = $paramPaginacion['inicio'];
            $fin = $paramPaginacion['fin'];
            $paginaactual = $paramPaginacion['nuevapagina'];
            $lista = $resultado->paginate($filas);
            $request->replace(array('page' => $paginaactual));

            return view($this->folderview . '.list')->with(compact('lista', 'paginacion', 'change', 'delete', 'inicio', 'fin', 'entidad', 'cabecera', 'titulo_modificar', 'titulo_eliminar', 'ruta'));
        }        
        return view($this->folderview . '.list')->with(compact('lista', 'entidad'));
    }


    /**
     * Función para confirmar la eliminación de un registrlo
     * @param  integer $id id del registro a intentar eliminar
     * @param  string $listarLuego consultar si luego de eliminar se listará
     * @return html              se retorna html, con la ventana de confirmar eliminar
     */
    public function eliminar($id, $listarLuego)
    {
//        if (!Libreria::verificarExistenciaPermiso('delete_empleador')) {
//            return view('app.401');
//        }

        $existe = Libreria::verificarExistencia($id, 'empleador');
        if ($existe !== true) {
            return $existe;
        }
        $listar = "NO";
        if (!is_null(Libreria::obtenerParametro($listarLuego))) {
            $listar = $listarLuego;
        }
        $modelo = Empleador::find($id);
        $entidad = 'Empleador';
        $formData = array('route' => array('empleador.destroy', $id), 'method' => 'DELETE', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Eliminar';
        return view('app.confirmarEliminar')->with(compact('modelo', 'formData', 'entidad', 'boton', 'listar'));
    }
}
