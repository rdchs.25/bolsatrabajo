<?php

namespace bolsatrabajo\Http\Controllers;

use bolsatrabajo\Arealaboral;
use bolsatrabajo\Cargolaboral;
use bolsatrabajo\Jornadalaboral;
use bolsatrabajo\Departamento;
use bolsatrabajo\Discapacidad;
use bolsatrabajo\Discapacidadrequerida;
use bolsatrabajo\Idioma;
use bolsatrabajo\Idiomarequerido;
use bolsatrabajo\Librerias\Libreria;
use bolsatrabajo\Nivelestudio;
use bolsatrabajo\Nivelproceso;
use bolsatrabajo\Ofertalaboral;
use bolsatrabajo\Empleador;
use bolsatrabajo\Pais;
use bolsatrabajo\Tipocontrato;
use bolsatrabajo\Escuela;
use bolsatrabajo\Tipodiscapacidad;
use bolsatrabajo\Conocimientoinformaticorequerido;
use bolsatrabajo\User;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Validator;
use bolsatrabajo\Ofertalaboralpersona;


class OfertalaboralController extends Controller
{
    protected $folderview = 'app.ofertalaboral';
    protected $tituloAdmin = 'Oferta Laboral';
    protected $tituloRegistrar = 'Registrar Oferta Laboral';
    protected $tituloModificar = 'Modificar Oferta Laboral';
    protected $tituloEliminar = 'Eliminar Oferta Laboral';
    protected $tituloVerPostulantes = 'Listado de Postulantes';
    protected $tituloVerOfertaLaboral = 'Descripción Oferta Laboral';
    protected $tituloListaPostulante = 'Postulantes';
    protected $rutas = array('create' => 'ofertalaboral.create',
        'verpostulantes' => 'ofertalaboral.verpostulantes',
        'verofertalaboral' => 'ofertalaboral.verofertalaboral',
        'edit' => 'ofertalaboral.edit',
        'delete' => 'ofertalaboral.eliminar',
        'search' => 'ofertalaboral.buscar',
        'index' => 'ofertalaboral.index',
    );

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function buscar(Request $request)
    {
        $pagina = $request->input('page');
        $filas = $request->input('filas');
        $entidad = 'Ofertalaboral';
        $name = Libreria::getParam($request->input('name'));
        $departamento = Libreria::getParam($request->input('departamento_id'));
        $arealaboral = Libreria::getParam($request->input('arealaboral_id'));
        $escuelaprofesional = Libreria::getParam($request->input('escuela_id'));


        $resultado = DB::table('ofertalaboral')
            ->leftJoin('cargolaboral', 'ofertalaboral.cargolaboral_id', '=', 'cargolaboral.id')
            ->leftJoin('arealaboral', 'ofertalaboral.arealaboral_id', '=', 'arealaboral.id')
            ->leftJoin('jornadalaboral', 'ofertalaboral.jornadalaboral_id', '=', 'jornadalaboral.id')
            ->leftJoin('pais', 'ofertalaboral.pais_id', '=', 'pais.id')
            ->leftJoin('nivelestudio', 'ofertalaboral.nivelestudio_id', '=', 'nivelestudio.id')
            ->leftJoin('empleador', 'empleador.id', '=', 'ofertalaboral.empleador_id')
            ->leftJoin('distrito', 'distrito.id', '=', 'empleador.distrito_id')
            ->leftJoin('provincia', 'provincia.id', '=', 'distrito.provincia_id')
            ->leftJoin('departamento', 'departamento.id', '=', 'provincia.departamento_id')
            ->leftJoin('escuela', 'escuela.id', '=', 'ofertalaboral.escuela_id')
//            ->where('ofertalaboral.titulo', 'like','%'.$name.'%')
//            ->where('ofertalaboral.arealaboral_id', '=',$arealaboral)
//            ->where('departamento.id', '=',$departamento)
            ->whereNull('ofertalaboral.deleted_at')
            ->select('ofertalaboral.id as id','ofertalaboral.*',
                'cargolaboral.nombre as cargolaboral_nombre',
                'arealaboral.nombre as arealaboral_nombre',
                'jornadalaboral.nombre as jornadalaboral_nombre',
                'pais.nombre as pais_nombre',
                'departamento.nombre as departamento_nombre',
                'ofertalaboral.fechapublicacion as fechapublicacion',
                'ofertalaboral.vigenciaoferta as vigenciaoferta',
                'nivelestudio.nombre as nivelestudio_nombre',
                'escuela.nombre as escuelaprofesional');

//        $resultado = Ofertalaboral::listar($name);
        if (!is_null($name)) {
            $resultado->where('ofertalaboral.titulo', 'like','%'.$name.'%');
        }
        if (!is_null($departamento)) {
            $resultado->where('departamento.id', '=',$departamento);
        }
        if (!is_null($arealaboral)) {
            $resultado->where('ofertalaboral.arealaboral_id', '=',$arealaboral);
        }
        if (!is_null($escuelaprofesional)) {
            $resultado->where('ofertalaboral.escuela_id', '=',$escuelaprofesional);
        }

        if(!auth()->user()->superusuario && auth()->user()->persona_id == null){
            $resultado->where('empleador_id',auth()->user()->empleador_id);
        }

        $lista = $resultado->get();
        $cabecera = array();
        $cabecera[] = array('valor' => '#', 'numero' => '1');
        $cabecera[] = array('valor' => 'Departamento', 'numero' => '1');
        $cabecera[] = array('valor' => 'Título.', 'numero' => '1');
        $cabecera[] = array('valor' => 'Fecha publicación.', 'numero' => '1');
        $cabecera[] = array('valor' => 'Fin oferta laboral', 'numero' => '1');
        $cabecera[] = array('valor' => 'Vacante', 'numero' => '1');
        $cabecera[] = array('valor' => 'Escuela', 'numero' => '1');
        $cabecera[] = array('valor' => 'Jornada Laboral', 'numero' => '1');
        $cabecera[] = array('valor' => 'Inicio', 'numero' => '1');
        $cabecera[] = array('valor' => 'Moneda', 'numero' => '1');
        $cabecera[] = array('valor' => 'Salario', 'numero' => '1');
        $cabecera[] = array('valor' => 'Postulantes', 'numero' => '1');
        $cabecera[] = array('valor' => 'Operaciones', 'numero' => '3');

        $titulo_modificar = $this->tituloModificar;
        $titulo_eliminar = $this->tituloEliminar;
        $titulo_listapostulantes = $this->tituloListaPostulante;
        $titulo_verofertalaboral = $this->tituloVerOfertaLaboral;
        $ruta = $this->rutas;
        if (count($lista) > 0) {
            $clsLibreria = new Libreria();
            $paramPaginacion = $clsLibreria->generarPaginacion($lista, $pagina, $filas, $entidad);
            $paginacion = $paramPaginacion['cadenapaginacion'];
            $inicio = $paramPaginacion['inicio'];
            $fin = $paramPaginacion['fin'];
            $paginaactual = $paramPaginacion['nuevapagina'];
            $lista = $resultado->paginate($filas);
            $request->replace(array('page' => $paginaactual));
            return view($this->folderview . '.list')->with(compact('lista', 'paginacion', 'inicio', 'fin', 'entidad', 'cabecera', 'titulo_modificar', 'titulo_eliminar','titulo_listapostulantes','titulo_verofertalaboral' ,'ruta'));
        }
        return view($this->folderview . '.list')->with(compact('lista', 'entidad'));
    } 


    public function verofertalaboral($id, Request $request)
    {   
        $entidad = "verofertalaboral";
        $ofertalaboral = Ofertalaboral::find($id);

        return view($this->folderview . '.verofertalaboral')->with(compact('ofertalaboral', 'entidad'));

    }

    public function verpostulantes($id, Request $request)
    {   
        if ($request->input('page') != null) {
            $pagina = $request->input('page') ;       
        }else{
            $pagina = 1;
        } 
        if ($request->input('filas') != null) {
            $filas = $request->input('filas') ;       
        }else{
            $filas = 1;
        }
       
       //dd($pagina,$filas);
        
        $entidad = 'verpostulantes';
        $resultado = Ofertalaboralpersona::where('ofertalaboral_id','=',$id);
        $lista = $resultado->get();
        $cabecera = array();
        $cabecera[] = array('valor' => '#', 'numero' => '1');
        $cabecera[] = array('valor' => 'N° Documento', 'numero' => '1');
        $cabecera[] = array('valor' => 'Alumno', 'numero' => '1');
        $cabecera[] = array('valor' => 'Teléfono/Celular', 'numero' => '1');
        $cabecera[] = array('valor' => 'Ubigeo', 'numero' => '1');
        $cabecera[] = array('valor' => 'Dirección', 'numero' => '1');
        $cabecera[] = array('valor' => 'Escuela', 'numero' => '1');
        $cabecera[] = array('valor' => 'País', 'numero' => '1');

        $titulo_verpostulantes = $this->tituloVerPostulantes;
        $ruta = $this->rutas;

        if (count($lista) > 0) {
           
            return view($this->folderview . '.verpostulante')->with(compact('id','lista', 'entidad', 'cabecera', 'titulo_verpostulantes', 'ruta'));
        }
        return view($this->folderview . '.verpostulante')->with(compact('lista', 'entidad'));

    }

    public function index()
    {
        $entidad = 'Ofertalaboral';
        $title = $this->tituloAdmin;
        $titulo_registrar = $this->tituloRegistrar;
        $ruta = $this->rutas;
        $cboArealaboral = ['' => 'Todos'] + Arealaboral::pluck('nombre', 'id')->all();
        $cboEscuela = ['' => 'Todos'] + Escuela::pluck('nombre', 'id')->all();
        $cboDepartamento = ['' => 'Todos'] + Departamento::pluck('nombre', 'id')->all();
        return view($this->folderview . '.admin')->with(compact('entidad', 'title', 'titulo_registrar', 'ruta','cboArealaboral','cboDepartamento', 'cboEscuela'));
    }

    public function create(Request $request)
    {
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $entidad = 'Ofertalaboral';
        $cboCargolaboral = ['' => 'Seleccione'] + Cargolaboral::pluck('nombre', 'id')->all();
        $cboEscuela = ['' => 'Seleccione'] + Escuela::pluck('nombre', 'id')->all();
        $cboEmpleador = ['' => 'Seleccione'] + Empleador::pluck('razonsocial', 'id')->all();
        $cboArealaboral = ['' => 'Seleccione'] + Arealaboral::pluck('nombre', 'id')->all();
        $cboTipocontrato = ['' => 'Seleccione'] + Tipocontrato::pluck('nombre', 'id')->all();
        $cboJornadalaboral = ['' => 'Seleccione'] + Jornadalaboral::pluck('nombre', 'id')->all();
        $cboMonedapago = ['' => 'Seleccione', 'Soles' => 'Soles (S/.)', 'Dólares' => 'Dólares ($)'];
        $cboNivelestudio = ['' => 'Seleccione '] + Nivelestudio::pluck('nombre', 'id')->all();
        $cboExperiencialaboral = ['' => 'Seleccione', 'sinexperiencia' => 'Sin Experiencia', 'iguala' => 'Igual a (=)', 'mayoroigualque' => 'Mayor o Igual que (>=)', 'menorigualque' => 'Menor o Igual que (<=)'];
        //$cboEstudiominimo = ['' => 'Seleccione Estudio Mínimo'] + Nivelestudio::pluck('nombre', 'id')->all();
        $cboIdioma = ['' => 'Seleccione Idioma'] + Idioma::pluck('nombre', 'id')->all();
        $cboLecturaidioma = ['' => 'Seleccione Nivel', 'básico' => 'Básico', 'intermedio' => 'Intermedio', 'avanzado' => 'Avanzado'];
        $cboEscrituraidioma = ['' => 'Seleccione Nivel', 'básico' => 'Básico', 'intermedio' => 'Intermedio', 'avanzado' => 'Avanzado'];
        $cboConversacionidioma = ['' => 'Seleccione Nivel', 'básico' => 'Básico', 'intermedio' => 'Intermedio', 'avanzado' => 'Avanzado'];
        $cboNivelsoftware = ['' => 'Seleccione Nivel', 'bajo' => 'Bajo', 'usuario' => 'Usuario', 'usuarioavanzado' => 'Usuario Avanzado', 'tecnico' => 'Técnico', 'profesional' => 'Profesional', 'experto' => 'Experto'];
        $cboPais = ['' => 'Seleccione un País'] + Pais::where('nombre', '!=', 'Perú')->orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();

        $tipodiscapacidad = Tipodiscapacidad::where('id', '=', 1)->first();
        $discapacidad = Discapacidad::where('tipodiscapacidad_id', '=', $tipodiscapacidad->id)->first();
        $cboTipodiscapacidad = ['' => 'Seleccione'] + Tipodiscapacidad::orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        $cboDiscapacidad = ['' => 'Seleccione'] + Discapacidad::where('tipodiscapacidad_id', '=', $tipodiscapacidad->id)->orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        $ofertalaboral = null;
        $formData = array('ofertalaboral.store');
        $formData = array('route' => $formData, 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Registrar';
        return view($this->folderview . '.mant')->with(compact('ofertalaboral', 'formData', 'entidad', 'boton', 'cboCargolaboral', 'cboArealaboral', 'cboJornadalaboral','cboTipocontrato',
            'cboMonedapago', 'cboNivelestudio', 'cboExperiencialaboral', 'cboIdioma', 'cboLecturaidioma', 'cboEscrituraidioma', 'cboConversacionidioma',
            'cboNivelsoftware', 'cboPais', 'tipodiscapacidad', 'discapacidad', 'cboTipodiscapacidad', 'cboDiscapacidad', 'listar', 'cboEmpleador', 'cboEscuela'));
    }

    public function store(Request $request)
    {
        $listar = Libreria::getParam($request->input('listar'), 'NO');


        $mensajes = array(
            'titulo.required' => 'Debe ingresar Título',
            'nrovacante.required' => 'Debe ingresar Nro. de Vacantes',
            'cargolaboral_id.required' => 'Debe seleccionar un Tipo Cargo',
            'arealaboral_id.required' => 'Debe seleccionar un Área Laboral',
            'descripcion.required' => 'Debe seleccionar un Descripción de Puesto',
            'jornadalaboral_id.required' => 'Debe seleccionar Jornada Laboral',
            'tipocontrato_id.required' => 'Debe seleccionar Tipo de Contrato',
            'duracioncontrato.required' => 'Debe ingresar Duración de Contrato',
            'fechainiciocontrato.required' => 'Debe ingresar Inicio Contrato',
            'fechafincontrato.required' => 'Debe ingresar Fin Contrato',
            'horario.required' => 'Debe ingresar Horario',
            'monedapago.required' => 'Debe seleccionar Modo de Pago',
            'salario.required' => 'Debe ingresar Salario',
            'nivelestudio_id.required' => 'Debe seleccionar Nilvel Estudio',
            'experiencialaboral.required' => 'Debe seleccionar Experiencia Laboral',
            'descripcionrequisitos.required' => 'Debe ingresar Requisitos',
            'vigenciaoferta.required' => 'Debe ingresar  Vigencia Oferta',
            'observacionoferta.required' => 'Debe ingresar  Observación',
            'escuela_id.required' => 'Debe seleccionar una escuela profesional',
        );

        $reglas = array(
        	'titulo' => 'required|max:60',
            'nrovacante' => 'required|numeric',
            'cargolaboral_id' => 'required',
            'arealaboral_id' => 'required',
            'empleador_id' => 'required',
            'fechapublicacion' => 'required',
            'descripcion' => 'required|max:400',
            'escuela_id' => 'required',
            // 'jornadalaboral_id' => 'required',
            //'tipocontrato_id' => 'required',
            //'duracioncontrato' => 'required|max:200',
            //'fechainiciocontrato' => 'required',
            //'fechafincontrato' => 'required',
            //'horario' => 'required|max:200',
            //'monedapago' => 'required',
            //'salario' => 'required|numeric',
            'experiencialaboral' => 'required',
            'nivelestudio_id' => 'required',
            'descripcionrequisitos' => 'required',
            'vigenciaoferta' => 'required|numeric',
            'observacionoferta' => 'required|String',
        );
        if($request->input('jornadalaboral_id') == 1 ){
        	unset($mensajes["duracioncontrato.required"]);
        	unset($reglas["duracioncontrato"]);

        	unset($mensajes["fechafincontrato.required"]);
        	unset($reglas["fechafincontrato"]);
        }
        $validacion = Validator::make($request->all(), $reglas, $mensajes);
        if ($validacion->fails()) {
            return $validacion->messages()->toJson();
        }

        /*if (is_null(Libreria::getParam($request->input('empleador_id')))) {
        	if(auth()->user()->empleador_id ===null || auth()->user()->persona_id !=null){
	            $datos["error"] = ["Solo pueden crear oferta laboral los empleadores(Empresas)."];
	            return json_encode($datos);
	        }
        }*/
       
        $error = DB::transaction(function () use ($request) {
            $session_id = auth()->user()->id;

            //$user = User::find($session_id);
            //$empleador_id= $user->empleador_id;
//            $user= User::find($session_id);


            $ofertalaboral = new Ofertalaboral();
            $ofertalaboral->titulo = $request->input('titulo');
            $ofertalaboral->nrovacante = $request->input('nrovacante');
            $ofertalaboral->cargolaboral_id = $request->input('cargolaboral_id');
            $ofertalaboral->escuela_id = $request->input('escuela_id');
            $ofertalaboral->arealaboral_id = $request->input('arealaboral_id');
            $ofertalaboral->descripcion = $request->input('descripcion');
            $ofertalaboral->jornadalaboral_id = $request->input('jornadalaboral_id');
            $ofertalaboral->tipocontrato_id = $request->input('tipocontrato_id');
            $ofertalaboral->duracioncontrato = $request->input('duracioncontrato');
            $ofertalaboral->fechainiciocontrato = $request->input('fechainiciocontrato');
            $ofertalaboral->fechafincontrato = $request->input('fechafincontrato');
            $ofertalaboral->horario = $request->input('horario');
            $ofertalaboral->monedapago = $request->input('monedapago');
            $ofertalaboral->salario = $request->input('salario');
            $ofertalaboral->mostrarsalario = $request->input('mostrarsalario');
            $ofertalaboral->comentariosalario = $request->input('comentariosalario');
            $ofertalaboral->cambioresidencia = $request->input('cambioresidencia');
            $ofertalaboral->disponibilidadviajar = $request->input('disponibilidadviajar');
            $ofertalaboral->nivelestudio_id = $request->input('nivelestudio_id');
            $ofertalaboral->experiencialaboral = $request->input('experiencialaboral');
            $ofertalaboral->anioexperiencia = $request->input('anioexperiencia');
            $ofertalaboral->estudiominimo = $request->input('nivelestudio_id');
            $ofertalaboral->requiereidioma = $request->input('requiereidioma');
            $ofertalaboral->requieresoftware = $request->input('requieresoftware');
            $ofertalaboral->descripcionrequisitos = $request->input('descripcionrequisitos');
            $ofertalaboral->vehiculopropio = $request->input('vehiculopropio');
            $ofertalaboral->tienelicencia = $request->input('tienelicencia');
            $ofertalaboral->tipolicencia = $request->input('tipolicencia');
            $ofertalaboral->ofertaextranjero = $request->input('ofertaextranjero');
            $ofertalaboral->pais_id = $request->input('pais_id');
            $ofertalaboral->direccionextranjero = $request->input('direccionextranjero');
            $ofertalaboral->admitediscapacidad = $request->input('admitediscapacidad');
            $ofertalaboral->vigenciaoferta = $request->input('vigenciaoferta');
            $ofertalaboral->observacionoferta = $request->input('observacionoferta');

            $fechaactual = new DateTime();
            $ofertalaboral->fechapublicacion =$request->input('fechapublicacion');
            $ofertalaboral->publicar = $request->input('publicar');
            $ofertalaboral->empleador_id = $request->input('empleador_id');


            $ofertalaboral->save();

            $insertaridofertalaboral = $ofertalaboral->id;

            $iidioma = 0;
            while (++$iidioma) {
                if ($request->input('idioma_id' . $iidioma) === null) {
                    break;
                } else {
                    $idiomarequerido = new Idiomarequerido();
                    $idiomarequerido->lectura = $request->input('lecturaidioma' . $iidioma);
                    $idiomarequerido->escritura = $request->input('escrituraidioma' . $iidioma);
                    $idiomarequerido->conversacion = $request->input('conversacionidioma' . $iidioma);
                    $idiomarequerido->ofertalaboral_id = $insertaridofertalaboral;
                    $idiomarequerido->idioma_id = $request->input('idioma_id' . $iidioma);
                    $idiomarequerido->save();
                }

            }

            $isoftware = 0;
            while (++$isoftware) {
                if ($request->input('software_id' . $isoftware) === null) {
                    break;
                } else {
                    $softwarerequerido = new Conocimientoinformaticorequerido();
                    $softwarerequerido->nombre = $request->input('software_id' . $isoftware);
                    $softwarerequerido->nivel = $request->input('nivelsoftware' . $isoftware);
                    $softwarerequerido->ofertalaboral_id = $insertaridofertalaboral;
                    $softwarerequerido->save();
                }

            }

            $idiscapacidad= 0;
            while (++$idiscapacidad) {
                if ($request->input('discapacidad_id' . $idiscapacidad) === null) {
                    break;
                } else {
                    $discapacidadrequerida = new Discapacidadrequerida();
                    $discapacidadrequerida->discapacidad_id = $request->input('discapacidad_id' . $idiscapacidad);
                    $discapacidadrequerida->ofertalaboral_id = $insertaridofertalaboral;
                    $discapacidadrequerida->save();
                }

            }

            $inivelproceso= 0;
            while (++$inivelproceso) {
                if ($request->input('nivelproceso_id' . $inivelproceso) === null) {
                    break;
                } else {
                    $nivelproceso = new Nivelproceso();
                    $nivelproceso->nombre = $request->input('nivelproceso_id' . $inivelproceso);
                    $nivelproceso->prioridad = $request->input('prioridadnivelproceso' . $inivelproceso);
                    $nivelproceso->ofertalaboral_id = $insertaridofertalaboral;
                    $nivelproceso->save();
                }

            }
        });
        return is_null($error) ? "OK" : $error;
    }

    public function edit($id, Request $request)
    {
        $existe = Libreria::verificarExistencia($id, 'ofertalaboral');
        if ($existe !== true) {
            return $existe;
        }

        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $ofertalaboral = Ofertalaboral::find($id);
        $idiomarequerido = Idiomarequerido::where('ofertalaboral_id', '=', $ofertalaboral->id)->get();
        $softwarerequerido = Conocimientoinformaticorequerido::where('ofertalaboral_id', '=', $ofertalaboral->id)->get();
        $discapacidadrequerida = Discapacidadrequerida::where('ofertalaboral_id', '=', $ofertalaboral->id)->get();
        $nivelproceso = Nivelproceso::where('ofertalaboral_id', '=', $ofertalaboral->id)->get();
        $cboEmpleador = ['' => 'Seleccione'] + Empleador::pluck('razonsocial', 'id')->all();


        $entidad = 'Ofertalaboral';
        $cboCargolaboral = ['' => 'Seleccione'] + Cargolaboral::pluck('nombre', 'id')->all();
        $cboEscuela = ['' => 'Seleccione'] + Escuela::pluck('nombre', 'id')->all();
        $cboArealaboral = ['' => 'Seleccione'] + Arealaboral::pluck('nombre', 'id')->all();
        $cboJornadalaboral = ['' => 'Seleccione'] + Jornadalaboral::pluck('nombre', 'id')->all();
        $cboTipocontrato = ['' => 'Seleccione'] + Tipocontrato::pluck('nombre', 'id')->all();
        $cboMonedapago = ['' => 'Seleccione', 'Soles' => 'Soles (S/.)', 'Dólares' => 'Dólares ($)'];
        $cboNivelestudio = ['' => 'Seleccione'] + Nivelestudio::pluck('nombre', 'id')->all();
        $cboExperiencialaboral = ['' => 'Seleccione', 'sinexperiencia' => 'Sin Experiencia', 'iguala' => 'Igual a (=)', 'mayoroigualque' => 'Mayor o Igual que (>=)', 'menorigualque' => 'Menor o Igual que (<=)'];
        $cboIdioma = ['' => 'Seleccione'] + Idioma::pluck('nombre', 'id')->all();

        $cboLecturaidioma = ['' => 'Seleccione', 'básico' => 'Básico', 'intermedio' => 'Intermedio', 'avanzado' => 'Avanzado'];
        $cboEscrituraidioma = ['' => 'Seleccione', 'básico' => 'Básico', 'intermedio' => 'Intermedio', 'avanzado' => 'Avanzado'];
        $cboConversacionidioma = ['' => 'Seleccione', 'básico' => 'Básico', 'intermedio' => 'Intermedio', 'avanzado' => 'Avanzado'];
        $cboNivelsoftware = ['' => 'Seleccione', 'bajo' => 'Bajo', 'usuario' => 'Usuario', 'usuarioavanzado' => 'Usuario Avanzado', 'tecnico' => 'Técnico', 'profesional' => 'Profesional', 'experto' => 'Experto'];
        $cboPais = ['' => 'Seleccione'] + Pais::where('nombre', '!=', 'Perú')->orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        $tipodiscapacidad = Tipodiscapacidad::where('id', '!=', 0)->first();
        $discapacidad = Discapacidad::where('tipodiscapacidad_id', '=', $tipodiscapacidad->id)->first();
        $cboTipodiscapacidad = ['' => 'Seleccione'] + Tipodiscapacidad::orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        $cboDiscapacidad = ['' => 'Seleccione'] + Discapacidad::where('tipodiscapacidad_id', '=', $tipodiscapacidad->id)->orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        $formData = array('ofertalaboral.update', $id);
        $formData = array('route' => $formData, 'method' => 'PUT', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Modificar';
        return view($this->folderview . '.mant')
            ->with(compact('ofertalaboral', 'formData','entidad', 'boton','cboCargolaboral', 'cboArealaboral', 'cboJornadalaboral','cboTipocontrato',
                'cboMonedapago', 'cboNivelestudio', 'cboExperiencialaboral',
                'cboNivelsoftware', 'cboPais', 'tipodiscapacidad', 'discapacidad', 'cboTipodiscapacidad', 'cboDiscapacidad', 'listar','idiomarequerido','cboIdioma',
                'cboLecturaidioma','cboEscrituraidioma','cboConversacionidioma','softwarerequerido','discapacidadrequerida','nivelproceso', 'cboEmpleador', 'cboEscuela'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \bolsatrabajo\Departamento  $departamento
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $existe = Libreria::verificarExistencia($id, 'ofertalaboral');
        if ($existe !== true) {
            return $existe;
        }

        $mensajes = array(
            'titulo.required' => 'Debe ingresar Título',
            'nrovacante.required' => 'Debe ingresar Nro. de Vacantes',
            'cargolaboral_id.required' => 'Debe seleccionar un Tipo Cargo',
            'arealaboral_id.required' => 'Debe seleccionar un Área Laboral',
            'descripcion.required' => 'Debe seleccionar un Descripción de Puesto',
            'jornadalaboral_id.required' => 'Debe seleccionar Jornada Laboral',
            'tipocontrato_id.required' => 'Debe seleccionar Tipo de Contrato',
            'duracioncontrato.required' => 'Debe ingresar Duración de Contrato',
            'fechainiciocontrato.required' => 'Debe ingresar Inicio Contrato',
            'fechafincontrato.required' => 'Debe ingresar Fin Contrato',
            'horario.required' => 'Debe ingresar Horario',
            'monedapago.required' => 'Debe seleccionar Modo de Pago',
            'salario.required' => 'Debe ingresar Salario',
            'nivelestudio_id.required' => 'Debe seleccionar Nilvel Estudio',
            'experiencialaboral.required' => 'Debe seleccionar Experiencia Laboral',
            'descripcionrequisitos.required' => 'Debe ingresar Requisitos',
            'vigenciaoferta.required' => 'Debe ingresar  Vigencia Oferta',
            'observacionoferta.required' => 'Debe ingresar  Observación',
            'escuela_id.required' => 'Debe seleccionar una escuela profesional',
        );

        $reglas = array('titulo' => 'required|max:60',
            'nrovacante' => 'required|numeric',
            'cargolaboral_id' => 'required',
            'fechapublicacion' => 'required',
            'arealaboral_id' => 'required',
            'descripcion' => 'required|max:400',
            'empleador_id' => 'required',
            'escuela_id' => 'required',
            //'jornadalaboral_id' => 'required',
            //'tipocontrato_id' => 'required',
            //'duracioncontrato' => 'required|max:200',
            //'fechainiciocontrato' => 'required',
            //'fechafincontrato' => 'required',
            //'horario' => 'required|max:200',
            //'monedapago' => 'required',
            //'salario' => 'required|numeric',
            'nivelestudio_id' => 'required',
            'experiencialaboral' => 'required',
            'descripcionrequisitos' => 'required',
            'vigenciaoferta' => 'required|numeric',
            'observacionoferta' => 'required|String',
        );
        //$mensajes = array();

        $validacion = Validator::make($request->all(), $reglas, $mensajes);
        if ($validacion->fails()) {
            return $validacion->messages()->toJson();
        }
        $error = DB::transaction(function () use ($request, $id) {
            $ofertalaboral = Ofertalaboral::find($id);
            $session_id = session_id();

            $ofertalaboral->titulo = $request->input('titulo');
            $ofertalaboral->empleador_id = $request->input('empleador_id');
            $ofertalaboral->escuela_id = $request->input('escuela_id');
            $ofertalaboral->nrovacante = $request->input('nrovacante');
            $ofertalaboral->cargolaboral_id = $request->input('cargolaboral_id');
            $ofertalaboral->arealaboral_id = $request->input('arealaboral_id');
            $ofertalaboral->descripcion = $request->input('descripcion');
            $ofertalaboral->jornadalaboral_id = $request->input('jornadalaboral_id');
            $ofertalaboral->tipocontrato_id = $request->input('tipocontrato_id');
            $ofertalaboral->duracioncontrato = $request->input('duracioncontrato');
            $ofertalaboral->fechainiciocontrato = $request->input('fechainiciocontrato');
            $ofertalaboral->fechafincontrato = $request->input('fechafincontrato');
            $ofertalaboral->horario = $request->input('horario');
            $ofertalaboral->monedapago = $request->input('monedapago');
            $ofertalaboral->salario = $request->input('salario');
            $ofertalaboral->mostrarsalario = $request->input('mostrarsalario');
            $ofertalaboral->comentariosalario = $request->input('comentariosalario');
            $ofertalaboral->cambioresidencia = $request->input('cambioresidencia');
            $ofertalaboral->disponibilidadviajar = $request->input('disponibilidadviajar');
            $ofertalaboral->nivelestudio_id = $request->input('nivelestudio_id');
            $ofertalaboral->experiencialaboral = $request->input('experiencialaboral');
            $ofertalaboral->anioexperiencia = $request->input('anioexperiencia');
            $ofertalaboral->estudiominimo = $request->input('nivelestudio_id');
            $ofertalaboral->requiereidioma = $request->input('requiereidioma');
            $ofertalaboral->requieresoftware = $request->input('requieresoftware');
            $ofertalaboral->descripcionrequisitos = $request->input('descripcionrequisitos');
            $ofertalaboral->vehiculopropio = $request->input('vehiculopropio');
            $ofertalaboral->tienelicencia = $request->input('tienelicencia');
            $ofertalaboral->tipolicencia = $request->input('tipolicencia');
            $ofertalaboral->ofertaextranjero = $request->input('ofertaextranjero');
            $ofertalaboral->pais_id = $request->input('pais_id');
            $ofertalaboral->direccionextranjero = $request->input('direccionextranjero');
            $ofertalaboral->admitediscapacidad = $request->input('admitediscapacidad');
            $ofertalaboral->vigenciaoferta = $request->input('vigenciaoferta');
            $ofertalaboral->observacionoferta = $request->input('observacionoferta');

            $fechaactual = new DateTime();
            $ofertalaboral->fechapublicacion = $request->input('fechapublicacion');
            $ofertalaboral->publicar = $request->input('publicar');

            $ofertalaboral->save();

            $insertaridofertalaboral = $ofertalaboral->id;


            /*PARA IDIOMA REQUERIDO*/
            $iidioma = 0;
            while (++$iidioma) {
                if ($request->input('idioma_id' . $iidioma) === null
                    && $request->input('lecturaidioma' . $iidioma) === null
                    && $request->input('escrituraidioma' . $iidioma) === null
                    && $request->input('conversacionidioma' . $iidioma) === null
                    && $request->input('id_idiomarequerido' . $iidioma) === null) {
                    break;
                }
                if($request->input('id_idiomarequerido' . $iidioma) === null){
                    $idiomarequerido = new Idiomarequerido();
                    $idiomarequerido->lectura = $request->input('lecturaidioma' . $iidioma);
                    $idiomarequerido->escritura = $request->input('escrituraidioma' . $iidioma);
                    $idiomarequerido->conversacion = $request->input('conversacionidioma' . $iidioma);
                    $idiomarequerido->ofertalaboral_id = $insertaridofertalaboral;
                    $idiomarequerido->idioma_id = $request->input('idioma_id' . $iidioma);
                    $idiomarequerido->save();
                }
                if ($request->input('idioma_id' . $iidioma) !== null
                    && $request->input('lecturaidioma' . $iidioma) !== null
                    && $request->input('escrituraidioma' . $iidioma) !== null
                    && $request->input('conversacionidioma' . $iidioma) !== null
                    && $request->input('id_idiomarequerido' . $iidioma) !== null){
                    $idiomaofertalaboral=DB::table('idioma_ofertalaboral')->select(DB::raw('id'))
                        ->where('ofertalaboral_id', '=', $id)->get('id');
                    foreach($idiomaofertalaboral as $key => $value){
//                        dd($value->id);
                        $idiomarequerido= Idiomarequerido::find($value->id);
                        $idiomarequerido->lectura = $request->input('lecturaidioma' . $iidioma);
                        $idiomarequerido->escritura = $request->input('escrituraidioma' . $iidioma);
                        $idiomarequerido->conversacion = $request->input('conversacionidioma' . $iidioma);
                        $idiomarequerido->ofertalaboral_id = $insertaridofertalaboral;
                        $idiomarequerido->idioma_id = $request->input('idioma_id' . $iidioma);
                        $idiomarequerido->save();
                        $iidioma++;
                    }
                    --$iidioma;
                }
            }


//            $iidioma = 0;
//            while (++$iidioma) {
//                if ($request->input('idioma_id' . $iidioma) === null) {
//                    break;
//                } else {
//                    $idiomarequerid=DB::table('idioma_ofertalaboral')->select(DB::raw('id'))
//                    ->where('ofertalaboral_id', '=', $id)->get('id');
//
//                    $idiomarequerido = new Idiomarequerido();
//                    $idiomarequerido->lectura = $request->input('lecturaidioma' . $iidioma);
//                    $idiomarequerido->escritura = $request->input('escrituraidioma' . $iidioma);
//                    $idiomarequerido->conversacion = $request->input('conversacionidioma' . $iidioma);
//                    $idiomarequerido->ofertalaboral_id = $insertaridofertalaboral;
//                    $idiomarequerido->idioma_id = $request->input('idioma_id' . $iidioma);
//                    $idiomarequerido->save();
//
//                    foreach($idiomarequerid as $key => $value){
//
//                        $idiomarequerido= Idiomarequerido::find($value->id);
//                        $idiomarequerido->lectura = $request->input('lecturaidioma' . $iidioma);
//                        $idiomarequerido->escritura = $request->input('escrituraidioma' . $iidioma);
//                        $idiomarequerido->conversacion = $request->input('conversacionidioma' . $iidioma);
//                        $idiomarequerido->ofertalaboral_id = $insertaridofertalaboral;
//                        $idiomarequerido->idioma_id = $request->input('idioma_id' . $iidioma);
//                        $idiomarequerido->save();
//
//                        $iidioma++;
//                    }
//
//
//                }
//
//            }

            /*PARA SOFTWARE REQUERIDO*/
//            $isoftware = 0;
//            while (++$isoftware) {
//                if ($request->input('software_id' . $isoftware) === null) {
//                    break;
//                } else {
//                    $conocimientoinformatico=DB::table('conocimientoinformaticorequerido')->select(DB::raw('id,nombre'))
//                    ->where('ofertalaboral_id', '=', $id)->get('id','nombre');
//
//                    foreach($conocimientoinformatico as $key => $value){
//
//                        $softwarerequerido= Conocimientoinformaticorequerido::find($value->id);
//                        $softwarerequerido->nombre = $request->input('software_id' . $isoftware);
//                        $softwarerequerido->nivel = $request->input('nivelsoftware' . $isoftware);
//                        $softwarerequerido->ofertalaboral_id = $insertaridofertalaboral;
//                        $softwarerequerido->save();
//                        $isoftware++;
//                    }
//
//
//                }
//
//            }


            $isoftware = 0;
            while (++$isoftware) {
                if ($request->input('software_id' . $isoftware) === null
                    && $request->input('nivelsoftware' . $isoftware) === null
                    && $request->input('id_softwarerequerido' . $isoftware) === null) {
                    break;
                }
                if($request->input('id_softwarerequerido' . $isoftware) === null){
                    $softwarerequerido = new Conocimientoinformaticorequerido();
                    $softwarerequerido->nombre = $request->input('software_id' . $isoftware);
                    $softwarerequerido->nivel = $request->input('nivelsoftware' . $isoftware);
                    $softwarerequerido->ofertalaboral_id = $insertaridofertalaboral;
                    $softwarerequerido->save();
                }
                if ($request->input('software_id' . $isoftware) !== null
                    && $request->input('nivelsoftware' . $isoftware) !== null
                    && $request->input('id_softwarerequerido' . $isoftware) !== null){
                    $conocimientoinformatico=DB::table('conocimientoinformaticorequerido')->select(DB::raw('id,nombre'))
                        ->where('ofertalaboral_id', '=', $id)->get('id','nombre');
                    foreach($conocimientoinformatico as $key => $value){
                        $softwarerequerido= Conocimientoinformaticorequerido::find($value->id);
                        $softwarerequerido->nombre = $request->input('software_id' . $isoftware);
                        $softwarerequerido->nivel = $request->input('nivelsoftware' . $isoftware);
                        $softwarerequerido->ofertalaboral_id = $insertaridofertalaboral;
                        $softwarerequerido->save();
                        $isoftware++;
                    }
                    --$isoftware;
                }
            }

            /*PARA DISCAPACIDAD REQUERIDA*/
            $idiscapacidad = 0;
            while (++$idiscapacidad) {
                if ($request->input('tipodiscapacidad_id' . $idiscapacidad) === null
                    && $request->input('discapacidad_id' . $idiscapacidad) === null
                    && $request->input('id_discapacidadrequerida' . $idiscapacidad) === null) {
                    break;
                }
                if($request->input('id_discapacidadrequerida' . $idiscapacidad) === null){
                    $discapacidadrequerida = new Discapacidadrequerida();
                    $discapacidadrequerida->discapacidad_id = $request->input('discapacidad_id' . $idiscapacidad);
                    $discapacidadrequerida->ofertalaboral_id = $insertaridofertalaboral;
                    $discapacidadrequerida->save();
                }
                if ($request->input('tipodiscapacidad_id' . $idiscapacidad) !== null
                    && $request->input('discapacidad_id' . $idiscapacidad) !== null
                    && $request->input('id_discapacidadrequerida' . $idiscapacidad) !== null){
                    $discapacidadofertalaboral=DB::table('discapacidad_ofertalaboral')->select(DB::raw('id'))
                        ->where('ofertalaboral_id', '=', $id)->get('id');
                    foreach($discapacidadofertalaboral as $key => $value){
                        $discapacidadrequerida= Discapacidadrequerida::find($value->id);
                        $discapacidadrequerida->discapacidad_id = $request->input('discapacidad_id' . $idiscapacidad);
                        $discapacidadrequerida->ofertalaboral_id = $insertaridofertalaboral;
                        $discapacidadrequerida->save();
                        $idiscapacidad++;
                    }
                    --$idiscapacidad;
                }
            }

            /*PARA NIVEL PROCESO*/
            $inivelproceso = 0;
            while (++$inivelproceso) {
                if ($request->input('nivelproceso_id' . $inivelproceso) === null
                    && $request->input('prioridadnivelproceso' . $inivelproceso) === null
                    && $request->input('id_nivelprocesorequerido' . $inivelproceso) === null) {
                    break;
                }
                if($request->input('id_nivelprocesorequerido' . $inivelproceso) === null){
                    $nivelproceso = new Nivelproceso();
                    $nivelproceso->nombre = $request->input('nivelproceso_id' . $inivelproceso);
                    $nivelproceso->prioridad = $request->input('prioridadnivelproceso' . $inivelproceso);
                    $nivelproceso->ofertalaboral_id = $insertaridofertalaboral;
                    $nivelproceso->save();
                }
                if ($request->input('nivelproceso_id' . $inivelproceso) !== null
                    && $request->input('prioridadnivelproceso' . $inivelproceso) !== null
                    && $request->input('id_nivelprocesorequerido' . $inivelproceso) !== null){
                    $nivelprocesoofertalaboral=DB::table('nivelproceso')->select(DB::raw('id,nombre'))
                        ->where('ofertalaboral_id', '=', $id)->get('id','nombre');
                    foreach($nivelprocesoofertalaboral as $key => $value){
                        $nivelproceso= Nivelproceso::find($value->id);
//                        dd($nivelproceso);
                        $nivelproceso->nombre = $request->input('nivelproceso_id' . $inivelproceso);
                        $nivelproceso->prioridad = $request->input('prioridadnivelproceso' . $inivelproceso);
                        $nivelproceso->ofertalaboral_id = $insertaridofertalaboral;
                        $nivelproceso->save();
                        $inivelproceso++;
                    }
                    --$inivelproceso;
                }
            }

        });
        return is_null($error) ? "OK" : $error;
    }


    public function destroy($id)
    {
        $existe = Libreria::verificarExistencia($id, 'ofertalaboral');
        if ($existe !== true) {
            return $existe;
        }
        $error = DB::transaction(function () use ($id) {
            $ofertalaboral = Ofertalaboral::find($id);
            $ofertalaboral->delete();
            $conocimientoinformatico = Conocimientoinformaticorequerido::where('ofertalaboral_id', '=', $id);
            $conocimientoinformatico->delete();
            $discapacidadrequerida = Discapacidadrequerida::where('ofertalaboral_id', '=', $id);
            $discapacidadrequerida->delete();
            $idiomarequerido = Idiomarequerido::where('ofertalaboral_id', '=', $id);
            $idiomarequerido->delete();
        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Función para confirmar la eliminación de un registrlo
     * @param  integer $id id del registro a intentar eliminar
     * @param  string $listarLuego consultar si luego de eliminar se listará
     * @return html              se retorna html, con la ventana de confirmar eliminar
     */
    public function eliminar($id, $listarLuego)
    {
        $existe = Libreria::verificarExistencia($id, 'ofertalaboral');
        if ($existe !== true) {
            return $existe;
        }
        $listar = "NO";
        if (!is_null(Libreria::obtenerParametro($listarLuego))) {
            $listar = $listarLuego;
        }
        $modelo = Ofertalaboral::find($id);
        $entidad = 'Ofertalaboral';
        $formData = array('route' => array('ofertalaboral.destroy', $id), 'method' => 'DELETE', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Eliminar';
        return view('app.confirmarEliminar')->with(compact('modelo', 'formData', 'entidad', 'boton', 'listar'));
    }

    public function eliminaridiomarequerido(Request $r,$id)
    {
        if($r->ajax()){
            $idiomarequerido = Idiomarequerido::find($r->id);
            if(!is_null($idiomarequerido->ofertalaboral_id)){
                $idofertalaboral=$idiomarequerido->ofertalaboral_id;
//            dd($idiomarequerido->ofertalaboral_id);
                $ofertalaboral = Ofertalaboral::find($idofertalaboral);
                $ofertalaboral->requiereidioma = $r->input('requiereidioma');
                $ofertalaboral->save();

                Idiomarequerido::destroy($r->id);
                return response(['id'=>$r->id]);
            }else{
                return;
            }
        }
    }

    public function eliminarsoftwarerequerido(Request $r,$id)
    {
        if($r->ajax()){
            $softwarerequerido = Conocimientoinformaticorequerido::find($r->id);
            $idsoftwarerequerido = $softwarerequerido->ofertalaboral_id;

            $ofertalaboral = Ofertalaboral::find($idsoftwarerequerido);
            $ofertalaboral->requieresoftware = $r->input('requieresoftware');
            $ofertalaboral->save();

            Conocimientoinformaticorequerido::destroy($r->id);
            return response(['id'=>$r->id]);
        }
    }

    public function eliminardiscapacidadrequerida(Request $r,$id)
    {
        if($r->ajax()){
//            Session::['id'];
            $discapacidadrequerida = Discapacidadrequerida::find($r->id);
            $iddiscapacidadrequerida = $discapacidadrequerida->ofertalaboral_id;

            $ofertalaboral = Ofertalaboral::find($iddiscapacidadrequerida);
            $ofertalaboral->admitediscapacidad = $r->input('admitediscapacidad');
            $ofertalaboral->save();

            Discapacidadrequerida::destroy($r->id);
            return response(['id'=>$r->id]);
        }
    }

    public function eliminarnivelprocesorequerida(Request $r,$id)
    {
        if($r->ajax()){
            Nivelproceso::destroy($r->id);
            return response(['id'=>$r->id]);
        }
    }

}
