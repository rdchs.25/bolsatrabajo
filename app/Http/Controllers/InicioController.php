<?php

namespace bolsatrabajo\Http\Controllers;

use bolsatrabajo\Permiso;
use bolsatrabajo\Tipousuario;
use Illuminate\Support\Facades\Auth;


class InicioController extends Controller
{
    public function menu()
    {
        if (Auth::check()) {


            $menu = '<ul class="treeview-menu"><li><a href="/"><i class="fa fa-circle-o"></i>Inicio</a></li></ul>';
        } else {
            $menu = '<ul class="treeview-menu">
                             <li><a href="/"><i class="fa fa-circle-o"></i> Inicios</a></li>
                    </ul>';
        }
        return view('home')->with(compact('menu'));
    }




}
