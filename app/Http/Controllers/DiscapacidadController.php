<?php

namespace bolsatrabajo\Http\Controllers;

use bolsatrabajo\Http\Requests;
use bolsatrabajo\Librerias\Libreria;
use bolsatrabajo\Discapacidad;
use bolsatrabajo\Tipodiscapacidad;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class DiscapacidadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $folderview = 'app.discapacidad';
    protected $tituloAdmin = 'Discapacidad';
    protected $tituloRegistrar = 'Registrar Discapacidad';
    protected $tituloModificar = 'Modificar Discapacidad';
    protected $tituloEliminar = 'Eliminar Discapacidad';
    protected $rutas = array('create' => 'discapacidad.create',
        'edit' => 'discapacidad.edit',
        'delete' => 'discapacidad.eliminar',
        'search' => 'discapacidad.buscar',
        'index' => 'discapacidad.index',
    );

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function buscar(Request $request)
    {
        $pagina = $request->input('page');
        $filas = $request->input('filas');
        $entidad = 'Discapacidad';
        $name = Libreria::getParam($request->input('name'));
        $resultado = DB::table('discapacidad')
            ->join('tipodiscapacidad', 'tipodiscapacidad.id', '=', 'discapacidad.tipodiscapacidad_id')
            ->where('discapacidad.nombre', 'like','%'.$name.'%')
            ->whereNull('discapacidad.deleted_at')
            ->select('discapacidad.id as id','discapacidad.nombre as discapacidad_nombre', 'tipodiscapacidad.nombre as tipodiscapacidad_nombre');
        $lista = $resultado->get();
        $cabecera = array();
        $cabecera[] = array('valor' => '#', 'numero' => '1');
        $cabecera[] = array('valor' => 'Nombre', 'numero' => '1');
        $cabecera[] = array('valor' => 'Tipo Discapacidad', 'numero' => '1');
        $cabecera[] = array('valor' => 'Operaciones', 'numero' => '2');

        $titulo_modificar = $this->tituloModificar;
        $titulo_eliminar = $this->tituloEliminar;
        $ruta = $this->rutas;
        if (count($lista) > 0) {
            $clsLibreria = new Libreria();
            $paramPaginacion = $clsLibreria->generarPaginacion($lista, $pagina, $filas, $entidad);
            $paginacion = $paramPaginacion['cadenapaginacion'];
            $inicio = $paramPaginacion['inicio'];
            $fin = $paramPaginacion['fin'];
            $paginaactual = $paramPaginacion['nuevapagina'];
            $lista = $resultado->paginate($filas);
            $request->replace(array('page' => $paginaactual));
            return view($this->folderview . '.list')->with(compact('lista', 'paginacion', 'inicio', 'fin', 'entidad', 'cabecera', 'titulo_modificar', 'titulo_eliminar', 'ruta'));
        }
        return view($this->folderview . '.list')->with(compact('lista', 'entidad'));
    }

    public function index()
    {
        $entidad = 'Discapacidad';
        $title = $this->tituloAdmin;
        $titulo_registrar = $this->tituloRegistrar;
        $ruta = $this->rutas;
        return view($this->folderview . '.admin')->with(compact('entidad', 'title', 'titulo_registrar', 'ruta'));
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $entidad = 'Discapacidad';
        $cboTipodiscapacidad = ['' => 'Seleccione Tipo de Discapacidad'] + Tipodiscapacidad::pluck('nombre', 'id')->all();
        $discapacidad = null;
        $formData = array('discapacidad.store');
        $formData = array('route' => $formData, 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Registrar';
        return view($this->folderview . '.mant')->with(compact('discapacidad', 'formData', 'entidad', 'boton', 'cboTipodiscapacidad','listar'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $reglas = array('nombre' => 'required|max:60','tipodiscapacidad_id' => 'required|max:60');
        $mensajes = array();
        $validacion = Validator::make($request->all(), $reglas, $mensajes);
        if ($validacion->fails()) {
            return $validacion->messages()->toJson();
        }
        $error = DB::transaction(function () use ($request) {
            $discapacidad = new Discapacidad();
            $discapacidad->nombre = $request->input('nombre');
            $discapacidad->tipodiscapacidad_id = $request->input('tipodiscapacidad_id');
            $discapacidad->save();
        });
        return is_null($error) ? "OK" : $error;
    }

    public function edit($id, Request $request)
    {
        $existe = Libreria::verificarExistencia($id, 'discapacidad');
        if ($existe !== true) {
            return $existe;
        }
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $cboTipodiscapacidad = array('' => 'Seleccione') + Tipodiscapacidad::pluck('nombre', 'id')->all();
        $discapacidad = Discapacidad::find($id);
        $entidad = 'Discapacidad';
        $formData = array('discapacidad.update', $id);
        $formData = array('route' => $formData, 'method' => 'PUT', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Modificar';
        return view($this->folderview . '.mant')->with(compact('discapacidad', 'formData', 'entidad', 'boton', 'listar','cboTipodiscapacidad'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \bolsatrabajo\Provincia  $provincia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $existe = Libreria::verificarExistencia($id, 'discapacidad');
        if ($existe !== true) {
            return $existe;
        }
        $reglas = array('nombre' => 'required|max:100','tipodiscapacidad_id' => 'required|max:60');
        $mensajes = array();
        $validacion = Validator::make($request->all(), $reglas, $mensajes);
        if ($validacion->fails()) {
            return $validacion->messages()->toJson();
        }
        $error = DB::transaction(function () use ($request, $id) {
            $discapacidad = Discapacidad::find($id);
            $discapacidad->nombre = $request->input('nombre');
            $discapacidad->tipodiscapacidad_id = $request->input('tipodiscapacidad_id');
            $discapacidad->save();
        });
        return is_null($error) ? "OK" : $error;
    }

    public function destroy($id)
    {
        $existe = Libreria::verificarExistencia($id, 'discapacidad');
        if ($existe !== true) {
            return $existe;
        }
        $error = DB::transaction(function () use ($id) {
            $discapacidad = Discapacidad::find($id);
            $discapacidad->delete();
        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Función para confirmar la eliminación de un registrlo
     * @param  integer $id id del registro a intentar eliminar
     * @param  string $listarLuego consultar si luego de eliminar se listará
     * @return html              se retorna html, con la ventana de confirmar eliminar
     */
    public function eliminar($id, $listarLuego)
    {
        $existe = Libreria::verificarExistencia($id, 'discapacidad');
        if ($existe !== true) {
            return $existe;
        }
        $listar = "NO";
        if (!is_null(Libreria::obtenerParametro($listarLuego))) {
            $listar = $listarLuego;
        }
        $modelo = Discapacidad::find($id);
        $entidad = 'Discapacidad';
        $formData = array('route' => array('discapacidad.destroy', $id), 'method' => 'DELETE', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Eliminar';
        return view('app.confirmarEliminar')->with(compact('modelo', 'formData', 'entidad', 'boton', 'listar'));
    }

    public function cbodiscapacidad($tipodiscapacidad_id = null)
    {
        $existe = Libreria::verificarExistencia($tipodiscapacidad_id, 'tipodiscapacidad');
        if ($existe !== true) {
            return $existe;
        }
        $tipodiscapacidad = Tipodiscapacidad::find($tipodiscapacidad_id);
        $discapacidades = $tipodiscapacidad->discapacidades;
        if (count($discapacidades) > 0) {
            $cadena = '';
            foreach ($discapacidades as $key => $value) {
                $cadena .= '<option value="' . $value->id . '">' . $value->nombre . '</option>';
            }
            return $cadena;
        } else {
            return '';
        }
    }


}
