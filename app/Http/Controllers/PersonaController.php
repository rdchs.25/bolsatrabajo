<?php

namespace bolsatrabajo\Http\Controllers;

use bolsatrabajo\Anexo;
use bolsatrabajo\Arealaboral;
use bolsatrabajo\Cargolaboral;
use bolsatrabajo\Colegio;
use bolsatrabajo\Conocimientoinformatico;
use bolsatrabajo\Departamento;
use bolsatrabajo\Discapacidad;
use bolsatrabajo\Discapacidadpersona;
use bolsatrabajo\Distrito;
use bolsatrabajo\Escuela;
use bolsatrabajo\Formacionacademica;
use bolsatrabajo\Formacioncomplementaria;
use bolsatrabajo\Formacionlaboral;
use bolsatrabajo\Http\Requests;
use bolsatrabajo\Idioma;
use bolsatrabajo\Idiomapersona;
use bolsatrabajo\Jornadalaboral;
use bolsatrabajo\Librerias\Libreria;
use bolsatrabajo\Nivelestudio;
use bolsatrabajo\Pais;
use bolsatrabajo\Persona;
use bolsatrabajo\Preferencialaboral;
use bolsatrabajo\Provincia;
use bolsatrabajo\Sectorempresa;
use bolsatrabajo\Tipocontrato;
use bolsatrabajo\Tipodocumento;
use bolsatrabajo\Universidad;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Jenssegers\Date\Date;
use bolsatrabajo\User;
use Validator;


class PersonaController extends Controller
{

    protected $folderview = 'app.persona';
    protected $tituloAdmin = 'Persona';
    protected $tituloRegistrar = 'Registrar Persona';
    protected $tituloModificar = 'Modificar Persona';
    protected $tituloEliminar = 'Eliminar Persona';
    protected $titulo_registrar_conoc_informatico = 'Registrar Software y Tecnologías';
    protected $titulo_editar_conoc_informatico = 'Editar Software y Tecnologías';
    protected $titulo_registrar_form_laboral = 'Registrar Formación Laboral';
    protected $titulo_editar_form_laboral = 'Editar Formación Laboral';
    protected $titulo_registrar_form_academica = 'Registrar Formación Académica';
    protected $titulo_editar_form_academica = 'Editar Formación Académica';
    protected $titulo_registrar_form_complementaria = 'Registrar Formación Complementaria';
    protected $titulo_editar_form_complementaria = 'Editar Formación Complementaria';


    protected $rutas = array('create' => 'persona.create',
        'edit' => 'persona.edit',
        'delete' => 'persona.eliminar',
        'search' => 'persona.buscar',
        'index' => 'persona.index',
        'createconocimientoinformatico' => 'persona.createconocimientoinformatico',
        'editarconocimientoinformatico' => 'persona.editarconocimientoinformatico',
        'createformacionlaboral' => 'persona.createformacionlaboral',
        'editarformacionlaboral' => 'persona.editarformacionlaboral',
        'createformacionacademica' => 'persona.createformacionacademica',
        'editarformacionacademica' => 'persona.editarformacionacademica',
        'createformacioncomplementaria' => 'persona.createformacioncomplementaria',
        'editarformacioncomplementaria' => 'persona.editarformacioncomplementaria',
    );


    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (!Libreria::verificarExistenciaPermiso('list_persona')) {
            return view('app.401');
        }
        $add = Libreria::verificarExistenciaPermiso('add_persona');
        $entidad = 'Persona';
        $title = $this->tituloAdmin;
        $titulo_registrar = $this->tituloRegistrar;
        $ruta = $this->rutas;
        $cboDistrito = ['' => 'Todos'] + Distrito::pluck('nombre', 'id')->all();
        $cboPais = ['' => 'Todos'] + Pais::pluck('nombre', 'id')->all();
        $cboEscuela = ['' => 'Todos'] + Escuela::pluck('nombre', 'id')->all();
        $cboTipodocumento = ['' => 'Todos'] + Tipodocumento::pluck('nombre', 'id')->all();
        $cboCargolaboral = ['' => 'Todos'] + Cargolaboral::pluck('nombre', 'id')->all();

        return view($this->folderview . '.admin')->with(compact('entidad', 'add', 'cboDistrito', 'cboPais', 'cboEscuela', 'cboTipodocumento', 'cboCargolaboral', 'title', 'titulo_registrar', 'ruta'));

    }

    public function mostrarformacionlaboral(Request $request, $idpersona)
    {
        if (!Libreria::verificarExistenciaPermiso('add_persona')) {
            return view('app.401');
        }

        /* if (!Libreria::verificarExistenciaPermiso('read_distrito')) {
            return view('app.401');
        }
        if (!Libreria::verificarExistenciaPermiso('read_pais')) {
            return view('app.401');
        }
        if (!Libreria::verificarExistenciaPermiso('read_escuela')) {
            return view('app.401');
        }
        if (!Libreria::verificarExistenciaPermiso('read_tipodocumento')) {
            return view('app.401');
        }
        if (!Libreria::verificarExistenciaPermiso('read_cargolaboral')) {
            return view('app.401');
        }*/
        $listaformlaboral = Formacionlaboral::where('persona_id', '=', $idpersona)->get();
        $boton_form_laboral = 'Registrar';
        $ruta = $this->rutas;
        $titulo_registrar_form_laboral = $this->titulo_registrar_conoc_informatico;
        return view($this->folderview . '.tab-formacionlaboral')->with(compact('listaformlaboral', 'boton_form_laboral', 'ruta', 'titulo_registrar_form_laboral'));
    }

    public function createformacionlaboral(Request $request)
    {
        if (!Libreria::verificarExistenciaPermiso('add_persona')) {
            return view('app.401');
        }

        /* if (!Libreria::verificarExistenciaPermiso('read_distrito')) {
            return view('app.401');
        }
        if (!Libreria::verificarExistenciaPermiso('read_pais')) {
            return view('app.401');
        }
        }
        if (!Libreria::verificarExistenciaPermiso('read_escuela')) {
            return view('app.401');
        }
        if (!Libreria::verificarExistenciaPermiso('read_tipodocumento')) {
            return view('app.401');
        }
        if (!Libreria::verificarExistenciaPermiso('read_cargolaboral')) {
            return view('app.401');
        }*/


        $cboTipocontrato = ['' => 'Seleccione'] + Tipocontrato::orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        $cboSectorempresa = ['' => 'Seleccione'] + Sectorempresa::orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        $cboArealaboral = ['' => 'Seleccione'] + Arealaboral::orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        $cboCargolaboral = ['' => 'Seleccione'] + Cargolaboral::orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        $cboPais = ['' => 'Seleccione'] + Pais::orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        $cboNropersonalcargo = ['' => 'Seleccione', '0' => '0', '1 a 5' => '1 a 5', '6 a 10' => '6 a 10', '11 a 20' => '11 a 20', '21 a 50' => '21 a 50', '50 a 100' => '50 a 100', 'Mas de 100' => 'Más de 100'];
        $cboMoneda = ['' => 'Seleccione', 'nuevosol' => 'Nuevo Sol', "dolar" => "Dólar"];
        $trabajaactualmente = false;
        $mostrarsalario = false;
        $titulo_registrar = $this->titulo_registrar_conoc_informatico;
        $boton = 'Registrar';
        $idboton = 'btn_addformlaboral';
        $editar = 'NO';
        return view($this->folderview . '.mant-tab-formacionlaboral')->with(compact('titulo_registrar', 'boton', 'idboton', 'editar', 'cboTipocontrato', 'cboSectorempresa', 'cboArealaboral', 'cboCargolaboral', 'cboPais', 'cboMoneda', 'cboNropersonalcargo', 'trabajaactualmente', 'mostrarsalario'));
    }

    public function editarformacionlaboral(Request $request)
    {
        if (!Libreria::verificarExistenciaPermiso('add_persona')) {
            return view('app.401');
        }

        /* if (!Libreria::verificarExistenciaPermiso('read_distrito')) {
            return view('app.401');
        }
        if (!Libreria::verificarExistenciaPermiso('read_pais')) {
            return view('app.401');
        }
        if (!Libreria::verificarExistenciaPermiso('read_escuela')) {
            return view('app.401');
        }
        if (!Libreria::verificarExistenciaPermiso('read_tipodocumento')) {
            return view('app.401');
        }
        if (!Libreria::verificarExistenciaPermiso('read_cargolaboral')) {
            return view('app.401');
        }*/
        $cboTipocontrato = ['' => 'Seleccione'] + Tipocontrato::orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        $cboSectorempresa = ['' => 'Seleccione'] + Sectorempresa::orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        $cboArealaboral = ['' => 'Seleccione'] + Arealaboral::orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        $cboCargolaboral = ['' => 'Seleccione'] + Cargolaboral::orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        $cboPais = ['' => 'Seleccione'] + Pais::orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        $cboNropersonalcargo = ['' => 'Seleccione', '0' => '0', '1 a 5' => '1 a 5', '6 a 10' => '6 a 10', '11 a 20' => '11 a 20', '21 a 50' => '21 a 50', '50 a 100' => '50 a 100', 'Mas de 100' => 'Más de 100'];
        $cboMoneda = ['' => 'Seleccione', 'nuevosol' => 'Nuevo Sol', "dolar" => "Dólar"];

        $empresa = $request->input('empresa');
        $sectorempresa_id = $request->input('sectorempresa_id');
        $tipocontrato_id = $request->input('tipocontrato_id');
        $arealaboral_id = $request->input('arealaboral_id');
        $cargolaboral_id = $request->input('cargolaboral_id');
        $nropersonalcargo = $request->input('nropersonalcargo');
        $salario = $request->input('salario');
        $moneda = $request->input('moneda');
        $fechainicio = $request->input('fechainicio');
        $fechatermino = $request->input('fechatermino');
        $descripcionlaboral = $request->input('descripcionlaboral');
        $index = $request->input('index');
        $titulo_registrar = $this->titulo_editar_form_laboral;
        $boton = 'Editar';
        $idboton = 'btn_editformlaboral';

        if ($request->input('mostrarsalario')) {
            $mostrarsalario = true;
        } else {
            $mostrarsalario = false;
        }

        if ($request->input('trabajaactualmente')) {
            $trabajaactualmente = true;
        } else {
            $trabajaactualmente = false;
        }


        return view($this->folderview . '.mant-tab-formacionlaboral')->with(compact('titulo_registrar', 'boton', 'idboton', 'index', 'cboTipocontrato', 'cboSectorempresa', 'cboArealaboral', 'cboCargolaboral', 'cboPais', 'cboMoneda', 'cboNropersonalcargo', 'tipocontrato_id', 'empresa', 'sectorempresa_id', 'arealaboral_id', 'cargolaboral_id', 'nropersonalcargo', 'salario', 'moneda', 'mostrarsalario', 'fechainicio', 'trabajaactualmente', 'fechatermino', 'descripcionlaboral'));

    }


    public function mostrarconocimientoinformatico(Request $request, $idpersona)
    {
        if (!Libreria::verificarExistenciaPermiso('add_persona')) {
            return view('app.401');
        }

        /* if (!Libreria::verificarExistenciaPermiso('read_distrito')) {
            return view('app.401');
        }
        if (!Libreria::verificarExistenciaPermiso('read_pais')) {
            return view('app.401');
        }
        if (!Libreria::verificarExistenciaPermiso('read_escuela')) {
            return view('app.401');
        }
        if (!Libreria::verificarExistenciaPermiso('read_tipodocumento')) {
            return view('app.401');
        }
        if (!Libreria::verificarExistenciaPermiso('read_cargolaboral')) {
            return view('app.401');
        }*/
        $listaconcinfor = Conocimientoinformatico::where('persona_id', '=', $idpersona)->get();
        $boton_conoc_informatico = 'Registrar';
        $ruta = $this->rutas;
        $titulo_registrar_conoc_informatico = $this->titulo_registrar_conoc_informatico;
        return view($this->folderview . '.tab-conocimientoinformatico')->with(compact('listaconcinfor', 'boton_conoc_informatico', 'ruta', 'titulo_registrar_conoc_informatico'));
    }

    public function createconocimientoinformatico(Request $request)
    {
        if (!Libreria::verificarExistenciaPermiso('add_persona')) {
            return view('app.401');
        }

        /* if (!Libreria::verificarExistenciaPermiso('read_distrito')) {
            return view('app.401');
        }
        if (!Libreria::verificarExistenciaPermiso('read_pais')) {
            return view('app.401');
        }
        }
        if (!Libreria::verificarExistenciaPermiso('read_escuela')) {
            return view('app.401');
        }
        if (!Libreria::verificarExistenciaPermiso('read_tipodocumento')) {
            return view('app.401');
        }
        if (!Libreria::verificarExistenciaPermiso('read_cargolaboral')) {
            return view('app.401');
        }*/

        $nombre = null;
        $nivel = null;
        $anioexperiencia = null;
        $descripcionsoftware = null;
        $index = null;
        $cboNivel = ['' => 'Seleccione', 'Basico' => 'Basico', 'Intermedio' => 'Intermedio', 'Avanzado' => 'Avanzado'];
        $titulo_registrar = $this->titulo_registrar_conoc_informatico;
        $boton = 'Registrar';
        $idboton = 'btn_add';
        $editar = 'NO';
        return view($this->folderview . '.mant-tab-conocimientoinformatico')->with(compact('cboNivel', 'nombre', 'nivel', 'anioexperiencia', 'descripcionsoftware', 'titulo_registrar', 'boton', 'idboton', 'editar', 'index'));
    }

    public function editarconocimientoinformatico(Request $request)
    {
        if (!Libreria::verificarExistenciaPermiso('add_persona')) {
            return view('app.401');
        }

        /* if (!Libreria::verificarExistenciaPermiso('read_distrito')) {
            return view('app.401');
        }
        if (!Libreria::verificarExistenciaPermiso('read_pais')) {
            return view('app.401');
        }
        if (!Libreria::verificarExistenciaPermiso('read_escuela')) {
            return view('app.401');
        }
        if (!Libreria::verificarExistenciaPermiso('read_tipodocumento')) {
            return view('app.401');
        }
        if (!Libreria::verificarExistenciaPermiso('read_cargolaboral')) {
            return view('app.401');
        }*/
        $nombre = $request->input('nombre');
        $nivel = $request->input('nivel');
        $anioexperiencia = $request->input('anioexperiencia');
        $descripcionsoftware = $request->input('descripcionsoftware');
        $index = $request->input('index');
        $cboNivel = ['' => 'Seleccione', 'Basico' => 'Basico', 'Intermedio' => 'Intermedio', 'Avanzado' => 'Avanzado'];
        $titulo_registrar = $this->titulo_editar_conoc_informatico;
        $boton = 'Editar';
        $idboton = 'btn_edit';
        return view($this->folderview . '.mant-tab-conocimientoinformatico')->with(compact('cboNivel', 'nombre', 'nivel', 'anioexperiencia', 'descripcionsoftware', 'index', 'titulo_registrar', 'boton', 'idboton'));
    }


    public function createformacionacademica(Request $request)
    {
        if (!Libreria::verificarExistenciaPermiso('add_persona')) {
            return view('app.401');
        }

        /* if (!Libreria::verificarExistenciaPermiso('read_distrito')) {
            return view('app.401');
        }
        if (!Libreria::verificarExistenciaPermiso('read_pais')) {
            return view('app.401');
        }
        }
        if (!Libreria::verificarExistenciaPermiso('read_escuela')) {
            return view('app.401');
        }
        if (!Libreria::verificarExistenciaPermiso('read_tipodocumento')) {
            return view('app.401');
        }
        if (!Libreria::verificarExistenciaPermiso('read_cargolaboral')) {
            return view('app.401');
        }*/

        $cboColegio = ['' => 'Seleccione'] + Colegio::orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        $cboNivelestudio = ['' => 'Seleccione'] + Nivelestudio::orderBy('id', 'ASC')->pluck('nombre', 'id')->all();
        $cboUniversidad = ['' => 'Seleccione'] + Universidad::orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        $cboMeses = ['' => 'seleccione', 'enero' => 'Enero', 'febrero' => 'Febrero', 'marzo' => 'Marzo', 'abril' => 'Abril', 'mayo' => 'Mayo', 'junio' => 'Junio', 'julio' => 'Julio', 'agosto' => 'Agosto', 'septiembre' => 'Septiembre', 'Octubre' => 'Octubre', 'noviembre' => 'Noviembre', 'diciembre' => 'Diciembre'];
        $cboTipoestudio = ['' => 'Seleccione', 'especializacion - lato sensu' => 'Especialización - lato sensu', 'master' => 'Master', 'doctorada' => 'Doctorado', 'post-doctorado' => 'Pos-doctorado', 'MBA' => 'MBA'];
        $cboSituacion = ['' => 'Seleccione', 'estudiando' => 'Estudiando', 'egresado' => 'Egresado', 'bachiller' => 'Bachiller', 'titulado' => 'Titulado', 'incompleto' => 'Incompleto', 'completo' => 'Completo'];
        $cboSemestre = ['' => 'Seleccione', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9', '10' => '10', '11' => '11', '12' => '12'];
        $estudiandoactualmente = false;
        $anioactual = date("Y");
        $cboAnios = ['' => 'seleccione'];
        for ($i = $anioactual; $i >= $anioactual - 50; $i--) {
            $cboAnios[$i] = $i;
        }
        $titulo_registrar = $this->titulo_registrar_form_academica;
        $boton = 'Registrar';
        $idboton = 'btn_addformacademica';
        $editar = 'NO';
        return view($this->folderview . '.mant-tab-formacionacademica')->with(compact('titulo_registrar', 'boton', 'idboton', 'editar', 'cboColegio', 'cboNivelestudio', 'cboUniversidad', 'estudiandoactualmente', 'cboAnios', 'cboMeses', 'cboTipoestudio', 'cboSituacion', 'cboSemestre'));
    }

    public function editarformacionacademica(Request $request)
    {
        if (!Libreria::verificarExistenciaPermiso('add_persona')) {
            return view('app.401');
        }

        /* if (!Libreria::verificarExistenciaPermiso('read_distrito')) {
            return view('app.401');
        }
        if (!Libreria::verificarExistenciaPermiso('read_pais')) {
            return view('app.401');
        }
        if (!Libreria::verificarExistenciaPermiso('read_escuela')) {
            return view('app.401');
        }
        if (!Libreria::verificarExistenciaPermiso('read_tipodocumento')) {
            return view('app.401');
        }
        if (!Libreria::verificarExistenciaPermiso('read_cargolaboral')) {
            return view('app.401');
        }*/

        $cboColegio = ['' => 'Seleccione'] + Colegio::orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        $cboNivelestudio = ['' => 'Seleccione'] + Nivelestudio::orderBy('id', 'ASC')->pluck('nombre', 'id')->all();
        $cboUniversidad = ['' => 'Seleccione'] + Universidad::orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        $cboMeses = ['' => 'seleccione', 'enero' => 'Enero', 'febrero' => 'Febrero', 'marzo' => 'Marzo', 'abril' => 'Abril', 'mayo' => 'Mayo', 'junio' => 'Junio', 'julio' => 'Julio', 'agosto' => 'Agosto', 'septiembre' => 'Septiembre', 'Octubre' => 'Octubre', 'noviembre' => 'Noviembre', 'diciembre' => 'Diciembre'];
        $cboTipoestudio = ['' => 'Seleccione', 'especializacion - lato sensu' => 'Especialización - lato sensu', 'master' => 'Master', 'doctorada' => 'Doctorado', 'post-doctorado' => 'Pos-doctorado', 'MBA' => 'MBA'];
        $cboSituacion = ['' => 'Seleccione', 'estudiando' => 'Estudiando', 'egresado' => 'Egresado', 'bachiller' => 'Bachiller', 'titulado' => 'Titulado', 'incompleto' => 'Incompleto', 'completo' => 'Completo'];
        $cboSemestre = ['' => 'Seleccione', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9', '10' => '10', '11' => '11', '12' => '12'];
        $anioactual = date("Y");
        $cboAnios = ['' => 'seleccione'];
        for ($i = $anioactual; $i >= $anioactual - 50; $i--) {
            $cboAnios[$i] = $i;
        }


        $nivelestudio_id = $request->input('nivelestudio_id');
        $universidad_id = $request->input('universidad_id');
        $colegio_id = $request->input('colegio_id');
        $especialidad = $request->input('especialidad');
        $tipoestudio = $request->input('tipoestudio');
        $anioinicio = $request->input('anioinicio');
        $mesinicio = $request->input('mesinicio');
        $situacion = $request->input('situacion');
        $semestre = $request->input('semestre');
        $aniotermino = $request->input('aniotermino');
        $mestermino = $request->input('mestermino');

        if ($request->input('estudiandoactualmente') == 'true') {
            $estudiandoactualmente = true;
        } else {
            $estudiandoactualmente = false;
        }


        $index = $request->input('index');
        $titulo_registrar = $this->titulo_editar_form_academica;
        $boton = 'Editar';
        $idboton = 'btn_editformacademica';
        return view($this->folderview . '.mant-tab-formacionacademica')->with(compact('titulo_registrar', 'boton', 'idboton', 'editar', 'index', 'cboColegio', 'cboNivelestudio', 'cboUniversidad', 'estudiandoactualmente', 'cboAnios', 'cboMeses', 'cboTipoestudio', 'cboSituacion', 'cboSemestre', 'nivelestudio_id', 'universidad_id', 'colegio_id', 'especialidad', 'tipoestudio', 'anioinicio', 'mesinicio', 'situacion', 'semestre', 'estudiandoactualmente', 'aniotermino', 'mestermino'));


    }


    public function mostrarformacionacademica(Request $request, $idpersona)
    {
        if (!Libreria::verificarExistenciaPermiso('add_persona')) {
            return view('app.401');
        }

        /* if (!Libreria::verificarExistenciaPermiso('read_distrito')) {
            return view('app.401');
        }
        if (!Libreria::verificarExistenciaPermiso('read_pais')) {
            return view('app.401');
        }
        if (!Libreria::verificarExistenciaPermiso('read_escuela')) {
            return view('app.401');
        }
        if (!Libreria::verificarExistenciaPermiso('read_tipodocumento')) {
            return view('app.401');
        }
        if (!Libreria::verificarExistenciaPermiso('read_cargolaboral')) {
            return view('app.401');
        }*/
        $listaformacademica = Formacionacademica::where('persona_id', '=', $idpersona)->get();
        $boton_form_academica = 'Registrar';
        $ruta = $this->rutas;
        $titulo_registrar_form_academica = $this->titulo_registrar_form_academica;
        return view($this->folderview . '.tab-formacionacademica')->with(compact('listaformacademica', 'boton_form_academica', 'ruta', 'titulo_registrar_form_academica'));
    }

    public function createformacioncomplementaria(Request $request)
    {
        if (!Libreria::verificarExistenciaPermiso('add_persona')) {
            return view('app.401');
        }

        /* if (!Libreria::verificarExistenciaPermiso('read_distrito')) {
            return view('app.401');
        }
        if (!Libreria::verificarExistenciaPermiso('read_pais')) {
            return view('app.401');
        }
        }
        if (!Libreria::verificarExistenciaPermiso('read_escuela')) {
            return view('app.401');
        }
        if (!Libreria::verificarExistenciaPermiso('read_tipodocumento')) {
            return view('app.401');
        }
        if (!Libreria::verificarExistenciaPermiso('read_cargolaboral')) {
            return view('app.401');
        }*/

        $cboMeses = ['' => 'seleccione', 'enero' => 'Enero', 'febrero' => 'Febrero', 'marzo' => 'Marzo', 'abril' => 'Abril', 'mayo' => 'Mayo', 'junio' => 'Junio', 'julio' => 'Julio', 'agosto' => 'Agosto', 'septiembre' => 'Septiembre', 'Octubre' => 'Octubre', 'noviembre' => 'Noviembre', 'diciembre' => 'Diciembre'];
        $cboTipoformacion = ['' => 'seleccione', 'diplomado' => 'Diplomado', 'seminario' => 'Seminario', 'curso' => 'Curso', 'taller' => 'Taller', 'conferencia' => 'Conferencia', 'otro' => 'Otro'];
        $anioactual = date("Y");
        $cboAnios = ['' => 'seleccione'];
        for ($i = $anioactual; $i >= $anioactual - 50; $i--) {
            $cboAnios[$i] = $i;
        }
        $estudiandoactual = false;
        $titulo_registrar = $this->titulo_registrar_form_complementaria;
        $boton = 'Registrar';
        $idboton = 'btn_addformcomplementaria';
        $editar = 'NO';
        return view($this->folderview . '.mant-tab-formacioncomplementaria')->with(compact('titulo_registrar', 'boton', 'idboton', 'editar', 'cboMeses', 'cboTipoformacion', 'cboAnios', 'estudiandoactual'));
    }

    public function editarformacioncomplementaria(Request $request)
    {
        if (!Libreria::verificarExistenciaPermiso('add_persona')) {
            return view('app.401');
        }

        /* if (!Libreria::verificarExistenciaPermiso('read_distrito')) {
            return view('app.401');
        }
        if (!Libreria::verificarExistenciaPermiso('read_pais')) {
            return view('app.401');
        }
        if (!Libreria::verificarExistenciaPermiso('read_escuela')) {
            return view('app.401');
        }
        if (!Libreria::verificarExistenciaPermiso('read_tipodocumento')) {
            return view('app.401');
        }
        if (!Libreria::verificarExistenciaPermiso('read_cargolaboral')) {
            return view('app.401');
        }*/

        $cboMeses = ['' => 'seleccione', 'enero' => 'Enero', 'febrero' => 'Febrero', 'marzo' => 'Marzo', 'abril' => 'Abril', 'mayo' => 'Mayo', 'junio' => 'Junio', 'julio' => 'Julio', 'agosto' => 'Agosto', 'septiembre' => 'Septiembre', 'Octubre' => 'Octubre', 'noviembre' => 'Noviembre', 'diciembre' => 'Diciembre'];
        $cboTipoformacion = ['' => 'seleccione', 'diplomado' => 'Diplomado', 'seminario' => 'Seminario', 'curso' => 'Curso', 'taller' => 'Taller', 'conferencia' => 'Conferencia', 'otro' => 'Otro'];
        $anioactual = date("Y");
        $cboAnios = ['' => 'seleccione'];
        for ($i = $anioactual; $i >= $anioactual - 50; $i--) {
            $cboAnios[$i] = $i;
        }


        $tipoformacion = $request->input('tipoformacion');
        $titulo = $request->input('titulo');
        $institucion = $request->input('institucion');
        $cantidadhoras = $request->input('cantidadhoras');
        $anioiniciocomple = $request->input('anioiniciocomple');
        $mesiniciocomple = $request->input('mesiniciocomple');
        $anioterminocomple = $request->input('anioterminocomple');
        $mesterminocomple = $request->input('mesterminocomple');

        if ($request->input('estudiandoactual')) {
            $estudiandoactual = true;
        } else {
            $estudiandoactual = false;
        }


        $index = $request->input('index');
        $titulo_registrar = $this->titulo_editar_form_complementaria;
        $boton = 'Editar';
        $idboton = 'btn_editformcomplementaria';
        return view($this->folderview . '.mant-tab-formacioncomplementaria')->with(compact('titulo_registrar', 'boton', 'idboton', 'editar', 'index', 'cboMeses', 'cboTipoformacion', 'cboAnios', 'tipoformacion', 'titulo', 'institucion', 'cantidadhoras', 'anioiniciocomple', 'mesiniciocomple', 'anioterminocomple', 'mesterminocomple', 'estudiandoactual'));


    }

    public function mostrarformacioncomplementaria(Request $request, $idpersona)
    {
        if (!Libreria::verificarExistenciaPermiso('add_persona')) {
            return view('app.401');
        }

        /* if (!Libreria::verificarExistenciaPermiso('read_distrito')) {
            return view('app.401');
        }
        if (!Libreria::verificarExistenciaPermiso('read_pais')) {
            return view('app.401');
        }
        if (!Libreria::verificarExistenciaPermiso('read_escuela')) {
            return view('app.401');
        }
        if (!Libreria::verificarExistenciaPermiso('read_tipodocumento')) {
            return view('app.401');
        }
        if (!Libreria::verificarExistenciaPermiso('read_cargolaboral')) {
            return view('app.401');
        }*/
        $listaformcomplementaria = Formacioncomplementaria::where('persona_id', '=', $idpersona)->get();
        $boton_form_complementaria = 'Registrar';
        $ruta = $this->rutas;
        $titulo_registrar_form_complementaria = $this->titulo_registrar_form_complementaria;
        return view($this->folderview . '.tab-formacioncomplementaria')->with(compact('listaformcomplementaria', 'boton_form_complementaria', 'ruta', 'titulo_registrar_form_complementaria'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
//        if (!Libreria::verificarExistenciaPermiso('add_persona')) {
//            return view('app.401');
//        }
        /* if (!Libreria::verificarExistenciaPermiso('read_distrito')) {
             return view('app.401');
         }
         if (!Libreria::verificarExistenciaPermiso('read_pais')) {
             return view('app.401');
         }
         if (!Libreria::verificarExistenciaPermiso('read_escuela')) {
             return view('app.401');
         }
         if (!Libreria::verificarExistenciaPermiso('read_tipodocumento')) {
             return view('app.401');
         }
         if (!Libreria::verificarExistenciaPermiso('read_cargolaboral')) {
             return view('app.401');
         }*/

        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $entidad = 'Persona';
        $cboDepartamento = ['' => 'Seleccione Departamento'] + Departamento::orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        $cboProvincia = ['' => 'Seleccione'];
        $cboDistrito = ['' => 'Seleccione'];
        $cboPais = ['' => 'Seleccione'] + Pais::orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        $cboEscuela = ['' => 'Seleccione'] + Escuela::orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        $cboTipodocumento = ['' => 'Seleccione'] + Tipodocumento::orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        $cboCargolaboral = ['' => 'Seleccione'] + Cargolaboral::orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        $cboDiscapacidad = Discapacidad::orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        $cboSexo = ['' => 'Seleccione', 'M' => 'Masculino', 'F' => 'Femenino'];
        $cboEstadocivil = ['' => 'Seleccione', 'S' => 'Soltero', 'C' => 'Casado', 'V' => 'Viudo(a)'];
        $cboIdioma = ['' => 'Seleccione'] + Idioma::orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        $cboNivelidioma = ['' => 'Seleccione', 'Basico' => 'Basico', 'Intermedio' => 'Intermedio', 'Avanzado' => 'Avanzado'];
        $persona = null;
        $formData = array('persona.store');
        $formData = array('route' => $formData, 'files' => true, 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off', 'files' => true);
        $boton = 'Registrar';
        $botoneditar = 'Registrar y continuar editando';

        /*tab-conocimiento informatico*/
        $boton_conoc_informatico = 'Registrar';
        $titulo_registrar_conoc_informatico = $this->titulo_registrar_conoc_informatico;
        $listaconcinfor = [];

        /*tab-formacionlaboral*/
        $boton_form_laboral = 'Registrar';
        $titulo_registrar_form_laboral = $this->titulo_registrar_form_laboral;
        $listaformlaboral = [];

        /*tab-formacionlaboral*/
        $boton_form_academica = 'Registrar';
        $titulo_registrar_form_academica = $this->titulo_registrar_form_academica;
        $listaformacademica = [];

        /*tab-formacion complementaria*/
        $boton_form_complementaria = 'Registrar';
        $titulo_registrar_form_complementaria = $this->titulo_registrar_form_complementaria;
        $listaformcomplementaria = [];

        /*tab-preferencia laboral*/
        $cboJornadalaboral = ['' => 'Seleccione'] + Jornadalaboral::orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        $cboMoneda = ['' => 'Seleccione', 'nuevosol' => 'Nuevo Sol', "dolar" => "Dólar"];
        $cboTipocontrato = ['' => 'Seleccione'] + Tipocontrato::orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        $cboSectorempresa = ['' => 'Seleccione'] + Sectorempresa::orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        $cboArealaboral = ['' => 'Seleccione'] + Arealaboral::orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();

        $ruta = $this->rutas;
        return view($this->folderview . '.mant')->with(compact('persona', 'formData', 'entidad', 'cboDepartamento', 'cboProvincia', 'cboEstadocivil', 'cboDistrito', 'cboPais', 'cboEscuela', 'cboTipodocumento', 'cboSexo', 'cboCargolaboral', 'cboDiscapacidad', 'cboIdioma', 'cboNivelidioma', 'listaconcinfor', 'boton', 'botoneditar', 'boton_conoc_informatico', 'ruta', 'titulo_registrar_conoc_informatico', 'boton_form_laboral', 'titulo_registrar_form_laboral', 'listaformlaboral', 'boton_form_academica', 'titulo_registrar_form_academica', 'listaformacademica', 'boton_form_complementaria', 'titulo_registrar_form_complementaria', 'listaformcomplementaria', 'listar', 'cboJornadalaboral', 'cboMoneda', 'cboTipocontrato', 'cboSectorempresa', 'cboArealaboral', 'cboCargopreferencialaboral'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

//        if (!Libreria::verificarExistenciaPermiso('add_persona')) {
//            return view('app.401');
//        }

        $mensajes = array(
            'codigouniversitario.required' => 'Debe ingresar código universitario',
            'nombres.required' => 'Debe ingresar nombres completos',
            'apellidopaterno.required' => 'Debe ingresar apellido paterno',
            'apellidomaterno.required' => 'Debe ingresar apellido materno',
            'tipodocumento_id.required' => 'Debe seleccionar un tipo documento',
            'nrodocumento.required' => 'Debe ingresar número de documento',
            'fechanacimiento.required' => 'Debe ingresar fecha nacimiento',
            'estadocivil.required' => 'Debe seleccionar un estado civil',
            'sexo.required' => 'Debe seleccionar su género',
            'email.required' => 'Debe ingresar email',
            'telefono.required' => 'Debe ingresar teléfono',
            'celular.required' => 'Debe ingresar celular',
            'pais_id.required' => 'Debe seleccionar su país',
            'departamento_id.required' => 'Debe seleccionar un departamento',
            'provincia_id.required' => 'Debe seleccionar una provincia',
            'distrito_id.required' => 'Debe seleccionar un distrito',
            'escuela_id.required' => 'Debe seleccionar una escuela',
            'direccion.required' => 'Debe ingresar su dirección',
            'descripcionpersona.required' => 'Debe ingresar su descripción',
            'foto.required' => 'Debe seleccionar una foto ',
            'aceptoterminos.required' => 'Debe aceptar los términos ',
//            'jornadalaboral_id.required' => 'Debe seleccionar una jornadalaboral ',
//            'salariolaboral.required' => 'Debe ingresar salario',
//            'moneda.required' => 'Debe seleccionar una moneda ',
//            'preferencia_sectorempresa_id.required' => 'Debe seleccionar sector laboral',
//            'preferencia_arealaboral_id.required' => 'Debe seleccionar área laboral',
//            'preferencia_cargolaboral_id.required' => 'Debe seleccionar cargo laboral',
//            'preferencia_tipocontrato_id.required' => 'Debe seleccionar tipo contrato',
//            'preferencia_departamento_id.required' => 'Debe seleccionar preferencia departamento',

        );

        $reglas = array(
            'codigouniversitario' => 'required|max:50|unique:persona',
            'nombres' => 'required',
            'apellidopaterno' => 'required',
            'apellidomaterno' => 'required',
            'tipodocumento_id' => 'required|integer|exists:tipodocumento,id',
            'nrodocumento' => 'required|integer|unique:persona',
            'fechanacimiento' => 'required',
            'estadocivil' => 'required',
            'sexo' => 'required',
            'email' => 'required|email',
            'telefono' => 'required|integer',
            'celular' => 'required|integer',
            'pais_id' => 'required|integer|exists:pais,id',
            'departamento_id' => 'required|integer|exists:departamento,id',
            'provincia_id' => 'required:departamento_id|integer|exists:provincia,id',
            'distrito_id' => 'required:provincia_id|integer|exists:distrito,id',
            'escuela_id' => 'required|integer|exists:escuela,id',
            'direccion' => 'required',
            'descripcion' => 'required',
            'foto' => 'required|image',
            'aceptoterminos' => 'required',
//            'jornadalaboral_id' => 'required|integer|exists:jornadalaboral,id',
//            'salariolaboral' => 'required|integer',
//            'moneda' => 'required',
//            'preferencia_sectorempresa_id' => 'required|integer|exists:sectorempresa,id',
//            'preferencia_arealaboral_id' => 'required|integer|exists:arealaboral,id',
//            'preferencia_cargolaboral_id' => 'required|integer|exists:cargolaboral,id',
//            'preferencia_tipocontrato_id' => 'required|integer|exists:tipocontrato,id',
//            'preferencia_departamento_id' => 'required|integer|exists:departamento,id',

        );

        $validacion = Validator::make($request->all(), $reglas, $mensajes);

        if ($validacion->fails()) {
            return $validacion->messages()->toJson();
        }

        $error = DB::transaction(function () use ($request) {
            if ($request->hasFile('foto')) {
                $extension = $request->file('foto')->getClientOriginalExtension();
                $ruta = "/public/".$request->foto->storeAs('foto', 'foto_' . $request->input('nrodocumento') . '.' . $extension);
            }

            $persona = new Persona();
            $persona->codigouniversitario = $request->input('codigouniversitario');
            $persona->nombres = $request->input('nombres');
            $persona->apellidopaterno = $request->input('apellidopaterno');
            $persona->apellidomaterno = $request->input('apellidomaterno');
            $persona->tipodocumento_id = $request->input('tipodocumento_id');
            $persona->nrodocumento = $request->input('nrodocumento');
            $persona->fechanacimiento = Date::createFromFormat('d/m/Y', $request->input('fechanacimiento'))->format('Y-m-d');
            $persona->estadocivil = $request->input('estadocivil');
            $persona->sexo = $request->input('sexo');
            $persona->email = $request->input('email');
            $persona->telefono = $request->input('telefono');
            $persona->celular = $request->input('celular');
            $persona->pais_id = $request->input('pais_id');
            $persona->distrito_id = $request->input('distrito_id');
            $persona->escuela_id = $request->input('escuela_id');
            $persona->direccion = $request->input('direccion');
            $persona->descripcion = $request->input('descripcion');
            $persona->foto = $ruta;
            $persona->save();

            /*Discapacidades*/

            $discapacidades = $request->input('discapacidad_id');
            if (count($discapacidades) > 0) {
                foreach ($discapacidades as $clave => $valor) {
                    $discapacidad = new Discapacidadpersona();
                    $discapacidad->persona_id = $persona->id;
                    $discapacidad->discapacidad_id = $valor;
                    $discapacidad->save();
                }
            }

            /*Idioma*/

            $idiomas = $request->get('idioma_id');
            $lectura = $request->get('lectura');
            $escritura = $request->get('escritura');
            $conversacion = $request->get('conversacion');
            $nativo = $request->get('nativo');
            $cont = 0;
            while ($cont < count($idiomas)) {
                $idiomapersona = new Idiomapersona();
                $idiomapersona->persona_id = $persona->id;
                $idiomapersona->idioma_id = $idiomas[$cont];
                if ($nativo[$cont] === true) {
                    $idiomapersona->nativo = true;
                } else {
                    $idiomapersona->nativo = false;
                    $idiomapersona->lectura = $lectura[$cont];
                    $idiomapersona->escritura = $escritura[$cont];
                    $idiomapersona->conversacion = $conversacion[$cont];
                }
                $idiomapersona->save();
                $cont = $cont + 1;
            }

            /*Conocimiento informático*/

            $nombre = $request->get('nombre');
            $nivel = $request->get('nivel');
            $anioexperiencia = $request->get('anioexperiencia');
            $descripcionsoftware = $request->get('descripcionsoftware');
            $contconocinfo = 0;
            while ($contconocinfo < count($nombre)) {
                $conocimientoinformatico = new Conocimientoinformatico();
                $conocimientoinformatico->persona_id = $persona->id;
                $conocimientoinformatico->nombre = $nombre[$contconocinfo];
                $conocimientoinformatico->nivel = $nivel[$contconocinfo];
                $conocimientoinformatico->anioexperiencia = $anioexperiencia[$contconocinfo];
                if ($descripcionsoftware[$contconocinfo] != null) {
                    $conocimientoinformatico->descripcionsoftware = $descripcionsoftware[$contconocinfo];
                }
                $conocimientoinformatico->save();
                $contconocinfo = $contconocinfo + 1;
            }

            /*Formacion laboral*/
            $fechainicio = $request->get('fechainicio');
            $fechatermino = $request->get('fechatermino');
            $nropersonalcargo = $request->get('nropersonalcargo');
            $empresa = $request->get('empresa');
            $salario = $request->get('salario');
            $moneda = $request->get('moneda');
            $descripcionlaboral = $request->get('descripcionlaboral');
            $trabajaactualmente = $request->get('trabajaactualmente');
            $mostrarsalario = $request->get('monstrarsalario');
            $tipocontrato_id = $request->get('tipocontrato_id');
            $sectorempresa_id = $request->get('sectorempresa_id');
            $arealaboral_id = $request->get('arealaboral_id');
            $cargolaboral_id = $request->get('cargolaboral_id');
            $contformlaboral = 0;
            while ($contformlaboral < count($empresa)) {

                $formacionlaboral = new Formacionlaboral();
                $formacionlaboral->persona_id = $persona->id;
                $formacionlaboral->fechainicio = Date::createFromFormat('d/m/Y', $fechainicio[$contformlaboral])->format('Y-m-d');
                if ($fechatermino[$contformlaboral] != null) {
                    $formacionlaboral->fechatermino = Date::createFromFormat('d/m/Y', $fechatermino[$contformlaboral])->format('Y-m-d');
                }
                $formacionlaboral->nropersonalcargo = $nropersonalcargo[$contformlaboral];
                $formacionlaboral->empresa = $nropersonalcargo[$contformlaboral];
                $formacionlaboral->salario = $salario[$contformlaboral];
                $formacionlaboral->moneda = $moneda[$contformlaboral];
                $formacionlaboral->tipocontrato_id = $tipocontrato_id[$contformlaboral];
                $formacionlaboral->sectorempresa_id = $sectorempresa_id[$contformlaboral];
                $formacionlaboral->arealaboral_id = $arealaboral_id[$contformlaboral];
                $formacionlaboral->cargolaboral_id = $cargolaboral_id[$contformlaboral];

                if ($trabajaactualmente[$contformlaboral] === 'true') {
                    $formacionlaboral->trabajaactualmente = true;
                } else {
                    $formacionlaboral->trabajaactualmente = false;
                }

                if ($mostrarsalario[$contformlaboral] === 'true') {
                    $formacionlaboral->mostrarsalario = true;
                } else {
                    $formacionlaboral->mostrarsalario = false;
                }

                if ($descripcionlaboral[$contformlaboral] != null) {
                    $formacionlaboral->descripcionlaboral = $descripcionlaboral[$contformlaboral];
                }
                $formacionlaboral->save();
                $contformlaboral = $contformlaboral + 1;
            }


            /*Formacion académica*/

            $nivelestudio_id = $request->get('nivelestudio_id');
            $universidad_id = $request->get('universidad_id');
            $colegio_id = $request->get('colegio_id');
            $especialidad = $request->get('especialidad');
            $tipoestudio = $request->get('tipoestudio');
            $anioinicio = $request->get('anioinicio');
            $mesinicio = $request->get('mesinicio');
            $situacion = $request->get('situacion');
            $semestre = $request->get('semestre');
            $estudiandoactualmente = $request->get('estudiandoactualmente');
            $aniotermino = $request->get('aniotermino');
            $mestermino = $request->get('mestermino');
            $contformacademica = 0;
            while ($contformacademica < count($nivelestudio_id)) {

                $formacionacademica = new Formacionacademica();
                $formacionacademica->persona_id = $persona->id;

                $formacionacademica->nivelestudio_id = $nivelestudio_id[$contformacademica];
                $formacionacademica->anioinicio = $anioinicio[$contformacademica];
                $formacionacademica->mesinicio = $mesinicio[$contformacademica];
                $formacionacademica->situacion = $situacion[$contformacademica];

                if ($nivelestudio_id == 1) {
                    $formacionacademica->colegio_id = $colegio_id[$contformacademica];
                }
                if ($nivelestudio_id >= 2) {
                    $formacionacademica->semestre = $semestre[$contformacademica];
                    $formacionacademica->universidad_id = $universidad_id[$contformacademica];
                }
                if ($nivelestudio_id >= 3) {
                    $formacionacademica->especialidad = $especialidad[$contformacademica];
                    $formacionacademica->tipoestudio = $tipoestudio[$contformacademica];
                }
                if ($estudiandoactualmente[$contformacademica] === 'true') {
                    $formacionacademica->estudiandoactualmente = true;

                } else {
                    $formacionacademica->estudiandoactualmente = false;
                    $formacionacademica->aniotermino = $aniotermino[$contformacademica];
                    $formacionacademica->mestermino = $mestermino[$contformacademica];
                }
                $formacionacademica->save();
                $contformacademica = $contformacademica + 1;
            }

            /*Formacion complementaria*/

            $tipoformacion = $request->get('tipoformacion');
            $titulo = $request->get('titulo');
            $institucion = $request->get('institucion');
            $cantidadhoras = $request->get('cantidadhoras');
            $anioiniciocomple = $request->get('anioiniciocomple');
            $mesiniciocomple = $request->get('mesiniciocomple');
            $estudiandoactual = $request->get('estudiandoactual');
            $anioterminocomple = $request->get('anioterminocomple');
            $mesterminocomple = $request->get('mesterminocomple');
            $contformcomplementaria = 0;

            while ($contformcomplementaria < count($tipoformacion)) {

                $formacioncomplementaria = new Formacioncomplementaria();
                $formacioncomplementaria->persona_id = $persona->id;

                $formacioncomplementaria->tipoformacion = $tipoformacion[$contformcomplementaria];
                $formacioncomplementaria->titulo = $titulo[$contformcomplementaria];
                $formacioncomplementaria->institucion = $institucion[$contformcomplementaria];
                $formacioncomplementaria->cantidadhoras = $cantidadhoras[$contformcomplementaria];
                $formacioncomplementaria->anioinicio = $anioiniciocomple[$contformcomplementaria];
                $formacioncomplementaria->mesinicio = $mesiniciocomple[$contformcomplementaria];

                if ($estudiandoactual[$contformcomplementaria] === 'true') {
                    $formacioncomplementaria->estudiandoactualmente = true;

                } else {
                    $formacioncomplementaria->estudiandoactualmente = false;
                    $formacioncomplementaria->aniotermino = $anioterminocomple[$contformcomplementaria];
                    $formacioncomplementaria->mestermino = $mesterminocomple[$contformcomplementaria];
                }
                $formacioncomplementaria->save();
                $contformcomplementaria = $contformcomplementaria + 1;
            }

            /* Preferencia laboral*/
            if ($request->input('salariolaboral') != '') {

                $preferencialaboral = new Preferencialaboral();
                $preferencialaboral->persona_id = $persona->id;
                $preferencialaboral->salario = $request->input('salariolaboral');
                $preferencialaboral->moneda = $request->input('preferenciamoneda');
                $preferencialaboral->sectorempresa_id = $request->input('preferencia_sectorempresa_id');
                $preferencialaboral->arealaboral_id = $request->input('preferencia_arealaboral_id');
                $preferencialaboral->cargolaboral_id = $request->input('preferencia_cargolaboral_id');
                $preferencialaboral->departamento_id = $request->input('preferencia_departamento_id');
                $preferencialaboral->jornadalaboral_id = $request->input('jornadalaboral_id');
                $preferencialaboral->tipocontrato_id = $request->input('preferencia_tipocontrato_id');

                if ($request->input('disponibilidadextranjero')) {
                    $preferencialaboral->disponibilidadextranjero = true;
                } else {
                    $preferencialaboral->disponibilidadextranjero = false;
                }
                if ($request->input('mostrarpreferenciasalario')) {
                    $preferencialaboral->mostrarpreferenciasalario = true;
                } else {
                    $preferencialaboral->mostrarpreferenciasalario = false;
                }
                if ($request->input('mostrarcv')) {
                    $preferencialaboral->mostrarcv = true;
                } else {
                    $preferencialaboral->mostrarcv = false;
                }
                if ($request->input('disponibilidadviajar')) {
                    $preferencialaboral->disponibilidadviajar = true;
                } else {
                    $preferencialaboral->disponibilidadviajar = false;
                }
                if ($request->input('cambioresidencia')) {
                    $preferencialaboral->cambioresidencia = true;
                } else {
                    $preferencialaboral->cambioresidencia = false;
                }
                $preferencialaboral->save();
            }

            /*Anexos*/
            if ($request->hasFile('documento')) {
                $extensionanexo = $request->file('documento')->getClientOriginalExtension();
                $rutaanexo = "/public/".$request->foto->storeAs('documento', 'documento_' . $request->input('nrodocumento') . $request->input('anexo_tipodocumento_id') . '.' . $extensionanexo);
                $anexo = new Anexo();
                $anexo->persona_id = $persona->id;
                $anexo->descripcion = $request->input('anexo_descripcion');
                $anexo->tipodocumento_id = $request->input('anexo_tipodocumento_id');
                $anexo->direccionarchivo = $rutaanexo;
                $anexo->save();
            }

            /*Retorno*/
            if ($request->input('guardareditar') == 'SI') {
                if ($persona->id != 0) {
                    return "OK@" . $persona->id;
                }
            }

        });

        return is_null($error) ? "OK" : $error;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {

//        if (!Libreria::verificarExistenciaPermiso('change_persona')) {
//            return view('app.401');
//        }
        $existe = Libreria::verificarExistencia($id, 'persona');
        if ($existe !== true) {
            return $existe;
        }
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $persona = Persona::find($id);
        $preferencialaboral = Preferencialaboral::where('persona_id', $id)->first();
        $entidad = 'Persona';

        /*inicio*/

        $distrito = Distrito::where('id', '=', $persona->distrito_id)->first();

        if ($distrito !== null) {
            $provincia = Provincia::where('id', '=', $distrito->provincia_id)->first();
        }
        if (isset($provincia) && $provincia !== null) {
            $departamento = Departamento::where('id', '=', $provincia->departamento_id)->first();
            $cboDistrito = array('' => 'Seleccione') + Distrito::where('provincia_id', '=', $provincia->id)->orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        } else {
            $cboDistrito = array('' => 'Seleccione') + Distrito::orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        }

        if (isset($departamento) && $departamento !== null) {
            $cboProvincia = array('' => 'Seleccione') + Provincia::where('departamento_id', '=', $departamento->id)->orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        } else {
            $cboProvincia = array('' => 'Seleccione') + Provincia::orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();

        }

        $cboDepartamento = array('' => 'Seleccione Departamento') + Departamento::orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        $cboPais = ['' => 'Seleccione'] + Pais::orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        $cboEscuela = ['' => 'Seleccione'] + Escuela::orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        $cboTipodocumento = ['' => 'Seleccione'] + Tipodocumento::orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        $cboCargolaboral = ['' => 'Seleccione'] + Cargolaboral::orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        $cboDiscapacidad = Discapacidad::orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        $cboSexo = ['' => 'Seleccione', 'M' => 'Masculino', 'F' => 'Femenino'];
        $cboEstadocivil = ['' => 'Seleccione', 'S' => 'Soltero', 'C' => 'Casado', 'V' => 'Viudo(a)'];
        $cboIdioma = ['' => 'Seleccione'] + Idioma::orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        $cboNivelidioma = ['' => 'Seleccione', 'Basico' => 'Basico', 'Intermedio' => 'Intermedio', 'Avanzado' => 'Avanzado'];

        /*tab-conocimiento informatico*/

        $boton_conoc_informatico = 'Registrar';
        $titulo_registrar_conoc_informatico = $this->titulo_editar_conoc_informatico;
        $listaconcinfor = Conocimientoinformatico::where('persona_id', '=', $id)->get();


        /*tab-formacionlaboral*/
        $boton_form_laboral = 'Registrar';
        $titulo_registrar_form_laboral = $this->titulo_editar_form_laboral;
        $listaformlaboral = Formacionlaboral::where('persona_id', '=', $id)->get();


        /*tab-formacionlaboral*/
        $boton_form_academica = 'Registrar';
        $titulo_registrar_form_academica = $this->titulo_editar_form_academica;
        $listaformacademica = Formacionacademica::where('persona_id', '=', $id)->get();


        /*tab-formacion complementaria*/
        $boton_form_complementaria = 'Registrar';
        $titulo_registrar_form_complementaria = $this->titulo_editar_form_complementaria;
        $listaformcomplementaria = Formacioncomplementaria::where('persona_id', '=', $id)->get();

        /*tab-preferencia laboral*/
        $cboJornadalaboral = ['' => 'Seleccione'] + Jornadalaboral::orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        $cboMoneda = ['' => 'Seleccione', 'nuevosol' => 'Nuevo Sol', "dolar" => "Dólar"];
        $cboTipocontrato = ['' => 'Seleccione'] + Tipocontrato::orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        $cboSectorempresa = ['' => 'Seleccione'] + Sectorempresa::orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        $cboArealaboral = ['' => 'Seleccione'] + Arealaboral::orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();

        $ruta = $this->rutas;

        /*fin*/

        $formData = array('persona.update', $id);
        $formData = array('route' => $formData, 'method' => 'PUT', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off', 'files' => true);
        $boton = 'Modificar';
        $botoneditar = 'Guardar y continuar Modificando';

        return view($this->folderview . '.mant')->with(compact('persona', 'preferencialaboral', 'formData', 'entidad', 'boton', 'listar', 'botoneditar', 'ruta', 'cboDepartamento', 'cboProvincia', 'cboDistrito', 'cboPais', 'cboEscuela', 'cboTipodocumento', 'cboCargolaboral', 'cboDiscapacidad', 'cboSexo', 'cboEstadocivil', 'cboIdioma', 'cboNivelidioma', 'distrito', 'provincia', 'departamento', 'boton_conoc_informatico', 'titulo_registrar_conoc_informatico', 'listaconcinfor', 'boton_form_laboral', 'titulo_registrar_form_laboral', 'listaformlaboral', 'boton_form_academica', 'titulo_registrar_form_academica', 'listaformacademica', 'boton_form_complementaria', 'titulo_registrar_form_complementaria', 'listaformcomplementaria', 'cboJornadalaboral', 'cboMoneda', 'cboTipocontrato', 'cboSectorempresa', 'cboArealaboral'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        if (!Libreria::verificarExistenciaPermiso('change_persona')) {
//            return view('app.401');
//        }
        $existe = Libreria::verificarExistencia($id, 'persona');
        if ($existe !== true) {
            return $existe;
        }
//        $request->merge(array_map('trim', $request->all()));
        $mensajes = array(
            'codigouniversitario.required' => 'Debe ingresar código universitario',
            'nombres.required' => 'Debe ingresar nombres completos',
            'apellidopaterno.required' => 'Debe ingresar apellido paterno',
            'apellidomaterno.required' => 'Debe ingresar apellido materno',
            'tipodocumento_id.required' => 'Debe seleccionar un tipo documento',
            'nrodocumento.required' => 'Debe ingresar número de documento',
            'fechanacimiento.required' => 'Debe ingresar fecha nacimiento',
            'estadocivil.required' => 'Debe seleccionar un estado civil',
            'sexo.required' => 'Debe seleccionar su género',
            'email.required' => 'Debe ingresar email',
            'telefono.required' => 'Debe ingresar teléfono',
            'celular.required' => 'Debe ingresar celular',
            'pais_id.required' => 'Debe seleccionar su país',
            'departamento_id.required' => 'Debe seleccionar un departamento',
            'provincia_id.required' => 'Debe seleccionar una provincia',
            'distrito_id.required' => 'Debe seleccionar un distrito',
            'escuela_id.required' => 'Debe seleccionar una escuela',
            'direccion.required' => 'Debe ingresar su dirección',
            'descripcionpersona.required' => 'Debe ingresar su descripción',
            'aceptoterminos.required' => 'Debe aceptar los términos ',
//            'jornadalaboral_id.required' => 'Debe seleccionar una jornadalaboral ',
//            'salariolaboral.required' => 'Debe ingresar salario',
//            'moneda.required' => 'Debe seleccionar una moneda ',
//            'preferencia_sectorempresa_id.required' => 'Debe seleccionar sector laboral',
//            'preferencia_arealaboral_id.required' => 'Debe seleccionar área laboral',
//            'preferencia_cargolaboral_id.required' => 'Debe seleccionar cargo laboral',
//            'preferencia_tipocontrato_id.required' => 'Debe seleccionar tipo contrato',
//            'preferencia_departamento_id.required' => 'Debe seleccionar preferencia departamento',
        );

        $validacion = Validator::make($request->all(),
            array(
                'codigouniversitario' => [
                    'required',
                    Rule::unique('persona')->ignore($id),
                ],
                'nrodocumento' => [
                    'required',
                    Rule::unique('persona')->ignore($id),
                ],
                'email' => [
                    'required',
                    Rule::unique('persona')->ignore($id),
                ],
                'nombres' => 'required',
                'apellidopaterno' => 'required',
                'apellidomaterno' => 'required',
                'tipodocumento_id' => 'required|integer|exists:tipodocumento,id',
                'fechanacimiento' => 'required',
                'estadocivil' => 'required',
                'sexo' => 'required',
                'email' => 'required|email',
                'telefono' => 'required|integer',
                'celular' => 'required|integer',
                'pais_id' => 'required|integer|exists:pais,id',
                'departamento_id' => 'required|integer|exists:departamento,id',
                'provincia_id' => 'required:departamento_id|integer|exists:provincia,id',
                'distrito_id' => 'required:provincia_id|integer|exists:distrito,id',
                'escuela_id' => 'required|integer|exists:escuela,id',
                'direccion' => 'required',
                'descripcion' => 'required',
                'aceptoterminos' => 'required',
//                'jornadalaboral_id' => 'required|integer|exists:jornadalaboral,id',
//                'salariolaboral' => 'required|integer',
//                'moneda' => 'required',
//                'preferencia_sectorempresa_id' => 'required|integer|exists:sectorempresa,id',
//                'preferencia_arealaboral_id' => 'required|integer|exists:arealaboral,id',
//                'preferencia_cargolaboral_id' => 'required|integer|exists:cargolaboral,id',
//                'preferencia_tipocontrato_id' => 'required|integer|exists:tipocontrato,id',
//                'preferencia_departamento_id' => 'required|integer|exists:departamento,id',
            ), $mensajes);

        if ($validacion->fails()) {
            return $validacion->messages()->toJson();
        }

        $error = DB::transaction(function () use ($request, $id) {

            if ($request->hasFile('foto')) {
                $extension = $request->file('foto')->getClientOriginalExtension();
                $ruta = "/public/".$request->foto->storeAs('foto', 'foto_' . $request->input('nrodocumento') . '.' . $extension);
            }

            $persona = Persona::find($id);
            $persona->codigouniversitario = $request->input('codigouniversitario');
            $persona->nombres = $request->input('nombres');
            $persona->apellidopaterno = $request->input('apellidopaterno');
            $persona->apellidomaterno = $request->input('apellidomaterno');
            $persona->tipodocumento_id = $request->input('tipodocumento_id');
            $persona->nrodocumento = $request->input('nrodocumento');
            $persona->fechanacimiento = Date::createFromFormat('d/m/Y', $request->input('fechanacimiento'))->format('Y-m-d');
            $persona->estadocivil = $request->input('estadocivil');
            $persona->sexo = $request->input('sexo');
            $persona->email = $request->input('email');
            $persona->telefono = $request->input('telefono');
            $persona->celular = $request->input('celular');
            $persona->pais_id = $request->input('pais_id');
            $persona->distrito_id = $request->input('distrito_id');
            $persona->escuela_id = $request->input('escuela_id');
            $persona->direccion = $request->input('direccion');
            $persona->descripcion = $request->input('descripcion');
//            $persona->foto = $ruta;
//            $persona->save();

            if ($request->hasFile('foto')) {
                $persona->foto = $ruta;

            }
            $persona->save();

            /*Usuario*/
            $usuario = User::where('persona_id', '=',$id)->first();
            $usuario->nombre = $request->input('nombres') . " " . $request->input('apellidopaterno') . " ". $request->input('apellidomaterno');
            $usuario->email = $request->input('email');
            $usuario->save();

            /*Discapacidades*/
            $discapacidades_asignadas = [];
            $discapacidades = $request->input('discapacidad_id');
            $discapacidad_persona = Discapacidadpersona::where('persona_id', '=', $id)->get();
            if ($discapacidad_persona->count() && count($discapacidades) > 0) {
                foreach ($discapacidad_persona as $key => $value) {

                    if (!in_array($value->discapacidad_id, $discapacidades)) {
                        DB::table('discapacidad_persona')->where('persona_id', '=', $id)->where('discapacidad_id', '=', $value->discapacidad_id)->delete();
                    } else {
                        $discapacidades_asignadas[] = $value->discapacidad_id;
                    }
                }
            }
            if (count($discapacidades) > 0) {
                foreach ($discapacidades as $clave => $valor) {
                    if (!in_array($valor, $discapacidades_asignadas)) {
                        $discapacidad = new Discapacidadpersona();
                        $discapacidad->persona_id = $id;
                        $discapacidad->discapacidad_id = $valor;
                        $discapacidad->save();
                    }
                }
            } else {
                DB::table('discapacidad_persona')->where('persona_id', '=', $id)->delete();
            }

            /*Idioma*/
            $idiomas_asignadas = [];
            $idioma_persona = Idiomapersona::where('persona_id', '=', $id)->get();
            $idiomas = $request->get('idioma_id');
            $lectura = $request->get('lectura');
            $escritura = $request->get('escritura');
            $conversacion = $request->get('conversacion');
            $nativo = $request->get('nativo');

            if ($idioma_persona->count() && count($idiomas) > 0) {
                foreach ($idioma_persona as $key => $value) {
                    if (!in_array($value->idioma_id, $idiomas)) {
                        DB::table('idioma_persona')->where('persona_id', '=', $id)->where('idioma_id', '=', $value->idioma_id)->delete();
                    } else {
                        $idiomas_asignadas[] = $value->idioma_id;
                    }
                }
            }
            if (count($idiomas) > 0) {
                foreach ($idiomas as $clave => $valor) {
                    if (!in_array($valor, $idiomas_asignadas)) {
                        $idiomapersona = new Idiomapersona();
                        $idiomapersona->persona_id = $id;
                        $idiomapersona->idioma_id = $valor;
                        if ($nativo[$clave] === true) {
                            $idiomapersona->nativo = true;
                        } else {
                            $idiomapersona->nativo = false;
                            $idiomapersona->lectura = $lectura[$clave];
                            $idiomapersona->escritura = $escritura[$clave];
                            $idiomapersona->conversacion = $conversacion[$clave];
                        }
                        $idiomapersona->save();
                    }
                }
            } else {
                DB::table('idioma_persona')->where('persona_id', '=', $id)->delete();
            }


            /*Conocimiento informatico*/

            $conocimientoinformatico_persona = Conocimientoinformatico::where('persona_id', '=', $id)->get();
            $nombre = $request->get('nombre');
            $nivel = $request->get('nivel');
            $anioexperiencia = $request->get('anioexperiencia');
            $descripcionsoftware = $request->get('descripcionsoftware');
            $contconocinfo = 0;

            if ($conocimientoinformatico_persona->count()) {
                foreach ($conocimientoinformatico_persona as $key => $value) {

                    $idconocinfor = $request->input('editar' . $value->id);
                    $existe = Libreria::verificarExistencia($idconocinfor, 'conocimientoinformatico');
                    if ($existe && $idconocinfor != '') {

                        $conocimientoinformatico = Conocimientoinformatico::find($idconocinfor);
                        if ($request->input('nombre' . $idconocinfor) != null) {
                            $conocimientoinformatico->nombre = $request->input('nombre' . $idconocinfor);
                            $conocimientoinformatico->nivel = $request->input('nivel' . $idconocinfor);
                            $conocimientoinformatico->anioexperiencia = $request->input('anioexperiencia' . $idconocinfor);
                            if ($request->input('descripcionsoftware' . $idconocinfor) != null) {
                                $conocimientoinformatico->descripcionsoftware = $request->input('descripcionsoftware' . $idconocinfor);
                            }
                            $conocimientoinformatico->save();
                        }

                    } else {
                        DB::table('conocimientoinformatico')->where('id', $value->id)->delete();

                    }
                }
            }

            while ($contconocinfo < count($nombre)) {
                $conocimientoinformatico = new Conocimientoinformatico();
                $conocimientoinformatico->persona_id = $persona->id;
                $conocimientoinformatico->nombre = $nombre[$contconocinfo];
                $conocimientoinformatico->nivel = $nivel[$contconocinfo];
                $conocimientoinformatico->anioexperiencia = $anioexperiencia[$contconocinfo];
                if ($descripcionsoftware[$contconocinfo] != null) {
                    $conocimientoinformatico->descripcionsoftware = $descripcionsoftware[$contconocinfo];
                }
                $conocimientoinformatico->save();
                $contconocinfo = $contconocinfo + 1;
            }

            /*Formación laboral*/

            $formacionlaboral_persona = Formacionlaboral::where('persona_id', '=', $id)->get();
            $fechainicio = $request->get('fechainicio');
            $fechatermino = $request->get('fechatermino');
            $nropersonalcargo = $request->get('nropersonalcargo');
            $empresa = $request->get('empresa');
            $salario = $request->get('salario');
            $moneda = $request->get('moneda');
            $descripcionlaboral = $request->get('descripcionlaboral');
            $trabajaactualmente = $request->get('trabajaactualmente');
            $mostrarsalario = $request->get('monstrarsalario');
            $tipocontrato_id = $request->get('tipocontrato_id');
            $sectorempresa_id = $request->get('sectorempresa_id');
            $arealaboral_id = $request->get('arealaboral_id');
            $cargolaboral_id = $request->get('cargolaboral_id');
            $contformlaboral = 0;

            if ($formacionlaboral_persona->count()) {
                foreach ($formacionlaboral_persona as $key => $value) {

                    $idformlaboral = $request->input('editarformlab' . $value->id);
                    $existe = Libreria::verificarExistencia($idformlaboral, 'formacionlaboral');
                    if ($existe && $idformlaboral != '') {

                        $formacionlaboral = Formacionlaboral::find($idformlaboral);

                        if ($request->input('empresa' . $idformlaboral) != null) {
                            $formacionlaboral->fechainicio = Date::createFromFormat('d/m/Y', $request->input('fechainicio' . $idformlaboral))->format('Y-m-d');

                            if ($request->input('fechatermino' . $idformlaboral) != null) {
                                $formacionlaboral->fechatermino = Date::createFromFormat('d/m/Y', $request->input('fechatermino' . $idformlaboral))->format('Y-m-d');
                            } else {
                                $formacionlaboral->fechatermino = null;
                            }
                            $formacionlaboral->nropersonalcargo = $request->input('nropersonalcargo' . $idformlaboral);
                            $formacionlaboral->empresa = $request->input('empresa' . $idformlaboral);
                            $formacionlaboral->salario = $request->input('salario' . $idformlaboral);
                            $formacionlaboral->moneda = $request->input('moneda' . $idformlaboral);
                            $formacionlaboral->tipocontrato_id = $request->input('tipocontrato_id' . $idformlaboral);
                            $formacionlaboral->sectorempresa_id = $request->input('sectorempresa_id' . $idformlaboral);
                            $formacionlaboral->arealaboral_id = $request->input('arealaboral_id' . $idformlaboral);
                            $formacionlaboral->cargolaboral_id = $request->input('cargolaboral_id' . $idformlaboral);
                            if ($request->input('trabajaactualmente' . $idformlaboral) === 'true') {
                                $formacionlaboral->trabajaactualmente = true;
                            } else {
                                $formacionlaboral->trabajaactualmente = false;
                            }

                            if ($request->input('mostrarsalario' . $idformlaboral) === 'true') {
                                $formacionlaboral->mostrarsalario = true;
                            } else {
                                $formacionlaboral->mostrarsalario = false;
                            }
                            if ($request->input('descripcionlaboral' . $idformlaboral) != null) {
                                $formacionlaboral->descripcionlaboral = $request->input('descripcionlaboral' . $idformlaboral);
                            }
                            $formacionlaboral->save();
                        }

                    } else {
                        DB::table('formacionlaboral')->where('id', $value->id)->delete();

                    }
                }
            }

            while ($contformlaboral < count($empresa)) {
                $formacionlaboral = new Formacionlaboral();
                $formacionlaboral->persona_id = $persona->id;
                $formacionlaboral->fechainicio = Date::createFromFormat('d/m/Y', $fechainicio[$contformlaboral])->format('Y-m-d');
                if ($fechatermino[$contformlaboral] != null) {
                    $formacionlaboral->fechatermino = Date::createFromFormat('d/m/Y', $fechatermino[$contformlaboral])->format('Y-m-d');
                } else {
                    $formacionlaboral->fechatermino = null;
                }
                $formacionlaboral->nropersonalcargo = $nropersonalcargo[$contformlaboral];
                $formacionlaboral->empresa = $nropersonalcargo[$contformlaboral];
                $formacionlaboral->salario = $salario[$contformlaboral];
                $formacionlaboral->moneda = $moneda[$contformlaboral];
                $formacionlaboral->tipocontrato_id = $tipocontrato_id[$contformlaboral];
                $formacionlaboral->sectorempresa_id = $sectorempresa_id[$contformlaboral];
                $formacionlaboral->arealaboral_id = $arealaboral_id[$contformlaboral];
                $formacionlaboral->cargolaboral_id = $cargolaboral_id[$contformlaboral];

                if ($trabajaactualmente[$contformlaboral] === 'true') {
                    $formacionlaboral->trabajaactualmente = true;
                } else {
                    $formacionlaboral->trabajaactualmente = false;
                }

                if ($mostrarsalario[$contformlaboral] === 'true') {
                    $formacionlaboral->mostrarsalario = true;
                } else {
                    $formacionlaboral->mostrarsalario = false;
                }

                if ($descripcionlaboral[$contformlaboral] != null) {
                    $formacionlaboral->descripcionlaboral = $descripcionlaboral[$contformlaboral];
                }
                $formacionlaboral->save();
                $contformlaboral = $contformlaboral + 1;
            }

            /*Formación academica*/

            $formacionacademica_persona = Formacionacademica::where('persona_id', '=', $id)->get();
            $nivelestudio_id = $request->get('nivelestudio_id');
            $universidad_id = $request->get('universidad_id');
            $colegio_id = $request->get('colegio_id');
            $especialidad = $request->get('especialidad');
            $tipoestudio = $request->get('tipoestudio');
            $anioinicio = $request->get('anioinicio');
            $mesinicio = $request->get('mesinicio');
            $situacion = $request->get('situacion');
            $semestre = $request->get('semestre');
            $estudiandoactualmente = $request->get('estudiandoactualmente');
            $aniotermino = $request->get('aniotermino');
            $mestermino = $request->get('mestermino');
            $contformacademica = 0;

            if ($formacionacademica_persona->count()) {
                foreach ($formacionacademica_persona as $key => $value) {

                    $idformacademica = $request->input('editarformacad' . $value->id);
                    $existe = Libreria::verificarExistencia($idformacademica, 'formacionacademica');
                    if ($existe && $idformacademica != '') {

                        $formacionacademica = Formacionacademica::find($idformacademica);

                        if ($request->input('nivelestudio_id' . $idformacademica) != null) {

                            $formacionacademica->nivelestudio_id = $request->input('nivelestudio_id' . $idformacademica);
                            $formacionacademica->anioinicio = $request->input('anioinicio' . $idformacademica);
                            $formacionacademica->mesinicio = $request->input('mesinicio' . $idformacademica);
                            $formacionacademica->situacion = $request->input('situacion' . $idformacademica);

                            if ($request->input('nivelestudio_id' . $idformacademica) == 1) {
                                $formacionacademica->colegio_id = $request->input('colegio_id' . $idformacademica);
                            } else {
                                $formacionacademica->colegio_id = null;
                            }
                            if ($request->input('nivelestudio_id' . $idformacademica) >= 2) {
                                $formacionacademica->semestre = $request->input('semestre' . $idformacademica);
                                $formacionacademica->universidad_id = $request->input('universidad_id' . $idformacademica);
                            } else {
                                $formacionacademica->semestre = null;
                                $formacionacademica->universidad_id = null;
                            }
                            if ($request->input('nivelestudio_id' . $idformacademica) >= 3) {
                                $formacionacademica->especialidad = $request->input('especialidad' . $idformacademica);
                                $formacionacademica->tipoestudio = $request->input('tipoestudio' . $idformacademica);
                            } else {
                                $formacionacademica->especialidad = null;
                                $formacionacademica->tipoestudio = null;
                            }

                            if ($request->input('estudiandoactualmente' . $idformacademica) === 'true') {
                                $formacionacademica->estudiandoactualmente = true;
                                $formacionacademica->aniotermino = null;
                                $formacionacademica->mestermino = null;

                            } else {
                                $formacionacademica->estudiandoactualmente = false;
                                $formacionacademica->aniotermino = $request->input('aniotermino' . $idformacademica);
                                $formacionacademica->mestermino = $request->input('mestermino' . $idformacademica);
                            }

                            $formacionacademica->save();
                        }

                    } else {
                        DB::table('formacionacademica')->where('id', $value->id)->delete();

                    }
                }
            }

            while ($contformacademica < count($nivelestudio_id)) {
                $formacionacademica = new Formacionacademica();
                $formacionacademica->persona_id = $persona->id;

                $formacionacademica->nivelestudio_id = $nivelestudio_id[$contformacademica];
                $formacionacademica->anioinicio = $anioinicio[$contformacademica];
                $formacionacademica->mesinicio = $mesinicio[$contformacademica];
                $formacionacademica->situacion = $situacion[$contformacademica];


                if ($nivelestudio_id[$contformacademica] == 1) {
                    $formacionacademica->colegio_id = $colegio_id[$contformacademica];
                }
                if ($nivelestudio_id[$contformacademica] >= 2) {
                    $formacionacademica->semestre = $semestre[$contformacademica];
                    $formacionacademica->universidad_id = $universidad_id[$contformacademica];
                }
                if ($nivelestudio_id[$contformacademica] >= 3) {
                    $formacionacademica->especialidad = $especialidad[$contformacademica];
                    $formacionacademica->tipoestudio = $tipoestudio[$contformacademica];
                }
                if ($estudiandoactualmente[$contformacademica] === 'true') {
                    $formacionacademica->estudiandoactualmente = true;

                } else {
                    $formacionacademica->estudiandoactualmente = false;
                    $formacionacademica->aniotermino = $aniotermino[$contformacademica];
                    $formacionacademica->mestermino = $mestermino[$contformacademica];
                }

                $formacionacademica->save();
                $contformacademica = $contformacademica + 1;
            }


            /*Formación complementaria*/

            $formacioncomplementaria_persona = Formacioncomplementaria::where('persona_id', '=', $id)->get();
            $tipoformacion = $request->get('tipoformacion');
            $titulo = $request->get('titulo');
            $institucion = $request->get('institucion');
            $cantidadhoras = $request->get('cantidadhoras');
            $anioiniciocomple = $request->get('anioiniciocomple');
            $mesiniciocomple = $request->get('mesiniciocomple');
            $estudiandoactual = $request->get('estudiandoactual');
            $anioterminocomple = $request->get('anioterminocomple');
            $mesterminocomple = $request->get('mesterminocomple');
            $contformcomplementaria = 0;

            if ($formacioncomplementaria_persona->count()) {
                foreach ($formacioncomplementaria_persona as $key => $value) {

                    $idformcomplementaria = $request->input('editarformcomplement' . $value->id);
                    $existe = Libreria::verificarExistencia($idformcomplementaria, 'formacioncomplementaria');
                    if ($existe && $idformcomplementaria != '') {
                        $formacioncomplementaria = Formacioncomplementaria::find($idformcomplementaria);

                        if ($request->input('tipoformacion' . $idformcomplementaria) != null) {

                            $formacioncomplementaria->tipoformacion = $request->input('tipoformacion' . $idformcomplementaria);
                            $formacioncomplementaria->titulo = $request->input('titulo' . $idformcomplementaria);
                            $formacioncomplementaria->institucion = $request->input('institucion' . $idformcomplementaria);
                            $formacioncomplementaria->cantidadhoras = $request->input('cantidadhoras' . $idformcomplementaria);
                            $formacioncomplementaria->anioinicio = $request->input('anioiniciocomple' . $idformcomplementaria);
                            $formacioncomplementaria->mesinicio = $request->input('mesiniciocomple' . $idformcomplementaria);

                            if ($request->input('estudiandoactual' . $idformcomplementaria) === 'true') {
                                $formacioncomplementaria->estudiandoactualmente = true;

                            } else {
                                $formacioncomplementaria->estudiandoactualmente = false;
                                $formacioncomplementaria->aniotermino = $request->input('anioterminocomple' . $idformcomplementaria);
                                $formacioncomplementaria->mestermino = $request->input('mesterminocomple' . $idformcomplementaria);
                            }
                            $formacioncomplementaria->save();
                        }

                    } else {
                        DB::table('formacioncomplementaria')->where('id', $value->id)->delete();

                    }
                }
            }

            while ($contformcomplementaria < count($tipoformacion)) {

                $formacioncomplementaria = new Formacioncomplementaria();
                $formacioncomplementaria->persona_id = $persona->id;
                $formacioncomplementaria->tipoformacion = $tipoformacion[$contformcomplementaria];
                $formacioncomplementaria->titulo = $titulo[$contformcomplementaria];
                $formacioncomplementaria->institucion = $institucion[$contformcomplementaria];
                $formacioncomplementaria->cantidadhoras = $cantidadhoras[$contformcomplementaria];
                $formacioncomplementaria->anioinicio = $anioiniciocomple[$contformcomplementaria];
                $formacioncomplementaria->mesinicio = $mesiniciocomple[$contformcomplementaria];

                if ($estudiandoactual[$contformcomplementaria] === 'true') {
                    $formacioncomplementaria->estudiandoactualmente = true;

                } else {
                    $formacioncomplementaria->estudiandoactualmente = false;
                    $formacioncomplementaria->aniotermino = $anioterminocomple[$contformcomplementaria];
                    $formacioncomplementaria->mestermino = $mesterminocomple[$contformcomplementaria];
                }
                $formacioncomplementaria->save();
                $contformcomplementaria = $contformcomplementaria + 1;
            }

            /*Preferencia laboral*/

            if ($request->input('salariolaboral') !== null) {
                $preferencialaboral = Preferencialaboral::where('persona_id', '=', $id)->first();
                if ($preferencialaboral === null) {
                    $preferencialaboral = new Preferencialaboral();
                    $preferencialaboral->persona_id = $persona->id;
                }

                $preferencialaboral->salario = $request->input('salariolaboral');
                $preferencialaboral->salario = $request->input('salariolaboral');
                $preferencialaboral->moneda = $request->input('preferenciamoneda');
                $preferencialaboral->sectorempresa_id = $request->input('preferencia_sectorempresa_id');
                $preferencialaboral->arealaboral_id = $request->input('preferencia_arealaboral_id');
                $preferencialaboral->cargolaboral_id = $request->input('preferencia_cargolaboral_id');
                $preferencialaboral->departamento_id = $request->input('preferencia_departamento_id');
                $preferencialaboral->jornadalaboral_id = $request->input('jornadalaboral_id');
                $preferencialaboral->tipocontrato_id = $request->input('preferencia_tipocontrato_id');

                if ($request->input('disponibilidadextranjero')) {
                    $preferencialaboral->disponibilidadextranjero = true;
                } else {
                    $preferencialaboral->disponibilidadextranjero = false;
                }
                if ($request->input('mostrarpreferenciasalario')) {
                    $preferencialaboral->mostrarpreferenciasalario = true;
                } else {
                    $preferencialaboral->mostrarpreferenciasalario = false;
                }
                if ($request->input('mostrarcv')) {
                    $preferencialaboral->mostrarcv = true;
                } else {
                    $preferencialaboral->mostrarcv = false;
                }
                if ($request->input('disponibilidadviajar')) {
                    $preferencialaboral->disponibilidadviajar = true;
                } else {
                    $preferencialaboral->disponibilidadviajar = false;
                }
                if ($request->input('cambioresidencia')) {
                    $preferencialaboral->cambioresidencia = true;
                } else {
                    $preferencialaboral->cambioresidencia = false;
                }
                $preferencialaboral->save();
            }

            /*Anexos*/
            if ($request->hasFile('documento')) {
                $extensionanexo = $request->file('documento')->getClientOriginalExtension();
                $rutaanexo = "/public/".$request->foto->storeAs('documento', 'documento_' . $request->input('nrodocumento') . $request->input('anexo_tipodocumento_id') . '.' . $extensionanexo);

                $anexo = Anexo::where('persona_id', '=', $id)->first();;
                $anexo->persona_id = $persona->id;
                $anexo->descripcion = $request->input('anexo_descripcion');
                $anexo->tipodocumento_id = $request->input('anexo_tipodocumento_id');
                $anexo->direccionarchivo = $rutaanexo;
                $anexo->save();
            }

        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!Libreria::verificarExistenciaPermiso('delete_persona')) {
            return view('app.401');
        }
        $existe = Libreria::verificarExistencia($id, 'persona');
        if ($existe !== true) {
            return $existe;
        }
        $error = DB::transaction(function () use ($id) {
            $persona = Persona::find($id);
            $persona->delete();

        });
        return is_null($error) ? "OK" : $error;
    }


    /**
     * Mostrar el resultado de búsquedas
     *
     * @return Response
     */
    public function buscar(Request $request)
    {
        $change = Libreria::verificarExistenciaPermiso('change_persona');
        $delete = Libreria::verificarExistenciaPermiso('delete_persona');

        if (!Libreria::verificarExistenciaPermiso('list_persona')) {
            return view('app.401');
        }

        $pagina = $request->input('page');
        $filas = $request->input('filas');
        $entidad = 'Persona';
        $querysearch = Libreria::getParam($request->input('querysearch'));
        $distrito_id = Libreria::getParam($request->input('distrito_id'));
        $pais_id = Libreria::getParam($request->input('pais_id'));
        $escuela = Libreria::getParam($request->input('escuela_id'));
        $tipodocumento_id = Libreria::getParam($request->input('tipodocumento_id'));
        $cargolaboral = Libreria::getParam($request->input('cargolaboral'));
        $resultado = Persona::listar($querysearch, $distrito_id, $pais_id, $escuela, $tipodocumento_id, $cargolaboral);
        $lista = $resultado->get();
        $cabecera = array();
        $cabecera[] = array('valor' => '#', 'numero' => '1');
        $cabecera[] = array('valor' => 'Código Universitario', 'numero' => '1');
        $cabecera[] = array('valor' => 'N° Documento', 'numero' => '1');
        $cabecera[] = array('valor' => 'Alumno', 'numero' => '1');
        $cabecera[] = array('valor' => 'Teléfono/Celular', 'numero' => '1');
        $cabecera[] = array('valor' => 'Dirección', 'numero' => '1');
        $cabecera[] = array('valor' => 'Ubigeo', 'numero' => '1');
        $cabecera[] = array('valor' => 'Escuela', 'numero' => '1');
        $cabecera[] = array('valor' => 'País', 'numero' => '1');
        $cabecera[] = array('valor' => 'Foto', 'numero' => '1');
        $cabecera[] = array('valor' => 'Operaciones', 'numero' => '2');



        $titulo_modificar = $this->tituloModificar;
        $titulo_eliminar = $this->tituloEliminar;
        $ruta = $this->rutas;
        if (count($lista) > 0) {
            $clsLibreria = new Libreria();
            $paramPaginacion = $clsLibreria->generarPaginacion($lista, $pagina, $filas, $entidad);
            $paginacion = $paramPaginacion['cadenapaginacion'];
            $inicio = $paramPaginacion['inicio'];
            $fin = $paramPaginacion['fin'];
            $paginaactual = $paramPaginacion['nuevapagina'];
            $lista = $resultado->paginate($filas);
            $request->replace(array('page' => $paginaactual));
            
            return view($this->folderview . '.list')->with(compact('lista', 'paginacion', 'change', 'delete', 'inicio', 'fin', 'entidad', 'cabecera', 'titulo_modificar', 'titulo_eliminar', 'ruta'));
        }
        return view($this->folderview . '.list')->with(compact('lista', 'entidad'));
    }

    /**
     * Función para confirmar la eliminación de un registrlo
     * @param  integer $id id del registro a intentar eliminar
     * @param  string $listarLuego consultar si luego de eliminar se listará
     * @return html              se retorna html, con la ventana de confirmar eliminar
     */
    public function eliminar($id, $listarLuego)
    {
        if (!Libreria::verificarExistenciaPermiso('delete_persona')) {
            return view('app.401');
        }

        $existe = Libreria::verificarExistencia($id, 'persona');
        if ($existe !== true) {
            return $existe;
        }
        $listar = "NO";
        if (!is_null(Libreria::obtenerParametro($listarLuego))) {
            $listar = $listarLuego;
        }
        $modelo = Persona::find($id);
        $entidad = 'Persona';
        $formData = array('route' => array('persona.destroy', $id), 'method' => 'DELETE', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Eliminar';
        return view('app.confirmarEliminar')->with(compact('modelo', 'formData', 'entidad', 'boton', 'listar'));
    }


}
