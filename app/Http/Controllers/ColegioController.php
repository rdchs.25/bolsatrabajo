<?php

namespace bolsatrabajo\Http\Controllers;

use bolsatrabajo\Colegio;
use bolsatrabajo\Departamento;
use bolsatrabajo\Distrito;
use bolsatrabajo\Librerias\Libreria;
use bolsatrabajo\Provincia;
use bolsatrabajo\Universidad;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Validator;

class ColegioController extends Controller
{
    protected $folderview = 'app.colegio';
    protected $tituloAdmin = 'Colegio';
    protected $tituloRegistrar = 'Registrar Colegio';
    protected $tituloModificar = 'Modificar Colegio';
    protected $tituloEliminar = 'Eliminar Colegio';
    protected $rutas = array('create' => 'colegio.create',
        'edit' => 'colegio.edit',
        'delete' => 'colegio.eliminar',
        'search' => 'colegio.buscar',
        'index' => 'colegio.index'
    );

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Libreria::verificarExistenciaPermiso('list_colegio')) {
            return view('app.401');
        }
        $entidad = 'Colegio';
        $title = $this->tituloAdmin;
        $titulo_registrar = $this->tituloRegistrar;
        $ruta = $this->rutas;
        return view($this->folderview . '.admin')->with(compact('entidad', 'title', 'titulo_registrar', 'ruta'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if (!Libreria::verificarExistenciaPermiso('add_colegio')) {
            return view('app.401');
        }
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $entidad = 'Colegio';
        $cboDepartamento = ['' => 'Seleccione un departamento'] + Departamento::orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        $cboProvincia = ['' => 'Seleccione provincia'];
        $cboDistrito = ['' => 'Seleccione distrito'];
        $colegio = null;
        $formData = array('colegio.store');
        $formData = array('route' => $formData, 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Registrar';
        return view($this->folderview . '.mant')->with(compact('colegio', 'formData', 'entidad', 'cboDepartamento', 'cboProvincia', 'cboDistrito', 'boton', 'listar'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!Libreria::verificarExistenciaPermiso('add_colegio')) {
            return view('app.401');
        }

        $mensajes = array(
            'departamento_id.required' => 'Debe seleccionar un departamento',
            'provincia_id.required' => 'Debe seleccionar una provincia',
            'distrito_id.required' => 'Debe seleccionar un distrito',
            'nombre.required' => 'Debe ingresar nombre del colegio',
        );

        $reglas = array(
            'nombre' => 'required|max:100|unique:colegio',
            'departamento_id' => 'required|integer|exists:departamento,id',
            'provincia_id' => 'required:departamento_id|integer|exists:provincia,id',
            'distrito_id' => 'required:provincia_id|integer|exists:distrito,id',
        );

        $validacion = Validator::make($request->all(), $reglas, $mensajes);

        if ($validacion->fails()) {
            return $validacion->messages()->toJson();
        }

        $error = DB::transaction(function () use ($request) {
            $colegio = new Colegio();
            $colegio->nombre = $request->input('nombre');
            $colegio->distrito_id = $request->input('distrito_id');
            $colegio->save();
        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {

        if (!Libreria::verificarExistenciaPermiso('change_colegio')) {
            return view('app.401');
        }
        $existe = Libreria::verificarExistencia($id, 'colegio');
        if ($existe !== true) {
            return $existe;
        }
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $colegio = Colegio::find($id);
        $entidad = 'Colegio';
        $distrito = Distrito::where('id', '=', $colegio->distrito_id)->first();
        $provincia = Provincia::where('id', '=', $distrito->provincia_id)->first();
        $departamento = Departamento::where('id', '=', $provincia->departamento_id)->first();
        $cboDepartamento = array('' => 'Seleccione') + Departamento::orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        $cboProvincia = array('' => 'Seleccione') + Provincia::where('departamento_id', '=', $departamento->id)->orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        $cboDistrito = array('' => 'Seleccione') + Distrito::where('provincia_id', '=', $provincia->id)->orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();

        $formData = array('colegio.update', $id);
        $formData = array('route' => $formData, 'method' => 'PUT', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Modificar';
        return view($this->folderview . '.mant')->with(compact('colegio', 'formData', 'entidad', 'boton', 'departamento', 'provincia', 'distrito', 'cboDepartamento', 'cboProvincia', 'cboDistrito', 'listar'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!Libreria::verificarExistenciaPermiso('change_colegio')) {
            return view('app.401');
        }
        $existe = Libreria::verificarExistencia($id, 'colegio');
        if ($existe !== true) {
            return $existe;
        }
        $request->merge(array_map('trim', $request->all()));

        $mensajes = array(
            'departamento_id.required' => 'Debe seleccionar un departamento',
            'provincia_id.required' => 'Debe seleccionar una provincia',
            'distrito_id.required' => 'Debe seleccionar un distrito',
            'nombre.required' => 'Debe ingresar nombre del colegio',
        );

        $validacion = Validator::make($request->all(),
            array(
                'nombre' => [
                    'required',
                    Rule::unique('colegio')->ignore($id),
                ],
                'departamento_id' => 'required|integer|exists:departamento,id',
                'provincia_id' => 'required:departamento_id|integer|exists:provincia,id',
                'distrito_id' => 'required:provincia_id|integer|exists:distrito,id',
            ), $mensajes);

        if ($validacion->fails()) {
            return $validacion->messages()->toJson();
        }

        $error = DB::transaction(function () use ($request, $id) {
            $colegio = Colegio::find($id);
            $colegio->nombre = $request->input('nombre');
            $colegio->distrito_id = $request->input('distrito_id');
            $colegio->save();
        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        if (!Libreria::verificarExistenciaPermiso('delete_colegio')) {
            return view('app.401');
        }
        $existe = Libreria::verificarExistencia($id, 'colegio');
        if ($existe !== true) {
            return $existe;
        }
        $error = DB::transaction(function () use ($id) {
            $colegio = Colegio::find($id);
            $colegio->delete();

        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Mostrar el resultado de búsquedas
     *
     * @return Response
     */
    public function buscar(Request $request)
    {
        $change = Libreria::verificarExistenciaPermiso('change_colegio');
        $delete = Libreria::verificarExistenciaPermiso('delete_colegio');

        if (!Libreria::verificarExistenciaPermiso('list_colegio')) {
            return view('app.401');
        }

        $pagina = $request->input('page');
        $filas = $request->input('filas');
        $entidad = 'Colegio';
        $nombre = Libreria::getParam($request->input('querysearch'));
        $resultado = Colegio::listar($nombre);
        $lista = $resultado->get();
        $cabecera = array();
        $cabecera[] = array('valor' => '#', 'numero' => '1');
        $cabecera[] = array('valor' => 'Nombre', 'numero' => '1');
        $cabecera[] = array('valor' => 'Ubigeo', 'numero' => '1');
        $cabecera[] = array('valor' => 'Operaciones', 'numero' => '3');

        $titulo_modificar = $this->tituloModificar;
        $titulo_eliminar = $this->tituloEliminar;
        $ruta = $this->rutas;
        if (count($lista) > 0) {
            $clsLibreria = new Libreria();
            $paramPaginacion = $clsLibreria->generarPaginacion($lista, $pagina, $filas, $entidad);
            $paginacion = $paramPaginacion['cadenapaginacion'];
            $inicio = $paramPaginacion['inicio'];
            $fin = $paramPaginacion['fin'];
            $paginaactual = $paramPaginacion['nuevapagina'];
            $lista = $resultado->paginate($filas);
            $request->replace(array('page' => $paginaactual));
            return view($this->folderview . '.list')->with(compact('lista', 'paginacion', 'change', 'delete', 'inicio', 'fin', 'entidad', 'cabecera', 'titulo_modificar', 'titulo_eliminar', 'ruta'));
        }
        return view($this->folderview . '.list')->with(compact('lista', 'entidad'));
    }

    /**
     * Función para confirmar la eliminación de un registrlo
     * @param  integer $id id del registro a intentar eliminar
     * @param  string $listarLuego consultar si luego de eliminar se listará
     * @return html              se retorna html, con la ventana de confirmar eliminar
     */
    public function eliminar($id, $listarLuego)
    {
        if (!Libreria::verificarExistenciaPermiso('delete_colegio')) {
            return view('app.401');
        }
        $existe = Libreria::verificarExistencia($id, 'colegio');
        if ($existe !== true) {
            return $existe;
        }
        $listar = "NO";
        if (!is_null(Libreria::obtenerParametro($listarLuego))) {
            $listar = $listarLuego;
        }
        $modelo = Colegio::find($id);
        $entidad = 'Colegio';
        $formData = array('route' => array('colegio.destroy', $id), 'method' => 'DELETE', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Eliminar';
        return view('app.confirmarEliminar')->with(compact('modelo', 'formData', 'entidad', 'boton', 'listar'));
    }
}
