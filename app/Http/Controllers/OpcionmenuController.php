<?php

namespace bolsatrabajo\Http\Controllers;

use bolsatrabajo\Categoriamenu;
use bolsatrabajo\Http\Requests;
use bolsatrabajo\Librerias\Libreria;
use bolsatrabajo\Opcionmenu;
use bolsatrabajo\Permiso;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;


class OpcionmenuController extends Controller
{
    protected $folderview = 'app.opcionmenu';
    protected $tituloAdmin = 'Opción menú';
    protected $tituloPermiso = 'opcionmenu';
    protected $tituloRegistrar = 'Registrar opción de menú';
    protected $tituloModificar = 'Modificar opción de menú';
    protected $tituloEliminar = 'Eliminar opción de menú';
    protected $rutas = array('create' => 'opcionmenu.create',
        'edit' => 'opcionmenu.edit',
        'delete' => 'opcionmenu.eliminar',
        'search' => 'opcionmenu.buscar',
        'index' => 'opcionmenu.index',
    );
    protected $permisos = array('list', 'read', 'add', 'change', 'delete');

    /**
     * Los link que puede elegir el usuario para crear una opción de menú
     * OJO Nomenclatura: "nombretabla.index"
     */
    protected $links = [
        '' => 'Seleccione un enlace',
        'opcionmenu.index' => 'Opciones de Menú',
        'categoriamenu.index' => 'Categoria de Menú',
        'tipousuario.index' => 'Tipo usuario',
        'universidad.index' => 'Universidad',
        'colegio.index' => 'Colegio',
        'empleador.index' => 'Empleador',
        'empresa.index' => 'Empresa',
        'persona.index' => 'Persona',
        'usuario.index' => 'Usuario',
        'pais.index' => 'País',
        'departamento.index' => 'Departamento',
        'provincia.index' => 'Provincia',
        'distrito.index' => 'Distrito',
        'sectorempresa.index' => 'Sector Empresa',
        'empleador.index' => 'Empleador',
        'escuela.index' => 'Escuela Profesional',
        'tipodocumento.index' => 'Tipo Documento',
        'tipodiscapacidad.index' => 'Tipo Discapacidad',
        'tipocontrato.index' => 'Tipo Contrato',
        'tipodocumentoanexo.index' => 'Tipo Documento Anexo',
        'idioma.index' => 'Idioma',
        'nivelestudio.index' => 'Nivel Estudio',
        'cargolaboral.index' => 'Cargo Laboral',
        'arealaboral.index' => 'Área Laboral',
        'jornadalaboral.index' => 'Jornada Laboral',
        'discapacidad.index' => 'Discapacidad',
        'ofertalaboral.index' => 'Oferta Laboral',
        'nivelproceso.index' => 'Nivel Proceso',
        'alertalaboral.index' => 'Alerta Laboral',
    ];


   /*public function __construct()
    {
        $this->middleware('auth');
    }*/

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        /*if (!Libreria::verificarExistenciaPermiso('list_opcionmenu')) {
            return view('app.401');
        }
        */
        $add = Libreria::verificarExistenciaPermiso('add_opcionmenu');
        $add = true;
        $entidad = 'Opcionmenu';
        $title = $this->tituloAdmin;
        $titulo_registrar = $this->tituloRegistrar;
        $ruta = $this->rutas;
        $cboCategoriamenu = ['' => 'Todos'] + Categoriamenu::pluck('nombre', 'id')->all();
        return view($this->folderview . '.admin')->with(compact('entidad', 'title', 'titulo_registrar', 'ruta', 'add', 'cboCategoriamenu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        /*if (!Libreria::verificarExistenciaPermiso('add_opcionmenu')) {
            return view('app.401');
        }*/
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $entidad = 'Opcionmenu';
        $cboCategoriamenu = ['' => 'Seleccione una categoría'] + Categoriamenu::pluck('nombre', 'id')->all();
        $cbolink = $this->links;
        $opcionmenu = null;
        $formData = array('opcionmenu.store');
        $formData = array('route' => $formData, 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Registrar';
        return view($this->folderview . '.mant')->with(compact('opcionmenu', 'formData', 'entidad', 'boton', 'cboCategoriamenu', 'cbolink', 'listar'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*if (!Libreria::verificarExistenciaPermiso('add_opcionmenu')) {
            return view('app.401');
        }*/

        $validacion = Validator::make($request->all(),
            array(
                'categoriamenu_id' => 'required|integer|exists:categoriamenu,id,deleted_at,NULL',
                'nombre' => 'required|max:60',
                'opcionmenu_id' => 'integer|exists:opcionmenu,id',
                'orden' => 'required|integer',
                'icono' => 'required',
                'link' => 'required'
            )
        );
        if ($validacion->fails()) {
            return $validacion->messages()->toJson();
        }
//        $nombre = $this->tituloAdmin;

        $error = DB::transaction(function () use ($request) {
            $opcionmenu = new Opcionmenu();
            $opcionmenu->nombre = $request->input('nombre');
            $opcionmenu->orden = $request->input('orden');
            $opcionmenu->icono = $request->input('icono');
            $opcionmenu->link = $request->input('link');
            $opcionmenu->categoriamenu_id = $request->input('categoriamenu_id');
            $opcionmenu->save();

            /*Crear los permisos*/
            $nombrepermiso = (explode('.', $request->input('link')))[0];
            foreach ($this->permisos as $clave => $valor) {
                $permiso = new Permiso();
                $permiso->nombre = 'can ' . $valor;
                $permiso->codigo = strtolower($valor . '_' . $nombrepermiso);
                $permiso->opcionmenu_id = $opcionmenu->id;
                $permiso->save();
            }


        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Display the specified resource.
     *
     * @param  \bolsatrabajo\Opcionmenu $opcionmenu
     * @return \Illuminate\Http\Response
     */
    public function show(Opcionmenu $opcionmenu)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \bolsatrabajo\Opcionmenu $opcionmenu
     * @return \Illuminate\Http\Response
     */

    public function edit($id, Request $request)
    {
        $existe = Libreria::verificarExistencia($id, 'opcionmenu');
        if ($existe !== true) {
            return $existe;
        }
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $opcionmenu = Opcionmenu::find($id);
        $entidad = 'Opcionmenu';
        $cboCategoriamenu = ['' => 'Seleccione una categoría'] + Categoriamenu::pluck('nombre', 'id')->all();
        $cbolink = $this->links;
        $formData = array('opcionmenu.update', $id);
        $formData = array('route' => $formData, 'method' => 'PUT', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Modificar';
        return view($this->folderview . '.mant')->with(compact('opcionmenu', 'formData', 'entidad', 'boton', 'cboCategoriamenu', 'cbolink', 'listar'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \bolsatrabajo\Opcionmenu $opcionmenu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $existe = Libreria::verificarExistencia($id, 'opcionmenu');
        if ($existe !== true) {
            return $existe;
        }
        $validacion = Validator::make($request->all(),
            array(
                'categoriamenu_id' => 'required|integer|exists:categoriamenu,id,deleted_at,NULL',
                'nombre' => 'required|max:60',
                'opcionmenu_id' => 'integer|exists:opcionmenu,id',
                'orden' => 'required|integer',
                'icono' => 'required',
                'link' => 'required'
            )
        );
        if ($validacion->fails()) {
            return $validacion->messages()->toJson();
        }
        $error = DB::transaction(function () use ($request, $id) {
            $opcionmenu = Opcionmenu::find($id);
            $opcionmenu->nombre = $request->input('nombre');
            $opcionmenu->orden = $request->input('orden');
            $opcionmenu->icono = $request->input('icono');
            $opcionmenu->link = $request->input('link');
            $opcionmenu->categoriamenu_id = $request->input('categoriamenu_id');
            $opcionmenu->save();
        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \bolsatrabajo\Opcionmenu $opcionmenu
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $existe = Libreria::verificarExistencia($id, 'opcionmenu');
        if ($existe !== true) {
            return $existe;
        }
        $error = DB::transaction(function () use ($id) {
            $opcionmenu = Opcionmenu::find($id);
            $opcionmenu->delete();

            /*Eliminar los permisos*/
            DB::table('permiso')->where('opcionmenu_id', '=', $id)->delete();

        });
        return is_null($error) ? "OK" : $error;
    }


    /**
     * Mostrar el resultado de búsquedas
     *
     * @return Response
     */
    public function buscar(Request $request)
    {
        $change = Libreria::verificarExistenciaPermiso('change_opcionmenu');
        $delete = Libreria::verificarExistenciaPermiso('delete_opcionmenu');
        /*Quitar al final*/
        $change = true;
        $delete = true ;
        /**/

        $pagina = $request->input('page');
        $filas = $request->input('filas');
        $entidad = 'Opcionmenu';
        $querysearch = Libreria::getParam($request->input('querysearch'));
        $categoriamenu_id = Libreria::getParam($request->input('categoriamenu_id'));
        $resultado = Opcionmenu::listar($querysearch, $categoriamenu_id);
        $lista = $resultado->get();
        $cabecera = array();
        $cabecera[] = array('valor' => '#', 'numero' => '1');
        $cabecera[] = array('valor' => 'Nombre', 'numero' => '1');
        $cabecera[] = array('valor' => 'Orden', 'numero' => '1');
        $cabecera[] = array('valor' => 'Categoria', 'numero' => '1');
        if ($change || $delete) {
            $cabecera[] = array('valor' => 'Operaciones', 'numero' => '2');
        }

        $titulo_modificar = $this->tituloModificar;
        $titulo_eliminar = $this->tituloEliminar;
        $ruta = $this->rutas;
        if (count($lista) > 0) {
            $clsLibreria = new Libreria();
            $paramPaginacion = $clsLibreria->generarPaginacion($lista, $pagina, $filas, $entidad);
            $paginacion = $paramPaginacion['cadenapaginacion'];
            $inicio = $paramPaginacion['inicio'];
            $fin = $paramPaginacion['fin'];
            $paginaactual = $paramPaginacion['nuevapagina'];
            $lista = $resultado->paginate($filas);
            $request->replace(array('page' => $paginaactual));
            return view($this->folderview . '.list')->with(compact('lista', 'paginacion', 'change', 'delete', 'inicio', 'fin', 'entidad', 'cabecera', 'titulo_modificar', 'titulo_eliminar', 'ruta'));
        }
        return view($this->folderview . '.list')->with(compact('lista', 'entidad'));
    }


    /**
     * Función para confirmar la eliminación de un registrlo
     * @param  integer $id id del registro a intentar eliminar
     * @param  string $listarLuego consultar si luego de eliminar se listará
     * @return html              se retorna html, con la ventana de confirmar eliminar
     */
    public function eliminar($id, $listarLuego)
    {
        $existe = Libreria::verificarExistencia($id, 'opcionmenu');
        if ($existe !== true) {
            return $existe;
        }
        $listar = "NO";
        if (!is_null(Libreria::obtenerParametro($listarLuego))) {
            $listar = $listarLuego;
        }
        $modelo = Opcionmenu::find($id);
        $entidad = 'Opcionmenu';
        $formData = array('route' => array('opcionmenu.destroy', $id), 'method' => 'DELETE', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Eliminar';
        return view('app.confirmarEliminar')->with(compact('modelo', 'formData', 'entidad', 'boton', 'listar'));
    }

}
