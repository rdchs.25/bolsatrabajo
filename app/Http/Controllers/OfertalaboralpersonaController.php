<?php

namespace bolsatrabajo\Http\Controllers;

use bolsatrabajo\Conocimientoinformaticorequerido;
use bolsatrabajo\Discapacidadrequerida;
use bolsatrabajo\Nivelproceso;
use bolsatrabajo\Ofertalaboral;
use bolsatrabajo\Idiomarequerido;
use bolsatrabajo\User;
use bolsatrabajo\Ofertalaboralpersona;
use Illuminate\Http\Request;
use bolsatrabajo\Librerias\Libreria;
use Illuminate\Support\Facades\DB;
use Validator;

class OfertalaboralpersonaController extends Controller
{
    protected $folderview = 'app.ofertalaboralpersona';
    protected $tituloAdmin = 'Oferta Laboral Persona';
    protected $tituloRegistrar = 'Registrar Oferta Laboral Persona';
    protected $tituloModificar = 'Postular a esta Oferta Laboral';
    protected $tituloEliminar = 'Eliminar Alerta Laboral Persona';
    protected $rutas = array('create' => 'ofertalaboralpersona.create',
        'edit' => 'ofertalaboralpersona.edit',
        'delete' => 'ofertalaboralpersona.eliminar',
        'search' => 'ofertalaboralpersona.buscar',
        'index' => 'ofertalaboralpersona.index',
    );

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function buscar(Request $request)
    {

    	$ofertaspostuladapersona  = null;
        $pagina = $request->input('page');
        $filas = $request->input('filas');
        $entidad = 'Ofertalaboralpersona';
        $name = Libreria::getParam($request->input('name'));
        
        $resultado = Ofertalaboral::listar($name,null,null,null);
        
         if(!auth()->user()->superusuario && auth()->user()->persona_id != null){
         	$resultadofertaspostulada = Ofertalaboralpersona::listar(null);
            $resultadofertaspostulada->where('persona_id',auth()->user()->persona_id);
            $listaofertaspostuladas = $resultadofertaspostulada->get();
            foreach ($listaofertaspostuladas as $key => $value) {
            	$ofertaspostuladapersona[] = $value->ofertalaboral_id;
            }

        }

        $lista = $resultado->get();
        $cabecera = array();
        $cabecera[] = array('valor' => '#', 'numero' => '1');
        $cabecera[] = array('valor' => 'Fecha', 'numero' => '1');
        $cabecera[] = array('valor' => 'Oferta Laboral', 'numero' => '1');
        $cabecera[] = array('valor' => 'Vacantes', 'numero' => '1');
        $cabecera[] = array('valor' => 'Tipo Contrato', 'numero' => '1');
        $cabecera[] = array('valor' => 'Jornada Laboral', 'numero' => '1');
        $cabecera[] = array('valor' => 'Años Experiencia', 'numero' => '1');
        $cabecera[] = array('valor' => 'Operaciones', 'numero' => '2');

        $titulo_modificar = $this->tituloModificar;
        $titulo_eliminar = $this->tituloEliminar;
        $ruta = $this->rutas;
        if (count($lista) > 0) {
            $clsLibreria = new Libreria();
            $paramPaginacion = $clsLibreria->generarPaginacion($lista, $pagina, $filas, $entidad);
            $paginacion = $paramPaginacion['cadenapaginacion'];
            $inicio = $paramPaginacion['inicio'];
            $fin = $paramPaginacion['fin'];
            $paginaactual = $paramPaginacion['nuevapagina'];
            $lista = $resultado->paginate($filas);
            $request->replace(array('page' => $paginaactual));
            return view($this->folderview . '.list')->with(compact('lista', 'paginacion', 'inicio', 'fin', 'entidad', 'cabecera','ofertaspostuladapersona', 'titulo_modificar', 'titulo_eliminar', 'ruta'));
        }
        return view($this->folderview . '.list')->with(compact('lista', 'entidad'));
    }

    public function index()
    {
        $entidad = 'Ofertalaboralpersona';
        $title = $this->tituloAdmin;
        $titulo_registrar = $this->tituloRegistrar;
        $ruta = $this->rutas;
        return view($this->folderview . '.admin')->with(compact('entidad', 'title', 'titulo_registrar', 'ruta'));
    }

    public function edit($id, Request $request)
    {
        $existe = Libreria::verificarExistencia($id, 'ofertalaboral');
        if ($existe !== true) {
            return $existe;
        }
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $ofertalaboralpersona = Ofertalaboral::find($id);
        $listaidiomarequerido=Idiomarequerido::where('ofertalaboral_id', '=',$id)
                            ->join('idioma','idioma.id','=','idioma_ofertalaboral.idioma_id')
                            ->select('idioma.nombre as idioma',
                                             'idioma_ofertalaboral.lectura as lectura',
                                             'idioma_ofertalaboral.escritura as escritura',
                                             'idioma_ofertalaboral.conversacion as conversacion'
                                )
                            ->get();
        $listasoftwarerequerido=Conocimientoinformaticorequerido::where('ofertalaboral_id', '=',$id)->get();
        $listadiscapacidadrequerida=Discapacidadrequerida::where('ofertalaboral_id', '=',$id)
            ->join('discapacidad','discapacidad.id','=','discapacidad_ofertalaboral.discapacidad_id')
            ->select('discapacidad.nombre as discapacidad')
            ->get();
        


        $entidad = 'Ofertalaboralpersona';
        $formData = array('ofertalaboralpersona.update', $id);
        $formData = array('route' => $formData, 'method' => 'PUT', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Modificar';
        return view($this->folderview . '.mant')->with(compact('ofertalaboralpersona', 'formData', 'entidad', 'boton',
                    'listaidiomarequerido','listasoftwarerequerido','listadiscapacidadrequerida',
                    'listar'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \bolsatrabajo\Departamento  $departamento
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    	$datos =null;
        $alumno  = null ;
        $existe = Libreria::verificarExistencia($id, 'ofertalaboral');
        if ($existe !== true) {
            return $existe;
        }
        $reglas = array();
        $mensajes = array();
        $validacion = Validator::make($request->all(), $reglas, $mensajes);
        if ($validacion->fails()) {
            return $validacion->messages()->toJson();
        }
        $alumno = auth()->user()->tiposusuarios->where('tipousuario_id','2');

        if(auth()->user()->superusuario || auth()->user()->empleador_id !=null || $alumno->count() == 0){
        	$datos["error"] = ["Solo pueden postular alumnos"];
        	return json_encode($datos);
        }
        if(auth()->user()->persona->codigouniversitario === null ){
			$datos["error"] = ["Debe completar sus datos personales en su perfil.(Click en su foto y luego perfil)"];
        	return json_encode($datos);
        }


        $error = DB::transaction(function () use ($request, $id) {

            $session_id = auth()->user()->id;
            $user = User::find($session_id);
            $persona_id= $user->persona_id;

            

            $nivelproceso = new Nivelproceso();
            $nivelproceso->nombre = 'Postulada';
            $nivelproceso->prioridad = 1;
            $nivelproceso->ofertalaboral_id = $id;
            $nivelproceso->save();
            $insertarnivelproceso = $nivelproceso->id;

            $ofertalaboralpersona = new Ofertalaboralpersona();
            $ofertalaboralpersona->persona_id = $persona_id;
            $ofertalaboralpersona->nivelproceso_id = $insertarnivelproceso;
            $ofertalaboralpersona->ofertalaboral_id = $id;
            $ofertalaboralpersona->save();
        });

        return is_null($error) ? "OK" : $error;
    }



}
