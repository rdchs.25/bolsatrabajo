<?php

namespace bolsatrabajo\Http\Controllers;

use bolsatrabajo\Http\Requests;
use bolsatrabajo\Librerias\Libreria;
use bolsatrabajo\Provincia;
use bolsatrabajo\Departamento;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class ProvinciaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $folderview = 'app.provincia';
    protected $tituloAdmin = 'Provincia';
    protected $tituloRegistrar = 'Registrar Provincia';
    protected $tituloModificar = 'Modificar Provincia';
    protected $tituloEliminar = 'Eliminar Provincia';
    protected $rutas = array('create' => 'provincia.create',
        'edit' => 'provincia.edit',
        'delete' => 'provincia.eliminar',
        'search' => 'provincia.buscar',
        'index' => 'provincia.index',
    );

    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    public function buscar(Request $request)
    {
        $pagina = $request->input('page');
        $filas = $request->input('filas');
        $entidad = 'Provincia';
        $name = Libreria::getParam($request->input('name'));
        $resultado = DB::table('provincia')
                    ->join('departamento', 'provincia.departamento_id', '=', 'departamento.id')
                    ->where('provincia.nombre', 'like','%'.$name.'%')
                    ->whereNull('provincia.deleted_at')
                    ->select('provincia.id as id', 'provincia.nombre as provincia_nombre', 'departamento.nombre as departamento_nombre');
//                    ->get();
//        $resultado = Provincia::listar($name);
        $lista = $resultado->get();
        $cabecera = array();
        $cabecera[] = array('valor' => '#', 'numero' => '1');
        $cabecera[] = array('valor' => 'Nombre', 'numero' => '1');
        $cabecera[] = array('valor' => 'Departamento', 'numero' => '1');
        $cabecera[] = array('valor' => 'Operaciones', 'numero' => '2');

        $titulo_modificar = $this->tituloModificar;
        $titulo_eliminar = $this->tituloEliminar;
        $ruta = $this->rutas;
        if (count($lista) > 0) {
            $clsLibreria = new Libreria();
            $paramPaginacion = $clsLibreria->generarPaginacion($lista, $pagina, $filas, $entidad);
            $paginacion = $paramPaginacion['cadenapaginacion'];
            $inicio = $paramPaginacion['inicio'];
            $fin = $paramPaginacion['fin'];
            $paginaactual = $paramPaginacion['nuevapagina'];
            $lista = $resultado->paginate($filas);
            $request->replace(array('page' => $paginaactual));
            return view($this->folderview . '.list')->with(compact('lista', 'paginacion', 'inicio', 'fin', 'entidad', 'cabecera', 'titulo_modificar', 'titulo_eliminar', 'ruta'));
        }
        return view($this->folderview . '.list')->with(compact('lista', 'entidad'));
    }

    public function index()
    {
        $entidad = 'Provincia';
        $title = $this->tituloAdmin;
        $titulo_registrar = $this->tituloRegistrar;
        $ruta = $this->rutas;
        return view($this->folderview . '.admin')->with(compact('entidad', 'title', 'titulo_registrar', 'ruta'));
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $entidad = 'Provincia';
        $cboDepartamento = ['' => 'Seleccione un Departamento'] + Departamento::pluck('nombre', 'id')->all();
        $provincia = null;
        $formData = array('provincia.store');
        $formData = array('route' => $formData, 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Registrar';
        return view($this->folderview . '.mant')->with(compact('provincia', 'formData', 'entidad', 'boton', 'cboDepartamento','listar'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $reglas = array('nombre' => 'required|max:60','departamento_id' => 'required|max:60');
        $mensajes = array();
        $validacion = Validator::make($request->all(), $reglas, $mensajes);
        if ($validacion->fails()) {
            return $validacion->messages()->toJson();
        }
        $error = DB::transaction(function () use ($request) {
            $provincia = new Provincia();
            $provincia->nombre = $request->input('nombre');
            $provincia->departamento_id = $request->input('departamento_id');
            $provincia->save();
        });
        return is_null($error) ? "OK" : $error;
    }


    /**
     * Display the specified resource.
     *
     * @param  \bolsatrabajo\Provincia  $provincia
     * @return \Illuminate\Http\Response
     */
    public function show(Provincia $provincia)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \bolsatrabajo\Provincia  $provincia
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $existe = Libreria::verificarExistencia($id, 'provincia');
        if ($existe !== true) {
            return $existe;
        }
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $cboDepartamento = array('' => 'Seleccione') + Departamento::pluck('nombre', 'id')->all();
        $provincia = Provincia::find($id);
        $entidad = 'Provincia';
        $formData = array('provincia.update', $id);
        $formData = array('route' => $formData, 'method' => 'PUT', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Modificar';
        return view($this->folderview . '.mant')->with(compact('provincia', 'formData', 'entidad', 'boton', 'listar','cboDepartamento'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \bolsatrabajo\Provincia  $provincia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $existe = Libreria::verificarExistencia($id, 'provincia');
        if ($existe !== true) {
            return $existe;
        }
        $reglas = array('nombre' => 'required|max:60','departamento_id' => 'required|max:60');
        $mensajes = array();
        $validacion = Validator::make($request->all(), $reglas, $mensajes);
        if ($validacion->fails()) {
            return $validacion->messages()->toJson();
        }
        $error = DB::transaction(function () use ($request, $id) {
            $provincia = Provincia::find($id);
            $provincia->nombre = $request->input('nombre');
            $provincia->departamento_id = $request->input('departamento_id');
            $provincia->save();
        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \bolsatrabajo\Provincia  $provincia
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $existe = Libreria::verificarExistencia($id, 'provincia');
        if ($existe !== true) {
            return $existe;
        }
        $error = DB::transaction(function () use ($id) {
            $provincia = Provincia::find($id);
            $provincia->delete();
        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Función para confirmar la eliminación de un registrlo
     * @param  integer $id id del registro a intentar eliminar
     * @param  string $listarLuego consultar si luego de eliminar se listará
     * @return html              se retorna html, con la ventana de confirmar eliminar
     */
    public function eliminar($id, $listarLuego)
    {
        $existe = Libreria::verificarExistencia($id, 'provincia');
        if ($existe !== true) {
            return $existe;
        }
        $listar = "NO";
        if (!is_null(Libreria::obtenerParametro($listarLuego))) {
            $listar = $listarLuego;
        }
        $modelo = Provincia::find($id);
        $entidad = 'Provincia';
        $formData = array('route' => array('provincia.destroy', $id), 'method' => 'DELETE', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Eliminar';
        return view('app.confirmarEliminar')->with(compact('modelo', 'formData', 'entidad', 'boton', 'listar'));
    }

    public function cboprovincia($departamento_id = null)
    {
        $existe = Libreria::verificarExistencia($departamento_id, 'departamento');
        if ($existe !== true) {
            return $existe;
        }
        $departamento = Departamento::find($departamento_id);
        $provincias = $departamento->provincias;
        if (count($provincias) > 0) {
            $cadena = '';
            foreach ($provincias as $key => $value) {
                $cadena .= '<option value="' . $value->id . '">' . $value->nombre . '</option>';
            }
            return $cadena;
        } else {
            return '';
        }
    }


}