<?php

namespace bolsatrabajo\Http\Controllers;

use bolsatrabajo\Departamento;
use bolsatrabajo\Distrito;
use bolsatrabajo\Librerias\Libreria;
use bolsatrabajo\Provincia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class DistritoController extends Controller
{

    protected $folderview = 'app.distrito';
    protected $tituloAdmin = 'Distrito';
    protected $tituloRegistrar = 'Registrar Distrito';
    protected $tituloModificar = 'Modificar Distrito';
    protected $tituloEliminar = 'Eliminar Distrito';
    protected $rutas = array('create' => 'distrito.create',
        'edit' => 'distrito.edit',
        'delete' => 'distrito.eliminar',
        'search' => 'distrito.buscar',
        'index' => 'distrito.index',
    );

    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    public function cbodistrito($provincia_id = null)
    {
        $existe = Libreria::verificarExistencia($provincia_id, 'provincia');
        if ($existe !== true) {
            return $existe;
        }
        $provincia = Provincia::find($provincia_id);
        $distritos = $provincia->distritos;
        if (count($distritos) > 0) {
            $cadena = '';
            foreach ($distritos as $key => $value) {
                $cadena .= '<option value="' . $value->id . '">' . $value->nombre . '</option>';
            }
            return $cadena;
        } else {
            return '';
        }
    }


    public function buscar(Request $request)
    {
        $pagina = $request->input('page');
        $filas = $request->input('filas');
        $entidad = 'Distrito';
        $name = Libreria::getParam($request->input('name'));
        $resultado = DB::table('distrito')
            ->join('provincia', 'provincia.id', '=', 'distrito.provincia_id')
            ->join('departamento', 'departamento.id', '=', 'provincia.departamento_id')
            ->where('distrito.nombre', 'like', '%' . $name . '%')
            ->whereNull('distrito.deleted_at')
            ->select('distrito.id as id', 'distrito.nombre as distrito_nombre', 'provincia.nombre as provincia_nombre', 'departamento.nombre as departamento_nombre');
        $lista = $resultado->get();
        $cabecera = array();
        $cabecera[] = array('valor' => '#', 'numero' => '1');
        $cabecera[] = array('valor' => 'Nombre', 'numero' => '1');
        $cabecera[] = array('valor' => 'Provincia', 'numero' => '1');
        $cabecera[] = array('valor' => 'Departamento', 'numero' => '1');
        $cabecera[] = array('valor' => 'Operaciones', 'numero' => '2');

        $titulo_modificar = $this->tituloModificar;
        $titulo_eliminar = $this->tituloEliminar;
        $ruta = $this->rutas;
        if (count($lista) > 0) {
            $clsLibreria = new Libreria();
            $paramPaginacion = $clsLibreria->generarPaginacion($lista, $pagina, $filas, $entidad);
            $paginacion = $paramPaginacion['cadenapaginacion'];
            $inicio = $paramPaginacion['inicio'];
            $fin = $paramPaginacion['fin'];
            $paginaactual = $paramPaginacion['nuevapagina'];
            $lista = $resultado->paginate($filas);
            $request->replace(array('page' => $paginaactual));
            return view($this->folderview . '.list')->with(compact('lista', 'paginacion', 'inicio', 'fin', 'entidad', 'cabecera', 'titulo_modificar', 'titulo_eliminar', 'ruta'));
        }
        return view($this->folderview . '.list')->with(compact('lista', 'entidad'));
    }

    public function index()
    {
        $entidad = 'Distrito';
        $title = $this->tituloAdmin;
        $titulo_registrar = $this->tituloRegistrar;
        $ruta = $this->rutas;
        return view($this->folderview . '.admin')->with(compact('entidad', 'title', 'titulo_registrar', 'ruta'));
    }

    public function create(Request $request)
    {
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $entidad = 'Distrito';

        $departamento = Departamento::where('nombre', '=', 'LAMBAYEQUE')->first();
        $provincia = Provincia::where('departamento_id', '=', $departamento->id)->first();
        $cboDepartamento = ['' => 'Seleccione'] + Departamento::orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        $cboProvincia = ['' => 'Seleccione'] + Provincia::where('departamento_id', '=', $departamento->id)->orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();

        $distrito = null;
        $formData = array('distrito.store');
        $formData = array('route' => $formData, 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Registrar';
        return view($this->folderview . '.mant')->with(compact('distrito', 'formData', 'entidad', 'boton', 'departamento', 'provincia', 'cboDepartamento', 'cboProvincia', 'listar'));
    }

    public function store(Request $request)
    {
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $mensajes = array(
            'departamento_id.required' => 'Debe seleccionar un departamento',
            'provincia_id.required_with' => 'Debe seleccionar una provincia',
            'nombre.required' => 'Debe ingresar un nombre'
        );
        $reglas = array(
            'departamento_id' => 'required|integer|exists:departamento,id',
            'provincia_id' => 'required_with:departamento_id|integer|exists:provincia,id',
            'nombre' => 'required|max:100'
        );

        $validacion = Validator::make($request->all(), $reglas, $mensajes);
        if ($validacion->fails()) {
            return $validacion->messages()->toJson();
        }
        $error = DB::transaction(function () use ($request) {
            $distrito = new Distrito();
            $distrito->nombre = $request->input('nombre');
            $distrito->provincia_id = $request->input('provincia_id');
            $distrito->save();
        });
        return is_null($error) ? "OK" : $error;
    }

    public function edit($id, Request $request)
    {
        $existe = Libreria::verificarExistencia($id, 'distrito');
        if ($existe !== true) {
            return $existe;
        }
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $distrito = Distrito::find($id);
        $entidad = 'Distrito';

        $provincia = Provincia::where('id', '=', $distrito->provincia_id)->first();
        $departamento = Departamento::where('id', '=', $provincia->departamento_id)->first();
        $cboDepartamento = array('' => 'Seleccione') + Departamento::orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        $cboProvincia = array('' => 'Seleccione') + Provincia::where('departamento_id', '=', $departamento->id)->orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();

        $formData = array('distrito.update', $id);
        $formData = array('route' => $formData, 'method' => 'PUT', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Modificar';
        return view($this->folderview . '.mant')->with(compact('distrito', 'formData', 'entidad', 'boton', 'departamento', 'provincia', 'cboDepartamento', 'cboProvincia', 'listar'));
    }


    public function update(Request $request, $id)
    {
        $existe = Libreria::verificarExistencia($id, 'distrito');
        if ($existe !== true) {
            return $existe;
        }
        $request->merge(array_map('trim', $request->all()));
        $mensajes = array(
            'departamento_id.required' => 'Debe seleccionar un departamento',
            'provincia_id.required_with' => 'Debe seleccionar una provincia',
            'nombre.required' => 'Debe ingresar un nombre'
        );
        $validacion = Validator::make($request->all(),
            array(
                'nombre' => 'required|max:100',
                'departamento_id' => 'required|integer|exists:departamento,id',
                'provincia_id' => 'required_with:departamento_id|integer|exists:provincia,id'
            ), $mensajes);
        if ($validacion->fails()) {
            return $validacion->messages()->toJson();
        }
        $error = DB::transaction(function () use ($request, $id) {
            $distrito = Distrito::find($id);
            $distrito->nombre = $request->input('nombre');
            $distrito->provincia_id = $request->input('provincia_id');
            $distrito->save();
        });
        return is_null($error) ? "OK" : $error;
    }

    public function destroy($id)
    {
        $existe = Libreria::verificarExistencia($id, 'distrito');
        if ($existe !== true) {
            return $existe;
        }
        $error = DB::transaction(function () use ($id) {
            $distrito = Distrito::find($id);
            $distrito->delete();
        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Función para confirmar la eliminación de un registrlo
     * @param  integer $id id del registro a intentar eliminar
     * @param  string $listarLuego consultar si luego de eliminar se listará
     * @return html              se retorna html, con la ventana de confirmar eliminar
     */
    public function eliminar($id, $listarLuego)
    {
        $existe = Libreria::verificarExistencia($id, 'distrito');
        if ($existe !== true) {
            return $existe;
        }
        $listar = "NO";
        if (!is_null(Libreria::obtenerParametro($listarLuego))) {
            $listar = $listarLuego;
        }
        $modelo = Distrito::find($id);
        $entidad = 'Distrito';
        $formData = array('route' => array('distrito.destroy', $id), 'method' => 'DELETE', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Eliminar';
        return view('app.confirmarEliminar')->with(compact('modelo', 'formData', 'entidad', 'boton', 'listar'));
    }
}
