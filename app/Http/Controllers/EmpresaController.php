<?php

namespace bolsatrabajo\Http\Controllers;

use bolsatrabajo\Departamento;
use bolsatrabajo\Distrito;
use bolsatrabajo\Empresa;
use bolsatrabajo\Librerias\Libreria;
use bolsatrabajo\Provincia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Validator;

class EmpresaController extends Controller
{

    protected $folderview = 'app.empresa';
    protected $tituloAdmin = 'Empresa';
    protected $tituloRegistrar = 'Registrar Empresa';
    protected $tituloModificar = 'Modificar Empresa';
    protected $tituloEliminar = 'Eliminar Empresa';
    protected $rutas = array('create' => 'empresa.create',
        'edit' => 'empresa.edit',
        'delete' => 'empresa.eliminar',
        'search' => 'empresa.buscar',
        'index' => 'empresa.index'
    );

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Libreria::verificarExistenciaPermiso('list_empresa')) {
            return view('app.401');
        }
        $add = Libreria::verificarExistenciaPermiso('add_empresa');
        $entidad = 'Empresa';
        $title = $this->tituloAdmin;
        $titulo_registrar = $this->tituloRegistrar;
        $ruta = $this->rutas;
        $cboDistrito = ['' => 'Todos'] + Distrito::pluck('nombre', 'id')->all();

        return view($this->folderview . '.admin')->with(compact('entidad', 'cboDistrito', 'title', 'titulo_registrar', 'add', 'ruta'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if (!Libreria::verificarExistenciaPermiso('add_empresa')) {
            return view('app.401"');
        }
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $entidad = 'Empresa';
        $cboDepartamento = ['' => 'Seleccione'] + Departamento::orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        $cboProvincia = ['' => 'Seleccione'];
        $cboDistrito = ['' => 'Seleccione'];
        $empresa = null;
        $formData = array('empresa.store');
        $formData = array('route' => $formData, 'files' => true, 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off', 'files' => true);
        $boton = 'Registrar';
        return view($this->folderview . '.mant')->with(compact('empresa', 'formData', 'entidad', 'cboDepartamento', 'cboProvincia', 'cboDistrito', 'boton', 'listar'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!Libreria::verificarExistenciaPermiso('add_empresa')) {
            return "permiso";
        }

        $mensajes = array(
            'ruc.required' => 'Debe ingresar el ruc',
            'razonsocial.required' => 'Debe ingresar razón social',
            'nombrecomercial.required' => 'Debe ingresar nombre comercial',
            'direccion.required' => 'Debe ingresar nombre comercial',
            'web.required' => 'Debe ingresar página web',
            'email.required' => 'Debe ingresar página web',
            'telefono.required' => 'Debe ingresar teléfono',
            'celular.required' => 'Debe ingresar celular',
            'departamento_id.required' => 'Debe seleccionar un departamento',
            'provincia_id.required' => 'Debe seleccionar una provincia',
            'distrito_id.required' => 'Debe seleccionar un distrito',
            'descripcion.required' => 'Debe ingresar descripción ',
            'logo.required' => 'Debe seleccionar un logo ',

        );

        $reglas = array(
            'ruc' => 'unique:empleador,ruc,NULL,id,deleted_at,NULL|unique:empresa|regex:/^[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$/',
            'razonsocial' => 'required|unique:empleador|unique:empresa',
            'nombrecomercial' => 'required|unique:empleador|unique:empresa',
            'direccion' => 'required',
            'web' => 'required',
            'email' => 'required|email|unique:empresa|unique:empleador',
            'telefono' => 'required|integer',
            'celular' => 'required|integer',
            'departamento_id' => 'required|integer|exists:departamento,id',
            'provincia_id' => 'required:departamento_id|integer|exists:provincia,id',
            'distrito_id' => 'required:provincia_id|integer|exists:distrito,id',
            'descripcion' => 'required',
            'logo' => 'required|image',

        );

        $validacion = Validator::make($request->all(), $reglas, $mensajes);

        if ($validacion->fails()) {
            return $validacion->messages()->toJson();
        }

        $error = DB::transaction(function () use ($request) {
            if ($request->hasFile('logo')) {
                $extension = $request->file('logo')->getClientOriginalExtension();
                $ruta = $request->logo->storeAs('logo', 'logo_' . $request->input('ruc') . '.' . $extension);
            }
            $empresa = new Empresa();
            $empresa->ruc = $request->input('ruc');
            $empresa->razonsocial = $request->input('razonsocial');
            $empresa->nombrecomercial = $request->input('nombrecomercial');
            $empresa->direccion = $request->input('direccion');
            $empresa->web = $request->input('web');
            $empresa->email = $request->input('email');
            $empresa->telefono = $request->input('telefono');
            $empresa->celular = $request->input('celular');
            $empresa->descripcion = $request->input('descripcion');
            $empresa->logo = $ruta;
            $empresa->distrito_id = $request->input('distrito_id');

            $empresa->save();

        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {

        if (!Libreria::verificarExistenciaPermiso('change_empresa')) {
            return view('app.401');
        }
        $existe = Libreria::verificarExistencia($id, 'empresa');
        if ($existe !== true) {
            return $existe;
        }
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $empresa = Empresa::find($id);
        $entidad = 'Empresa';
        if ($empresa->distrito_id) {
            $distrito = Distrito::where('id', '=', $empresa->distrito_id)->first();
            $provincia = Provincia::where('id', '=', $distrito->provincia_id)->first();
            $departamento = Departamento::where('id', '=', $provincia->departamento_id)->first();
            $cboDepartamento = array('' => 'Seleccione') + Departamento::orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
            $cboProvincia = array('' => 'Seleccione') + Provincia::where('departamento_id', '=', $departamento->id)->orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
            $cboDistrito = array('' => 'Seleccione') + Distrito::where('provincia_id', '=', $provincia->id)->orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        } else {
            $cboDepartamento = ['' => 'Seleccione'] + Departamento::orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
            $cboProvincia = ['' => 'Seleccione'];
            $cboDistrito = ['' => 'Seleccione'];
        }

        $formData = array('empresa.update', $id);
        $formData = array('route' => $formData, 'method' => 'PUT', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Modificar';
        return view($this->folderview . '.mant')->with(compact('empresa', 'formData', 'entidad', 'boton', 'departamento', 'provincia', 'distrito', 'cboDepartamento', 'cboProvincia', 'cboDistrito', 'listar'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!Libreria::verificarExistenciaPermiso('change_empresa')) {
            return view('app.401');
        }
        $existe = Libreria::verificarExistencia($id, 'empresa');
        if ($existe !== true) {
            return $existe;
        }
        $request->merge(array_map('trim', $request->all()));

        $mensajes = array(
            'ruc.required' => 'Debe ingresar el ruc',
            'razonsocial.required' => 'Debe ingresar razón social',
            'nombrecomercial.required' => 'Debe ingresar nombre comercial',
            'direccion.required' => 'Debe ingresar nombre comercial',
            'web.required' => 'Debe ingresar página web',
            'email.required' => 'Debe ingresar página web',
            'telefono.required' => 'Debe ingresar teléfono',
            'celular.required' => 'Debe ingresar celular',
            'departamento_id.required' => 'Debe seleccionar un departamento',
            'provincia_id.required' => 'Debe seleccionar una provincia',
            'distrito_id.required' => 'Debe seleccionar un distrito',
            'descripcion.required' => 'Debe ingresar descripción ',
        );

        $validacion = Validator::make($request->all(),
            array(
                'ruc' => [
                    'required',
                    Rule::unique('empresa')->ignore($id),
                ],
                'razonsocial' => [
                    'required',
                    Rule::unique('empresa')->ignore($id),
                ],
                'nombrecomercial' => [
                    'required',
                    Rule::unique('empresa')->ignore($id),
                ],
                'email' => [
                    'required',
                    Rule::unique('empresa')->ignore($id),
                ],
                'direccion' => 'required',
                'web' => 'required',
                'email' => 'required|email',
                'ruc' => 'regex:/^[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$/',
                'telefono' => 'required|integer',
                'celular' => 'required|integer',
                'departamento_id' => 'required|integer|exists:departamento,id',
                'provincia_id' => 'required:departamento_id|integer|exists:provincia,id',
                'distrito_id' => 'required:provincia_id|integer|exists:distrito,id',
                'descripcion' => 'required',
            ), $mensajes);

        if ($validacion->fails()) {
            return $validacion->messages()->toJson();
        }

        $error = DB::transaction(function () use ($request, $id) {
            if ($request->hasFile('logo')) {
                $extension = $request->file('logo')->getClientOriginalExtension();
                $ruta = $request->logo->storeAs('logo', 'logo_' . $request->input('ruc') . '.' . $extension);
            }
            $empresa = Empresa::find($id);
            $empresa->ruc = $request->input('ruc');
            $empresa->razonsocial = $request->input('razonsocial');
            $empresa->nombrecomercial = $request->input('nombrecomercial');
            $empresa->direccion = $request->input('direccion');
            $empresa->web = $request->input('web');
            $empresa->email = $request->input('email');
            $empresa->telefono = $request->input('telefono');
            $empresa->celular = $request->input('celular');
            $empresa->descripcion = $request->input('descripcion');
            $empresa->distrito_id = $request->input('distrito_id');
            if ($request->hasFile('logo')) {
                $empresa->logo = $ruta;

            }
            $empresa->save();
        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!Libreria::verificarExistenciaPermiso('delete_empresa')) {
            return view('app.401');
        }
        $existe = Libreria::verificarExistencia($id, 'empresa');
        if ($existe !== true) {
            return $existe;
        }
        $error = DB::transaction(function () use ($id) {
            $empresa = Empresa::find($id);
            $empresa->delete();

        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Mostrar el resultado de búsquedas
     *
     * @return Response
     */
    public function buscar(Request $request)
    {
        $change = Libreria::verificarExistenciaPermiso('change_empresa');
        $delete = Libreria::verificarExistenciaPermiso('delete_empresa');

        if (!Libreria::verificarExistenciaPermiso('list_empresa')) {
            return view('app.401');
        }

        $pagina = $request->input('page');
        $filas = $request->input('filas');
        $entidad = 'Empresa';
        $querysearch = Libreria::getParam($request->input('querysearch'));
        $distrito_id = Libreria::getParam($request->input('distrito_id'));
        $resultado = Empresa::listar($querysearch, $distrito_id);
        $lista = $resultado->get();
        $cabecera = array();
        $cabecera[] = array('valor' => '#', 'numero' => '1');
        $cabecera[] = array('valor' => 'RUC', 'numero' => '1');
        $cabecera[] = array('valor' => 'Razón Social', 'numero' => '1');
        $cabecera[] = array('valor' => 'Teléfono/Celular', 'numero' => '1');
        $cabecera[] = array('valor' => 'Dirección', 'numero' => '1');
        $cabecera[] = array('valor' => 'Ubigeo', 'numero' => '1');
        $cabecera[] = array('valor' => 'Logo', 'numero' => '1');
        $cabecera[] = array('valor' => 'Operaciones', 'numero' => '2');

        $titulo_modificar = $this->tituloModificar;
        $titulo_eliminar = $this->tituloEliminar;
        $ruta = $this->rutas;
        if (count($lista) > 0) {
            $clsLibreria = new Libreria();
            $paramPaginacion = $clsLibreria->generarPaginacion($lista, $pagina, $filas, $entidad);
            $paginacion = $paramPaginacion['cadenapaginacion'];
            $inicio = $paramPaginacion['inicio'];
            $fin = $paramPaginacion['fin'];
            $paginaactual = $paramPaginacion['nuevapagina'];
            $lista = $resultado->paginate($filas);
            $request->replace(array('page' => $paginaactual));
            return view($this->folderview . '.list')->with(compact('lista', 'paginacion', 'change', 'delete', 'inicio', 'fin', 'entidad', 'cabecera', 'titulo_modificar', 'titulo_eliminar', 'ruta'));
        }
        return view($this->folderview . '.list')->with(compact('lista', 'entidad'));
    }

    /**
     * Función para confirmar la eliminación de un registrlo
     * @param  integer $id id del registro a intentar eliminar
     * @param  string $listarLuego consultar si luego de eliminar se listará
     * @return html              se retorna html, con la ventana de confirmar eliminar
     */
    public function eliminar($id, $listarLuego)
    {
        if (!Libreria::verificarExistenciaPermiso('delete_empresa')) {
            return view('app.401');
        }

        $existe = Libreria::verificarExistencia($id, 'empresa');
        if ($existe !== true) {
            return $existe;
        }
        $listar = "NO";
        if (!is_null(Libreria::obtenerParametro($listarLuego))) {
            $listar = $listarLuego;
        }
        $modelo = Empresa::find($id);
        $entidad = 'Empresa';
        $formData = array('route' => array('empresa.destroy', $id), 'method' => 'DELETE', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Eliminar';
        return view('app.confirmarEliminar')->with(compact('modelo', 'formData', 'entidad', 'boton', 'listar'));
    }


}
