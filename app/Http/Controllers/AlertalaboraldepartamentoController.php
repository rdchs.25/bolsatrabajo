<?php

namespace bolsatrabajo\Http\Controllers;

use bolsatrabajo\Alertalaboraldepartamento;
use bolsatrabajo\Alertalaboral;
use bolsatrabajo\Departamento;
use Illuminate\Http\Request;
use bolsatrabajo\Librerias\Libreria;
use Illuminate\Support\Facades\DB;
use Validator;

class AlertalaboraldepartamentoController extends Controller
{
    protected $folderview = 'app.alertalaboraldepartamento';
    protected $tituloAdmin = 'Alerta Laboral Departamento';
    protected $tituloRegistrar = 'Registrar Alerta Laboral Departamento';
    protected $tituloModificar = 'Modificar Alerta Laboral Departamento';
    protected $tituloEliminar = 'Eliminar Alerta Laboral Departamento';
    protected $rutas = array('create' => 'alertalaboraldepartamento.create',
        'edit' => 'alertalaboraldepartamento.edit',
        'delete' => 'alertalaboraldepartamento.eliminar',
        'search' => 'alertalaboraldepartamento.buscar',
        'index' => 'alertalaboraldepartamento.index',
    );

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function buscar(Request $request)
    {
        $pagina = $request->input('page');
        $filas = $request->input('filas');
        $entidad = 'Alertalaboraldepartamento';
        $name = Libreria::getParam($request->input('name'));
        $resultado = DB::table('alertalaboraldepartamento')
            ->join('alertalaboral', 'alertalaboral.id', '=', 'alertalaboraldepartamento.alertalaboral_id')
            ->join('departamento', 'departamento.id', '=', 'alertalaboraldepartamento.departamento_id')
            ->where('departamento.nombre', 'like','%'.$name.'%')
            ->whereNull('alertalaboraldepartamento.deleted_at')
            ->select('alertalaboral.id as alertalaboral_id',
                'departamento.nombre as departamento_nombre',
                'alertalaboraldepartamento.id as id'
//                     'persona.nrodocumento as persona_nrodocumento',
//                     'persona.nombre as persona_nombre',
//                     'persona.apellidopaterno as persona_apellidopaterno',
//                     'persona.apellidomaterno as persona_apellidomaterno'
            );
//        $resultado = Nivelproceso::listar($name);
        $lista = $resultado->get();
        $cabecera = array();
        $cabecera[] = array('valor' => '#', 'numero' => '1');
        $cabecera[] = array('valor' => 'ID Alerta', 'numero' => '1');
        $cabecera[] = array('valor' => 'Departamento', 'numero' => '1');
        $cabecera[] = array('valor' => 'Operaciones', 'numero' => '2');

        $titulo_modificar = $this->tituloModificar;
        $titulo_eliminar = $this->tituloEliminar;
        $ruta = $this->rutas;
        if (count($lista) > 0) {
            $clsLibreria = new Libreria();
            $paramPaginacion = $clsLibreria->generarPaginacion($lista, $pagina, $filas, $entidad);
            $paginacion = $paramPaginacion['cadenapaginacion'];
            $inicio = $paramPaginacion['inicio'];
            $fin = $paramPaginacion['fin'];
            $paginaactual = $paramPaginacion['nuevapagina'];
            $lista = $resultado->paginate($filas);
            $request->replace(array('page' => $paginaactual));
            return view($this->folderview . '.list')->with(compact('lista', 'paginacion', 'inicio', 'fin', 'entidad', 'cabecera', 'titulo_modificar', 'titulo_eliminar', 'ruta'));
        }
        return view($this->folderview . '.list')->with(compact('lista', 'entidad'));
    }

    public function index()
    {
        $entidad = 'Alertalaboraldepartamento';
        $title = $this->tituloAdmin;
        $titulo_registrar = $this->tituloRegistrar;
        $ruta = $this->rutas;
        return view($this->folderview . '.admin')->with(compact('entidad', 'title', 'titulo_registrar', 'ruta'));
    }

    public function create(Request $request)
    {
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $entidad = 'Alertalaboraldepartamento';
        $cboAlertalaboral = ['' => 'Seleccione una Alerta Laboral'] + Alertalaboral::pluck('id', 'id')->all();
        $cboDepartamento = ['' => 'Seleccione un Departamento'] + Departamento::pluck('nombre', 'id')->all();
        $alertalaboraldepartamento = null;
        $formData = array('alertalaboraldepartamento.store');
        $formData = array('route' => $formData, 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Registrar';
        return view($this->folderview . '.mant')->with(compact('alertalaboraldepartamento', 'formData', 'entidad', 'boton', 'cboAlertalaboral','cboDepartamento','listar'));
    }

    public function store(Request $request)
    {
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $mensajes = array(
            'alertalaboral_id.required' => 'Debe seleccionar una Alerta Laboral',
            'departamento_id.required' => 'Debe seleccionar un Departamento',
        );
        $reglas = array(
            'alertalaboral_id' => 'required',
            'departamento_id' => 'required',
        );

        $validacion = Validator::make($request->all(), $reglas, $mensajes);
        if ($validacion->fails()) {
            return $validacion->messages()->toJson();
        }
        $error = DB::transaction(function () use ($request) {

            $alertalaboraldepartamento= new Alertalaboraldepartamento();
            $alertalaboraldepartamento->alertalaboral_id = $request->input('alertalaboral_id');
            $alertalaboraldepartamento->departamento_id = $request->input('departamento_id');
            $alertalaboraldepartamento->save();

        });
        return is_null($error) ? "OK" : $error;
    }

    public function edit($id, Request $request)
    {
        $existe = Libreria::verificarExistencia($id, 'alertalaboraldepartamento');
        if ($existe !== true) {
            return $existe;
        }
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $cboAlertalaboral = ['' => 'Seleccione una Alerta Laboral'] + Alertalaboral::pluck('id', 'id')->all();
        $cboDepartamento = ['' => 'Seleccione un Departamento'] + Departamento::pluck('nombre', 'id')->all();
        $alertalaboraldepartamento = Alertalaboraldepartamento::find($id);
        $entidad = 'Alertalaboraldepartamento';
        $formData = array('alertalaboraldepartamento.update', $id);
        $formData = array('route' => $formData, 'method' => 'PUT', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Modificar';
        return view($this->folderview . '.mant')->with(compact('alertalaboraldepartamento', 'formData', 'entidad', 'boton','cboAlertalaboral','cboDepartamento','listar'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \bolsatrabajo\Provincia  $provincia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $existe = Libreria::verificarExistencia($id, 'alertalaboraldepartamento');
        if ($existe !== true) {
            return $existe;
        }
        $reglas = array('alertalaboral_id' => 'required','departamento_id' => 'required');
        $mensajes = array();
        $validacion = Validator::make($request->all(), $reglas, $mensajes);
        if ($validacion->fails()) {
            return $validacion->messages()->toJson();
        }
        $error = DB::transaction(function () use ($request, $id) {
            $alertalaboraldepartamento = Alertalaboraldepartamento::find($id);
            $alertalaboraldepartamento->alertalaboral_id = $request->input('alertalaboral_id');
            $alertalaboraldepartamento->departamento_id = $request->input('departamento_id');
            $alertalaboraldepartamento->save();
        });
        return is_null($error) ? "OK" : $error;
    }

    public function destroy($id)
    {
        $existe = Libreria::verificarExistencia($id, 'alertalaboraldepartamento');
        if ($existe !== true) {
            return $existe;
        }
        $error = DB::transaction(function () use ($id) {
            $alertalaboraldepartamento = Alertalaboraldepartamento::find($id);
            $alertalaboraldepartamento->delete();
        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Función para confirmar la eliminación de un registrlo
     * @param  integer $id id del registro a intentar eliminar
     * @param  string $listarLuego consultar si luego de eliminar se listará
     * @return html              se retorna html, con la ventana de confirmar eliminar
     */
    public function eliminar($id, $listarLuego)
    {
        $existe = Libreria::verificarExistencia($id, 'alertalaboraldepartamento');
        if ($existe !== true) {
            return $existe;
        }
        $listar = "NO";
        if (!is_null(Libreria::obtenerParametro($listarLuego))) {
            $listar = $listarLuego;
        }
        $modelo = Alertalaboraldepartamento::find($id);
        $entidad = 'Alertalaboraldepartamento';
        $formData = array('route' => array('alertalaboraldepartamento.destroy', $id), 'method' => 'DELETE', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Eliminar';
        return view('app.confirmarEliminar')->with(compact('modelo', 'formData', 'entidad', 'boton', 'listar'));
    }


}
