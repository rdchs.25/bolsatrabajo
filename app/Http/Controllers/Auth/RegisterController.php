<?php

namespace bolsatrabajo\Http\Controllers\Auth;

use bolsatrabajo\Http\Controllers\Controller;
use bolsatrabajo\Persona;
use bolsatrabajo\Tipousuariousuario;
use bolsatrabajo\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/mensajeregister';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nombre' => 'required|string|max:255|unique:usuario',
            'email' => 'required|string|max:255',
            'usuario' => 'required|string|unique:usuario',
            'password' => 'required|string|min:6|confirmed',
            'termacept' => 'required',
            'g-recaptcha-response' => 'required|recaptcha',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return \bolsatrabajo\User
     */
    protected function create(array $data)
    {
        $persona = new Persona();
        $persona->nombres = $data['nombre'];
        $persona->save();
        $user = User::create([
            'nombre' => $data['nombre'],
            'email' => $data['email'],
            'usuario' => $data['usuario'],
            'password' => bcrypt($data['password']),
            'termacept' => $data['termacept'],
            'persona_id' => $persona->id,
        ]);

        /*creacion tipo usuario*/
        $tipousuario_usuario = new Tipousuariousuario();
        $tipousuario_usuario->usuario_id = $user->id;
        $tipousuario_usuario->tipousuario_id = 2;
        $tipousuario_usuario->save();
        return $user;
    }
}
