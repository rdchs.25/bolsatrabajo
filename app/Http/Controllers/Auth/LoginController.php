<?php

namespace bolsatrabajo\Http\Controllers\Auth;

use bolsatrabajo\Http\Controllers\Controller;
use bolsatrabajo\Tipousuario;
use bolsatrabajo\Tipousuariousuario;
use Illuminate\Support\Facades\Auth;
use Session;

class LoginController extends Controller
{


    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        $cboTipousuario = ['' => 'Seleccione un usuario'] + Tipousuario::Where('mostrar', true)->pluck('nombre', 'id')->all();
        return view('auth.login')->with(compact('cboTipousuario'));
    }

    public function login()
    {
        $remember = false;
        $this->validate(request(), [
            'usuario' => 'required|string',
            'password' => 'required|string',
            'tipousuario_id' => 'integer|exists:tipousuario,id',
        ]);

        if (request()->remember) {
            $remember = true;
        }
        if (Auth::attempt(['usuario' => request()->usuario, 'password' => request()->password, 'deleted_at' => null, 'estado' => 'Activado'], $remember)) {
            if (Auth::user()->superusuario) {
                session(['tipousuario_id' => 0]);
                return redirect('/home');
            }

            session(['tipousuario_id' => request()->tipousuario_id]);
            $dato = Tipousuariousuario::Where('tipousuario_id', '=', request()->tipousuario_id)->where('usuario_id', '=', Auth::user()->id)->get();
            if ($dato->count() > 0) {
                return redirect('/home');
            } else {
                Session::flush();
                Auth::logout();
            }
        }

        return redirect(route('login'))->withErrors(['failed' => trans('auth.failed')])->withInput(request(['usuario']));


    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {
        Session::flush();
        Auth::logout();
        return redirect('/');
    }
}
