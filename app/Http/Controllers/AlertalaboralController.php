<?php

namespace bolsatrabajo\Http\Controllers;

use bolsatrabajo\Alertalaboral;
use bolsatrabajo\Arealaboral;
use bolsatrabajo\Cargolaboral;
use bolsatrabajo\Departamento;
use bolsatrabajo\Escuela;
use bolsatrabajo\Alertalaboralcargo;
use bolsatrabajo\Alertalaboraldepartamento;
use bolsatrabajo\Librerias\Libreria;
use bolsatrabajo\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class AlertalaboralController extends Controller
{
    protected $folderview = 'app.alertalaboral';
    protected $tituloAdmin = 'Alerta Laboral';
    protected $tituloRegistrar = 'Registrar Alerta Laboral';
    protected $tituloModificar = 'Modificar Alerta Laboral';
    protected $tituloEliminar = 'Eliminar Alerta Laboral';
    protected $rutas = array('create' => 'alertalaboral.create',
        'edit' => 'alertalaboral.edit',
        'delete' => 'alertalaboral.eliminar',
        'search' => 'alertalaboral.buscar',
        'index' => 'alertalaboral.index',
    );

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function buscar(Request $request)
    {
        $pagina = $request->input('page');
        $filas = $request->input('filas');
        $entidad = 'Alertalaboral';
        $name = Libreria::getParam($request->input('name'));
        $resultado = Alertalaboral::listar($name);
        if(!auth()->user()->superusuario){
            $resultado->where('persona_id',auth()->user()->persona_id);
        }
        $lista = $resultado->get();
        $cabecera = array();
        $cabecera[] = array('valor' => '#', 'numero' => '1');
        $cabecera[] = array('valor' => 'Escuela', 'numero' => '1');
        $cabecera[] = array('valor' => 'Área Laboral', 'numero' => '1');
        $cabecera[] = array('valor' => 'Cargo Laboral', 'numero' => '1');
        $cabecera[] = array('valor' => 'Departamento', 'numero' => '1');
        if(auth()->user()->superusuario){
            $cabecera[] = array('valor' => 'Persona', 'numero' => '1');
        }
        $cabecera[] = array('valor' => 'Operaciones', 'numero' => '2');


        $titulo_modificar = $this->tituloModificar;
        $titulo_eliminar = $this->tituloEliminar;
        $ruta = $this->rutas;
        if (count($lista) > 0) {
            $clsLibreria = new Libreria();
            $paramPaginacion = $clsLibreria->generarPaginacion($lista, $pagina, $filas, $entidad);
            $paginacion = $paramPaginacion['cadenapaginacion'];
            $inicio = $paramPaginacion['inicio'];
            $fin = $paramPaginacion['fin'];
            $paginaactual = $paramPaginacion['nuevapagina'];
            $lista = $resultado->paginate($filas);
            $request->replace(array('page' => $paginaactual));
            return view($this->folderview . '.list')->with(compact('lista', 'paginacion', 'inicio', 'fin', 'entidad', 'cabecera', 'titulo_modificar', 'titulo_eliminar', 'ruta'));
        }
        return view($this->folderview . '.list')->with(compact('lista', 'entidad'));
    }

    public function index()
    {
        $entidad = 'Alertalaboral';
        $title = $this->tituloAdmin;
        $titulo_registrar = $this->tituloRegistrar;
        $ruta = $this->rutas;
        return view($this->folderview . '.admin')->with(compact('entidad', 'title', 'titulo_registrar', 'ruta'));
    }

    public function create(Request $request)
    {
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $entidad = 'Alertalaboral';
        $cboArealaboral = ['' => 'Seleccione un Área Laboral'] + Arealaboral::pluck('nombre', 'id')->all();
        $cboEscuela = ['' => 'Seleccione una Escuela'] + Escuela::pluck('nombre', 'id')->all();
        $cboDepartamento =  Departamento::pluck('nombre', 'id')->all();
        $cboCargolaboral = Cargolaboral::pluck('nombre', 'id')->all();
        $alertalaboral = null;
        $formData = array('alertalaboral.store');
        $formData = array('route' => $formData, 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Registrar';
        return view($this->folderview . '.mant')->with(compact('alertalaboral', 'formData', 'entidad', 'boton', 'cboArealaboral', 'cboEscuela', 'cboCargolaboral','cboDepartamento','listar'));
    }

    public function store(Request $request)
    {
        $listar = Libreria::getParam($request->input('listar'), 'NO');
       
        $mensajes = array(
            'arealaboral_id.required' => 'Debe seleccionar un Área Laboral',
            'escuela_id.required' => 'Debe seleccionar una Escuela',
            'cargolaboral_id.required' => 'Debe seleccionar un Cargo Laboral',
        );

        $reglas = array(
            'arealaboral_id' => 'required', 
            'escuela_id' => 'required', 
            'cargolaboral_id' => 'required'
        );

        $validacion = Validator::make($request->all(), $reglas, $mensajes);
        if ($validacion->fails()) {
            return $validacion->messages()->toJson();
        }
        $error = DB::transaction(function () use ($request) {

            $alertalaboral = new Alertalaboral();

            $session_id = auth()->user()->id;
            $user = User::find($session_id);
            $persona_id = $user->persona_id;

            $alertalaboral->arealaboral_id = $request->input('arealaboral_id');
            $alertalaboral->escuela_id = $request->input('escuela_id');
            $alertalaboral->persona_id = $persona_id;
            $alertalaboral->save();

            /*insertar alerta cargo*/
            $cargoslaborales= $request->input('cargolaboral_id');
            if (count($cargoslaborales) > 0) {
                foreach ($cargoslaborales as $clave => $valor) {
                    $alertalaboralcargo= new Alertalaboralcargo();
                    $alertalaboralcargo->alertalaboral_id = $alertalaboral->id;
                    $alertalaboralcargo->cargolaboral_id = $valor;
                    $alertalaboralcargo->save();
                }
            }

            /*insertar alerta departamento*/
            $departamentos= $request->input('departamento_id');
            if (count($departamentos) > 0) {
                foreach ($departamentos as $clave => $valor) {
                    $alertalaboraldepartamento= new Alertalaboraldepartamento();
                    $alertalaboraldepartamento->alertalaboral_id = $alertalaboral->id;
                    $alertalaboraldepartamento->departamento_id = $valor;
                    $alertalaboraldepartamento->save();
                }
            }

        });
        return is_null($error) ? "OK" : $error;
    }

    public function edit($id, Request $request)
    {
        $existe = Libreria::verificarExistencia($id, 'alertalaboral');
        if ($existe !== true) {
            return $existe;
        }
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $cboArealaboral = ['' => 'Seleccione un Área Laboral'] + Arealaboral::pluck('nombre', 'id')->all();
        $cboEscuela = ['' => 'Seleccione una Escuela'] + Escuela::pluck('nombre', 'id')->all();
        $cboCargolaboral = Cargolaboral::pluck('nombre', 'id')->all();
        $cboDepartamento = Departamento::pluck('nombre', 'id')->all();
        $alertalaboral = Alertalaboral::find($id);
        $entidad = 'Alertalaboral';
        $formData = array('alertalaboral.update', $id);
        $formData = array('route' => $formData, 'method' => 'PUT', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Modificar';
        return view($this->folderview . '.mant')->with(compact('alertalaboral', 'formData', 'entidad', 'boton', 'cboArealaboral', 'cboEscuela','cboCargolaboral','cboDepartamento', 'listar'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \bolsatrabajo\Provincia $provincia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $existe = Libreria::verificarExistencia($id, 'alertalaboral');
        if ($existe !== true) {
            return $existe;
        }
        $mensajes = array(
            'arealaboral_id.required' => 'Debe seleccionar un Área Laboral',
            'escuela_id.required' => 'Debe seleccionar una Escuela',
            'cargolaboral_id.required' => 'Debe seleccionar un Cargo Laboral',
        );

        $reglas = array(
            'arealaboral_id' => 'required', 
            'escuela_id' => 'required', 
            'cargolaboral_id' => 'required'
        );

        $validacion = Validator::make($request->all(), $reglas, $mensajes);
        if ($validacion->fails()) {
            return $validacion->messages()->toJson();
        }
        $error = DB::transaction(function () use ($request, $id) {
            $alertalaboral = Alertalaboral::find($id);

            $session_id = auth()->user()->id;
            $user = User::find($session_id);
            $persona_id = $user->persona_id;

            $alertalaboral->arealaboral_id = $request->input('arealaboral_id');
            $alertalaboral->escuela_id = $request->input('escuela_id');
            $alertalaboral->persona_id = $persona_id;
            $alertalaboral->save();

            /*Alerta Cargo Laboral*/

            $cargoslaborales_asignados = [];
            $cargoslaborales = $request->input('cargolaboral_id');
            $alertalaboralcargo = Alertalaboralcargo::where('alertalaboral_id', '=', $id)->get();
            if ($alertalaboralcargo->count() && count($cargoslaborales) > 0) {
                foreach ($alertalaboralcargo as $key => $value) {
                    if (!in_array($value->cargolaboral_id, $cargoslaborales)) {
                        DB::table('alertalaboralcargo')->where('alertalaboral_id', '=', $id)->where('cargolaboral_id', '=', $value->cargolaboral_id)->delete();
                    } else {
                        $cargoslaborales_asignados[] = $value->cargolaboral_id;
                    }
                }
            }
            if (count($cargoslaborales) > 0) {
                foreach ($cargoslaborales as $clave => $valor) {
                    if (!in_array($valor, $cargoslaborales_asignados)) {                        
                        $alertalaboralcargo = new Alertalaboralcargo();
                        $alertalaboralcargo->alertalaboral_id = $id;
                        $alertalaboralcargo->cargolaboral_id = $valor;
                        $alertalaboralcargo->save();
                    }
                }
            } else {
                DB::table('alertalaboralcargo')->where('alertalaboral_id', '=', $id)->delete();
            }

            /*Alerta Departamento*/

            $departamentos_asignados = [];
            $departamentos = $request->input('departamento_id');
            $alertalaboraldepartamento = Alertalaboraldepartamento::where('alertalaboral_id', '=', $id)->get();
            if ($alertalaboraldepartamento->count() && count($departamentos) > 0) {
                foreach ($alertalaboraldepartamento as $key => $value) {
                    if (!in_array($value->departamentos_id, $departamentos)) {
                        DB::table('alertalaboraldepartamento')->where('alertalaboral_id', '=', $id)->where('departamento_id', '=', $value->departamento_id)->delete();
                    } else {
                        $departamentos_asignados[] = $value->departamento_id;
                    }
                }
            }
            if (count($departamentos) > 0) {
                foreach ($departamentos as $clave => $valor) {
                    if (!in_array($valor, $departamentos_asignados)) {                        
                        $alertalaboraldepartamento = new Alertalaboraldepartamento();
                        $alertalaboraldepartamento->alertalaboral_id = $id;
                        $alertalaboraldepartamento->departamento_id = $valor;
                        $alertalaboraldepartamento->save();
                    }
                }
            } else {
                DB::table('alertalaboraldepartamento')->where('alertalaboral_id', '=', $id)->delete();
            }


        });
        return is_null($error) ? "OK" : $error;
    }

    public function destroy($id)
    {
        $existe = Libreria::verificarExistencia($id, 'alertalaboral');
        if ($existe !== true) {
            return $existe;
        }
        $error = DB::transaction(function () use ($id) {
            $alertalaboral = Alertalaboral::find($id);
            $alertalaboral->delete();
        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Función para confirmar la eliminación de un registrlo
     * @param  integer $id id del registro a intentar eliminar
     * @param  string $listarLuego consultar si luego de eliminar se listará
     * @return html              se retorna html, con la ventana de confirmar eliminar
     */
    public function eliminar($id, $listarLuego)
    {
        $existe = Libreria::verificarExistencia($id, 'alertalaboral');
        if ($existe !== true) {
            return $existe;
        }
        $listar = "NO";
        if (!is_null(Libreria::obtenerParametro($listarLuego))) {
            $listar = $listarLuego;
        }
        $modelo = Alertalaboral::find($id);
        $entidad = 'Alertalaboral';
        $formData = array('route' => array('alertalaboral.destroy', $id), 'method' => 'DELETE', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Eliminar';
        return view('app.confirmarEliminar')->with(compact('modelo', 'formData', 'entidad', 'boton', 'listar'));
    }

}

