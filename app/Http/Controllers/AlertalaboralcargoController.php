<?php

namespace bolsatrabajo\Http\Controllers;

use bolsatrabajo\Alertalaboral;
use bolsatrabajo\Alertalaboralcargo;
use bolsatrabajo\Cargolaboral;
use Illuminate\Http\Request;
use bolsatrabajo\Librerias\Libreria;
use Illuminate\Support\Facades\DB;
use Validator;

class AlertalaboralcargoController extends Controller
{
    protected $folderview = 'app.alertalaboralcargo';
    protected $tituloAdmin = 'Alerta Laboral Cargo';
    protected $tituloRegistrar = 'Registrar Alerta Laboral Cargo';
    protected $tituloModificar = 'Modificar Alerta Laboral Cargo';
    protected $tituloEliminar = 'Eliminar Alerta Laboral Cargo';
    protected $rutas = array('create' => 'alertalaboralcargo.create',
        'edit' => 'alertalaboralcargo.edit',
        'delete' => 'alertalaboralcargo.eliminar',
        'search' => 'alertalaboralcargo.buscar',
        'index' => 'alertalaboralcargo.index',
    );

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function buscar(Request $request)
    {
        $pagina = $request->input('page');
        $filas = $request->input('filas');
        $entidad = 'Alertalaboralcargo';
        $name = Libreria::getParam($request->input('name'));
        $resultado = DB::table('alertalaboralcargo')
            ->join('alertalaboral', 'alertalaboral.id', '=', 'alertalaboralcargo.alertalaboral_id')
            ->join('cargolaboral', 'cargolaboral.id', '=', 'alertalaboralcargo.cargolaboral_id')
            ->where('cargolaboral.nombre', 'like','%'.$name.'%')
            ->whereNull('alertalaboralcargo.deleted_at')
            ->select('alertalaboral.id as alertalaboral_id',
                'cargolaboral.nombre as cargolaboral_nombre',
                'alertalaboralcargo.id as id'
//                     'persona.nrodocumento as persona_nrodocumento',
//                     'persona.nombre as persona_nombre',
//                     'persona.apellidopaterno as persona_apellidopaterno',
//                     'persona.apellidomaterno as persona_apellidomaterno'
            );
//        $resultado = Nivelproceso::listar($name);
        $lista = $resultado->get();
        $cabecera = array();
        $cabecera[] = array('valor' => '#', 'numero' => '1');
        $cabecera[] = array('valor' => 'ID Alerta', 'numero' => '1');
        $cabecera[] = array('valor' => 'Cargo Laboral', 'numero' => '1');
        $cabecera[] = array('valor' => 'Operaciones', 'numero' => '2');

        $titulo_modificar = $this->tituloModificar;
        $titulo_eliminar = $this->tituloEliminar;
        $ruta = $this->rutas;
        if (count($lista) > 0) {
            $clsLibreria = new Libreria();
            $paramPaginacion = $clsLibreria->generarPaginacion($lista, $pagina, $filas, $entidad);
            $paginacion = $paramPaginacion['cadenapaginacion'];
            $inicio = $paramPaginacion['inicio'];
            $fin = $paramPaginacion['fin'];
            $paginaactual = $paramPaginacion['nuevapagina'];
            $lista = $resultado->paginate($filas);
            $request->replace(array('page' => $paginaactual));
            return view($this->folderview . '.list')->with(compact('lista', 'paginacion', 'inicio', 'fin', 'entidad', 'cabecera', 'titulo_modificar', 'titulo_eliminar', 'ruta'));
        }
        return view($this->folderview . '.list')->with(compact('lista', 'entidad'));
    }

    public function index()
    {
        $entidad = 'Alertalaboralcargo';
        $title = $this->tituloAdmin;
        $titulo_registrar = $this->tituloRegistrar;
        $ruta = $this->rutas;
        return view($this->folderview . '.admin')->with(compact('entidad', 'title', 'titulo_registrar', 'ruta'));
    }

    public function create(Request $request)
    {
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $entidad = 'Alertalaboralcargo';
        $cboAlertalaboral = ['' => 'Seleccione una Alerta Laboral'] + Alertalaboral::pluck('id', 'id')->all();
        $cboCargolaboral = ['' => 'Seleccione un Cargo'] + Cargolaboral::pluck('nombre', 'id')->all();
        $alertalaboralcargo = null;
        $formData = array('alertalaboralcargo.store');
        $formData = array('route' => $formData, 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Registrar';
        return view($this->folderview . '.mant')->with(compact('alertalaboralcargo', 'formData', 'entidad', 'boton', 'cboAlertalaboral','cboCargolaboral','listar'));
    }

    public function store(Request $request)
    {
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $mensajes = array(
            'alertalaboral_id.required' => 'Debe seleccionar una Alerta Laboral',
            'cargolaboral_id.required' => 'Debe seleccionar un Cargo',
        );
        $reglas = array(
            'alertalaboral_id' => 'required',
            'cargolaboral_id' => 'required',
        );

        $validacion = Validator::make($request->all(), $reglas, $mensajes);
        if ($validacion->fails()) {
            return $validacion->messages()->toJson();
        }
        $error = DB::transaction(function () use ($request) {

            $alertalaboralcargo= new Alertalaboralcargo();
            $alertalaboralcargo->alertalaboral_id = $request->input('alertalaboral_id');
            $alertalaboralcargo->cargolaboral_id = $request->input('cargolaboral_id');
            $alertalaboralcargo->save();

        });
        return is_null($error) ? "OK" : $error;
    }


    public function edit($id, Request $request)
    {
        $existe = Libreria::verificarExistencia($id, 'alertalaboralcargo');
        if ($existe !== true) {
            return $existe;
        }
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $cboAlertalaboral = ['' => 'Seleccione una Alerta Laboral'] + Alertalaboral::pluck('id', 'id')->all();
        $cboCargolaboral = ['' => 'Seleccione un Cargo'] + Cargolaboral::pluck('nombre', 'id')->all();
        $alertalaboralcargo = Alertalaboralcargo::find($id);
        $entidad = 'Alertalaboralcargo';
        $formData = array('alertalaboralcargo.update', $id);
        $formData = array('route' => $formData, 'method' => 'PUT', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Modificar';
        return view($this->folderview . '.mant')->with(compact('alertalaboralcargo', 'formData', 'entidad', 'boton','cboAlertalaboral','cboCargolaboral','listar'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \bolsatrabajo\Provincia  $provincia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $existe = Libreria::verificarExistencia($id, 'alertalaboralcargo');
        if ($existe !== true) {
            return $existe;
        }
        $reglas = array('alertalaboral_id' => 'required','cargolaboral_id' => 'required');
        $mensajes = array();
        $validacion = Validator::make($request->all(), $reglas, $mensajes);
        if ($validacion->fails()) {
            return $validacion->messages()->toJson();
        }
        $error = DB::transaction(function () use ($request, $id) {
            $alertalaboralcargo = Alertalaboralcargo::find($id);
            $alertalaboralcargo->alertalaboral_id = $request->input('alertalaboral_id');
            $alertalaboralcargo->cargolaboral_id = $request->input('cargolaboral_id');
            $alertalaboralcargo->save();
        });
        return is_null($error) ? "OK" : $error;
    }

    public function destroy($id)
    {
        $existe = Libreria::verificarExistencia($id, 'alertalaboralcargo');
        if ($existe !== true) {
            return $existe;
        }
        $error = DB::transaction(function () use ($id) {
            $alertalaboralcargo = Alertalaboralcargo::find($id);
            $alertalaboralcargo->delete();
        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Función para confirmar la eliminación de un registrlo
     * @param  integer $id id del registro a intentar eliminar
     * @param  string $listarLuego consultar si luego de eliminar se listará
     * @return html              se retorna html, con la ventana de confirmar eliminar
     */
    public function eliminar($id, $listarLuego)
    {
        $existe = Libreria::verificarExistencia($id, 'alertalaboralcargo');
        if ($existe !== true) {
            return $existe;
        }
        $listar = "NO";
        if (!is_null(Libreria::obtenerParametro($listarLuego))) {
            $listar = $listarLuego;
        }
        $modelo = Alertalaboralcargo::find($id);
        $entidad = 'Alertalaboralcargo';
        $formData = array('route' => array('alertalaboralcargo.destroy', $id), 'method' => 'DELETE', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Eliminar';
        return view('app.confirmarEliminar')->with(compact('modelo', 'formData', 'entidad', 'boton', 'listar'));
    }


}
