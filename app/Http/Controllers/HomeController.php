<?php

namespace bolsatrabajo\Http\Controllers;

use bolsatrabajo\Permiso;
use bolsatrabajo\Tipousuario;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $permisos = [];
        /*Permisos*/
        $permisosasignados = array();
        if (Auth::user()->superusuario) {
            $permisos = Permiso::all();
        } else {
            if (session('tipousuario_id') != 0 || session('tipousuario_id') !== null) {
                $permisos = Tipousuario::find(session('tipousuario_id'))->permisos()->get();
            }
        }
        foreach ($permisos as $key => $value) {
            $permisosasignados[] = $value->codigo;
        }
        $menu = $this->arbolpermisos(session('tipousuario_id'), $permisosasignados);
//        $menu = '<ul class="treeview-menu"><li><a href="/"><i class="fa fa-circle-o"></i>Inicios</a></li></ul>';
        return view('home')->with(compact("menu"));
    }

    /**
     *método para mostrar el árbol de los permisos
     * @param [type] $idtipousuario [description]
     * @return arbol de permisos
     */

    public function arbolpermisos($tipousuario_id, $permisosasignados)
    {
        $cadenacategoria = '<ul class="treeview-menu">';
        $catprincipales = DB::table('categoriamenu')->whereNull('categoriamenu_id')->whereNull('deleted_at')->orderBy('orden', 'ASC')->get();
        foreach ($catprincipales as $catprincipal) {

            $subcategoria = $this->buscarsubcategorias($catprincipal->id, $tipousuario_id, $permisosasignados);
            $usar = false;
            $aux = array();
            $opcionesmenu = DB::table('opcionmenu')->where('categoriamenu_id', '=', $catprincipal->id)->whereNull('deleted_at')->orderBy('orden', 'ASC')->get();
            if ($opcionesmenu->count()) {
                foreach ($opcionesmenu as $opcionmenu) {
                    $permisos = DB::table('permiso')->where('opcionmenu_id', '=', $opcionmenu->id)->whereNull('deleted_at')->get();
                    foreach ($permisos as $permiso) {
                        $permiso_codigo = (explode('_', $permiso->codigo))[0];
                        if ($permiso_codigo == 'list' && in_array($permiso->codigo, $permisosasignados)) {
                            $usar = true;
                            $aux[] = array(
                                'nombre' => $opcionmenu->nombre,
                                'link' => $opcionmenu->link,
                                'icono' => $opcionmenu->icono
                            );
                        }
                    }

                }
            }
            if ($subcategoria != '' || $usar) {
                $cadenacategoria .= '<li class="treeview"><a href="javascript:void(0);"><i class="fa ' . $catprincipal->icono . '"></i>' . $catprincipal->nombre . '<span class="pull-right-container">';
                $cadenacategoria .= '<i class="fa fa-angle-left pull-right"></i></span></a>';
                $cadenacategoria .= '<ul class="treeview-menu">';
                foreach ($aux as $key => $value) {
                    $cadenacategoria .= '<li><a href="javascript:void(0);" onclick="cargarRuta(\'' . route($value['link']) . '\', \'content\');"><i class="fa ' . $value['icono'] . '"></i>' . $value['nombre'] . '</a></li>';
                }
                if ($subcategoria != '') {
                    $cadenacategoria .= $subcategoria;
                }
                $cadenacategoria .= '</ul></li>';
            }
        }
        return $cadenacategoria . '</ul>';
    }

    /**
     *método para mostrar el árbol de los permisos
     * @param [type] $idcategoria $idtipousuario [description]
     * @return arbol de permisos
     */

    public function buscarsubcategorias($categoria_id, $tipousuario_id, $permisosasignados)
    {
        $cadenasubcategoria = '';
        $subcategorias = DB::table('categoriamenu')->where('categoriamenu_id', '=', $categoria_id)->whereNull('deleted_at')->orderBy('orden', 'ASC')->get();
        foreach ($subcategorias as $subcategoria) {
            $usar = false;
            $aux = array();
            $subcats = $this->buscarsubcategorias($subcategoria->id, $tipousuario_id, $permisosasignados);
            $opcionesmenu = DB::table('opcionmenu')->where('categoriamenu_id', '=', $subcategoria->id)->whereNull('deleted_at')->orderBy('orden', 'ASC')->get();
            if ($opcionesmenu->count()) {
                foreach ($opcionesmenu as $opcionmenu) {
                    $permisos = DB::table('permiso')->where('opcionmenu_id', '=', $opcionmenu->id)->whereNull('deleted_at')->get();
                    foreach ($permisos as $permiso) {
                        $permiso_codigo = (explode('_', $permiso->codigo))[0];
                        if ($permiso_codigo == 'list' && in_array($permiso->codigo, $permisosasignados)) {
                            $usar = true;
                            $aux[] = array(
                                'nombre' => $opcionmenu->nombre,
                                'link' => $opcionmenu->link,
                                'icono' => $opcionmenu->icono
                            );
                        }
                    }
                }
            }
            if ($subcats != '' || $usar) {
                $cadenasubcategoria .= '<li class="treeview"><a href="javascript:void(0);"><i class="fa ' . $subcategoria->icono . '"></i>' . $subcategoria->nombre . '<span class="pull-right-container">';
                $cadenasubcategoria .= '<i class="fa fa-angle-left pull-right"></i></span></a>';
                $cadenasubcategoria .= '<ul class="treeview-menu">';
                foreach ($aux as $key => $value) {
                    $cadenasubcategoria .= '<li><a href="javascript:void(0);" onclick="cargarRuta(\'' . route($value['link']) . '\', \'content\');"><i class="fa ' . $value['icono'] . '"></i>' . $value['nombre'] . '</a></li>';
                }
                if ($subcats != '') {
                    $cadenasubcategoria .= $subcats;
                }
                $cadenasubcategoria .= '</ul></li>';
            }
        }

        return $cadenasubcategoria;
    }

}
