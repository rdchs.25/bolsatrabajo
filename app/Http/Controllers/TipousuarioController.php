<?php

namespace bolsatrabajo\Http\Controllers;

use bolsatrabajo\Http\Requests;
use bolsatrabajo\Librerias\Libreria;
use bolsatrabajo\Permisotipousuario;
use bolsatrabajo\Tipousuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Validator;


/*App*/

class TipousuarioController extends Controller
{

    protected $folderview = 'app.tipousuario';
    protected $tituloAdmin = 'Tipos de Usuario';
    protected $tituloRegistrar = 'Registrar Tipo de Usuario';
    protected $tituloModificar = 'Modificar Tipo de Usuario';
    protected $tituloEliminar = 'Eliminar Tipo de Usuario';
    protected $rutas = array('create' => 'tipousuario.create',
        'edit' => 'tipousuario.edit',
        'delete' => 'tipousuario.eliminar',
        'search' => 'tipousuario.buscar',
        'index' => 'tipousuario.index',
        'permisos' => 'tipousuario.obtenerpermisos',
    );

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $entidad = 'Tipousuario';
        $title = $this->tituloAdmin;
        $titulo_registrar = $this->tituloRegistrar;
        $ruta = $this->rutas;
        return view($this->folderview . '.admin')->with(compact('entidad', 'title', 'titulo_registrar', 'ruta'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $entidad = 'Tipousuario';
        $tipousuario = null;
        $formData = array('tipousuario.store');
        $formData = array('route' => $formData, 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Registrar';
        return view($this->folderview . '.mant')->with(compact('tipousuario', 'formData', 'entidad', 'boton', 'listar'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validacion = Validator::make($request->all(),
            array(
                'nombre' => 'required|string|unique:tipousuario',
            )
        );
        if ($validacion->fails()) {
            return $validacion->messages()->toJson();
        }
        $error = DB::transaction(function () use ($request) {
            $tipousuario = new Tipousuario();
            if ($request->input('mostrar')) {
                $tipousuario->mostrar = true;
            }
            $tipousuario->nombre = $request->input('nombre');
            $tipousuario->save();
        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Display the specified resource.
     *
     * @param  \bolsatrabajo\Tipousuario $tipousuario
     * @return \Illuminate\Http\Response
     */
    public function show(Tipousuario $tipousuario)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \bolsatrabajo\Tipousuario $tipousuario
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $existe = Libreria::verificarExistencia($id, 'tipousuario');
        if ($existe !== true) {
            return $existe;
        }
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $tipousuario = Tipousuario::find($id);
        $entidad = 'Tipousuario';
        $formData = array('tipousuario.update', $id);
        $formData = array('route' => $formData, 'method' => 'PUT', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Modificar';
        return view($this->folderview . '.mant')->with(compact('tipousuario', 'formData', 'entidad', 'boton', 'listar'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \bolsatrabajo\Tipousuario $tipousuario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $existe = Libreria::verificarExistencia($id, 'tipousuario');
        if ($existe !== true) {
            return $existe;
        }
        $validacion = Validator::make($request->all(),
            array(
                'nombre' => [
                    'required',
                    Rule::unique('tipousuario')->ignore($id),
                ],
            )
        );
        if ($validacion->fails()) {
            return $validacion->messages()->toJson();
        }
        $error = DB::transaction(function () use ($request, $id) {
            $tipousuario = Tipousuario::find($id);
            if ($request->input('mostrar')) {
                $tipousuario->mostrar = true;
            } else {
                $tipousuario->mostrar = false;
            }
            $tipousuario->nombre = $request->input('nombre');
            $tipousuario->save();

        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \bolsatrabajo\Tipousuario $tipousuario
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $existe = Libreria::verificarExistencia($id, 'tipousuario');
        if ($existe !== true) {
            return $existe;
        }
        $error = DB::transaction(function () use ($id) {
            $tipousuario = Tipousuario::find($id);
            $tipousuario->delete();
        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Mostrar el resultado de búsquedas
     *
     * @return Response
     */
    public function buscar(Request $request)
    {
        $pagina = $request->input('page');
        $filas = $request->input('filas');
        $entidad = 'Tipousuario';
        $nombre = Libreria::getParam($request->input('nombre'));
        $resultado = Tipousuario::listar($nombre);
        $lista = $resultado->get();
        $cabecera = array();
        $cabecera[] = array('valor' => '#', 'numero' => '1');
        $cabecera[] = array('valor' => 'Nombre', 'numero' => '1');
        $cabecera[] = array('valor' => 'Operaciones', 'numero' => '3');

        $titulo_modificar = $this->tituloModificar;
        $titulo_eliminar = $this->tituloEliminar;
        $ruta = $this->rutas;
        if (count($lista) > 0) {
            $clsLibreria = new Libreria();
            $paramPaginacion = $clsLibreria->generarPaginacion($lista, $pagina, $filas, $entidad);
            $paginacion = $paramPaginacion['cadenapaginacion'];
            $inicio = $paramPaginacion['inicio'];
            $fin = $paramPaginacion['fin'];
            $paginaactual = $paramPaginacion['nuevapagina'];
            $lista = $resultado->paginate($filas);
            $request->replace(array('page' => $paginaactual));
            return view($this->folderview . '.list')->with(compact('lista', 'paginacion', 'inicio', 'fin', 'entidad', 'cabecera', 'titulo_modificar', 'titulo_eliminar', 'ruta'));
        }
        return view($this->folderview . '.list')->with(compact('lista', 'entidad'));
    }


    /**
     * Función para confirmar la eliminación de un registrlo
     * @param  integer $id id del registro a intentar eliminar
     * @param  string $listarLuego consultar si luego de eliminar se listará
     * @return html              se retorna html, con la ventana de confirmar eliminar
     */
    public function eliminar($id, $listarLuego)
    {

        $existe = Libreria::verificarExistencia($id, 'tipousuario');
        if ($existe !== true) {
            return $existe;
        }
        $listar = "NO";
        if (!is_null(Libreria::obtenerParametro($listarLuego))) {
            $listar = $listarLuego;
        }
        $modelo = Tipousuario::find($id);
        $entidad = 'Tipousuario';
        $formData = array('route' => array('tipousuario.destroy', $id), 'method' => 'DELETE', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Eliminar';
        return view('app.confirmarEliminar')->with(compact('modelo', 'formData', 'entidad', 'boton', 'listar'));
    }

    /**
     * método para cargar view para asignación de permisos
     * @param  [type] $listarParam [description]
     * @param  [type] $id          [description]
     * @return [type]              [description]
     */
    public function obtenerpermisos($listarParam, $id)
    {
        $existe = Libreria::verificarExistencia($id, 'tipousuario');
        if ($existe !== true) {
            return $existe;
        }
        $listar = "NO";
        $entidad = 'Tipousuario';
        if (isset($listarParam)) {
            $listar = $listarParam;
        }
        $tipousuario = Tipousuario::find($id);

        /*Permisos*/
        $permisosasignados = array();
        $permisos = Tipousuario::find($tipousuario->id)->permisos()->get();
        foreach ($permisos as $key => $value) {
            $permisosasignados[] = $value->id;
        }

        $vista = $this->arbolpermisos($tipousuario->id, $permisosasignados);
        return view($this->folderview . '.permisos')->with(compact('tipousuario', 'listar', 'entidad', 'vista'));
    }

    /**
     * método para guardar asignación de permisos
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function guardarpermisos($tipousuario_id, $permiso_id, $accion)
    {

        $existetipousuario = Libreria::verificarExistencia($tipousuario_id, 'tipousuario');
        $existepermiso = Libreria::verificarExistencia($permiso_id, 'permiso');
        if ($existetipousuario !== true) {
            return $existetipousuario;
        }
        if ($existepermiso !== true) {
            return $existepermiso;
        }
        if ($accion == 'true') {
            $error = DB::transaction(function () use ($permiso_id, $tipousuario_id) {
                $permisotipousuario = new Permisotipousuario();
                $permisotipousuario->permiso_id = $permiso_id;
                $permisotipousuario->tipousuario_id = $tipousuario_id;
                $permisotipousuario->save();
            });
        } else {
            $error = DB::transaction(function () use ($permiso_id, $tipousuario_id) {
                $permisotipousuario = Permisotipousuario::Where('permiso_id', $permiso_id)->Where('tipousuario_id', $tipousuario_id)->firstOrFail();;
                $permisotipousuario->delete();
            });
        }

        return is_null($error) ? "OK" : $error;


    }


    /**
     *método para mostrar el árbol de los permisos
     * @param [type] $idtipousuario [description]
     * @return arbol de permisos
     */

    public function arbolpermisos($tipousuario_id, $permisosasignados)
    {
        $entidad = 'Tipousuario';
        $cadenacategoria = '<ul style="list-style: none">';
        $catprincipales = DB::table('categoriamenu')->whereNull('categoriamenu_id')->whereNull('deleted_at')->orderBy('orden', 'ASC')->get();
        foreach ($catprincipales as $catprincipal) {
            $subcategoria = $this->buscarsubcategorias($catprincipal->id, $tipousuario_id, $permisosasignados);
            $cadenacategoria .= '<li><i class="fa ' . $catprincipal->icono . '">&nbsp;</i>' . $catprincipal->nombre;
            if ($subcategoria != '') {
                $cadenacategoria .= $subcategoria;
            }
            $opcionesmenu = DB::table('opcionmenu')->where('categoriamenu_id', '=', $catprincipal->id)->whereNull('deleted_at')->orderBy('orden', 'ASC')->get();
            foreach ($opcionesmenu as $opcionmenu) {
                $permisos = DB::table('permiso')->where('opcionmenu_id', '=', $opcionmenu->id)->whereNull('deleted_at')->get();
                $cadenacategoria .= '<ul style="list-style: none"><li><i class="fa ' . $opcionmenu->icono . '">&nbsp;</i>' . $opcionmenu->nombre . '<ul style="list-style: none">';
                foreach ($permisos as $permiso) {

                    if (in_array($permiso->id, $permisosasignados)) {
                        $cadenacategoria .= '<li><i class="fa fa-fw fa-check">&nbsp;</i>' . $permiso->nombre . '<input type="checkbox" name="' . $permiso->id . '" id="' . $permiso->id . '" checked onchange="guardarpermiso(\'' . $entidad . '\',\'' . $permiso->id . '\',this)" class="pull-right"></li>';

                    } else {
                        $cadenacategoria .= '<li><i class="fa fa-fw fa-check">&nbsp;</i>' . $permiso->nombre . '<input type="checkbox"  name="' . $permiso->id . '" id="' . $permiso->id . '"onchange="guardarpermiso(\'' . $entidad . '\',\'' . $permiso->id . '\',this)" class="pull-right"></li>';
                    }
                }
                $cadenacategoria .= '</ul></li></ul>';

            }
            $cadenacategoria .= '</li>';

        }
        return $cadenacategoria . '</ul>';
    }

    /**
     *método para mostrar el árbol de los permisos
     * @param [type] $idcategoria $idtipousuario [description]
     * @return arbol de permisos
     */

    public function buscarsubcategorias($categoria_id, $tipousuario_id, $permisosasignados)
    {
        $entidad = 'Tipousuario';
        $cadenasubcategoria = '';
        $subcategorias = DB::table('categoriamenu')->where('categoriamenu_id', '=', $categoria_id)->whereNull('deleted_at')->orderBy('orden', 'ASC')->get();
        if ($subcategorias->count() == 0) {
            return $cadenasubcategoria;
        }
        $cadenasubcategoria = '<ul style="list-style: none">';
        foreach ($subcategorias as $subcategoria) {
            $subcats = $this->buscarsubcategorias($subcategoria->id, $tipousuario_id, $permisosasignados);
            $cadenasubcategoria .= '<li><i class="fa ' . $subcategoria->icono . '">&nbsp;</i>' . $subcategoria->nombre;
            if ($subcats != '') {
                $cadenasubcategoria .= $subcats;
            }
            $opcionesmenu = DB::table('opcionmenu')->where('categoriamenu_id', '=', $subcategoria->id)->whereNull('deleted_at')->orderBy('orden', 'ASC')->get();
            foreach ($opcionesmenu as $opcionmenu) {
                $cadenasubcategoria .= '<ul style="list-style: none"><li><i class="fa ' . $opcionmenu->icono . '">&nbsp;</i>' . $opcionmenu->nombre . '<ul style="list-style: none">';
                $permisos = DB::table('permiso')->where('opcionmenu_id', '=', $opcionmenu->id)->whereNull('deleted_at')->get();
                foreach ($permisos as $permiso) {
                    if (in_array($permiso->id, $permisosasignados)) {
                        $cadenasubcategoria .= '<li><i class="fa fa-fw fa-check">&nbsp;</i>' . $permiso->nombre . '<input type="checkbox" checked  name="' . $permiso->id . '" id="' . $permiso->id . '" onchange="guardarpermiso(\'' . $entidad . '\',\'' . $permiso->id . '\',this)" class="pull-right"></li>';

                    } else {
                        $cadenasubcategoria .= '<li><i class="fa fa-fw fa-check">&nbsp;</i>' . $permiso->nombre . '<input type="checkbox" name="' . $permiso->id . '" id="' . $permiso->id . '" onchange="guardarpermiso(\'' . $entidad . '\',\'' . $permiso->id . '\',this)" class="pull-right"></li>';
                    }
                }
                $cadenasubcategoria .= '</ul></li></ul>';
            }
            $cadenasubcategoria .= '</li>';

        }

        return $cadenasubcategoria . '</ul>';
    }


}
