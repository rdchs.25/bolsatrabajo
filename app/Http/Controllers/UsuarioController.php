<?php

namespace bolsatrabajo\Http\Controllers;

use bolsatrabajo\Librerias\Libreria;
use bolsatrabajo\Persona;
use bolsatrabajo\Tipousuario;
use bolsatrabajo\Tipousuariousuario;
use bolsatrabajo\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Validator;
use Mail;


class Usuariocontroller extends Controller
{
    protected $folderview = 'app.usuario';
    protected $tituloAdmin = 'Usuario';
    protected $tituloRegistrar = 'Registrar Usuario';
    protected $tituloModificar = 'Modificar Usuario';
    protected $tituloEliminar = 'Eliminar Usuario';
    protected $tituloActivarusuario = 'Activar Usuario';
    protected $rutas = array('create' => 'usuario.create',
        'edit' => 'usuario.edit',
        'delete' => 'usuario.eliminar',
        'search' => 'usuario.buscar',
        'index' => 'usuario.index',
        'activarusuario' => 'usuario.activarusuario',
    );

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Libreria::verificarExistenciaPermiso('list_usuario')) {
            return view('app.401');
        }
        $entidad = 'Usuario';
        $title = $this->tituloAdmin;
        $titulo_registrar = $this->tituloRegistrar;
        $ruta = $this->rutas;
        return view($this->folderview . '.admin')->with(compact('entidad', 'title', 'titulo_registrar', 'ruta'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if (!Libreria::verificarExistenciaPermiso('add_usuario')) {
            return view('app.401');
        }
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $entidad = 'Usuario';
        $cboPersona = ['' => 'Seleccione'] + Persona::orderBy('nombres', 'ASC')->pluck('nombres', 'id')->all();
        $cboTipousuario = Tipousuario::orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        $usuario = null;
        $listtipousurio_usuario = [];
        $formData = array('usuario.store');
        $formData = array('route' => $formData, 'files' => true, 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off', 'files' => true);
        $boton = 'Registrar';
        return view($this->folderview . '.mant')->with(compact('usuario', 'formData', 'cboPersona', 'cboTipousuario', 'listtipousurio_usuario', 'entidad', 'boton', 'listar'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!Libreria::verificarExistenciaPermiso('add_usuario')) {
            return view('app.401');
        }

        $mensajes = array(
            'email' => 'Debe ingresar email',
            'usuario' => 'Debe ingresar usuario',
            'password' => 'Debe ingresar password',
            'tipousuario_id' => 'Debe seleccionar tipo usuarios',
            'persona_id' => 'Debe seleccionar una persona',

        );

        $reglas = array(
            'email' => 'required|string|max:255',
            'usuario' => 'required|string|unique:usuario',
            'password' => 'required|string|min:6|confirmed',
            'tipousuario_id' => 'required',
            'persona_id' => 'required|unique:usuario',

        );

        $validacion = Validator::make($request->all(), $reglas, $mensajes);

        if ($validacion->fails()) {
            return $validacion->messages()->toJson();
        }

        $error = DB::transaction(function () use ($request) {

            $persona = DB::table('persona')->where('id', $request->input('persona_id'))->first();
            $usuario = new User();

            $usuario->nombre = $persona->nombres." ".$persona->apellidopaterno." ".$persona->apellidomaterno;
            $usuario->email = $request->input('email');
            $usuario->usuario = $request->input('usuario');
            $usuario->password = bcrypt($request->input('password'));
            $usuario->persona_id = $request->input('persona_id');

//            $usuario->remember_token = str_random(60);
            if ($request->input('superusuario')) {
                $usuario->superusuario = true;
            } else {
                $usuario->superusuario = false;
            }
            $usuario->save();

            /*Tipo Usuario*/

            $tipousuario = $request->input('tipousuario_id');
            if (count($tipousuario) > 0) {
                foreach ($tipousuario as $clave => $valor) {
                    $tipousuario = new Tipousuariousuario();
                    $tipousuario->usuario_id = $usuario->id;
                    $tipousuario->tipousuario_id = $valor;
                    $tipousuario->save();
                }
            }

        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {

        if (!Libreria::verificarExistenciaPermiso('change_usuario')) {
            return view('app.401');
        }
        $existe = Libreria::verificarExistencia($id, 'usuario');
        if ($existe !== true) {
            return $existe;
        }
        $listar = Libreria::getParam($request->input('listar'), 'NO');
        $usuario = User::find($id);
        $entidad = 'Usuario';
        $cboTipousuario = Tipousuario::orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        $listtipousurio_usuario = Tipousuariousuario::where('usuario_id', '=', $id)->get();
        $cboPersona = ['' => 'Seleccione'] + Persona::orderBy('nombres', 'ASC')->pluck('nombres', 'id')->all();
        $formData = array('usuario.update', $id);
        $formData = array('route' => $formData, 'method' => 'PUT', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Modificar';
        return view($this->folderview . '.mant')->with(compact('usuario', 'formData', 'entidad', 'boton', 'listar', 'cboTipousuario', 'cboPersona', 'listtipousurio_usuario'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!Libreria::verificarExistenciaPermiso('change_usuario')) {
            return view('app.401');
        }
        $existe = Libreria::verificarExistencia($id, 'usuario');
        if ($existe !== true) {
            return $existe;
        }

        $mensajes = array(
            'email' => 'Debe ingresar email',
            'usuario' => 'Debe ingresar usuario',
            'password' => 'Debe ingresar password',
            'tipousuario_id' => 'Debe seleccionar tipo usuarios',

        );

        $validacion = Validator::make($request->all(),
            array(
                'email' => [
                    'required',
                    Rule::unique('usuario')->ignore($id),
                ],
                'usuario' => [
                    'required',
                    Rule::unique('usuario')->ignore($id),
                ],
                'email' => 'required|string|max:255',
                'usuario' => 'required|string',
                'password' => 'required|string|min:6|confirmed',
                'tipousuario_id' => 'required',
            ), $mensajes);

        if ($validacion->fails()) {
            return $validacion->messages()->toJson();
        }

        $error = DB::transaction(function () use ($request, $id) {

            $usuario = User::find($id);
            //$usuario->nombre = $request->input('nombre');
            $usuario->email = $request->input('email');
            $usuario->usuario = $request->input('usuario');
            $usuario->password = bcrypt($request->input('password'));
//            $usuario->persona_id = $request->input('persona_id');
            if ($request->input('superusuario')) {
                $usuario->superusuario = true;
            } else {
                $usuario->superusuario = false;
            }
            $usuario->save();


            /*Tipo Usuario*/

            $tipousuarios_asignados = [];
            $tipousuarios = $request->input('tipousuario_id');
            $tipousuario_usuario = Tipousuariousuario::where('usuario_id', '=', $id)->get();
            if ($tipousuario_usuario->count() && count($tipousuarios) > 0) {
                foreach ($tipousuario_usuario as $key => $value) {
                    if (!in_array($value->tipousuario_id, $tipousuarios)) {
                        DB::table('tipousuario_usuario')->where('usuario_id', '=', $id)->where('tipousuario_id', '=', $value->tipousuario_id)->delete();
                    } else {
                        $tipousuarios_asignados[] = $value->tipousuario_id;
                    }
                }
            }
            if (count($tipousuarios) > 0) {
                foreach ($tipousuarios as $clave => $valor) {
                    if (!in_array($valor, $tipousuarios_asignados)) {
                        $tipousuario = new Tipousuariousuario();
                        $tipousuario->usuario_id = $id;
                        $tipousuario->tipousuario_id = $valor;
                        $tipousuario->save();
                    }
                }
            } else {
                DB::table('tipousuario_usuario')->where('usuario_id', '=', $id)->delete();
            }


        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!Libreria::verificarExistenciaPermiso('delete_usuario')) {
            return view('app.401');
        }
        $existe = Libreria::verificarExistencia($id, 'usuario');
        if ($existe !== true) {
            return $existe;
        }
        $error = DB::transaction(function () use ($id) {
            $usuario = User::find($id);
            $usuario->delete();

        });
        return is_null($error) ? "OK" : $error;
    }

    /**
     * Mostrar el resultado de búsquedas
     *
     * @return Response
     */
    public function buscar(Request $request)
    {
        $change = Libreria::verificarExistenciaPermiso('change_usuario');
        $delete = Libreria::verificarExistenciaPermiso('delete_usuario');

        if (!Libreria::verificarExistenciaPermiso('list_usuario')) {
            return view('app.401');
        }

        $pagina = $request->input('page');
        $filas = $request->input('filas');
        $entidad = 'Usuario';
        $querysearch = Libreria::getParam($request->input('querysearch'));
        $resultado = User::listar($querysearch);
        $lista = $resultado->get();
        $cabecera = array();
        $cabecera[] = array('valor' => '#', 'numero' => '1');
        $cabecera[] = array('valor' => 'Nombres', 'numero' => '1');
        $cabecera[] = array('valor' => 'Email', 'numero' => '1');
        $cabecera[] = array('valor' => 'Usuario', 'numero' => '1');
        $cabecera[] = array('valor' => 'Superusuario', 'numero' => '1');
        $cabecera[] = array('valor' => 'Tipo Usuario', 'numero' => '1');
        $cabecera[] = array('valor' => 'Estado', 'numero' => '1');
        $cabecera[] = array('valor' => 'Operaciones', 'numero' => '2');

        $titulo_modificar = $this->tituloModificar;
        $titulo_eliminar = $this->tituloEliminar;
        $titulo_activarusuario = $this->tituloActivarusuario;
        $ruta = $this->rutas;
        if (count($lista) > 0) {
            $clsLibreria = new Libreria();
            $paramPaginacion = $clsLibreria->generarPaginacion($lista, $pagina, $filas, $entidad);
            $paginacion = $paramPaginacion['cadenapaginacion'];
            $inicio = $paramPaginacion['inicio'];
            $fin = $paramPaginacion['fin'];
            $paginaactual = $paramPaginacion['nuevapagina'];
            $lista = $resultado->paginate($filas);
            $request->replace(array('page' => $paginaactual));
            return view($this->folderview . '.list')->with(compact('lista', 'paginacion', 'change', 'delete', 'inicio', 'fin', 'entidad', 'cabecera', 'titulo_modificar', 'titulo_eliminar', 'ruta', 'titulo_activarusuario'));
        }
        return view($this->folderview . '.list')->with(compact('lista', 'entidad'));
    }

    /**
     * Función para confirmar la eliminación de un registrlo
     * @param  integer $id id del registro a intentar eliminar
     * @param  string $listarLuego consultar si luego de eliminar se listará
     * @return html              se retorna html, con la ventana de confirmar eliminar
     */
    public function eliminar($id, $listarLuego)
    {
        if (!Libreria::verificarExistenciaPermiso('delete_usuario')) {
            return view('app.401');
        }

        $existe = Libreria::verificarExistencia($id, 'usuario');
        if ($existe !== true) {
            return $existe;
        }
        $listar = "NO";
        if (!is_null(Libreria::obtenerParametro($listarLuego))) {
            $listar = $listarLuego;
        }
        $modelo = User::find($id);
        $entidad = 'Usuario';
        $formData = array('route' => array('usuario.destroy', $id), 'method' => 'DELETE', 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Eliminar';
        return view('app.confirmarEliminar')->with(compact('modelo', 'formData', 'entidad', 'boton', 'listar'));
    }

    /**
     * Función para autocompletar persona
     * @param  string $query , busca por nombre o apellidos
     * @param  string $query
     * @return html se retorna html, con la ventana de autocompletar
     */
    public function autocompletarpersona($querysearchpersona)
    {
        if (!Libreria::verificarExistenciaPermiso('read_usuario')) {
            return view('app.401');
        }
//        for ($i = 0; $i < 10; $i++) {
//
//            $data[] = array(
//                'label' => "asdas",
//                'id' => "ddd",
//                'value' => 'aaa',
//            );
//        }
        $persona = Persona::buscarpersona($querysearchpersona);
        return json_encode($persona);

    }


    public function activarusuario($id, $listarLuego)
    {
        if (!Libreria::verificarExistenciaPermiso('add_usuario')) {
            return view('app.401');
        }

        $existe = Libreria::verificarExistencia($id, 'usuario');
        if ($existe !== true) {
            return $existe;
        }
        $listar = "NO";
        if (!is_null(Libreria::obtenerParametro($listarLuego))) {
            $listar = $listarLuego;
        }
        $modelo = User::find($id);
        $entidad = 'Usuario';
        $formData = array('route' => array('usuario.activarcuenta', $id), 'class' => 'form-horizontal', 'id' => 'formMantenimiento' . $entidad, 'autocomplete' => 'off');
        $boton = 'Activar';
        return view($this->folderview . '.activarusuario')->with(compact('modelo', 'formData', 'entidad', 'boton', 'listar'));
    }

    public function activarcuenta($id)
    {
        if (!Libreria::verificarExistenciaPermiso('add_usuario')) {
            return view('app.401');
        }
        $existe = Libreria::verificarExistencia($id, 'usuario');
        if ($existe !== true) {
            return $existe;
        }
        $error = DB::transaction(function () use ($id) {
            $usuario = User::find($id);
            $usuario->estado = 'Activado';
            $usuario->save();

            /*Enviar Email*/
            
                        $data = array(
                            'usuario' => $usuario,
                        );

                        Mail::send($this->folderview . '.plantillaemail', $data, function ($message) use ($usuario) {
                            $message->from('egresados@udl.edu.pe', 'Universidad de Lambayeque');
                            $message->to($usuario->email)->subject('Activación cuenta bolsa trabajo');
                        });
            
            /**/

        });
        return is_null($error) ? "OK" : $error;
    }
}
