<?php

namespace bolsatrabajo;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    protected $table = 'usuario';
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre', 'email', 'usuario', 'password', 'persona_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function persona()
    {
        return $this->belongsTo('bolsatrabajo\Persona');
    }

    public function empleador()
    {
        return $this->belongsTo('bolsatrabajo\Empleador');
    }

    public function tipousuarios()
    {
        return $this->belongsToMany('bolsatrabajo\Tipousuario', 'tipousuario_usuario', 'usuario_id', 'tipousuario_id');

    }

    public function tiposusuarios()
    {
        return $this->hasMany('bolsatrabajo\Tipousuariousuario','usuario_id');
    }

    public function scopelistar($query, $querysearch)
    {
        return $query->where(function ($subquery) use ($querysearch) {
            if (!is_null($querysearch)) {
                $subquery->orwhere('nombre', 'LIKE', '%' . $querysearch . '%')
                    ->orwhere('email', 'LIKE', '%' . $querysearch . '%')
                    ->orwhere('usuario', 'LIKE', '%' . $querysearch . '%');
            }
        })->orderBy('nombre', 'ASC');
    }
}
