<?php

namespace bolsatrabajo;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Sectorempresa extends Model
{
    use SoftDeletes;
    protected $table = 'sectorempresa';
    protected $dates = ['deleted_at'];
    protected $fillable = ['nombre'];

    public function empresas()
    {
        return $this->hasMany('bolsatrabajo\Empresa');
    }

    public function formacionlaboral()
    {
        return $this->hasMany('bolsatrabajo\Formacionlaboral');
    }

    public function scopelistar($query, $name)
    {
        return $query->where(function($subquery) use($name)
        {
            if (!is_null($name)) {
                $subquery->where('nombre', 'LIKE', '%'.$name.'%');
            }
        })
            ->orderBy('nombre', 'ASC');
    }

}
