<?php

namespace bolsatrabajo;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Formacioncomplementaria extends Model
{
    use SoftDeletes;
    protected $table = 'formacioncomplementaria';
    protected $dates = ['deleted_at'];
}
