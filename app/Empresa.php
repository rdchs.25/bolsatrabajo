<?php

namespace bolsatrabajo;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Empresa extends Model
{
    use SoftDeletes;

    protected $table = 'empresa';
    protected $dates = ['deleted_at'];

    public function distrito()
    {
        return $this->belongsTo('bolsatrabajo\Distrito');
    }
    public function formacioneslaborales()
    {
        return $this->hasMany('bolsatrabajo\Formacionlaboral');
    }
    public function sectorempresa()
    {
        return $this->belongsTo('bolsatrabajo\Sectorempresa');
    }

    public function scopelistar($query, $querysearch, $distrito_id)
    {
        return $query->where(function ($subquery) use ($querysearch) {
            if (!is_null($querysearch)) {
                $subquery->orwhere('ruc', 'LIKE', '%' . $querysearch . '%')
                    ->orwhere('razonsocial', 'LIKE', '%' . $querysearch . '%')
                    ->orwhere('nombrecomercial', 'LIKE', '%' . $querysearch . '%')
                    ->orwhere('direccion', 'LIKE', '%' . $querysearch . '%');
            }
        })
            ->where(function ($subquery) use ($distrito_id) {

                if (!is_null($distrito_id)) {
                    $subquery->where('distrito_id', '=', $distrito_id);
                }
            })
            ->orderBy('ruc', 'ASC');
    }
}
