<?php

namespace bolsatrabajo;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tipodiscapacidad extends Model
{
    use SoftDeletes;
    protected $table = 'tipodiscapacidad';
    protected $dates = ['deleted_at'];
    protected $fillable = ['nombre'];


    public function scopelistar($query, $name)
    {
        return $query->where(function($subquery) use($name)
        {
            if (!is_null($name)) {
                $subquery->where('nombre', 'LIKE', '%'.$name.'%');
            }
        })
            ->orderBy('nombre', 'ASC');
    }

    public function discapacidades()
    {
        return $this->hasMany('bolsatrabajo\Discapacidad');
    }
}
