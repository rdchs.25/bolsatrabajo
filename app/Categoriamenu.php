<?php

namespace bolsatrabajo;

use Iatstuti\Database\Support\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Categoriamenu extends Model
{
    use SoftDeletes;
    use CascadeSoftDeletes;

    protected $table = 'categoriamenu';
    protected $dates = ['deleted_at'];
    protected $cascadeDeletes = ['opcionmenu'];


    public function mostrar()
    {
        return 'nombre';
    }

    public function cascadadelete()
    {
        return $this->cascadeDeletes;
    }

    public function opcionmenu()
    {
        return $this->hasMany('bolsatrabajo\Opcionmenu');
    }

    public function categoriamenupadre()
    {
        return $this->belongsTo('bolsatrabajo\Categoriamenu', 'categoriamenu_id');
    }

    public function Soncategoriamenu()
    {
        return $this->hasMany('bolsatrabajo\Categoriamenu', 'categoriamenu_id');
    }

    public function scopelistar($query, $querysearch)
    {
        return $query->where(function ($subquery) use ($querysearch) {
            if (!is_null($querysearch)) {
                $subquery->orwhere('nombre', 'LIKE', '%' . $querysearch . '%')
                    ->orwhere('orden', 'LIKE', '%' . $querysearch . '%')
                    ->orwhere('icono', 'LIKE', '%' . $querysearch . '%');
            }
        })->orderBy('nombre', 'ASC')
            ->orderBy('categoriamenu_id', 'desc');

    }

}
