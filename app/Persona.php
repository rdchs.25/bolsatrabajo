<?php

namespace bolsatrabajo;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Persona extends Model
{
    use SoftDeletes;
    protected $table = 'persona';
    protected $dates = ['deleted_at'];

    public function distrito()
    {
        return $this->belongsTo('bolsatrabajo\Distrito');
    }

    public function escuela()
    {
        return $this->belongsTo('bolsatrabajo\Escuela');
    }

    public function cargolaboral()
    {
        return $this->belongsTo('bolsatrabajo\Cargolaboral');
    }

    public function tipodocumento()
    {
        return $this->belongsTo('bolsatrabajo\Tipodocumento');
    }

    public function pais()
    {
        return $this->belongsTo('bolsatrabajo\Pais');
    }

    public function preferencialaboral()
    {
        return $this->hasMany('bolsatrabajo\Preferencialaboral');
    }


    public function scopelistar($query, $querysearch, $distrito_id, $pais_id, $escuela_id, $tipodocumento_id, $cargolaboral_id)
    {
        return $query->where(function ($subquery) use ($querysearch) {
            if (!is_null($querysearch)) {
                $subquery->orwhere('nombres', 'LIKE', '%' . $querysearch . '%')
                    ->orwhere('codigouniversitario', 'LIKE', '%' . $querysearch . '%')
                    ->orwhere('apellidopaterno', 'LIKE', '%' . $querysearch . '%')
                    ->orwhere('apellidomaterno', 'LIKE', '%' . $querysearch . '%')
                    ->orwhere('nrodocumento', 'LIKE', '%' . $querysearch . '%')
                    ->orwhere('nrodocumento', 'LIKE', '%' . $querysearch . '%');
            }
        })
            ->where(function ($subquery) use ($pais_id, $distrito_id, $escuela_id, $tipodocumento_id, $cargolaboral_id) {
                if (!is_null($pais_id)) {
                    $subquery->where('pais_id', '=', $pais_id);
                }
                if (!is_null($distrito_id)) {
                    $subquery->where('distrito_id', '=', $distrito_id);
                }
                if (!is_null($escuela_id)) {
                    $subquery->where('escuela_id', '=', $escuela_id);
                }
                if (!is_null($tipodocumento_id)) {
                    $subquery->where('tipodocumento_id', '=', $tipodocumento_id);
                }
                if (!is_null($cargolaboral_id)) {
                    $subquery->where('cargolaboral_id', '=', $cargolaboral_id);
                }
            })
            ->orderBy('apellidopaterno', 'ASC');
    }

    public static function buscarpersona($searchquery = null)
    {

        $results = array();

        $queries = DB::table('persona')
            ->orwhere('apellidopaterno', 'LIKE', '%' . $searchquery . '%')
            ->orWhere('apellidomaterno', 'LIKE', '%' . $searchquery . '%')
            ->orWhere('nombres', 'LIKE', '%' . $searchquery . '%')
            ->take(5)->get();

        foreach ($queries as $query) {
            $results[] = ['id' => $query->id, 'value' => $query->apellidopaterno . ' ' . $query->apellidomaterno . ' ' . $query->nombres];
        }
        return $results;
    }

}
