<?php

namespace bolsatrabajo;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Idioma extends Model
{

    use SoftDeletes;
    protected $table = 'idioma';
    protected $dates = ['deleted_at'];
    protected $fillable = ['nombre'];

    public function idiomarequeridos()
    {
        return $this->hasMany('bolsatrabajo\Idiomarequerido');
    }

    public function scopelistar($query, $name)
    {
        return $query->where(function ($subquery) use ($name) {
            if (!is_null($name)) {
                $subquery->where('nombre', 'LIKE', '%' . $name . '%');
            }
        })
            ->orderBy('nombre', 'ASC');
    }


}
