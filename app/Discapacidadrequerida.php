<?php

namespace bolsatrabajo;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Discapacidadrequerida extends Model
{
    use SoftDeletes;
    protected $table = 'discapacidad_ofertalaboral';
    protected $dates = ['deleted_at'];
    protected $fillable = ['discapacidad_id','ofertalaboral_id'];


    public function discapacidad(){
        return $this->belongsTo('bolsatrabajo\Discapacidad');
    }
    
    public function scopelistar($query, $name)
    {
        return $query->where(function($subquery) use($name)
        {
            if (!is_null($name)) {
                $subquery->where('nombre', 'LIKE', '%'.$name.'%');
            }
        })
            ->orderBy('nombre', 'ASC');
    }


}
