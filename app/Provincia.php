<?php

namespace bolsatrabajo;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Provincia extends Model
{
    use SoftDeletes;
    protected $table = 'provincia';
    protected $dates = ['deleted_at'];
    protected $fillable = ['nombre', 'departamento_id'];


    public function departamento()
    {
        return $this->belongsTo('bolsatrabajo\Departamento');
    }

    public function distritos()
    {
        return $this->hasMany('bolsatrabajo\Distrito');
    }

    public function scopelistar($query, $name)
    {
        return $query->where(function ($subquery) use ($name) {
            if (!is_null($name)) {
                $subquery->where('nombre', 'LIKE', '%' . $name . '%');
            }
        })
            ->orderBy('nombre', 'ASC');
    }
}
