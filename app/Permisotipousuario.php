<?php

namespace bolsatrabajo;

use Illuminate\Database\Eloquent\Model;

class Permisotipousuario extends Model
{
    protected $table = 'permiso_tipousuario';

    public function permiso()
    {
        return $this->belongsTo('bolsatrabajo\Permiso');
    }

    public function tipousuario()
    {
        return $this->belongsTo('bolsatrabajo\Tipousuario');
    }

    public function mostrar($id)
    {
         $tipousuario = Permisotipousuario::find($id);
         return $tipousuario->permiso->nombre. "-". $tipousuario->tipousuario->nombre;
    }


}
