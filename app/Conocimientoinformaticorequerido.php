<?php

namespace bolsatrabajo;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Conocimientoinformaticorequerido extends Model
{
    use SoftDeletes;
    protected $table = 'conocimientoinformaticorequerido';
    protected $dates = ['deleted_at'];
    protected $fillable = ['nombre','nivel','ofertalaboral_id'];

    public function scopelistar($query, $name)
    {
        return $query->where(function($subquery) use($name)
        {
            if (!is_null($name)) {
                $subquery->where('nombre', 'LIKE', '%'.$name.'%');
            }
        })
            ->orderBy('nombre', 'ASC');
    }


}
