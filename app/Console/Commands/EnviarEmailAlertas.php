<?php

namespace bolsatrabajo\Console\Commands;

use Illuminate\Console\Command;
use Mail;
use Illuminate\Support\Facades\DB;
use bolsatrabajo\User;


class EnviarEmailAlertas extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:sendalerts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envia una notificaciones a los usuarios que tienen alertas programadas';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $error = DB::transaction(function ()  {
            $usuario = User::find(4);

    		/*Enviar Email*/    
            $data = array(
                'usuario' => $usuario,
            );
            Mail::send('app.usuario.plantillaemail', $data, function ($message) use ($usuario) {
                $message->from('egresados@udl.edu.pe', 'Universidad de Lambayeque');
                $message->to($usuario->email)->subject('Nuevas Ofertas de Trabajo');
            });
    
    		/**/

        });
        echo is_null($error) ? "OK" : $error;
    }
}
