<?php

namespace bolsatrabajo;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Conocimientoinformatico extends Model
{
    use SoftDeletes;
    protected $table = 'conocimientoinformatico';
    protected $dates = ['deleted_at'];

}
