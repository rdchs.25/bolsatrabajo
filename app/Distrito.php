<?php

namespace bolsatrabajo;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Distrito extends Model
{
    use SoftDeletes;
    protected $table = 'distrito';
    protected $dates = ['deleted_at'];
    protected $fillable = ['nombre', 'provincia_id'];

    public function provincia()
    {
        return $this->belongsTo('bolsatrabajo\Provincia');
    }

    public function universidades()
    {
        return $this->hasMany('bolsatrabajo\Universidad');
    }

    public function colegios()
    {
        return $this->hasMany('bolsatrabajo\Colegio');
    }

    public function empleadores()
    {
        return $this->hasMany('bolsatrabajo\Empleador');
    }

    public function empresas()
    {
        return $this->hasMany('bolsatrabajo\Empresa');
    }

    public function personas()
    {
        return $this->hasMany('bolsatrabajo\Persona');
    }

    public function scopelistar($query, $name)
    {
        return $query->where(function ($subquery) use ($name) {
            if (!is_null($name)) {
                $subquery->where('nombre', 'LIKE', '%' . $name . '%');
            }
        })
            ->orderBy('nombre', 'ASC');
    }
}
