<?php

namespace bolsatrabajo;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Nivelestudio extends Model
{

    use SoftDeletes;
    protected $table = 'nivelestudio';
    protected $dates = ['deleted_at'];
    protected $fillable = ['nombre'];

    public function formacionlabores()
    {
        return $this->hasMany('bolsatrabajo\Formacionlaboral');
    }

    public function scopelistar($query, $name)
    {
        return $query->where(function($subquery) use($name)
        {
            if (!is_null($name)) {
                $subquery->where('nombre', 'LIKE', '%'.$name.'%');
            }
        })
            ->orderBy('nombre', 'ASC');
    }
}
