<?php

namespace bolsatrabajo;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Preferencialaboral extends Model
{
    use SoftDeletes;
    protected $table = 'preferencialaboral';
    protected $dates = ['deleted_at'];
    protected $fillable = ['nombre'];
}
