<?php

namespace bolsatrabajo;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Alertalaboraldepartamento extends Model
{
    use SoftDeletes;
    protected $table = 'alertalaboraldepartamento';
    protected $dates = ['deleted_at'];
    protected $fillable = ['departamento_id,alertalaboral_id'];

    public function scopelistar($query, $name)
    {
        return $query->where(function($subquery) use($name)
        {
            if (!is_null($name)) {
                $subquery->where('nombre', 'LIKE', '%'.$name.'%');
            }
        })
            ->orderBy('nombre', 'ASC');
    }
}
