<?php

namespace bolsatrabajo;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Discapacidad extends Model
{
    use SoftDeletes;
    protected $table = 'discapacidad';
    protected $dates = ['deleted_at'];
    protected $fillable = ['nombre','tipodiscapacidad_id'];

    public function tipodiscapacidad()
    {
        return $this->belongsTo('bolsatrabajo\Tipodiscapacidad');
    }

    public function discapacidadrequerida(){
        return $this->hasMany('bolsatrabajo\Discapacidadrequerida');
    }

    public function scopelistar($query, $name)
    {
        return $query->where(function($subquery) use($name)
        {
            if (!is_null($name)) {
                $subquery->where('nombre', 'LIKE', '%'.$name.'%');
            }
        })
            ->orderBy('nombre', 'ASC');
    }
}
