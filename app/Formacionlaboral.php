<?php

namespace bolsatrabajo;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Formacionlaboral extends Model
{
    use SoftDeletes;
    protected $table = 'formacionlaboral';
    protected $dates = ['deleted_at'];

    public function empresa()
    {
        return $this->belongsTo('bolsatrabajo\Empresa');
    }
    public function sectorempresa()
    {
        return $this->belongsTo('bolsatrabajo\Sectorempresa');
    }

}
