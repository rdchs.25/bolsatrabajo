<?php

namespace bolsatrabajo;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Alertalaboral extends Model
{
    use SoftDeletes;
    protected $table = 'alertalaboral';
    protected $dates = ['deleted_at'];
    protected $fillable = ['arealaboral_id,escuela_id,persona_id'];


    public function persona()
    {
        return $this->belongsTo('bolsatrabajo\Persona');
    }

    public function arealaboral()
    {
        return $this->belongsTo('bolsatrabajo\Arealaboral');
    }

    public function escuela()
    {
        return $this->belongsTo('bolsatrabajo\Escuela');
    }

    public function alertalaboralcargos()
    {
        return $this->belongsToMany('bolsatrabajo\Cargolaboral', 'alertalaboralcargo', 'alertalaboral_id', 'cargolaboral_id');

    }

    public function alertalaboraldepartamentos()
    {
        return $this->belongsToMany('bolsatrabajo\Departamento', 'alertalaboraldepartamento', 'alertalaboral_id', 'departamento_id');

    }

    /*public function scopelistar($query, $name)
    {
        return $query->select('arealaboral.nombre as arealaboral_nombre',
            'escuela.nombre as escuela_nombre',
            'persona.id as persona_id',
            'alertalaboral.id as id'
        )
            ->join('arealaboral', 'arealaboral.id', '=', 'alertalaboral.arealaboral_id')
            ->join('escuela', 'escuela.id', '=', 'alertalaboral.escuela_id')
            ->join('persona', 'persona.id', '=', 'alertalaboral.persona_id')
            ->whereNull('alertalaboral.deleted_at')
            ->where(function ($subquery) use ($name) {
                if (!is_null($name)) {
                    $subquery->where('arealaboral.nombre', 'LIKE', '%' . $name . '%');
                }
            });
    }*/

     public function scopelistar($query, $querysearch)
    {
        return $query->where(function ($subquery) use ($querysearch) {
            if (!is_null($querysearch)) {
                $subquery->persona()->orwhere('nombres', 'LIKE', '%' . $name . '%');
            }
        });
    }
}
