<?php

namespace bolsatrabajo;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tipocontrato extends Model
{
    use SoftDeletes;
    protected $table = 'tipocontrato';
    protected $dates = ['deleted_at'];
    protected $fillable = ['nombre'];

    public function scopelistar($query, $name)
    {
        return $query->where(function($subquery) use($name)
        {
            if (!is_null($name)) {
                $subquery->where('nombre', 'LIKE', '%'.$name.'%');
            }
        })
            ->orderBy('nombre', 'ASC');
    }
}
