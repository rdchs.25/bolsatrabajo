<?php

namespace bolsatrabajo;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Colegio extends Model
{
    use SoftDeletes;

    protected $table = 'colegio';
    protected $dates = ['deleted_at'];

    public function distrito()
    {
        return $this->belongsTo('bolsatrabajo\Distrito');
    }

    public function formacionacademicas()
    {
        return $this->hasMany('bolsatrabajo\Formacionacademica');
    }

    /**
     * Método para listar
     * @param  model $query modelo
     * @param  string $name nombre
     * @return sql        sql
     */
    public function scopelistar($query, $nombre)
    {
        return $query->where(function ($subquery) use ($nombre) {
            if (!is_null($nombre)) {
                $subquery->where('nombre', 'LIKE', '%' . $nombre . '%');
            }
        })
            ->orderBy('nombre', 'ASC');
    }
}
