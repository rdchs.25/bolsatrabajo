<?php

namespace bolsatrabajo;

use Illuminate\Database\Eloquent\Model;

class Tipousuariousuario extends Model
{
    protected $table = 'tipousuario_usuario';

    public function usuario()
    {
        return $this->belongsTo('bolsatrabajo\User');
    }
}
