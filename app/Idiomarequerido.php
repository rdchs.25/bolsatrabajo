<?php

namespace bolsatrabajo;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Idiomarequerido extends Model
{
    use SoftDeletes;
    protected $table = 'idioma_ofertalaboral';
    protected $dates = ['deleted_at'];
    protected $fillable = ['lectura','escritura','conversacion','ofertalaboral_id','idioma_id'];

    public function idioma(){
        return $this->belongsTo('bolsatrabajo\Idioma');
    }


    public function scopelistar($query, $name)
    {
        return $query->where(function($subquery) use($name)
        {
            if (!is_null($name)) {
                $subquery->where('nombre', 'LIKE', '%'.$name.'%');
            }
        })
            ->orderBy('nombre_distrito', 'ASC');
    }

   


}
