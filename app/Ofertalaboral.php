<?php

namespace bolsatrabajo;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ofertalaboral extends Model
{
    use SoftDeletes;
    protected $table = 'ofertalaboral';
    protected $dates = ['deleted_at'];
    protected $fillable = ['titulo','nrovacante','cargolaboral_id','arealaboral_id','descripcion',
                            'jornadalaboral_id','duracioncontrato','fechainiciocontrato','fechafincontrato',
                            'horario','monedapago','salario','mostrarsalario','comentariosalario','cambioresidencia',
                            'disponibilidadviajar','nivelestudio_id','experiencialaboral','anioexperiencia','estudiominimo',
                            'requiereidioma'];

    public function tipocontrato()
    {
        return $this->belongsTo('bolsatrabajo\Tipocontrato');
    } 

    public function empleador()
    {
        return $this->belongsTo('bolsatrabajo\Empleador');
    } 

    public function escuela()
    {
        return $this->belongsTo('bolsatrabajo\Escuela');
    } 

    public function jornadalaboral()
    {
        return $this->belongsTo('bolsatrabajo\Jornadalaboral');
    }

    public function nivelestudio()
    {
        return $this->belongsTo('bolsatrabajo\Nivelestudio');
    }

    public function ofertaslaboralespersona()
    {
        return $this->hasMany('bolsatrabajo\Ofertalaboralpersona','ofertalaboral_id');
    }

    public function idiomas()
    {
        return $this->hasMany('bolsatrabajo\Idiomarequerido','ofertalaboral_id');
    }

    public function discapacidades()
    {
        return $this->hasMany('bolsatrabajo\Discapacidadrequerida','ofertalaboral_id');
    } 

    public function conocimientosinformaticos()
    {
        return $this->hasMany('bolsatrabajo\Conocimientoinformaticorequerido','ofertalaboral_id');
    }

    public function scopelistar($query, $name,$arealaboral,$departamento)
    {
        return $query->where(function($subquery) use($name,$arealaboral,$departamento)
        {
            if (!is_null($name)) {
                $subquery->where('titulo', 'LIKE', '%'.$name.'%');
            }
            if (!is_null($arealaboral)) {
                $subquery->where('arealaboral_id', '=', $arealaboral);
            }
            if (!is_null($departamento)) {
                $subquery->where('departamento.id', '=', $departamento);
            }
        })
            ->orderBy('titulo', 'ASC');
    }


}
