<?php

namespace bolsatrabajo;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Formacionacademica extends Model
{
    use SoftDeletes;
    protected $table = 'formacionacademica';
    protected $dates = ['deleted_at'];

    public function nivelestudio()
    {
        return $this->belongsTo('bolsatrabajo\Nivelestudio');
    }

    public function universidad()
    {
        return $this->belongsTo('bolsatrabajo\Universidad');
    }

    public function colegio()
    {
        return $this->belongsTo('bolsatrabajo\Colegio');
    }

}
