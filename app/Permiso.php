<?php

namespace bolsatrabajo;

use Iatstuti\Database\Support\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Permiso extends Model
{
    use SoftDeletes;
    use CascadeSoftDeletes;

    protected $table = 'permiso';
    protected $dates = ['deleted_at'];

    protected $cascadeDeletes = ['permiso_tipousuario'];

    public function cascadadelete()
    {
        return $this->cascadeDeletes;
    }

    public function mostrar()
    {
        return 'nombre';
    }


    /**
     * The Tipousuario that belong to the permiso.
     */
    public function tipousuario()
    {
        return $this->belongsToMany('bolsatrabajo\Tipousuario');
    }

    public function opcionmenu()
    {
        return $this->belongsTo('bolsatrabajo\Opcionmenu');
    }

    /**
     * Relacion con la tabla intermedia permisotipousuario
     */
    public function permiso_tipousuario()
    {
        return $this->hasMany('bolsatrabajo\Permisotipousuario');
    }
}
