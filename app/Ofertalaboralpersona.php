<?php

namespace bolsatrabajo;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ofertalaboralpersona extends Model
{
    use SoftDeletes;
    protected $table = 'ofertalaboralpersona';
    protected $dates = ['deleted_at'];
    protected $fillable = ['persona_id','nivelproceso_id','ofertalaboral_id'];

     public function persona()
    {
        return $this->belongsTo('bolsatrabajo\Persona', 'persona_id');
    }
    

    public function ofertalaboral()
    {
        return $this->belongsTo('bolsatrabajo\Ofertalaboral','ofertalaboral_id');
    }

    public function scopelistar($query, $name)
    {
        return $query->where(function($subquery) use($name)
        {
            if (!is_null($name)) {
                $subquery->ofertalaboral()->where('titulo', 'LIKE', '%'.$name.'%');
            }
        });
    }

}
