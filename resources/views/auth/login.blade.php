@extends('layouts.layoutlogin')

@section('contentlogin')
    <body class="hold-transition login-page">
    <div class="login-box">
        <div class="login-logo">
            <a href="/"><b>IC</b>hamba</a>
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body">
            @if(session()->has('authmensaje'))
                <div class="alert alert-info text-center"> {{ session('authmensaje') }}</div>
            @else
                <p class="login-box-msg">Accede para inicar sesión</p>

            @endif

            <form method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}
                @if ($errors->has('failed'))
                    <div class="has-feedback has-error">
                        <span class="help-block">{{ $errors->first('failed') }}</span>
                    </div>
                @endif

                <div class="form-group has-feedback {!!  $errors->has('usuario') ? 'has-error' : '' !!} ">
                    <input class="form-control" id="usuario" name="usuario" placeholder="Usuario"
                           value="{{ old('usuario') }}" autofocus>
                    @if ($errors->has('usuario'))
                        <span class="help-block">{{ $errors->first('usuario') }}</span>
                    @endif

                </div>
                <div class="form-group has-feedback {!!  $errors->has('password') ? 'has-error' : '' !!} ">
                    <input type="password" id="password" name="password" class="form-control" placeholder="Password">
                    @if ($errors->has('password'))
                        <span class="help-block">{{ $errors->first('password') }}</span>
                    @endif
                </div>
                <div class="form-group has-feedback {!!  $errors->has('tipousuario_id') ? 'has-error' : '' !!} ">
                    {!! Form::select('tipousuario_id', $cboTipousuario, null, array('class' => 'form-control input-xs', 'id' => 'tipousuario_id')) !!}
                    @if ($errors->has('tipousuario_id'))
                        <span class="help-block">Debe seleccionar un usuario</span>
                    @endif
                </div>

                <div class="row">
                    <div class="col-xs-8">
                        <div class="checkbox icheck">
                            <label>
                                <input type="checkbox" name="remember"
                                       id="remember" {{ old('remember') ? 'checked' : '' }}>Recordar cuenta
                            </label>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Ingresar</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>

            <a href="{{ route('password.request') }}">Olvidé mi contraseña</a><br>
            <a href="{{ route('register') }}" class="text-center">Si no esta registrado, regístrate aquí</a>

        </div>
        <!-- /.login-box-body -->
    </div>
    <!-- /.login-box -->
    </body>
@endsection