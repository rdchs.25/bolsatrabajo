@extends('layouts.layoutlogin')

@section('contentlogin')
    <body class="hold-transition register-page">
    <div class="register-box">
        <div class="register-logo">
            <a href="/"><b>IC</b>hamba</a>
        </div>

        <div class="register-box-body">
            <p class="login-box-msg">Registra un nuevo usuario</p>

            <form method="POST" action="{{ route('register') }}">
                {{ csrf_field() }}
                <div class="form-group has-feedback {{ $errors->has('nombre') ? ' has-error' : '' }}">
                    <input id="nombre" name="nombre" class="form-control" value="{{ old('nombre') }}"
                           placeholder="Nombre completo" autofocus>
                    @if ($errors->has('nombre'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nombre') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
                    <input type="email" id="email" name="email" class="form-control" placeholder="Email"
                           value="{{ old('email') }}">
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group has-feedback {{ $errors->has('usuario') ? ' has-error' : '' }}">
                    <input id="usuario" name="usuario" class="form-control" placeholder="Usuario"
                           value="{{ old('usuario') }}">
                    @if ($errors->has('usuario'))
                        <span class="help-block">
                            <strong>{{ $errors->first('usuario') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group has-feedback {{ $errors->has('password') ? ' has-error' : '' }}">
                    <input type="password" name="password" id="password" class="form-control" placeholder="Contraseña">
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group has-feedback">
                    <input type="password" id="password-confirm" name="password_confirmation" class="form-control"
                           placeholder="Vuelva escribir la contraseña">

                </div>
                <div class="form-group has-feedback {{ $errors->has('g-recaptcha-response') ? ' has-error' : '' }}">
                    {!! Recaptcha::render() !!}
                    @if ($errors->has('g-recaptcha-response'))
                        <span class="help-block">
                            <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                        </span>
                    @endif
                </div >
                <div class="row">
                    <div class="col-xs-8 {{ $errors->has('termacept') ? ' has-error' : '' }}">
                        <div class="checkbox icheck">
                            <label>
                                <input type="checkbox" id="termacept" name="termacept" {{ old('termacept') ? 'checked' : '' }}> Acepto todos los <a href="#">términos</a>
                            </label>
                            @if ($errors->has('termacept'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('termacept') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Registrar</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>

            <a href="{{ route('login') }}" class="text-center">Ya tengo una cuenta</a>
        </div>
        <!-- /.form-box -->
    </div>
    <!-- /.register-box -->
    </body>
@endsection