<!DOCTYPE html>

<html>
<head>

{{--    <link rel="stylesheet" href="{{asset('template/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">--}}
{{--<link rel="stylesheet" href="{{asset('template/bower_components/iconpicker/bootstrap-iconpicker.css')}}">--}}


<body>
<button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-default">Launch Default Modal
</button>

<div class="modal fade" id="modal-default">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Default Modal</h4>
            </div>
            <div class="modal-body">
                <button class="btn btn-default" data-iconset="glyphicon" data-icon="glyphicon-camera"
                        role="iconpicker"></button>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
{{--    <script src="{{asset('template/bower_components/jquery/dist/jquery.min.js')}}"></script>--}}
{{--    <script src="{{asset('template/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>--}}
{{--<script src="{{asset('template/plugins/iconpicker/bootstrap-iconpicker-iconset-all.js')}}"></script>--}}
{{--<script src="{{asset('template/plugins/iconpicker/bootstrap-iconpicker.js')}}"></script>--}}


<!-- Prueba 2-->
<link rel="stylesheet" href="{{asset('template/plugins/iconpicker/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('template/plugins/iconpicker/css/font-awesome.min.css')}}">
<link rel="stylesheet" href="{{asset('template/plugins/iconpicker/css/fontawesome-iconpicker.min.css')}}">

<input class="form-control icp icp-auto action-create" value="fa-anchor" type="text"/>

<script src="{{asset('template/plugins/iconpicker/js/jquery-2.2.1.min.js')}}"></script>
    <script src="{{asset('template/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

{{--<script src="{{asset('template/plugins/iconpicker/js/bootstrap.min.js')}}"></script>--}}
<script src="{{asset('template/plugins/iconpicker/js/fontawesome-iconpicker.js')}}"></script>


<script type="text/javascript">
    $(function () {
        $('.action-destroy').on('click', function () {
            $.iconpicker.batch('.icp.iconpicker-element', 'destroy');
        });
        // Live binding of buttons
        $(document).on('click', '.action-placement', function (e) {
            $('.action-placement').removeClass('active');
            $(this).addClass('active');
            $('.icp-opts').data('iconpicker').updatePlacement($(this).text());
            e.preventDefault();
            return false;
        });

        $('.action-create').on('click', function () {
            $('.icp-auto').iconpicker();

            $('.icp-dd').iconpicker({
                //title: 'Dropdown with picker',
                //component:'.btn > i'
            });

            $('.icp-glyphs').iconpicker({
                title: 'Prepending glypghicons',
                icons: $.merge(['glyphicon-home', 'glyphicon-repeat', 'glyphicon-search',
                    'glyphicon-arrow-left', 'glyphicon-arrow-right', 'glyphicon-star'], $.iconpicker.defaultOptions.icons),
                fullClassFormatter: function (val) {
                    if (val.match(/^fa-/)) {
                        return 'fa ' + val;
                    } else {
                        return 'glyphicon ' + val;
                    }
                }
            });

            $('.icp-opts').iconpicker({
                title: 'With custom options',
                icons: ['fa-github', 'fa-heart', 'fa-html5', 'fa-css3'],
                selectedCustomClass: 'label label-success',
                mustAccept: true,
                placement: 'bottomRight',
                showFooter: true,
                // note that this is ignored cause we have an accept button:
                hideOnSelect: true,
                templates: {
                    footer: '<div class="popover-footer">' +
                    '<div style="text-align:left; font-size:12px;">Placements: \n\
    <a href="#" class=" action-placement">inline</a>\n\
    <a href="#" class=" action-placement">topLeftCorner</a>\n\
    <a href="#" class=" action-placement">topLeft</a>\n\
    <a href="#" class=" action-placement">top</a>\n\
    <a href="#" class=" action-placement">topRight</a>\n\
    <a href="#" class=" action-placement">topRightCorner</a>\n\
    <a href="#" class=" action-placement">rightTop</a>\n\
    <a href="#" class=" action-placement">right</a>\n\
    <a href="#" class=" action-placement">rightBottom</a>\n\
    <a href="#" class=" action-placement">bottomRightCorner</a>\n\
    <a href="#" class=" active action-placement">bottomRight</a>\n\
    <a href="#" class=" action-placement">bottom</a>\n\
    <a href="#" class=" action-placement">bottomLeft</a>\n\
    <a href="#" class=" action-placement">bottomLeftCorner</a>\n\
    <a href="#" class=" action-placement">leftBottom</a>\n\
    <a href="#" class=" action-placement">left</a>\n\
    <a href="#" class=" action-placement">leftTop</a>\n\
    </div><hr></div>'
                }
            }).data('iconpicker').show();


        }).trigger('click');


        // Events sample:
        // This event is only triggered when the actual input value is changed
        // by user interaction
        $('.icp').on('iconpickerSelected', function (e) {
            $('.lead .picker-target').get(0).className = 'picker-target fa-3x ' +
                e.iconpickerInstance.options.iconBaseClass + ' ' +
                e.iconpickerInstance.options.fullClassFormatter(e.iconpickerValue);
        });


    });
</script>


<!-- -->
</body>
</html>





