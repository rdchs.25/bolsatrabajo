<!-- jQuery 3 -->
<script src="{{asset('template/bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{asset('template/bower_components/jquery-ui/jquery-ui.min.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('template/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- Morris.js charts -->
<script src="{{asset('template/bower_components/raphael/raphael.min.js')}}"></script>
{{--<script src="{{asset('template/bower_components/morris.js/morris.min.js')}}"></script>--}}
<!-- Sparkline -->
<script src="{{asset('template/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js')}}"></script>
<!-- jvectormap -->
<script src="{{asset('template/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script src="{{asset('template/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
<!-- jQuery Knob Chart -->
<script src="{{asset('template/bower_components/jquery-knob/dist/jquery.knob.min.js')}}"></script>
<!-- daterangepicker -->
<script src="{{asset('template/bower_components/moment/min/moment.min.js')}}"></script>
<script src="{{asset('template/bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<!-- datepicker -->
<script src="{{asset('template/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{asset('template/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
<!-- Slimscroll -->
<script src="{{asset('template/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('template/bower_components/fastclick/lib/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('template/dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
{{--<script src="{{asset('template/dist/js/pages/dashboard.js')}}"></script>--}}
<!-- AdminLTE for demo purposes -->
<script src="{{asset('template/dist/js/demo.js')}}"></script>

<!-- DataTables -->
<script src="{{asset('template/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('template/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>

<!-- App -->
<script src="{{asset('app/js/funciones.js')}}"></script>


<!-- bootbox code -->
<script src="{{asset('template/build/js/bootbox.min.js')}}"></script>

<!-- Plugin Chosen-->
<script src="{{asset('template/plugins/chosen/chosen.jquery.js')}}"></script>

<!-- Plugin bloodhound (autocompletar)-->
<script src="{{asset('template/plugins/bloodhound/bloodhound.min.js')}}"></script>
<script src="{{asset('template/plugins/bloodhound/typeahead.bundle.min.js')}}"></script>


{{-- jquery.inputmask: para mascaras en cajas de texto --}}
{!! Html::script('template/plugins/input-mask/jquery.inputmask.js') !!}
{!! Html::script('template/plugins/input-mask/jquery.inputmask.extensions.js') !!}
{!! Html::script('template/plugins/input-mask/jquery.inputmask.date.extensions.js') !!}
{!! Html::script('template/plugins/input-mask/jquery.inputmask.numeric.extensions.js') !!}
{!! Html::script('template/plugins/input-mask/jquery.inputmask.phone.extensions.js') !!}
{!! Html::script('template/plugins/input-mask/jquery.inputmask.regex.extensions.js') !!}
@routes
@if (Route::has('login'))
    @auth
        <script type="text/javascript">
            $(document).ready(function () {
                var idpersona = '{!! Auth::user()->persona_id !!}';
                var idempleador = '{!! Auth::user()->empleador_id !!}';
                $('#perfil').click(function (e) {
                    if (idpersona !== '') {
                        modal(route('persona.edit', idpersona, 'NO'), 'Editar Persona');
                    } 
                    if (idempleador !== '') {
                        modal(route('empleador.edit', idempleador, 'NO'), 'Editar Empleador');
                    } 
                });
            });

        </script>
    @endauth
@endif



