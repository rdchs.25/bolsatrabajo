<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; 2014-<?php echo date("Y"); ?> <a href="http://udl.edu.pe">UDL</a>.</strong> Todos los derechos reservados.
</footer>