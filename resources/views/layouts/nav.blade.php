<header class="main-header">
    <!-- Logo -->
    @if (Route::has('login'))
        @auth
            <a href="/home" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"><b>I</b>CH</span>
                <!-- logo for regular state and mobile devices -->
                {{--<span class="logo-lg"><b>Admin</b>LTE</span>--}}
                <span class="logo-lg">@yield('title','IChamba')</span>
            </a>
            @else
                <a href="/" class="logo">
                    <span class="logo-mini"><b>I</b>CH</span>
                    <span class="logo-lg">@yield('title','IChamba')</span>
                </a>
            @endauth
        @endif

        <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>

                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- Notifications: style can be found in dropdown.less -->
                        @auth
                            @if(Auth::user()->superusuario ||  (isset(Auth::user()->tipousuarios[0]->nombre) &&  Auth::user()->tipousuarios[0]->nombre === 'Administrador'))
<!--                                --><?php // $userpendiente = bolsatrabajo\User::where('estado', '=', 'Pendiente')->get() ?>
                                <?php  $cantuserpendiente = bolsatrabajo\User::where('estado', '=', 'Pendiente')->get()->count() ?>
                                <li class="dropdown notifications-menu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-bell-o"></i>
                                        <span class="label label-danger" id="labelnotificaciones">{{ $cantuserpendiente }}</span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li class="header" id="headernotificaciones">Tiene {{ $cantuserpendiente }} notificaciones</li>
                                        <li>
                                            <!-- inner menu: contains the actual data -->
                                            <ul class="menu">
                                                <li>
                                                    <a href="javascript:void(0);" id="menunotificicaciones"
                                                       onclick="cargarRuta('{{route('usuario.index') }}','content')">
                                                        <i class="fa fa-users text-aqua"></i> {{ $cantuserpendiente }}
                                                        usuarios nuevos
                                                    </a>
                                                </li>

                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                            @endif
                        @endif
                    <!-- Tasks: style can be found in dropdown.less -->
                        @if (Route::has('login'))
                            @auth
                                @else
                                    <li class="dropdown user user-menu">
                                        <a href="{{ route('login') }}" class="dropdown-toggle">
                                            <span >Ingresar</span>
                                        </a>
                                    </li>
                                @endauth
                                @endauth
                                <!-- User Account: style can be found in dropdown.less -->
                                    <li class="dropdown user user-menu">
                                        @if (Route::has('login'))
                                            @auth
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    @if(Auth::user()->persona !== null)
                                                        <img src="{!! Storage::url(Auth::user()->persona->foto) !!}?img={{str_random(4)}}"
                                                             class="user-image"
                                                             alt="User Image">

                                                    @endif
                                                    @if(Auth::user()->empleador !== null)
                                                        <img src="{!! Storage::url(Auth::user()->empleador->logo) !!}?img={{str_random(4)}}"
                                                             class="user-image"
                                                             alt="User Image">
                                                    @endif
                                                    <span class="hidden-xs">{{ Auth::user()->nombre }}</span>
                                                </a>
                                                <ul class="dropdown-menu">
                                                    <!-- User image -->
                                                    <li class="user-header">
                                                        @if(Auth::user()->persona !== null)
                                                            <img src="{!! Storage::url(Auth::user()->persona->foto) !!}?img={{str_random(4)}}"
                                                                 class="img-circle"
                                                                 alt="User Image">

                                                        @endif
                                                        @if(Auth::user()->empleador !== null)
                                                            <img src="{!! Storage::url(Auth::user()->empleador->logo) !!}?img={{str_random(4)}}"
                                                                 class="img-circle"
                                                                 alt="User Image">
                                                        @endif
                                                        <p>
                                                            {{ Auth::user()->nombre }}
                                                            <small>{{ Auth::user()->created_at }}</small>
                                                        </p>
                                                    </li>
                                                    <!-- Menu Footer-->
                                                    <li class="user-footer">
                                                        <div class="pull-left">
                                                            <a href="javascript:void(0)"
                                                               {{--onclick="modal ('{!! Auth::user()->persona_id === null ? URL::route('persona.create','No') : URL::route('persona.edit','No') !!}',this);"--}}
                                                               id='perfil' class="btn btn-default btn-flat">Perfil</a>
                                                        </div>
                                                        <div class="pull-right">
                                                            <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"
                                                               class="btn btn-default btn-flat">Salir</a>
                                                            <form id="logout-form" action="{{ route('logout') }}"
                                                                  method="POST"
                                                                  style="display: none;">
                                                                {{ csrf_field() }}
                                                            </form>
                                                        </div>
                                                    </li>
                                                </ul>
                        @else
                           <li class="dropdown user user-menu"> 
                                <a href="{{ route('register') }}" class="dropdown-toggle">
                                <span >Registrate</span>
                                </a>
                            </li>
                            @endauth
                        @endif


                                    </li>
                                    <!-- Control Sidebar Toggle Button -->
                                    <li>
                                        <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                                    </li>
                    </ul>
                </div>
            </nav>
</header>

