<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Buscar...">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">Menú Principal</li>

            <li class="treeview active">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>Menú</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/"><i class="fa fa-circle-o"></i> Inicio</a></li>
                    <li><a style="cursor: pointer" onclick="cargarRuta('{{route('categoriamenu.index')}}','content')"><i
                                    class="fa fa-circle-o"></i>
                            Categoria Menú</a></li>
                    <li><a style="cursor: pointer" onclick="cargarRuta('{{route('opcionmenu.index')}}','content')"><i
                                    class="fa fa-circle-o"></i>
                            Opción Menú</a></li>
                    <li><a style="cursor: pointer" onclick="cargarRuta('{{route('tipousuario.index')}}','content')"><i
                                    class="fa fa-circle-o"></i>
                            Tipo Usuario</a></li>
                    {{--<li class="treeview">--}}
                    {{--<a href="#"><i class="fa fa-circle-o"></i> Level One--}}
                    {{--<span class="pull-right-container">--}}
                    {{--<i class="fa fa-angle-left pull-right"></i>--}}
                    {{--</span>--}}
                    {{--</a>--}}
                    {{--<ul class="treeview-menu">--}}
                    {{--<li><a href="#"><i class="fa fa-circle-o"></i> Level Two</a></li>--}}
                    {{--<li class="treeview">--}}
                    {{--<a href="#"><i class="fa fa-circle-o"></i> Level Two--}}
                    {{--<span class="pull-right-container">--}}
                    {{--<i class="fa fa-angle-left pull-right"></i>--}}
                    {{--</span>--}}
                    {{--</a>--}}
                    {{--<ul class="treeview-menu">--}}
                    {{--<li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>--}}
                    {{--<li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>--}}
                    {{--</ul>--}}
                    {{--</li>--}}
                    {{--</ul>--}}
                    {{--</li>--}}
                </ul>
            </li>

        </ul>
    </section>
    <!-- /.sidebar -->
</aside>


<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    {{--<section class="content-header">--}}
        {{--<h1>--}}
            {{--Principal--}}
            {{--<small>Panel de Control</small>--}}
        {{--</h1>--}}
        {{--<ol class="breadcrumb">--}}
            {{--<li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>--}}
            {{--<li class="active">Principal</li>--}}
        {{--</ol>--}}
        {{--<hr class="line">--}}
    {{--</section>--}}

    <!-- Main content -->
    <section class="content" id="content">

        <!-- Small boxes (Stat box) -->

        <div class="row">
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>Empresa</h3>

                        <p>Acceso</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-fw fa-institution"></i>
                    </div>
                    <a href="#" class="small-box-footer">+ info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>Empresa</h3>

                        <p>Registro</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="#" class="small-box-footer">+ info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>Acceso</h3>

                        <p>Profesionales</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                    <a href="#" class="small-box-footer">+ info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3>Oportunidades</h3>

                        <p>Oferta Laboral</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-pie-graph"></i>
                    </div>
                    <a href="#" class="small-box-footer">+ info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
        </div>
        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
            <!-- Aqui va el contenido -->
        </div>
        <!-- /.row (main row) -->


    </section>
</div>
