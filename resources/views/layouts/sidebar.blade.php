<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Buscar...">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">Menú Principal</li>

            <li class="treeview active">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>Menú</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                {!! $menu !!}
            </li>
            {{--<li class="treeview active">--}}
                {{--<a href="#">--}}
                    {{--<i class="fa fa-dashboard"></i> <span>Ubigeo</span>--}}
                    {{--<span class="pull-right-container">--}}
                        {{--<i class="fa fa-angle-left pull-right"></i>--}}
                    {{--</span>--}}
                {{--</a>--}}
                {{--<ul class="treeview-menu">--}}
                    {{--<li><a href="{!! action('DepartamentoController@index') !!}"><i class="fa fa-circle-o"></i> Departamento</a></li>--}}
                    {{--<li><a href="{!! action('ProvinciaController@index') !!}"><i class="fa fa-circle-o"></i> Provincia</a></li>--}}
                    {{--<li><a href="{!! action('DistritoController@index') !!}"><i class="fa fa-circle-o"></i> Distrito</a></li>--}}

                {{--</ul>--}}
            {{--</li>--}}
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>


<div class="content-wrapper">
    <!-- Content Header (Page header) -->
{{--<section class="content-header">--}}
{{--<h1>--}}
{{--Principal--}}
{{--<small>Panel de Control</small>--}}
{{--</h1>--}}
{{--<ol class="breadcrumb">--}}
{{--<li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>--}}
{{--<li class="active">Principal</li>--}}
{{--</ol>--}}
{{--<hr class="line">--}}
{{--</section>--}}

<!-- Main content -->

    <section class="content" id="content">

        <!-- Small boxes (Stat box) -->
        @auth
            @else
                <div class="row">
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-aqua">
                            <div class="inner">
                                <h3 class="hidden-xs">Empresa</h3>
                                <h4 class="hidden-sm hidden-md hidden-lg hidden-xl"><strong>Empresa</strong></h4>

                                <p>Acceso</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-fw fa-institution"></i>
                            </div>
                            <a href="{{ route('login') }}" class="small-box-footer">Ingresa <i
                                        class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-green">
                            <div class="inner">
                                <h3 class="hidden-xs">Empresa</h3>
                                <h4 class="hidden-sm hidden-md hidden-lg hidden-xl"><strong> Empresa</strong></h4>

                                <p>Registro</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-stats-bars"></i>
                            </div>
                            <a href="javascript:void(0)"
                               onclick="modal('{{URL::route('empleador.create','No')}}','Registra Empleador',this);"
                               class="small-box-footer">Ingresa <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-yellow">
                            <div class="inner">
                                <h3 class="hidden-xs">Profesionales</h3>
                                <h4 class="hidden-sm hidden-md hidden-lg hidden-xl"><b>Profesionales</b></h3>

                                <p>Acceso</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-person-add"></i>
                            </div>
                            <a href="{{ route('login') }}" class="small-box-footer">Ingresa <i
                                        class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                </div>
                <!-- /.row -->
                <!-- Main row -->
                <div class="row">
                    <!-- Aqui va el contenido -->
                </div>
            @endauth
            <!-- /.row (main row) -->
    </section>

</div>
