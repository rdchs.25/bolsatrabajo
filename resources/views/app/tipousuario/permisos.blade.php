{!! Form::open(array('route' => array('tipousuario.guardarpermisos', $tipousuario->id,'id','false'), 'id' => 'formMantenimiento'.$entidad)) !!}
<div class="form-group border">

    {!! $vista !!}
</div>
<div class="form-group text-center">
    {{--    {!! Form::button('Guardar', array('class' => 'btn btn-success btn-sm', 'id' => 'btnGuardar', 'onclick' => 'guardar(\''.$entidad.'\', this)')) !!}--}}
    {!! Form::button('Aceptar', array('class' => 'btn btn-success btn-sm', 'id' => 'btnCancelar'.$entidad, 'onclick' => 'cerrarModal((contadorModal - 1));')) !!}
</div>
{!! Form::close() !!}

<script type="text/javascript">
    $(document).ready(function () {
        init(IDFORMMANTENIMIENTO + '{!! $entidad !!}');
    });

    function submitFormpermiso(idformulario, permiso_id,action) {
        var parametros = $(idformulario).serialize();
        var accion = $(idformulario).attr('action').toLowerCase();
        accion = accion.split('id')[0]  + permiso_id+'/'+action;
        var metodo = $(idformulario).attr('method').toLowerCase();
        var respuesta = $.ajax({
            url: accion,
            type: metodo,
            data: parametros
        });
        return respuesta;
    }

    function guardarpermiso(entidad, permiso_id) {

        var idformulario = IDFORMMANTENIMIENTO + entidad;
        var respuesta = '';
        var listar = 'NO';
        var action = '';
        if($('#'+permiso_id).is(':checked') ) {
            action = true;
        }else{
            action = false;
        }

        var data = submitFormpermiso(idformulario, permiso_id,action);
        data.done(function (msg) {
            respuesta = msg;
        }).fail(function (xhr, textStatus, errorThrown) {
            respuesta = 'ERROR';
        }).always(function () {
            if (respuesta === 'ERROR') {
            } else {
                if (respuesta != 'OK') {
                    mostrarErrores(respuesta, idformulario, entidad);

                }
            }
        });
    }

</script>