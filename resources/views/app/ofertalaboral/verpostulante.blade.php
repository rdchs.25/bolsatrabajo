@if(count($lista) == 0)
    <table id="example1" class="table table-bordered table-striped table-condensed table-hover">
        <th >No se encontraron resultados</th>
    </table>

@else
   
    <table id="tblpostulantes" class="table table-bordered table-striped table-condensed table-hover">

        <thead>
        <tr>
            @foreach($cabecera as $key => $value)
                <th @if((int)$value['numero'] > 1) colspan="{{ $value['numero'] }}" @endif style="background-color: #3c8dbc ;color: white;">{!! $value['valor'] !!}</th>
            @endforeach
        </tr>
        </thead>
        <tbody>
        <?php
        $contador = 1;
        ?>
        @foreach ($lista as $key => $value)
            <tr>
                <td>{{ $contador }}</td>
                <td>{{ $value->persona->nrodocumento }}</td>
                <td>{{ $value->persona->apellidopaterno." ". $value->persona->materno . " " . $value->persona->nombres }}</td>
                <td>{{ $value->persona->telefono ."/". $value->persona->celular }}</td>
                 <td>{{ $value->persona->distrito !== null ? $value->persona->distrito->provincia->departamento->nombre.'/'. $value->persona->distrito->provincia->nombre.'/'. $value->persona->distrito->nombre: '' }}</td>
                <td>{{ $value->persona->direccion }}</td>
                <td>{{ $value->persona->escuela !== null ? $value->persona->escuela->nombre : '' }}</td>
                <td>{{ $value->persona->pais !== null ? $value->persona->pais->nombre : '' }}</td> 
            </tr>
            <?php
            $contador = $contador + 1;
            ?>
        @endforeach
        </tbody>

    </table>
@endif

<script type="text/javascript">
    $(document).ready(function () {
        configurarAnchoModal('900');
        init(IDFORMMANTENIMIENTO + '{!! $entidad !!}', 'M', '{!! $entidad !!}');
    });
    $(function () {
        $('#tblpostulantes').DataTable()
    
    });
</script>
