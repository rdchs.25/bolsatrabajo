<div id="divMensajeError{!! $entidad !!}"></div>
{!! Form::model($ofertalaboral, $formData) !!}
{!! Form::hidden('listar', $listar, array('id' => 'listar')) !!}
<fieldset>
    <legend><h6 style="color:saddlebrown"><b>Información Principal:</b></h6></legend>
    <div class="form-group">
        {!! Form::label('escuela_id', 'Escuela profesional:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
        <div class="col-lg-3 col-md-3 col-sm-3">
            {!! Form::select('escuela_id', $cboEscuela, NULL, array('class' => 'form-control input-xs', 'id' => 'escuela_id')) !!}
        </div>
        {!! Form::label('empleador_id', 'Empleador:', array('class' => 'col-lg-2 col-md-2 col-sm-2 control-label')) !!}
        <div class="col-lg-4 col-md-4 col-sm-4">
            {!! Form::select('empleador_id', $cboEmpleador, NULL, array('class' => 'form-control input-xs', 'id' => 'empleador_id')) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('titulo', 'Título:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
        <div class="col-lg-9 col-md-9 col-sm-9">
            {!! Form::text('titulo', null, array('class' => 'form-control input-xs', 'id' => 'titulo', 'placeholder' => 'Ingrese Título')) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('nrovacante', 'Nro Vacante:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
        <div class="col-lg-1 col-md-1 col-sm-1">
            {!! Form::number('nrovacante', null, array('class' => 'form-control input-xs', 'id' => 'nrovacante','min'=>'1','max'=>'100')) !!}
        </div>
        {!! Form::label('cargolaboral_id', 'Tipo de Cargo:', array('class' => 'col-lg-2 col-md-2 col-sm-2 control-label')) !!}
        <div class="col-lg-2 col-md-2 col-sm-2">
            {!! Form::select('cargolaboral_id', $cboCargolaboral, NULL, array('class' => 'form-control input-xs', 'id' => 'cargolaboral_id')) !!}
        </div>

        {!! Form::label('arealaboral_id', 'Área Laboral:', array('class' => 'col-lg-2 col-md-2 col-sm-2 control-label')) !!}
        <div class="col-lg-2 col-md-2 col-sm-2">
            {!! Form::select('arealaboral_id', $cboArealaboral, NULL, array('class' => 'form-control input-xs', 'id' => 'arealaboral_id')) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('descripcion', 'Descripción del Puesto:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
        <div class="col-lg-9 col-md-9 col-sm-9">
            {!! Form::textarea('descripcion', null, array('class' => 'form-control input-xs', 'rows'=>'3','id' => 'descripcion', 'placeholder' => 'Descripción')) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('fechapublicacion', 'Fecha Publicación:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
        <div class="col-lg-3 col-md-3 col-sm-3">
            {!! Form::date('fechapublicacion', null, array('class' => 'form-control input-xs', 'id' => 'fechapublicacion', 'placeholder' => 'Fecha')) !!}
        </div>
        {!! Form::label('vigenciaoferta', 'Vigencia (días):', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
        <div class="col-lg-3 col-md-3 col-sm-3">
            {!! Form::number('vigenciaoferta', null, array('class' => 'form-control input-xs', 'id' => 'vigenciaoferta', 'placeholder' => 'Vigencia (días)')) !!}
        </div>
    </div>

</fieldset>

<fieldset>
    <legend><h6 style="color:saddlebrown"><b>Contrato:</b></h6></legend>
    <div class="form-group">
        {!! Form::label('tipocontrato_id', 'Tipo de Contrato:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
        <div class="col-lg-5 col-md-5 col-sm-5">
            {!! Form::select('tipocontrato_id', $cboTipocontrato, NULL, array('class' => 'form-control input-xs', 'id' => 'tipocontrato_id')) !!}
        </div>
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('jornadalaboral_id', 'Jornada Laboral:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
        <div class="col-lg-5 col-md-5 col-sm-5">
            {!! Form::select('jornadalaboral_id', $cboJornadalaboral, NULL, array('class' => 'form-control input-xs', 'id' => 'jornadalaboral_id')) !!}
        </div>
        {!! Form::label('duracioncontrato', 'Duración de Contrato:', array('class' => 'col-lg-2 col-md-2 col-sm-2 control-label')) !!}
        <div class="col-lg-2 col-md-2 col-sm-2">
            {!! Form::text('duracioncontrato', null, array('class' => 'form-control input-xs', 'id' => 'duracioncontrato', 'placeholder' => 'Duración de Contrato')) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('fechainiciocontrato', 'Inicio Contrato:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
        <div class="col-lg-2 col-md-2 col-sm-2">
            {!! Form::date('fechainiciocontrato', null, array('class' => 'form-control input-xs', 'id' => 'fechainiciocontrato', 'placeholder' => 'Fecha')) !!}
        </div>
        {!! Form::label('fechafincontrato', 'Fin Contrato:', array('class' => 'col-lg-2 col-md-2 col-sm-2 control-label')) !!}
        <div class="col-lg-2 col-md-2 col-sm-2">
            {!! Form::date('fechafincontrato', null, array('class' => 'form-control input-xs', 'id' => 'fechafincontrato', 'placeholder' => 'Fecha')) !!}
        </div>
        {!! Form::label('horario', 'Horario:', array('class' => 'col-lg-1 col-md-1 col-sm-1 control-label')) !!}
        <div class="col-lg-2 col-md-2 col-sm-2">
            {!! Form::text('horario', null, array('class' => 'form-control input-xs', 'id' => 'horario', 'placeholder' => 'Horario')) !!}
        </div>
    </div>

    <div class="form-group">
        
    </div>

    <div class="form-group">
         {!! Form::label('salario', 'Salario:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
        <div class="col-lg-2 col-md-2 col-sm-2">
            {!! Form::number('salario', null, array('class' => 'form-control input-xs', 'id' => 'salario', 'placeholder' => 'Salario')) !!}
        </div>
        {!! Form::label('monedapago', 'Moneda de Pago:', array('class' => 'col-lg-2 col-md-2 col-sm-2 control-label')) !!}
        <div class="col-lg-3 col-md-3 col-sm-3">
            {!! Form::select('monedapago', $cboMonedapago, NULL, array('class' => 'form-control input-xs', 'id' => 'monedapago')) !!}
        </div>        
        <div class="col-lg-2 col-md-2 col-sm-2">
            {{--<input type="checkbox" id="chkmostrarsalario" name="chkmostrarsalario" value="1" >--}}
            {!! Form::checkbox('mostrarsalario',1, old('mostrarsalario'),array('id' => 'mostrarsalario')) !!}
            ¿Mostrar Salario?
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('comentariosalario', 'Comentario del Salario:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
        <div class="col-lg-9 col-md-9 col-sm-9">
            {!! Form::text('comentariosalario', null, array('class' => 'form-control input-xs', 'id' => 'comentariosalario', 'placeholder' => 'Comentario de Salario')) !!}
        </div>

    </div>

    <div class="form-group">
        {!! Form::label('', '', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
        <div class="col-lg-3 col-md-3 col-sm-3">
            {{--<input type="checkbox" value="1" id="chkcambioresidencia" name="chkcambioresidencia">--}}
            {!! Form::checkbox('cambioresidencia',1, old('cambioresidencia'),array('id' => 'cambioresidencia')) !!}
            <b>Con Cambio de Residencia</b>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6" id="chkdisponibilidadviajar" name="chkdisponibilidadviajar">
            {{--<input type="checkbox" value="1">--}}
            {!! Form::checkbox('disponibilidadviajar',1, old('disponibilidadviajar'),array('id' => 'disponibilidadviajar')) !!}
            <b>Con Disponibilidad de Viajar</b>
        </div>
    </div>

</fieldset>

<fieldset>
    <legend><h6 style="color:saddlebrown"><b>Requisitos:</b></h6></legend>
    <div class="form-group">
        {!! Form::label('nivelestudio_id', 'Estudios Mínimos:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
        <div class="col-lg-3 col-md-3 col-sm-3">
            {!! Form::select('nivelestudio_id', $cboNivelestudio, NULL, array('class' => 'form-control input-xs', 'id' => 'nivelestudio_id')) !!}
        </div>  
         {!! Form::label('experiencialaboral', 'Experiencia Laboral:', array('class' => 'col-lg-2 col-md-2 col-sm-2 control-label')) !!}
        <div class="col-lg-2 col-md-2 col-sm-2">
            {!! Form::select('experiencialaboral', $cboExperiencialaboral, NULL, array('class' => 'form-control input-xs', 'id' => 'experiencialaboral','onchange'=>'habilitar(this.value);')) !!}
        </div>
        <div class="col-lg-2 col-md-2 col-sm-2">
            {!! Form::number('anioexperiencia', null, array('class' => 'form-control input-xs', 'id' => 'anioexperiencia', 'placeholder' => 'Años','min'=>'1','max'=>'50')) !!}
        </div>      
    </div>

    


    <div class="form-group">
        <div class="col-lg-3 col-md-3 col-sm-3">
            {{--<input type="checkbox" value="1" id="chk_idiomaofertalaboral" name="chk_idiomaofertalaboral">--}}
            {!! Form::checkbox('requiereidioma',1, old('requiereidioma'),array('id' => 'requiereidioma','class'=>'checkbox_requiereidioma')) !!}
            <b>Requiere Idiomas:</b>
        </div>
        <div class="col-lg-9 col-md-9 col-sm-9">
            <button type="submit" class="btn btn-success btn-sm" id="add_row_idioma">(+) Agregar</button>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12" id="div_idiomaofertalaboral" name="div_idiomaofertalaboral">
            <table class="table table-bordered table-hover table-sortable" id="tab_logic">
                <thead>
                <tr>

                    <th class="text-center">
                        Idioma
                    </th>
                    <th class="text-center">
                        Lectura
                    </th>
                    <th class="text-center">
                        Escritura
                    </th>
                    <th class="text-center">
                        Conversación
                    </th>
                </tr>
                </thead>
                <tbody>
                @if(isset($idiomarequerido))
                    <tr id='addridioma0' data-id="0" class="hidden">
                        {{--{{ dd($value->idioma_id,$cboIdioma) }}--}}
                        {{--@if(in_array($idiomarequerid->id,$cboIdioma) )--}}
                        {{--{{dd($idiomarequerid->id) }}--}}
                        {{--@else--}}
                        {{--{{dd(null)}}--}}
                        {{--@endif--}}
                        <td data-name="idioma_id">
                            {!! Form::select('idioma_id', $cboIdioma, NULL, array('class' => 'form-control input-xs','id'=>'idioma_id')) !!}
                        </td>
                        <td data-name="lecturaidioma" class="align:center">
                            {!! Form::select('lecturaidioma', $cboLecturaidioma,NULL, array('class' => 'form-control input-xs','id'=>'lecturaidioma')) !!}
                        </td>
                        <td data-name="escrituraidioma" class="align:center">
                            {!! Form::select('escrituraidioma', $cboEscrituraidioma,NULL, array('class' => 'form-control input-xs','id'=>'escrituraidioma')) !!}
                        </td>
                        <td data-name="conversacionidioma" class="align:center">
                            {!! Form::select('conversacionidioma', $cboConversacionidioma, NULL, array('class' => 'form-control input-xs','id'=>'conversacionidioma')) !!}
                        </td>
                        <td data-name="id_idiomarequerido" class="align:center">
                            {{ Form::hidden('id_idiomarequerido', NULL, array('id' => 'id_idiomarequerido')) }}
                        </td>
                        <td data-name="id_deleteidioma" class="align:center">
                            <button class='btn btn-danger glyphicon glyphicon-remove row-remove-idioma'></button>
                        </td>
                    </tr>
                    <div style="display: none;">
                        {{$numeroinput=1 }}
                    </div> 
                    @foreach($idiomarequerido as $key => $value)
                        <tr id='addridioma0' data-id="{{ $numeroinput++ }}" class="idi{{$value->id}}">
                            {{--{{ dd($value->idioma_id,$cboIdioma) }}--}}
                            {{--@if(in_array($idiomarequerid->id,$cboIdioma) )--}}
                            {{--{{dd($idiomarequerid->id) }}--}}
                            {{--@else--}}
                            {{--{{dd(null)}}--}}
                            {{--@endif--}}
                            <td data-name="idioma_id">
                                {!! Form::select('idioma_id'.$i=$key+1, $cboIdioma, in_array($value->idioma->nombre,$cboIdioma) ? $value->idioma_id : dd($value->idioma_id,$cboIdioma), array('class' => 'form-control input-xs clsidiomaid','id'=>'idioma_id'.$i=$key+1)) !!}
                            </td>
                            <td data-name="lecturaidioma" class="align:center">
                                {!! Form::select('lecturaidioma'.$i=$key+1, $cboLecturaidioma,$value->lectura, array('class' => 'form-control input-xs','id'=>'lecturaidioma'.$i=$key+1)) !!}
                            </td>
                            <td data-name="escrituraidioma" class="align:center">
                                {!! Form::select('escrituraidioma'.$i=$key+1, $cboEscrituraidioma,$value->escritura, array('class' => 'form-control input-xs','id'=>'escrituraidioma'.$i=$key+1)) !!}
                            </td>
                            <td data-name="conversacionidioma" class="align:center">
                                {!! Form::select('conversacionidioma'.$i=$key+1, $cboConversacionidioma, $value->conversacion, array('class' => 'form-control input-xs','id'=>'conversacionidioma'.$i=$key+1)) !!}
                            </td>
                            <td data-name="id_idiomarequerido" class="align:center">
                                {{ Form::hidden('id_idiomarequerido'.$i=$key+1, $value->id, array('id' => 'id_idiomarequerido'.$i=$key+1)) }}
                            </td>

                            <td data-name="id_deleteidioma" class="align:center">
                                <button class='btn btn-danger glyphicon glyphicon-remove row-remove-idioma' value="{{$value->id}}"></button>
                            </td>

                        </tr>
                    @endforeach
                @else
                    <tr id='addridioma0' data-id="0" class="hidden">
                        {{--{{ dd($value->idioma_id,$cboIdioma) }}--}}
                        {{--@if(in_array($idiomarequerid->id,$cboIdioma) )--}}
                        {{--{{dd($idiomarequerid->id) }}--}}
                        {{--@else--}}
                        {{--{{dd(null)}}--}}
                        {{--@endif--}}
                        <td data-name="idioma_id">
                            {!! Form::select('idioma_id', $cboIdioma, NULL, array('class' => 'form-control input-xs','id'=>'idioma_id')) !!}
                        </td>
                        <td data-name="lecturaidioma" class="align:center">
                            {!! Form::select('lecturaidioma', $cboLecturaidioma,NULL, array('class' => 'form-control input-xs','id'=>'lecturaidioma')) !!}
                        </td>
                        <td data-name="escrituraidioma" class="align:center">
                            {!! Form::select('escrituraidioma', $cboEscrituraidioma,NULL, array('class' => 'form-control input-xs','id'=>'escrituraidioma')) !!}
                        </td>
                        <td data-name="conversacionidioma" class="align:center">
                            {!! Form::select('conversacionidioma', $cboConversacionidioma, NULL, array('class' => 'form-control input-xs','id'=>'conversacionidioma')) !!}
                        </td>
                        <td data-name="id_deleteidioma" class="align:center">
                            <button class='btn btn-danger glyphicon glyphicon-remove row-remove-idioma'></button>
                        </td>
                    </tr>
                @endif

                </tbody>
            </table>


        </div>
    </div>

    <div class="form-group">
        <div class="col-lg-3 col-md-3 col-sm-3">
            {{--<input type="checkbox" value="1" id="chk_softwareofertalaboral" name="chk_softwareofertalaboral">--}}
            {!! Form::checkbox('requieresoftware',1, old('requieresoftware'),array('id' => 'requieresoftware','class'=>'checkbox_requieresoftware')) !!}
            <b>Requiere Software:</b>
        </div>
        <div class="col-lg-9 col-md-9 col-sm-9">
            <button type="submit" class="btn btn-success btn-sm" id="add_row_software">(+) Agregar</button>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12" id="div_softwareofertalaboral" name="div_softwareofertalaboral">
            <table class="table table-bordered table-hover table-sortable" id="tab_logic_software">
                <thead>
                <tr>
                    <th class="text-center">
                        Software
                    </th>
                    <th class="text-center">
                        Nivel
                    </th>
                </tr>
                </thead>
                <tbody>
                @if(isset($softwarerequerido))
                    <tr id='addrsoftware0' data-id="0" class="hidden">
                        <td data-name="software_id">
                            {!! Form::text('software_id', NULL, array('class' => 'form-control input-xs', 'id' => 'software_id', 'placeholder' => 'Software')) !!}
                        </td>
                        <td data-name="nivelsoftware" class="align:center">
                            {!! Form::select('nivelsoftware', $cboNivelsoftware, NULL, array('class' => 'form-control input-xs', 'id' => 'nivelsoftware')) !!}
                        </td>
                        <td data-name="id_softwarerequerido" class="align:center">
                            {{ Form::hidden('id_softwarerequerido', NULL, array('id' => 'id_softwarerequerido')) }}
                        </td>
                        <td data-name="id_deletesoftware" class="align:center">
                            <button class='btn btn-danger glyphicon glyphicon-remove row-remove-software'></button>
                        </td>
                    </tr>
                    <div style="display: none;">
                        {{ $numeroinput=1 }}
                    </div>
                    @foreach($softwarerequerido as $key => $value)
                        <tr id='addrsoftware0' data-id="{{ $numeroinput++ }}" class="ids{{$value->id}}">
                            {{--                    {{dd($value->id)}}--}}
                            <td data-name="software_id">
                                {!! Form::text('software_id'.$s=$key+1, $value->nombre, array('class' => 'form-control input-xs', 'id' => 'software_id'.$s=$key+1, 'placeholder' => 'Software')) !!}
                            </td>
                            <td data-name="nivelsoftware" class="align:center">
                                {!! Form::select('nivelsoftware'.$s=$key+1, $cboNivelsoftware, $value->nivel, array('class' => 'form-control input-xs', 'id' => 'nivelsoftware'.$s=$key+1)) !!}
                            </td>
                            <td data-name="id_softwarerequerido" class="align:center">
                                {{ Form::hidden('id_softwarerequerido'.$s=$key+1, $value->id, array('id' => 'id_softwarerequerido'.$s=$key+1)) }}
                            </td>

                            <td data-name="id_deletesoftware" class="align:center">
                                <button class='btn btn-danger glyphicon glyphicon-remove row-remove-software' value="{{$value->id}}"></button>
                            </td>
                        </tr>

                    @endforeach
                @else
                    <tr id='addrsoftware0' data-id="0" class="hidden">
                        <td data-name="software_id">
                            {!! Form::text('software_id', NULL, array('class' => 'form-control input-xs', 'id' => 'software_id', 'placeholder' => 'Software')) !!}
                        </td>
                        <td data-name="nivelsoftware" class="align:center">
                            {!! Form::select('nivelsoftware', $cboNivelsoftware, NULL, array('class' => 'form-control input-xs', 'id' => 'nivelsoftware')) !!}
                        </td>
                        <td data-name="id_deletesoftware" class="align:center">
                            <button class='btn btn-danger glyphicon glyphicon-remove row-remove-software'></button>
                        </td>
                    </tr>
                @endif

                </tbody>
            </table>


        </div>
    </div>

    <div class="form-group">
        {!! Form::label('descripcionrequisitos', 'Requisitos:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
        <div class="col-lg-9 col-md-9 col-sm-9">
            {!! Form::textarea('descripcionrequisitos', null, array('class' => 'form-control input-xs', 'rows'=>'3','id' => 'descripcionrequisitos', 'placeholder' => 'Descripción de Requisitos')) !!}
        </div>
    </div>

    <div class="form-group">
        <div class="col-lg-3 col-md-3 col-sm-3">
            {{--<input type="checkbox" value="1" id="chk_vehiculoofertalaboral" name="chk_vehiculoofertalaboral">--}}
            {!! Form::checkbox('vehiculopropio',1, old('vehiculopropio'),array('id' => 'vehiculopropio')) !!}
            <b>¿Vehículo Propio?</b>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-4">
            {{--<input type="checkbox" value="1" id="chk_licenciaofertalaboral" name="chk_licenciaofertalaboral">--}}
            {!! Form::checkbox('tienelicencia',1, old('tienelicencia'),array('id' => 'tienelicencia')) !!}
            <b>¿Licencia de Conducir?</b>
        </div>
        <div class="col-lg-5 col-md-5 col-sm-5">
            {!! Form::text('tipolicencia', NULL, array('class' => 'form-control input-xs', 'id' => 'tipolicencia', 'placeholder' => 'Tipo de Licencia de Conducir')) !!}
        </div>
    </div>


</fieldset>

<fieldset>
    <legend><h6 style="color:saddlebrown"><b>Clasificación:</b></h6></legend>
    <div>
        {{--<input type="checkbox" value="1" id="chk_extranjeroofertalaboral" name="chk_extranjeroofertalaboral">--}}
        {!! Form::checkbox('ofertaextranjero',1, old('ofertaextranjero'),array('id' => 'ofertaextranjero')) !!}
        <b>Es una oferta para el extranjero</b>
    </div>
    <div class="form-group" id="div_extranejeroofertalaboral" name="div_extranejeroofertalaboral">
        <div class="col-lg-6 col-md-6 col-sm-6">
            {!! Form::select('pais_id', $cboPais, NULL, array('class' => 'form-control input-xs', 'id' => 'pais_id','onchange'=>'habilitarpais(this.value);')) !!}
        </div>

        <div class="col-lg-6 col-md-6 col-sm-6">
            {!! Form::text('direccionextranjero', null, array('class' => 'form-control input-xs', 'id' => 'direccionextranjero', 'placeholder' => 'Dirección en el Extranjero')) !!}
        </div>
    </div>
    <div class="form-group">
        <div class="col-lg-3 col-md-3 col-sm-3">
            {{--<input type="checkbox" value="1" id="s">--}}
            {!! Form::checkbox('admitediscapacidad',1, old('publicar'),array('id' => 'admitediscapacidad','class'=>'checkbox_requierediscapacidad')) !!}
            <b>Admite discapacidad:</b>
        </div>
        <div class="col-lg-9 col-md-9 col-sm-9">
            <button type="submit" class="btn btn-success btn-sm" id="add_row_discapacidad">(+) Agregar
            </button>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12" id="div_discapacidadofertalaboral"
             name="div_discapacidadofertalaboral">
            <table class="table table-bordered table-hover table-sortable" id="tab_logic_discapacidad">
                <thead>
                <tr>
                    <th class="text-center">
                        Tipo de Discapacidad
                    </th>
                    <th class="text-center">
                        Discapacidad
                    </th>
                </tr>
                </thead>
                <tbody>
                @if(isset($discapacidadrequerida))
                    <tr id='addrdiscapacidad0' data-id="0" class="hidden">
                        <td data-name="tipodiscapacidad_id">
                            {!! Form::select('tipodiscapacidad_id', $cboTipodiscapacidad,$tipodiscapacidad->id, array('class' => 'form-control input-xs', 'id' => 'tipodiscapacidad_id','onchange' => 'mostrarDiscapacidades(\''.URL::route('discapacidad.cbodiscapacidad').'\',\''.$entidad.'\', \'M\',this.name)')) !!}
                        </td>
                        <td data-name="discapacidad_id" class="align:center">
                            {!! Form::select('discapacidad_id', $cboDiscapacidad, $discapacidad->id, array('class' => 'form-control input-xs','id' => 'discapacidad_id')) !!}
                        </td>
                        <td data-name="id_discapacidadrequerida" class="align:center">
                            {{ Form::hidden('id_discapacidadrequerida', NULL, array('id' => 'id_discapacidadrequerida')) }}
                        </td>
                        <td data-name="id_deletediscapacidad" class="align:center">
                            <button class='btn btn-danger glyphicon glyphicon-remove row-remove-discapacidad'></button>
                        </td>
                    </tr>
                    <div style="display: none;">
                        {{ $numeroinput=1 }}
                    </div>
                    @foreach($discapacidadrequerida as $key => $value)
                        <tr id='addrdiscapacidad0' data-id="{{ $numeroinput++ }}" class="idd{{$value->id}}">
                            <td data-name="tipodiscapacidad_id">
                                {!! Form::select('tipodiscapacidad_id'.$d=$key+1, $cboTipodiscapacidad,in_array($value->discapacidad->tipodiscapacidad->nombre,$cboTipodiscapacidad) ? $value->discapacidad->tipodiscapacidad_id : dd($value->discapacidad->tipodiscapacidad->nombre), array('class' => 'form-control input-xs', 'id' => 'tipodiscapacidad_id'.$d=$key+1,'onchange' => 'mostrarDiscapacidades(\''.URL::route('discapacidad.cbodiscapacidad').'\',\''.$entidad.'\', \'M\',this.name)')) !!}
                            </td>
                            <td data-name="discapacidad_id" class="align:center">
                                {!! Form::select('discapacidad_id'.$d=$key+1, $cboDiscapacidad, $value->discapacidad_id, array('class' => 'form-control input-xs','id' => 'discapacidad_id'.$d=$key+1)) !!}
                            </td>
                            <td data-name="id_discapacidadrequerida" class="align:center">
                                {{ Form::hidden('id_discapacidadrequerida'.$d=$key+1, $value->id, array('id' => 'id_discapacidadrequerida'.$d=$key+1)) }}
                            </td>
                            <td data-name="id_deletediscapacidad" class="align:center">
                                <button class='btn btn-danger glyphicon glyphicon-remove row-remove-discapacidad' value="{{$value->id}}"></button>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr id='addrdiscapacidad0' data-id="0" class="hidden">
                        <td data-name="tipodiscapacidad_id">
                            {!! Form::select('tipodiscapacidad_id', $cboTipodiscapacidad,$tipodiscapacidad->id, array('class' => 'form-control input-xs', 'id' => 'tipodiscapacidad_id','onchange' => 'mostrarDiscapacidades(\''.URL::route('discapacidad.cbodiscapacidad').'\',\''.$entidad.'\', \'M\',this.name)')) !!}
                        </td>
                        <td data-name="discapacidad_id" class="align:center">
                            {!! Form::select('discapacidad_id', $cboDiscapacidad, $discapacidad->id, array('class' => 'form-control input-xs','id' => 'discapacidad_id')) !!}
                        </td>
                        <td data-name="id_deletediscapacidad" class="align:center">
                            <button class='btn btn-danger glyphicon glyphicon-remove row-remove-discapacidad'></button>
                        </td>
                    </tr>
                @endif
                </tbody>
            </table>


        </div>
    </div>


    <div class="form-group">
        {!! Form::label('observacionoferta', 'Observación:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
        <div class="col-lg-9 col-md-9 col-sm-9">
            {!! Form::textarea('observacionoferta', null, array('class' => 'form-control input-xs', 'rows'=>'2','id' => 'observacionoferta', 'placeholder' => 'Observación de la oferta')) !!}
        </div>
    </div>

</fieldset>

<fieldset>
    <legend><h6 style="color:saddlebrown"><b>Proceso de Candidatura:</b></h6></legend>

    <div class="form-group">
        <div class="col-lg-3 col-md-3 col-sm-3">
            {{--<input type="checkbox" value="1" id="chk_softwareofertalaboral" name="chk_softwareofertalaboral">--}}
            {!! Form::checkbox('requierenivelproceso',1, true,array('id' => 'requierenivelproceso','class'=>'checkbox_requierenivelproceso','disabled')) !!}
            <b>Nivel de Proceso:</b>
        </div>
        <div class="col-lg-9 col-md-9 col-sm-9">
            <button type="submit" class="btn btn-success btn-sm" id="add_row_nivelproceso">(+) Agregar</button>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12" id="div_nivelprocesoofertalaboral" name="div_nivelprocesoofertalaboral">
            <table class="table table-bordered table-hover table-sortable" id="tab_logic_nivelproceso">
                <thead>
                <tr>
                    <th class="text-center">
                        Nombre
                    </th>
                    <th class="text-center">
                        Prioridad
                    </th>
                </tr>
                </thead>
                <tbody>
                @if(isset($nivelproceso))
                    <tr id='addrnivelproceso0' data-id="0" class="hidden">
                        <td data-name="nivelproceso_id">
                            {!! Form::text('nivelproceso_id', NULL, array('class' => 'form-control input-xs', 'id' => 'nivelproceso_id', 'placeholder' => 'Descripción del Proceso')) !!}
                        </td>
                        <td data-name="prioridadnivelproceso" class="align:center">
                            {!! Form::number('prioridadnivelproceso', NULL, array('class' => 'form-control input-xs', 'id' => 'prioridadnivelproceso', 'placeholder' => 'Prioridad del Proceso')) !!}
                        </td>
                        <td data-name="id_nivelprocesorequerido" class="align:center">
                            {{ Form::hidden('id_nivelprocesorequerido', NULL, array('id' => 'id_nivelprocesorequerido')) }}
                        </td>
                        <td data-name="id_deletenivelproceso" class="align:center">
                            <button class='btn btn-danger glyphicon glyphicon-remove row-remove-nivelproceso'></button>
                        </td>
                    </tr>
                    <div style="display: none;">
                        {{ $numeroinput=1 }}
                    </div>
                    @foreach($nivelproceso as $key => $value)
                        <tr id='addrnivelproceso0' data-id="{{ $numeroinput++ }}" class="idnp{{$value->id}}">
                            {{--                    {{dd($value->id)}}--}}
                            <td data-name="nivelproceso_id">
                                {!! Form::text('nivelproceso_id'.$s=$key+1, $value->nombre, array('class' => 'form-control input-xs', 'id' => 'nivelproceso_id'.$s=$key+1, 'placeholder' => 'Descripción del Proceso')) !!}
                            </td>
                            <td data-name="prioridadnivelproceso" class="align:center">
                                {!! Form::number('prioridadnivelproceso'.$s=$key+1, $value->prioridad, array('class' => 'form-control input-xs', 'id' => 'prioridadnivelproceso'.$s=$key+1, 'placeholder' => 'Prioridad del Proceso')) !!}
                            </td>
                            <td data-name="id_nivelprocesorequerido" class="align:center">
                                {{ Form::hidden('id_nivelprocesorequerido'.$s=$key+1, $value->id, array('id' => 'id_nivelprocesorequerido'.$s=$key+1)) }}
                            </td>

                            <td data-name="id_deletesoftware" class="align:center">
                                <button class='btn btn-danger glyphicon glyphicon-remove row-remove-nivelproceso' value="{{$value->id}}"></button>
                            </td>
                        </tr>

                    @endforeach
                @else
                    <tr id='addrnivelproceso0' data-id="0" class="hidden">
                        <td data-name="nivelproceso_id">
                            {!! Form::text('nivelproceso_id', NULL, array('class' => 'form-control input-xs', 'id' => 'nivelproceso_id', 'placeholder' => 'Descripción del Proceso')) !!}
                        </td>
                        <td data-name="prioridadnivelproceso" class="align:center">
                            {!! Form::number('prioridadnivelproceso', NULL, array('class' => 'form-control input-xs', 'id' => 'prioridadnivelproceso', 'placeholder' => 'Prioridad del Proceso')) !!}
                        </td>
                        <td data-name="id_deletenivelproceso" class="align:center">
                            <button class='btn btn-danger glyphicon glyphicon-remove row-remove-nivelproceso'></button>
                        </td>
                    </tr>
                @endif

                </tbody>
            </table>


        </div>
    </div>
</fieldset>

<div class="form-group">
    <div class="col-lg-12 col-md-12 col-sm-12" style="text-align: right">
        {!! Form::checkbox('publicar',1, old('publicar')) !!}
        {{--<input type="checkbox" {!!old("publicar")? "checked": ''!!} value="1" id="publicar" name="publicar">--}}
        <label style="color: #e34f4f">Si Deseo Publicar la Oferta Laboral</label>
    </div>
</div>

<div class="form-group">
    <div class="col-lg-12 col-md-12 col-sm-12 text-right">
        {!! Form::button('<i class="fa fa-check fa-lg"></i> '.$boton, array('class' => 'btn btn-success btn-sm btn_geofertalaboral'.$boton, 'id' => 'btnGuardar', 'onclick' => 'guardar(\''.$entidad.'\', this)')) !!}
        {!! Form::button('<i class="glyphicon glyphicon-remove"></i> Cancelar', array('class' => 'btn btn-danger btn-sm', 'id' => 'btnCancelar'.$entidad, 'onclick' => 'cerrarModal();')) !!}
    </div>
</div>
{!! Form::close() !!}
<script type="text/javascript">
    $(document).ready(function () {
        configurarAnchoModal('350');
        init(IDFORMMANTENIMIENTO + '{!! $entidad !!}', 'M', '{!! $entidad !!}');
        configurarAnchoModal('1100');
    });
</script>
<script src="<?php echo e(asset('app/js/ofertalaboral/mantenimiento.js')); ?>"></script>
