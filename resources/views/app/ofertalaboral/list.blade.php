@if(count($lista) == 0)
    <h3 class="text-warning">No se encontraron resultados.</h3>
@else
    {!! $paginacion or '' !!}
    <table id="example1" class="table table-bordered table-striped table-condensed table-hover">

        <thead>
        <tr>
            @foreach($cabecera as $key => $value)
                <th @if((int)$value['numero'] > 1) colspan="{{ $value['numero'] }}" @endif style="background-color: #3c8dbc ;color: white;">{!! $value['valor'] !!}</th>
            @endforeach
        </tr>
        </thead>
        <tbody>
        <?php
        $contador = $inicio + 1;
        ?>
        @foreach ($lista as $key => $value)
            <tr>
                <?php
                $fecha = $value->fechapublicacion;
                $suma = '+'.$value->vigenciaoferta.' day';
                $nuevafecha = strtotime ( $suma , strtotime ( $fecha ) ) ;
                $nuevafecha = date ( 'Y-m-d' , $nuevafecha );
                ?>
                <td>{{ $contador }}</td>
                <td>{{ $value->departamento_nombre }}</td>
                <td>{{ $value->titulo }}</td>
                <td>{{ $value->fechapublicacion }}</td>
                <td>{{ $nuevafecha }}</td>
                <td>{{ $value->nrovacante }}</td>
                <td>{{ $value->escuelaprofesional }}</td>
                <td>{{ $value->jornadalaboral_nombre }}</td>
                <td>{{ $value->fechainiciocontrato }}</td>
                <td>{{ $value->monedapago }}</td>
                <td>{{ $value->salario }}</td>                            
                <td>{!! Form::button('<div class="fa fa-fw fa-list-ol"></div> Ver', array('onclick' => 'modal (\''.URL::route($ruta["verpostulantes"], array($value->id)).'\', \''.$titulo_listapostulantes.'\', this);', 'class' => 'btn btn-success btn-xs')) !!}</td>
                <td>{!! Form::button('<div class="fa fa-fw fa-search-plus"></div> Ver', array('onclick' => 'modal (\''.URL::route($ruta["verofertalaboral"], array($value->id)).'\', \''.$titulo_verofertalaboral.'\', this);', 'class' => 'btn btn-success btn-xs')) !!}</td>
                <td>{!! Form::button('<div class="glyphicon glyphicon-pencil"></div> Editar', array('onclick' => 'modal (\''.URL::route($ruta["edit"], array($value->id, 'listar'=>'SI')).'\', \''.$titulo_modificar.'\', this);', 'class' => 'btn btn-xs btn-warning')) !!}</td>
                <td>{!! Form::button('<div class="glyphicon glyphicon-remove"></div> Eliminar', array('onclick' => 'modal (\''.URL::route($ruta["delete"], array($value->id, 'SI')).'\', \''.$titulo_eliminar.'\', this);', 'class' => 'btn btn-xs btn-danger')) !!}</td>
            </tr>
            <?php
            $contador = $contador + 1;
            ?>
        @endforeach
        </tbody>

    </table>
    {!! $paginacion or '' !!}
@endif