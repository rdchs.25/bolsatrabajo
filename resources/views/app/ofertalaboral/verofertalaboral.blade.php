
<div class="box box-solid">
<div class="box-header with-border">
  <i class="fa fa-text-width"></i>

  <h3 class="box-title">Información del Puesto:</h3>
</div>
<!-- /.box-header -->
<div class="box-body">
  <dl class="dl-horizontal">
    <dt>Título</dt>
    <dd>{{ $ofertalaboral->titulo }}.</dd>

    <dt>Escuela profesional</dt>
    

    @if(is_null($ofertalaboral->escuela_id))
    <dd>{{ '' }}.</dd>
    @else
    <dd>{{ $ofertalaboral->escuela->nombre }}.</dd>
    @endif


    <dt>Empleador</dt>
    <dd>{{ $ofertalaboral->empleador->razonsocial }}.</dd>
    	
    <dt>Salario</dt>
    <dd>{{ $ofertalaboral->salario ." ". $ofertalaboral->monedapago  }}</dd>

    <dt>Descripción</dt>
    <dd>{{ $ofertalaboral->descripcion }}.</dd>

    <dt>Fecha publicación</dt>
    <dd>{{ $ofertalaboral->fechapublicacion }}.</dd>

    <dt>Vigencia (días)</dt>
    <dd>{{ $ofertalaboral->vigenciaoferta }}.</dd>

    <?php
    $fecha = $ofertalaboral->fechapublicacion;
    $suma = '+'.$ofertalaboral->vigenciaoferta.' day';
    $nuevafecha = strtotime ( $suma , strtotime ( $fecha ) ) ;
    $nuevafecha = date ( 'Y-m-d' , $nuevafecha );
    ?>

    <dt>Fecha fin de oferta</dt>
    <dd>{{ $nuevafecha }}</dd>

    <dt>Vacantes</dt>
    <dd>{{ $ofertalaboral->nrovacante }}</dd>

    <dt>Tipo Contrato</dt>
    <dd>
        @if(!is_null($ofertalaboral->tipocontrato_id))
        {{ $ofertalaboral->tipocontrato->nombre }}
        @else
        {{ "" }}
        @endif
    </dd>

    <dt>Duración</dt>
    <dd>{{ $ofertalaboral->duracioncontrato }}</dd>

    <dt>Horario</dt>
    <dd>{{ $ofertalaboral->horario }}</dd>

    <dt>Cambio Residencia</dt>
    @if($ofertalaboral->cambioresidencia!='' || $ofertalaboral->cambioresidencia!=null)
		<dd>Si</dd>
    @else
		<dd>No</dd>
    @endif

     <dt>Disponibilidad de Viaje</dt>
    @if($ofertalaboral->disponibilidadviajar!='' || $ofertalaboral->disponibilidadviajar!=null)
		<dd>Si</dd>
    @else
		<dd>No</dd>
    @endif            
  </dl>
</div>

<div class="box-header with-border">
  <i class="fa fa-text-width"></i>
  <h4 class="box-title">Requisitos:</h4>
</div>
<div class="box-body">
  <dl class="dl-horizontal">
    
    <dt>Estudios Mínimos</dt>
    <dd>{{ $ofertalaboral->nivelestudio->nombre }}</dd>
    
    <dt>Experiencia Laboral</dt>
    @if( $ofertalaboral->experiencialaboral== 'sinexperiencia' )
    <dd>Sin Experiencia </dd>
	@endif
	@if( $ofertalaboral->experiencialaboral== 'iguala' )
    <dd>Igual a {{ $ofertalaboral->anioexperiencia}} Años</dd>
	@endif
	@if( $ofertalaboral->experiencialaboral== 'mayoroigualque' )
    <dd>Mayor o Igual que {{ $ofertalaboral->anioexperiencia}} Años</dd>
	@endif
	@if( $ofertalaboral->experiencialaboral== 'menorigualque' )
    <dd>Menor  o Igual que {{ $ofertalaboral->anioexperiencia}} Años</dd>
	@endif

    
    <dt>Requiere Idioma</dt>
    @if($ofertalaboral->requiereidioma != '' || $ofertalaboral->requiereidioma != null || $ofertalaboral->idiomas->count() > 0)
					
		@foreach($ofertalaboral->idiomas as $listasidiomasrequeridos => $value)
			<dd>(*) Idioma: {{ $value->idioma->nombre }} | Lectura: {{ $value->lectura }} | Escritura: {{$value->escritura }} | Conversación: {{$value->conversacion }} </dd>						
		@endforeach
    @else
		<dd>No</dd>
    @endif 

    <dt>Requiere Software</dt>
    @if($ofertalaboral->requieresoftware != '' || $ofertalaboral->requieresoftware != null || $ofertalaboral->conocimientosinformaticos->count() > 0)
        @foreach($ofertalaboral->conocimientosinformaticos as $listasoftwaresrequeridos => $value)

            <dd>(*) Software: {{ $value->nombre }} | Nivel:  {{ $value->nivel }}</dd>                       
        @endforeach	
		
    @else
		<dd>No</dd>
    @endif

    <dt>Vehículo Propio</dt>
	@if($ofertalaboral->vehiculopropio!='' || $ofertalaboral->vehiculopropio!=null)
		<dd>Si</dd>
    @else
		<dd>No</dd>
    @endif 

    <dt>Vehículo Propio</dt>
	@if($ofertalaboral->tienelicencia!='' || $ofertalaboral->tienelicencia!=null)
		<dd>Si</dd>
    @else
		<dd>No</dd>
    @endif
             
  </dl>
</div>

<div class="box-header with-border">
  <i class="fa fa-text-width"></i>
  <h4 class="box-title">Clasificación:</h4>
</div>
<div class="box-body">
  <dl class="dl-horizontal">
    
		<dt>Oferta para el Extranjero</dt>
	@if($ofertalaboral->ofertaextranjero!='' || $ofertalaboral->ofertaextranjero!=null)
		<dd>Si</dd>
    @else
		<dd>No</dd>
    @endif            
    
    <dt>Admite Discapacidad</dt>
	@if($ofertalaboral->requierediscapacidad!='' || $ofertalaboral->requierediscapacidad!=null || $ofertalaboral->discapacidades->count() > 0)			
		@foreach($ofertalaboral->discapacidades as $listadiscapacidadrequeridas => $value)
            <dd>(*) Tipo Discapacidad : {{ $value->discapacidad->tipodiscapacidad->nombre }} | Discapacidad: {{ $value->discapacidad->nombre }} </dd>                        
        @endforeach
    @else
		<dd>No</dd>
    @endif 

    <dt>Observación Oferta</dt>
    <dd>{{ $ofertalaboral->observacionoferta }}.</dd>                     
             
  </dl>
</div>


<!-- /.box-body -->
</div>
<!-- /.box -->

<div class="form-group">
    <div class="col-lg-12 col-md-12 col-sm-12 text-right">
    	{!! Form::button('<i class="glyphicon glyphicon-remove"></i> Cerrar', array('class' => 'btn btn-danger btn-sm', 'id' => 'btnCancelar'.$entidad, 'onclick' => 'cerrarModal();')) !!}
    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
	configurarAnchoModal('700');
	init(IDFORMMANTENIMIENTO+'{!! $entidad !!}', 'M', '{!! $entidad !!}');
}); 
</script>