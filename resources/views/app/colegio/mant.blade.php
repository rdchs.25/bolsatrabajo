<!-- Plugin app -->


<div id="divMensajeError{!! $entidad !!}"></div>
{!! Form::model($colegio, $formData) !!}
{!! Form::hidden('listar', $listar, array('id' => 'listar')) !!}

<div class="form-group">
    {!! Form::label('nombre', 'Nombre:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::text('nombre', null, array('class' => 'form-control input-xs', 'id' => 'nombre', 'placeholder' => 'Ingrese nombre' ,'autofocus')) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('departamento_id', 'Departamento:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::select('departamento_id', $cboDepartamento, isset($departamento) ? $departamento->id: null, array('class' => 'form-control input-xs ', 'id' => 'departamento_id', 'onchange' => 'mostrarProvincias(\''.URL::route('provincia.cboprovincia').'\',\''.$entidad.'\', \'M\')')) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('provincia_id', 'Provincia:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::select('provincia_id', $cboProvincia, isset($provincia) ? $provincia->id: null  , array('class' => 'form-control input-xs ', 'id' => 'provincia_id', 'onchange' => 'mostrarDistritos(\''.URL::route('distrito.cbodistrito').'\',\''.$entidad.'\', \'M\')')) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('distrito_id', 'Categoria:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::select('distrito_id', $cboDistrito, null, array('class' => 'form-control input-xs', 'id' => 'distrito_id')) !!}
    </div>
</div>

<div class="form-group">
    <div class="col-lg-12 col-md-12 col-sm-12 text-right">
        {!! Form::button('<i class="fa fa-check fa-lg"></i> '.$boton, array('class' => 'btn btn-success btn-sm', 'id' => 'btnGuardar', 'onclick' => 'guardar(\''.$entidad.'\', this)')) !!}
        {!! Form::button('<i class="glyphicon glyphicon-remove"></i> Cancelar', array('class' => 'btn btn-danger btn-sm', 'id' => 'btnCancelar'.$entidad, 'onclick' => 'cerrarModal();')) !!}
    </div>
</div>
{!! Form::close() !!}

<script type="text/javascript">
    $(document).ready(function () {
//        $(".chosen-select").chosen({width: "100%"});
//        $('.chosen-select').chosen({no_results_text: "Oops, nothing found!"});
        configurarAnchoModal('550');
        init(IDFORMMANTENIMIENTO + '{!! $entidad !!}', 'M', '{!! $entidad !!}');
    });

</script>

