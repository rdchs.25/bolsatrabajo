<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>¡Activación de Cuenta!</h2>
<div>
    <p>¡Cuenta Activada¡</p>
    <p>{{ $usuario->nombre }}.La Universidad de Lambayeque le da la bienvenida a nuestro sistema de bolsa de trabajo
        ICHAMBA!.
    <p>Su cuenta ha sido activada. <a href="https://goo.gl/7bUFkx">Ingrese al sistema</a></p>
    <p>Usuario: {{ $usuario->usuario }}</p>
    <p>correo : {{ $usuario->email }}</p>
    </p>

    <p><strong> Si no fue usted, ignore este correo electrónico y su dirección se eliminará de nuestros
            registros.</strong></p>
</div>
</body>
</html>

