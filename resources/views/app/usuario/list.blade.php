@if(count($lista) == 0)
    <h3 class="text-warning">No se encontraron resultados.</h3>
@else
    {!! $paginacion or '' !!}
    <table id="example1" class="table table-bordered table-striped table-condensed table-hover">

        <thead>
        <tr>
            @foreach($cabecera as $key => $value)
                <th @if((int)$value['numero'] > 1) colspan="{{ $value['numero'] }}" @endif>{!! $value['valor'] !!}</th>
            @endforeach
        </tr>
        </thead>
        <tbody>
        <?php
        $contador = $inicio + 1;
        ?>
        @foreach ($lista as $key => $value)
            <tr>
                <td>{{ $contador }}</td>
                <td>{{ $value->nombre }}</td>
                <td>{{ $value->email }}</td>
                <td>{{ $value->usuario }}</td>
                @if($value->superusuario)
                    <td>Si</td>
                @else
                    <td>No</td>
                @endif
                <td> @foreach ($value->tipousuarios  as $keytipousuario => $valuetipousuario)
                    {{ $valuetipousuario->nombre ." | "}}
                @endforeach</td>
                @if( $value->estado ==='Pendiente')
                    <td>{!! Form::button('<div class="fa fa-fw fa-check-square"></div> Activar', array('onclick' => 'modal (\''.URL::route($ruta["activarusuario"], array($value->id, 'listar'=>'SI')).'\', \''.$titulo_activarusuario.'\', this);', 'class' => 'btn btn-xs btn-success')) !!}</td>
                @else
                    <td>{{ $value->estado }}</td>
                @endif
                @if($change)
                    <td>{!! Form::button('<div class="glyphicon glyphicon-pencil"></div> Editar', array('onclick' => 'modal (\''.URL::route($ruta["edit"], array($value->id, 'listar'=>'SI')).'\', \''.$titulo_modificar.'\', this);', 'class' => 'btn btn-xs btn-warning')) !!}</td>
                @endif
                @if($delete)
                    <td>{!! Form::button('<div class="glyphicon glyphicon-remove"></div> Eliminar', array('onclick' => 'modal (\''.URL::route($ruta["delete"], array($value->id, 'SI')).'\', \''.$titulo_eliminar.'\', this);', 'class' => 'btn btn-xs btn-danger')) !!}</td>
                @endif
            </tr>
            <?php
            $contador = $contador + 1;
            ?>
        @endforeach
        </tbody>
    </table>
    {!! $paginacion or '' !!}
@endif




