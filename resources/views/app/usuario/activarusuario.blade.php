<div id="divMensajeError{!! $entidad !!}"></div>

{!! Form::model($modelo, $formData) !!}
{!! Form::hidden('listar', $listar, array('id' => 'listar')) !!}
<br>
{!! $mensaje or '<h4><p class="text-info text-center">¿Esta seguro de activar el usuario?</p><h4>' !!}
<br>
<p class="text-center"><strong class="text-center"> {!! $modelo->nombre !!}</strong></p>
<br>
<div class="form-group">
    <div class="col-lg-12 col-md-12 col-sm-12 text-right">
        {!! Form::button('<i class="fa fa-check fa-lg"></i> '.$boton, array('class' => 'btn btn-success btn-sm', 'id' => 'btnGuardar', 'onclick' => 'guardar(\''.$entidad.'\', this);actualizarnotificacion();')) !!}
        {{--        {!! Form::button('<i class="fa fa-check fa-lg"></i> '.$boton, array('class' => 'btn btn-success btn-sm', 'id' => 'btnGuardar', 'onclick' => 'actualizarnotificacion();')) !!}--}}
        {!! Form::button('<i class="fa fa-exclamation fa-lg"></i> Cancelar', array('class' => 'btn btn-warning btn-sm', 'id' => 'btnCancelar'.$entidad, 'onclick' => 'cerrarModal((contadorModal - 1));')) !!}
    </div>
</div>
{!! Form::close() !!}
<script type="text/javascript">
    $(document).ready(function () {
        init(IDFORMMANTENIMIENTO + '{!! $entidad !!}', 'M', '{!! $entidad !!}');
        configurarAnchoModal('400');
    });

    function actualizarnotificacion() {
        var userpendiente = '{!!bolsatrabajo\User::where('estado', '=', 'Pendiente')->get()->count() !!}';
        $('#labelnotificaciones').html(userpendiente - 1);
        $('#headernotificaciones').html('Tiene ' + (userpendiente - 1) + ' notificaciones');
        $('#menunotificicaciones').html(' <i class="fa fa-users text-aqua"></i>' + (userpendiente - 1) + ' usuarios nuevos');
    }
</script>