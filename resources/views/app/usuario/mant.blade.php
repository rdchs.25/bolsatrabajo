<!-- Plugin app -->

<div id="divMensajeError{!! $entidad !!}"></div>
{!! Form::model($usuario, $formData) !!}
{!! Form::hidden('listar', $listar, array('id' => 'listar')) !!}


@if($boton == 'Registrar')
    <div class="form-group">
        {!! Form::label('persona_id', 'Persona:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
        <div class="col-lg-9 col-md-9 col-sm-9">
            {{--        {!! Form::select('persona_id', $cboPersona, null, array('class' => 'form-control input-xs', 'id' => 'persona_id')) !!}--}}
            {!! Form::text('persona', isset($usuario->persona_id) ? $usuario->persona->apellidopaterno." ".$usuario->persona->apellidomaterno." ".$usuario->persona->nombres :null, array('class' => 'form-control input-xs', 'id' => 'persona', 'placeholder' => 'Apellidos o Nombre' , 'maxlength' => '11')) !!}
            {!! Form::hidden('persona_id', null, array('id' => 'persona_id')) !!}
        </div>
    </div>
@endif

<!-- <div class="form-group">
    {!! Form::label('nombre', 'Nombres:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::text('nombre', null, array('class' => 'form-control input-xs', 'id' => 'nombre', 'placeholder' => 'Nombre y Apellidos completos' ,'autofocus')) !!}
    </div>
</div> -->
<div class="form-group">
    {!! Form::label('email', 'Email:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::email('email', null, array('class' => 'form-control input-xs', 'id' => 'email', 'placeholder' => 'Ingrese Email' )) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('usuario', 'Usuario:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::text('usuario', null, array('class' => 'form-control input-xs', 'id' => 'usuario', 'placeholder' => 'Usuario' , 'maxlength' => '11')) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('tipousuario_id', 'Tipo Usuario:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::select('tipousuario_id[]', $cboTipousuario, isset($usuario) ? $usuario->tipousuarios : null, array('data-placeholder'=>'Seleccione...', 'multiple class'=>'chosen-select','class' => 'form-control input-xs', 'id' => 'tipousuario_id')) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('password', 'Contraseña:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::password('password',  array('class' => 'form-control input-xs', 'id' => 'password', 'placeholder' => 'Contraseña' , 'maxlength' => '11')) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('password', 'Repita la contraseña:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::password('password_confirmation',  array('class' => 'form-control input-xs', 'id' => 'password-confirm', 'placeholder' => 'Vuelva escribir la contraseña' , 'maxlength' => '11')) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('superusuario', '¿Superusuario?', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9 ">
        {!! Form::checkbox('superusuario',null, old('superusuario')) !!}
    </div>
</div>
<div class="form-group">
    <div class="col-lg-12 col-md-12 col-sm-12 text-right">
        {!! Form::button('<i class="fa fa-check fa-lg"></i> '.$boton, array('class' => 'btn btn-success btn-sm', 'id' => 'btnGuardar', 'onclick' => 'guardararchivos(\''.$entidad.'\', this)')) !!}
       {!! Form::button('<i class="glyphicon glyphicon-remove"></i> Cancelar', array('class' => 'btn btn-danger btn-sm', 'id' => 'btnCancelar'.$entidad, 'onclick' => 'cerrarModal();')) !!}
    </div>
</div>
{!! Form::close() !!}
@routes
<script type="text/javascript">
    $(document).ready(function () {
        $(".chosen-select").chosen({width: "100%"});
        configurarAnchoModal('650');
        init(IDFORMMANTENIMIENTO + '{!! $entidad !!}', 'M', '{!! $entidad !!}');

        /* Autocompletar Cliente -- Inicio */
        $('#persona').change(function (e) {
            if ($(this).val() === '') {
                $('#persona_id').val('');
            }
        });
        var persona = new Bloodhound({
            datumTokenizer: function (d) {
                return Bloodhound.tokenizers.whitespace(d.value);
            },
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: route('usuario.autocompletarpersona', '%QUERY'),
                filter: function (persona) {
                    return $.map(persona, function (movie) {
                        return {
                            value: movie.value,
                            id: movie.id
                        };
                    });
                }
            }
        });
        persona.initialize();
        $('#persona').typeahead(null, {
            displayKey: 'value',
            source: persona.ttAdapter()
        }).on('typeahead:selected', function (object, datum) {
            $('#persona_id').val(datum.id);
        });

        /* Autocompletar Cliente -- Fin */
    });

</script>

