@extends('.index')

@section('maincontent')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Principal
                <small>Panel de Control</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
                <li class="active">Principal</li>
            </ol>
            <hr class="line">
        </section>

        <!-- Main content -->
        <section class="content">

            <div class="row ">
                <div class="col-xs-8 col-lg-4 col-md-4">
                    <form action="#" method="get">
                        <div class="input-group margin ">
                            <input type="text" name="q" class="form-control" placeholder="Buscar...">
                            <span class="input-group-btn">
                            <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i
                                        class="fa fa-search"></i>
                            </button>
                          </span>
                        </div>
                    </form>
                </div>
                <div class="col-xs-4 col-lg-8 col-md-8">
                    <div class="box-footer" style="background: transparent">
                        <button type="button" class="btn btn-primary pull-right">Nuevo
                        </button>
                    </div>
                </div>

            </div>

            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">

                    <table id="example1" class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>Nombre</th>
                            <th class="hidden-sm hidden-xs">orden</th>
                            <th width="60">Opciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>le-hoverr
                            </td>
                            <td class="hidden-sm hidden-xs">tstriped table-bordered table-hover
                            </td>
                            <td>
                                <div class="icon">
                                    <a  href="#" title="Editar elemento" data-toggle="tooltip" >
                                        <i  class="editar glyphicon glyphicon-pencil"></i>
                                        &nbsp;
                                    </a>
                                    <a  href="#" title="Eliminar" data-toggle="tooltip" >
                                        <i class="eliminar glyphicon glyphicon-trash"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>

                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </section>


    </div>
@endsection
@section('appjavascript')
    <script src="{{asset('app/js/categoriamenu/listar.js')}}"></script>

@endsection
