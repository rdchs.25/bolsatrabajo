<div id="divMensajeError{!! $entidad !!}"></div>
{!! Form::model($ofertalaboralpersona, $formData) !!}
	{!! Form::hidden('listar', $listar, array('id' => 'listar')) !!}

      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>

          <h3 class="box-title">Información del Puesto:</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <dl class="dl-horizontal">
            <dt>Título</dt>
            <dd>{{ $ofertalaboralpersona->titulo }}.</dd>
            	
            <dt>Salario</dt>
            <dd>{{ $ofertalaboralpersona->salario ." ". $ofertalaboralpersona->monedapago  }}</dd>

            <dt>Descripción</dt>
            <dd>{{ $ofertalaboralpersona->descripcion }}.</dd>

            <dt>Vacantes</dt>
            <dd>{{ $ofertalaboralpersona->nrovacante }}</dd>

            <dt>Tipo Contrato</dt>
            <dd>{{ $ofertalaboralpersona->tipocontrato->nombre }}</dd>

            <dt>Duración</dt>
            <dd>{{ $ofertalaboralpersona->duracioncontrato }}</dd>

            <dt>Horario</dt>
            <dd>{{ $ofertalaboralpersona->horario }}</dd>

            <dt>Cambio Residencia</dt>
            @if($ofertalaboralpersona->cambioresidencia!='' || $ofertalaboralpersona->cambioresidencia!=null)
				<dd>Si</dd>
            @else
				<dd>No</dd>
            @endif

             <dt>Disponibilidad de Viaje</dt>
            @if($ofertalaboralpersona->disponibilidadviajar!='' || $ofertalaboralpersona->disponibilidadviajar!=null)
				<dd>Si</dd>
            @else
				<dd>No</dd>
            @endif            
          </dl>
        </div>

		<div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h4 class="box-title">Requisitos:</h4>
        </div>
        <div class="box-body">
          <dl class="dl-horizontal">
            
            <dt>Estudios Mínimos</dt>
            <dd>{{ $ofertalaboralpersona->nivelestudio->nombre }}</dd>
            
            <dt>Experiencia Laboral</dt>
            @if( $ofertalaboralpersona->experiencialaboral== 'sinexperiencia' )
            <dd>Sin Experiencia </dd>
			@endif
			@if( $ofertalaboralpersona->experiencialaboral== 'iguala' )
            <dd>Igual a {{ $ofertalaboralpersona->anioexperiencia}} Años</dd>
			@endif
			@if( $ofertalaboralpersona->experiencialaboral== 'mayoroigualque' )
            <dd>Mayor o Igual que {{ $ofertalaboralpersona->anioexperiencia}} Años</dd>
			@endif
			@if( $ofertalaboralpersona->experiencialaboral== 'menorigualque' )
            <dd>Menor  o Igual que {{ $ofertalaboralpersona->anioexperiencia}} Años</dd>
			@endif

            
            <dt>Requiere Idioma</dt>
            @if($ofertalaboralpersona->requiereidioma != '' || $ofertalaboralpersona->requiereidioma != null)
							
				@foreach($listaidiomarequerido as $listasidiomasrequeridos => $value)
					<dd>(*) Idioma: {{ $value->idioma }} | Lectura: {{ $value->lectura }} | Escritura: {{$value->escritura }} | Conversación: {{$value->conversacion }} </dd>						
				@endforeach
            @else
				<dd>No</dd>
            @endif 

            <dt>Requiere Software</dt>
            @if($ofertalaboralpersona->requieresoftware != '' || $ofertalaboralpersona->requieresoftware != null)
							
				@foreach($listasoftwarerequerido as $listasoftwaresrequeridos => $value)
					<dd>(*) Software: {{ $value->nombre }} | Nivel:  {{ $value->nivel }}</dd>						
				@endforeach
            @else
				<dd>No</dd>
            @endif

            <dt>Vehículo Propio</dt>
			@if($ofertalaboralpersona->vehiculopropio!='' || $ofertalaboralpersona->vehiculopropio!=null)
				<dd>Si</dd>
            @else
				<dd>No</dd>
            @endif 

            <dt>Vehículo Propio</dt>
			@if($ofertalaboralpersona->tienelicencia!='' || $ofertalaboralpersona->tienelicencia!=null)
				<dd>Si</dd>
            @else
				<dd>No</dd>
            @endif
                     
          </dl>
        </div>

        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h4 class="box-title">Clasificación:</h4>
        </div>
        <div class="box-body">
          <dl class="dl-horizontal">
            
 			<dt>Oferta para el Extranjero</dt>
			@if($ofertalaboralpersona->ofertaextranjero!='' || $ofertalaboralpersona->ofertaextranjero!=null)
				<dd>Si</dd>
            @else
				<dd>No</dd>
            @endif            

            <dt>Admite Discapacidad</dt>
			@if($ofertalaboralpersona->requierediscapacidad!='' || $ofertalaboralpersona->requierediscapacidad!=null)							
				@foreach($listadiscapacidadrequerida as $listadiscapacidadrequeridas => $value)
					<dd>(*) Discapacidad: : {{ $value->discapacidad }} </dd>						
				@endforeach
            @else
				<dd>No</dd>
            @endif 

            <dt>Observación Oferta</dt>
            <dd>{{ $ofertalaboralpersona->observacionoferta }}.</dd>                     
                     
          </dl>
        </div>


        <!-- /.box-body -->
      </div>
      <!-- /.box -->

    <div class="form-group">
		<div class="col-lg-12 col-md-12 col-sm-12 text-right">
			{!! Form::button('<i class="fa fa-check fa-lg"></i> Postular a Vacante', array('class' => 'btn btn-success btn-sm', 'id' => 'btnGuardar', 'onclick' => 'guardar(\''.$entidad.'\', this)')) !!}
			{!! Form::button('<i class="glyphicon glyphicon-remove"></i> Cancelar', array('class' => 'btn btn-danger btn-sm', 'id' => 'btnCancelar'.$entidad, 'onclick' => 'cerrarModal();')) !!}
		</div>
	</div>



{!! Form::close() !!}
<script type="text/javascript">
$(document).ready(function() {
	configurarAnchoModal('700');
	init(IDFORMMANTENIMIENTO+'{!! $entidad !!}', 'M', '{!! $entidad !!}');
}); 
</script>