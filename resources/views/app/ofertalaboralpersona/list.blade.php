@if(count($lista) == 0)
    <h3 class="text-warning">No se encontraron resultados.</h3>
@else
    {!! $paginacion or '' !!}
    <table id="example1" class="table table-bordered table-striped table-condensed table-hover">

        <thead>
        <tr>
            @foreach($cabecera as $key => $value)
                <th @if((int)$value['numero'] > 1) colspan="{{ $value['numero'] }}" @endif style="background-color: #3c8dbc ;color: white;">{!! $value['valor'] !!}</th>
            @endforeach
        </tr>
        </thead>
        <tbody>
        <?php
        $contador = $inicio + 1;
        ?>
        @foreach ($lista as $key => $value)
            <tr>
                <td>{{ $contador }}</td>
                <td>{{ $value->fechapublicacion}}</td>
                <td>{{ $value->titulo}}</td>
                <td>{{ $value->nrovacante}}</td>
                @if(!is_null($value->tipocontrato_id))
                <td>{{ $value->tipocontrato->nombre }}</td>
                @else
                <td>{{ "" }}</td>
                @endif
                @if(!is_null($value->jornadalaboral_id))
                <td>{{ $value->jornadalaboral->nombre }}</td>
                @else
                <td>{{ "" }}</td>
                @endif
                @if($value->anioexperiencia == '')
                    <td>Ninguna</td>
                @else
                    <td>{{ $value->anioexperiencia }}</td>

                @endif

                @if(!Auth::user()->superusuario)
                    @if(count($ofertaspostuladapersona) > 0 && in_array($value->id, $ofertaspostuladapersona))
                        <td style="color: #9F9F9F">CV Enviado</td>
                    @else
                        <td>{!! Form::button('<div class="glyphicon glyphicon-check"></div> Postular', array('onclick' => 'modal (\''.URL::route($ruta["edit"], array($value->id, 'listar'=>'SI')).'\', \''.$titulo_modificar.'\', this);', 'class' => 'btn btn-xs btn-success')) !!}</td>
                    @endif
                @else
                    <td>Ninguna</td>
                @endif            

            </tr>
            <?php
            $contador = $contador + 1;
            ?>
        @endforeach
        </tbody>

    </table>
    {!! $paginacion or '' !!}
@endif