<div id="divMensajeError{!! $entidad !!}"></div>

<h4><p>Se borrarán los siguientes objetos relacionados:</p></h4>
<strong><h5 style="color:black">Objetos:</h5></strong>

{!! isset($data) ? $data : '' !!}

{!! Form::model($modelo, $formData) !!}
{!! Form::hidden('listar', $listar, array('id' => 'listar')) !!}
<br>
{!! $mensaje or '<h4><p class="text-danger">¿Esta seguro de eliminar el registro?</p><h4>' !!}

<div class="form-group">
    <div class="col-lg-12 col-md-12 col-sm-12 text-right">
        {!! Form::button('<i class="fa fa-check fa-lg"></i> '.$boton, array('class' => 'btn btn-success btn-sm', 'id' => 'btnGuardar', 'onclick' => 'guardar(\''.$entidad.'\', this)')) !!}
        {!! Form::button('<i class="fa fa-exclamation fa-lg"></i> Cancelar', array('class' => 'btn btn-warning btn-sm', 'id' => 'btnCancelar'.$entidad, 'onclick' => 'cerrarModal((contadorModal - 1));')) !!}
    </div>
</div>
{!! Form::close() !!}
<script type="text/javascript">
    $(document).ready(function () {
        init(IDFORMMANTENIMIENTO + '{!! $entidad !!}', 'M', '{!! $entidad !!}');
        configurarAnchoModal('400');
    });
</script>