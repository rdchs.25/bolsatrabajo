<!-- Plugin app -->

<div id="divMensajeError{!! $entidad !!}"></div>
{!! Form::model($empresa, $formData) !!}
{!! Form::hidden('listar', $listar, array('id' => 'listar')) !!}


<div class="form-group">
    {!! Form::label('ruc', 'Ruc:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::text('ruc', null, array('class' => 'form-control input-xs', 'id' => 'ruc', 'placeholder' => 'Ingrese Ruc' ,'autofocus', 'maxlength' => '11')) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('razonsocial', 'Razón Social:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::text('razonsocial', null, array('class' => 'form-control input-xs', 'id' => 'razonsocial', 'placeholder' => 'Ingrese Razón Social' )) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('nombrecomercial', 'Nombre Comercial:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::text('nombrecomercial', null, array('class' => 'form-control input-xs', 'id' => 'nombrecomercial', 'placeholder' => 'Ingrese Nombre Comercial' )) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('direccion', 'Dirección:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::text('direccion', null, array('class' => 'form-control input-xs', 'id' => 'direccion', 'placeholder' => 'Ingrese Dirección' )) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('web', 'Web:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::text('web', null, array('class' => 'form-control input-xs', 'id' => 'web', 'placeholder' => 'Ingrese Web' )) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('email', 'Email:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::email('email', null, array('class' => 'form-control input-xs', 'id' => 'email', 'placeholder' => 'Ingrese Email' )) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('telefono', 'Teléfono:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::text('telefono', null, array('class' => 'form-control input-xs', 'id' => 'telefono', 'placeholder' => 'Ingrese Teléfono' )) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('celular', 'Celular:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::text('celular', null, array('class' => 'form-control input-xs', 'id' => 'celular', 'placeholder' => 'Ingrese Celular' )) !!}
    </div>
</div>
<div class="form-group" id="divubigeo">
    {!! Form::label('departamento_id', 'Ubigeo:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::select('departamento_id', $cboDepartamento, isset($departamento)  ? $departamento->id: null, array('style' => 'display: inline; width: 33%;','class' => 'form-control input-xs ', 'id' => 'departamento_id', 'onchange' => 'mostrarProvincias(\''.URL::route('provincia.cboprovincia').'\',\''.$entidad.'\', \'M\')')) !!}
        {!! Form::select('provincia_id', $cboProvincia, isset($provincia) ? $provincia->id: null  , array('style' => 'display: inline; width: 33%;','class' => 'form-control input-xs ', 'id' => 'provincia_id', 'onchange' => 'mostrarDistritos(\''.URL::route('distrito.cbodistrito').'\',\''.$entidad.'\', \'M\')')) !!}
        {!! Form::select('distrito_id', $cboDistrito, null, array('style' => 'display: inline; width: 32%;','class' => 'form-control input-xs', 'id' => 'distrito_id')) !!}

    </div>
</div>
<div class="form-group">
    {!! Form::label('descripcion', 'Descripcion:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::textarea('descripcion', null, array('style' => 'resize: none;', 'rows' => '3','class' => 'form-control input-xs', 'id' => 'descripcion', 'placeholder' => 'Ingrese drescripción')) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('logo', 'Logo:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::file('logo') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-lg-12 col-md-12 col-sm-12 text-right">
        {!! Form::button('<i class="fa fa-check fa-lg"></i> '.$boton, array('class' => 'btn btn-success btn-sm', 'id' => 'btnGuardar', 'onclick' => 'guardararchivos(\''.$entidad.'\', this)')) !!}
        {!! Form::button('<i class="glyphicon glyphicon-remove"></i> Cancelar', array('class' => 'btn btn-danger btn-sm', 'id' => 'btnCancelar'.$entidad, 'onclick' => 'cerrarModal();')) !!}
    </div>
</div>
{!! Form::close() !!}


<script type="text/javascript">
    $(document).ready(function () {
//        $(".chosen-select").chosen({width: "100%"});
//        $('.chosen-select').chosen({no_results_text: "Oops, nothing found!"});
        $(IDFORMMANTENIMIENTO + '{!! $entidad !!} :input[id="ruc"]').inputmask("99999999999");
        $(IDFORMMANTENIMIENTO + '{!! $entidad !!} :input[id="telefono"]').inputmask('Regex', {regex: "[0-9]+-[0-9]+"});
        $(IDFORMMANTENIMIENTO + '{!! $entidad !!} :input[id="celular"]').inputmask('Regex', {regex: "[*]?[#]?[0-9]+-[0-9]+"});

        if ($('#extranjero').prop('checked')) {
            $('#divpais').show();
            $('#divubigeo').hide();
        } else {
            $('#divpais').hide();
            $('#divubigeo').show();
        }

        $('#extranjero').click(function (e) {
            if ($(this).prop('checked')) {
                $('#divpais').show();
                $('#divubigeo').hide();
            } else {
                $('#divpais').hide();
                $('#divubigeo').show();
            }
        });
        configurarAnchoModal('650');
        init(IDFORMMANTENIMIENTO + '{!! $entidad !!}', 'M', '{!! $entidad !!}');
    });

</script>

