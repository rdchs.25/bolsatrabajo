<div class="row">
    <div class="panel-primary">
        <div class="panel-body">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <br> {!! Form::hidden('contformacademica',  count($listaformacademica) == 0 ? 0:($listaformacademica->last())['id']+1, array('id' => 'contformacademica')) !!}
                <table id="tbl-formacion-academica"
                       class="table table-bordered table-striped table-condensed table-hover">
                    <thead style="background: #A9D0F5">
                    <th>Especialidad</th>
                    <th>Nivel de Estudio</th>
                    <th>Centro / Institución</th>
                    <th>Situación</th>
                    <th colspan="2">Opciones</th>
                    <tbody>
                    @foreach ($listaformacademica as $key => $value)
                        <tr class="selected" id="filaformacademica{{$value->id}}">
                            <input type="hidden" name="editarformacad{{$value->id}}" id="editarformacad{{$value->id}}"
                                   value="{{$value->id}}">
                            <td><input type="hidden" name="especialidad{{$value->id}}"
                                       value="{{$value->especialidad}}">{{$value->especialidad}}
                            </td>
                            <td><input type="hidden" name="nivelestudio_id{{$value->id}}"
                                       value="{{$value->nivelestudio_id}}">{{$value->nivelestudio->nombre}}
                            </td>
                            <td><input type="hidden" name="universidad_id{{$value->id}}"
                                       value="{{$value->universidad_id}}"><input type="hidden"
                                                                                 name="colegio_id{{$value->id}}"
                                                                                 value="{{$value->colegio_id}}">
                                {{  $value->universidad_id != null ? $value->universidad->nombre :  '' }}
                                {{  $value->colegio_id != null ? $value->colegio->nombre :  '' }}
                            </td>

                            <td><input type="hidden" name="situacion{{$value->id}}"
                                       value="{{$value->situacion}}">
                                <input type="hidden" name="tipoestudio{{$value->id}}"
                                       value="{{$value->tipoestudio}}">
                                <input type="hidden" name="anioinicio{{$value->id}}"
                                       value="{{$value->anioinicio}}">
                                <input type="hidden" name="mesinicio{{$value->id}}"
                                       value="{{$value->mesinicio}}">
                                <input type="hidden" name="semestre{{$value->id}}"
                                       value="{{$value->semestre}}">
                                <input type="hidden" name="estudiandoactualmente{{$value->id}}"
                                       value="{{$value->estudiandoactualmente}}">
                                <input type="hidden" name="aniotermino{{$value->id}}"
                                       value="{{$value->aniotermino}}">
                                <input type="hidden" name="mestermino{{$value->id}}"
                                       value="{{$value->mestermino}}">
                                {{$value->situacion}}
                            </td>
                            <td>
                                <button onclick="editarformacionacademica('{{$value->id}}');"
                                        class="btn btn-xs btn-warning" type="button">
                                    <div class="glyphicon glyphicon-pencil"></div>
                                    Editar
                                </button>
                            </td>
                            <td>
                                <button onclick="eliminarformacionacademica('{{$value->id}}');" class="btn btn-xs btn-danger"
                                        type="button">
                                    <div class="glyphicon glyphicon-remove"></div>
                                    Eliminar
                                </button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th></th>
                        <th></th>
                        <th class="text-center">
                            {!! Form::button('<i class="glyphicon glyphicon-plus"></i>'.$boton_form_academica, array('class' => 'btn btn-info waves-effect waves-light m-l-10 btn-md', 'id' => 'btnNuevoformacionacademica', 'onclick' => 'modal(\''.URL::route($ruta["createformacionacademica"], array('NO')).'\', \''.$titulo_registrar_form_academica.'\', this);')) !!}
                        </th>
                        <th></th>
                        <th></th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>

    </div>
</div>
