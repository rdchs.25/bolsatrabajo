<!-- Plugin app -->
<form class="form-horizontal">
    <div class="box-body">
        {!! Form::hidden('indexcomple',isset($index)? $index : null, array('id' => 'indexcomple')) !!}
        <div id="formacioncomplementaria-error" class="alert alert-danger" style="display: none">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>Por favor corrige los errores: </strong>
        </div>
        <div class="form-group">
            {!! Form::label('tipoformacion', 'Nivel Estudio:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
            <div class="col-lg-9 col-md-9 col-sm-9">
                {!! Form::select('tipoformacion', $cboTipoformacion, isset($tipoformacion) ? $tipoformacion : null, array('class' => 'form-control input-xs ', 'id' => 'tipoformacion')) !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('titulo', 'Título:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
            <div class="col-lg-9 col-md-9 col-sm-9">
                {!! Form::text('titulo', isset($titulo) ? $titulo : null, array('class' => 'form-control input-xs', 'id' => 'titulo', 'placeholder' => 'Ingrese titulo' )) !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('institucion', 'Centro  / Institución:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
            <div class="col-lg-9 col-md-9 col-sm-9">
                {!! Form::text('institucion', isset($institucion) ? $institucion : null, array('class' => 'form-control input-xs', 'id' => 'institucion', 'placeholder' => 'Ingrese instirución' )) !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('cantidadhoras', 'N° Horas:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
            <div class="col-lg-9 col-md-9 col-sm-9">
                {!! Form::number('cantidadhoras', isset($cantidadhoras)? $cantidadhoras: null, array('class' => 'form-control input-xs', 'id' => 'cantidadhoras', 'placeholder' => 'Ingrese N° Horas' )) !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('anioiniciocomple', 'Inicio:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
            <div class="col-lg-4 col-md-4 col-sm-4">
                {!! Form::select('anioiniciocomple', $cboAnios, isset($anioiniciocomple) ? $anioiniciocomple : null, array('class' => 'form-control input-xs ', 'id' => 'anioiniciocomple')) !!}

            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                {!! Form::select('mesiniciocomple', $cboMeses, isset($mesiniciocomple) ? $mesiniciocomple : null, array('class' => 'form-control input-xs ', 'id' => 'mesiniciocomple')) !!}
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-3 col-md-3 col-sm-3 text-right">
                <input type="checkbox" id="estudiandoactual" name="estudiandoactual"
                        {!!  $estudiandoactual ? 'checked' : ''   !!}>
            </div>
            Estudiando Actualmente.
        </div>
        <div class="form-group" id="divestudiando" style="display: {!! $estudiandoactual ? 'none' : 'block'!!}">
            {!! Form::label('anioterminocomple', 'Término:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
            <div class="col-lg-4 col-md-4 col-sm-4">
                {!! Form::select('anioterminocomple', $cboAnios, isset($anioterminocomple) ? $anioterminocomple : null, array('class' => 'form-control input-xs ', 'id' => 'anioterminocomple')) !!}
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                {!! Form::select('mesterminocomple', $cboMeses, isset($mesterminocomple) ? $mesterminocomple : null, array('class' => 'form-control input-xs ', 'id' => 'mesterminocomple')) !!}
            </div>
        </div>

        <div class="form-group">
            <div class="col-lg-12 col-md-12 col-sm-12 text-right">
                {!! Form::button('<i class="fa fa-check fa-lg"></i>'.$boton, array('class' => 'btn btn-success btn-sm', 'id' => $idboton)) !!}
                {!! Form::button('<i class="glyphicon glyphicon-remove"></i> Cancelar', array('class' => 'btn btn-danger btn-sm', 'onclick' => 'cerrarModal();')) !!}
            </div>
        </div>
    </div>
</form>
@routes
<script src="{{asset('app/js/persona/mant.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        configurarAnchoModal('650');
    });

</script>
