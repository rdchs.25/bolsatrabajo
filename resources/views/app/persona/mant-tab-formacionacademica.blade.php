<!-- Plugin app -->
<form class="form-horizontal">
    <div class="box-body">
        {!! Form::hidden('indexacad',isset($index)? $index : null, array('id' => 'indexacad')) !!}
        <div id="formacionacademica-error" class="alert alert-danger" style="display: none">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>Por favor corrige los errores: </strong>
        </div>
        <div class="form-group">
            {!! Form::label('nivelestudio_id', 'Nivel Estudio:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
            <div class="col-lg-9 col-md-9 col-sm-9">
                {!! Form::select('nivelestudio_id', $cboNivelestudio, isset($nivelestudio_id) ? $nivelestudio_id : null, array('class' => 'form-control input-xs ', 'id' => 'nivelestudio_id')) !!}
            </div>
        </div>
        <div class="form-group" id="divuniversidad" style="display: {{ empty($universidad_id) ? 'none' : 'block' }}">
            {!! Form::label('universidad_id', 'Centro / Institución:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
            <div class="col-lg-9 col-md-9 col-sm-9">
                {!! Form::select('universidad_id', $cboUniversidad, isset($universidad_id) ? $universidad_id : null, array('class' => 'form-control input-xs ', 'id' => 'universidad_id')) !!}
            </div>
        </div>
        <div class="form-group" id="divcolegio" style="display: {{ empty($colegio_id) ? 'none' : 'block' }}">
            {!! Form::label('colegio_id', 'Colegio:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
            <div class="col-lg-9 col-md-9 col-sm-9">
                {!! Form::select('colegio_id', $cboColegio, isset($colegio_id) ? $colegio_id : null, array('class' => 'form-control input-xs ', 'id' => 'colegio_id')) !!}
            </div>
        </div>
        <div class="form-group" id="divespecialidad" style="display: {{ empty($especialidad) ? 'none' : 'block' }}">
            {!! Form::label('especialidad', 'Especialidad:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
            <div class="col-lg-9 col-md-9 col-sm-9">
                {!! Form::text('especialidad', isset($especialidad) ? $especialidad : null, array('class' => 'form-control input-xs', 'id' => 'especialidad', 'placeholder' => 'Ingrese especialidad' ,'autofocus')) !!}
            </div>
        </div>
        <div class="form-group" id="divtipoespecialidad" style="display: {{ empty($tipoestudio) ? 'none' : 'block' }}">
            {!! Form::label('tipoestudio', 'Tipo Estudio:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
            <div class="col-lg-9 col-md-9 col-sm-9">
                {!! Form::select('tipoestudio', $cboTipoestudio, isset($tipoestudio) ? $tipoestudio : null, array('class' => 'form-control input-xs ', 'id' => 'tipoestudio')) !!}
            </div>
        </div>
        <div class="form-group" id="divinicioacademico" style="display: {{ empty($anioinicio) ? 'none' : 'block' }}">
            {!! Form::label('anioinicio', 'Inicio:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
            <div class="col-lg-4 col-md-4 col-sm-4">
                {!! Form::select('anioinicio', $cboAnios, isset($anioinicio) ? $anioinicio : null, array('class' => 'form-control input-xs ', 'id' => 'anioinicio')) !!}
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                {!! Form::select('mesinicio', $cboMeses, isset($mesinicio) ? $mesinicio : null, array('class' => 'form-control input-xs ', 'id' => 'mesinicio')) !!}
            </div>
        </div>
        <div class="form-group" id="divsituacionacademica" style="display: {{ empty($situacion) ? 'none' : 'block' }}">
            {!! Form::label('situacion', 'Situación:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
            <div class="col-lg-9 col-md-9 col-sm-9">
                {!! Form::select('situacion', $cboSituacion, isset($situacion) ? $situacion : null, array('class' => 'form-control input-xs ', 'id' => 'situacion')) !!}
            </div>
        </div>
        <div class="form-group" id="divsemestre" style="display: {{ empty($semestre) ? 'none' : 'block' }}">
            {!! Form::label('semestre', 'Semestre:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
            <div class="col-lg-9 col-md-9 col-sm-9">
                {!! Form::select('semestre', $cboSemestre, isset($semestre) ? $semestre : null, array('class' => 'form-control input-xs ', 'id' => 'semestre')) !!}
            </div>
        </div>
        <div class="form-group" id="divestudiandoactualmente"
             style="display: {{  empty($anioinicio) ? 'none' : 'block'  }}">
            <div class="col-lg-3 col-md-3 col-sm-3 text-right">
                <input type="checkbox" id="estudiandoactualmente" name="estudiandoactualmente"
                        {!!  $estudiandoactualmente ? 'checked' : ''   !!}>
            </div>
            Estudiando Actualmente.
        </div>
        <div class="form-group" id="divsigueestudiando"
             style="display: {{ empty($aniotermino) && empty($mestermino) ? 'none' : 'block' }}">
            {!! Form::label('termino', 'Término:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
            <div class="col-lg-4 col-md-4 col-sm-4">
                {!! Form::select('aniotermino', $cboAnios, isset($aniotermino) ? $aniotermino : null, array('class' => 'form-control input-xs ', 'id' => 'aniotermino')) !!}
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                {!! Form::select('mestermino', $cboMeses, isset($mestermino) ? $mestermino : null, array('class' => 'form-control input-xs ', 'id' => 'mestermino')) !!}
            </div>
        </div>

        <div class="form-group">
            <div class="col-lg-12 col-md-12 col-sm-12 text-right">
                {!! Form::button('<i class="fa fa-check fa-lg"></i>'.$boton, array('class' => 'btn btn-success btn-sm', 'id' => $idboton)) !!}
                {!! Form::button('<i class="glyphicon glyphicon-remove"></i> Cancelar', array('class' => 'btn btn-danger btn-sm', 'onclick' => 'cerrarModal();')) !!}
            </div>
        </div>
    </div>
</form>
@routes
<script src="{{asset('app/js/persona/mant.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        configurarAnchoModal('650');
    });

</script>
