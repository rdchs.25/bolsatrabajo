<div class="row">
    <div class="panel-primary">
        <div class="panel-body">
            <div id="idiomamensaje-error" class="alert alert-danger" style="display: none">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Por favor seleccione una opcion</strong>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="col-lg-8 col-md-8 col-sm-8">
                    <div class="form-group">
                        {!! Form::label('idioma_id', 'Idioma:') !!}
                        {!! Form::select('idioma_id', $cboIdioma, null, array('class' => 'input-xs chosen-select', 'id' => 'idioma_id')) !!}
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2">
                    {!! Form::label('nativo', '¿Nativo?') !!}<br>
                    {!! Form::checkbox('nativo',null, old('nativo')) !!}
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12" id="niveles">
                <div class="col-lg-3 col-md-3 col-sm-3 ">
                    <div class="form-group">
                        {!! Form::label('lectura', 'Lectura:') !!}
                        {!! Form::select('lectura', $cboNivelidioma, null, array('class' => 'form-control input-xs chosen-select', 'id' => 'lectura')) !!}

                    </div>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-1"></div>
                <div class="col-lg-3 col-md-3 col-sm-3 ">
                    <div class="form-group">
                        {!! Form::label('escritura', 'Escritura:') !!}
                        {!! Form::select('escritura', $cboNivelidioma, null, array('class' => 'form-control input-xs chosen-select', 'id' => 'escritura')) !!}

                    </div>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-1"></div>
                <div class="col-lg-3 col-md-3 col-sm-3 ">
                    <div class="form-group">
                        {!! Form::label('conversacion', 'Conversación:') !!}
                        {!! Form::select('conversacion', $cboNivelidioma, null, array('class' => 'form-control input-xs chosen-select', 'id' => 'conversacion')) !!}

                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 text-center">
                {!! Form::button('<i class="fa fa-check fa-lg"></i> Agregar', array('class' => 'btn btn-success btn-sm', 'id' => 'btn_addidioma')) !!}

            </div>
            <div class="col-lg-12 col-md-12 col-sm-12">
                <br>
                <table id="tbl-idiomas" class="table table-striped table-bordered">
                    <thead style="background: #A9D0F5">
                    <th>Idioma</th>
                    <th>Lectura</th>
                    <th>Escritura</th>
                    <th>Conversación</th>
                    <th>Nativo</th>
                    <th>Opción</th>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>