<div class="form-group">
    {!! Form::label('anexo_tipodocumento_id', 'Sector Empresa:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9 ">
        {!! Form::select('anexo_tipodocumento_id', $cboTipodocumento, isset($anexo_tipodocumento_id) ? $anexo_tipodocumento_id : null,array('class' => 'form-control input-xs', 'id' => 'anexo_tipodocumento_id')) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('anexo_descripcion', 'Descripción:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::text('anexo_descripcion', null, array('class' => 'form-control input-xs', 'id' => 'anexo_descripcion', 'placeholder' => 'Ingrese descripción' )) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('documento', 'Documento:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::file('documento') !!}
    </div>
</div>