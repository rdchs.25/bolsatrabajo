<!-- Plugin app -->
<form class="form-horizontal">
    <div class="box-body">
        {!! Form::hidden('indexlab',isset($index)? $index : null, array('id' => 'indexlab')) !!}
        <div id="divMensajeErrorformacionlaboral">
            <div id="formacionlaboral-error" class="alert alert-danger" style="display: none">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Por favor corrige los errores: </strong>
            </div>
        </div>
       
        <div class="form-group">
            {!! Form::label('empresa', 'Empresa:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
            <div class="col-lg-9 col-md-9 col-sm-9">
                {!! Form::text('empresa', isset($empresa)? $empresa: null, array('class' => 'form-control input-xs', 'id' => 'empresa', 'placeholder' => 'Ingrese empresa' ,'autofocus')) !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('sectorempresa_id', 'Sector Empresa:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
            <div class="col-lg-9 col-md-9 col-sm-9">
                {!! Form::select('sectorempresa_id', $cboSectorempresa, isset($sectorempresa_id) ? $sectorempresa_id : null, array('class' => 'form-control input-xs ', 'id' => 'sectorempresa_id')) !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('arealaboral_id', 'Área Laboral:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
            <div class="col-lg-9 col-md-9 col-sm-9">
                {!! Form::select('arealaboral_id', $cboArealaboral, isset($arealaboral_id) ? $arealaboral_id : null, array('class' => 'form-control input-xs ', 'id' => 'arealaboral_id')) !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('cargolaboral_id', 'Cargo Laboral:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
            <div class="col-lg-9 col-md-9 col-sm-9">
                {!! Form::select('cargolaboral_id', $cboCargolaboral, isset($cargolaboral_id) ? $cargolaboral_id : null, array('class' => 'form-control input-xs ', 'id' => 'cargolaboral_id')) !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('nropersonalcargo', 'N° Personal a Cargo:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
            <div class="col-lg-2 col-md-2 col-sm-2">
                {!! Form::number('nropersonalcargo', isset($nropersonalcargo)? $nropersonalcargo: null, array('class' => 'form-control input-xs','min'=>'0' , 'id' => 'nropersonalcargo' )) !!}
            </div>
            {!! Form::label('tipocontrato_id', 'Tipo Contrato:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
            <div class="col-lg-4 col-md-4 col-sm-4">
                {!! Form::select('tipocontrato_id', $cboTipocontrato, isset($tipocontrato_id) ? $tipocontrato_id : null, array('class' => 'form-control input-xs ', 'id' => 'tipocontrato_id')) !!}
            </div>
        </div>
        <!-- <div class="form-group">
            {!! Form::label('tipocontrato_id', 'Tipo Contrato:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
            <div class="col-lg-9 col-md-9 col-sm-9">
                {!! Form::select('tipocontrato_id', $cboTipocontrato, isset($tipocontrato_id) ? $tipocontrato_id : null, array('class' => 'form-control input-xs ', 'id' => 'tipocontrato_id')) !!}
            </div>
        </div> -->
        <div class="form-group">
            {!! Form::label('salario', 'Salario:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
            <div class="col-lg-3 col-md-3 col-sm-3">
                {!! Form::number('salario', isset($salario)? $salario: null, array('class' => 'form-control input-xs','min'=>'0' ,'id' => 'salario', 'placeholder' => 'Ingrese salario' )) !!}
            </div>
            {!! Form::label('moneda', 'Moneda:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
            <div class="col-lg-3 col-md-3 col-sm-3">
                {!! Form::select('moneda', $cboMoneda, isset($moneda)? $moneda: null, array('class' => 'form-control input-xs ', 'id' => 'moneda')) !!}
            </div>
        </div>       
        <div class="form-group">
            <div class="col-lg-3 col-md-3 col-sm-3 text-right">
                <input type="checkbox" id="mostrarsalario" name="mostrarsalario"
                        {!! $mostrarsalario ? 'checked' : ''  !!} >
            </div>
            Mostrar el salario.
        </div>
        <div class="form-group">
            {!! Form::label('fechainicio', 'Fecha Inicio:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
            <div class="col-lg-9 col-md-9 col-sm-9">
                <div class="input-group date">
                    {!! Form::text('fechainicio', isset($fechainicio)? $fechainicio : null, array('class' => 'form-control input-xs', 'id' => 'fechainicio', 'placeholder' => 'Ingrese Fecha de Inicio')) !!}
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-3 col-md-3 col-sm-3 text-right">
                <input type="checkbox" id="trabajaactualmente" name="trabajaactualmente"
                        {!! $trabajaactualmente ? 'checked' : ''  !!}>

            </div>
            Trabaja actualmente en este cargo.
        </div>
        <div class="form-group" id="divfechatermino"
             style="display: {!! $trabajaactualmente ? 'none' :'block'!!}">
            {!! Form::label('fechatermino', 'Fecha Término:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
            <div class="col-lg-9 col-md-9 col-sm-9">
                <div class="input-group date">
                    {!! Form::text('fechatermino', isset($fechatermino) ? $fechatermino : null, array('class' => 'form-control input-xs', 'id' => 'fechatermino', 'placeholder' => 'Ingrese Fecha Término')) !!}
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('descripcionlaboral', 'Descripcion:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
            <div class="col-lg-9 col-md-9 col-sm-9">
                {!! Form::textarea('descripcionlaboral', isset($descripcionlaboral) ? $descripcionlaboral : null , array('style' => 'resize: none;', 'rows' => '3','class' => 'form-control input-xs', 'id' => 'descripcionlaboral', 'placeholder' => 'Ingrese drescripción')) !!}
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-12 col-md-12 col-sm-12 text-right">
                {!! Form::button('<i class="fa fa-check fa-lg"></i>'.$boton, array('class' => 'btn btn-success btn-sm', 'id' => $idboton)) !!}
                {!! Form::button('<i class="glyphicon glyphicon-remove"></i> Cancelar', array('class' => 'btn btn-danger btn-sm', 'onclick' => 'cerrarModal();')) !!}
            </div>
        </div>
    </div>
</form>
@routes
<script src="{{asset('app/js/persona/mant.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        configurarAnchoModal('650');
    });

</script>
