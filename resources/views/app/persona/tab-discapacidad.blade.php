
<div class="form-group">
    {!! Form::label('discapacidad_id', 'Selecciona una discapacidad:', array('class' => 'col-lg-4 col-md-4 col-sm-4 control-label')) !!}
    <div class="col-lg-8 col-md-8 col-sm-8">
        {!! Form::select('discapacidad_id[]', $cboDiscapacidad, null, array('data-placeholder'=>'Seleccione...', 'multiple class'=>'chosen-select','class' => 'form-control input-xs', 'id' => 'discapacidad_id[]')) !!}
    </div>
</div>