<div class="row">
    <div class="panel-primary">
        <div class="panel-body">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <br> {!! Form::hidden('contformlaboral',  count($listaformlaboral) == 0 ? 0:($listaformlaboral->last())['id']+1, array('id' => 'contformlaboral')) !!}
                <table id="tbl-formacion-laboral"
                       class="table table-bordered table-striped table-condensed table-hover">
                    <thead style="background: #A9D0F5">
                    <th>Fecha Inicio</th>
                    <th>Fecha Término</th>
                    <th>Sector Empresa</th>
                    <th>Empresa</th>
                    <th colspan="2">Opciones</th>
                    <tbody>
                    @foreach ($listaformlaboral as $key => $value)
                        <tr class="selected" id="filaformlaboral{{$value->id}}">
                            <input type="hidden" name="editarformlab{{$value->id}}" id="editarformlab{{$value->id}}"
                                   value="{{$value->id}}">
                            <td><input type="hidden" name="fechainicio{{$value->id}}"
                                       value="{{\Illuminate\Support\Carbon::parse($value->fechainicio)->format('d/m/Y')}}">{{ \Illuminate\Support\Carbon::parse($value->fechainicio)->format('d/m/Y') }}
                            </td>
                            <td><input type="hidden" name="fechatermino{{$value->id}}"
                                       value="{{$value->fechatermino != '' ?  \Illuminate\Support\Carbon::parse($value->fechatermino)->format('d/m/Y') :'' }}">{{ $value->fechatermino != '' ? \Illuminate\Support\Carbon::parse($value->fechatermino)->format('d/m/Y'): 'Actual' }}
                            </td>
                            <td><input type="hidden" name="sectorempresa_id{{$value->id}}"
                                       value="{{$value->sectorempresa_id}}">{{$value->sectorempresa->nombre}}
                            </td>
                            <td><input type="hidden" name="empresa{{$value->id}}"
                                       value="{{$value->empresa}}">{{$value->empresa}}
                                <input type="hidden" name="arealaboral_id{{$value->id}}"
                                       value="{{$value->arealaboral_id}}">
                                <input type="hidden" name="cargolaboral_id{{$value->id}}"
                                       value="{{$value->cargolaboral_id}}">
                                <input type="hidden" name="nropersonalcargo{{$value->id}}"
                                       value="{{$value->nropersonalcargo}}">
                                <input type="hidden" name="salario{{$value->id}}"
                                       value="{{$value->salario}}">
                                <input type="hidden" name="moneda{{$value->id}}"
                                       value="{{$value->moneda}}">
                                <input type="hidden" name="mostrarsalario{{$value->id}}"
                                       value="{{$value->mostrarsalario}}">
                                <input type="hidden" name="trabajaactualmente{{$value->id}}"
                                       value="{{$value->trabajaactualmente}}">
                                <input type="hidden" name="descripcionlaboral{{$value->id}}"
                                       value="{{$value->descripcionlaboral}}">
                                <input type="hidden" name="tipocontrato_id{{$value->id}}"
                                       value="{{$value->tipocontrato_id}}">
                            </td>
                            <td>
                                <button onclick="editarformacionlaboral('{{$value->id}}');"
                                        class="btn btn-xs btn-warning" type="button">
                                    <div class="glyphicon glyphicon-pencil"></div>
                                    Editar
                                </button>
                            </td>
                            <td>
                                <button onclick="eliminarformlaboral('{{$value->id}}');" class="btn btn-xs btn-danger"
                                        type="button">
                                    <div class="glyphicon glyphicon-remove"></div>
                                    Eliminar
                                </button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th></th>
                        <th></th>
                        <th class="text-center">
                            {!! Form::button('<i class="glyphicon glyphicon-plus"></i>'.$boton_form_laboral, array('class' => 'btn btn-info waves-effect waves-light m-l-10 btn-md', 'id' => 'btnNuevoformacionlaboral', 'onclick' => 'modal(\''.URL::route($ruta["createformacionlaboral"], array('NO')).'\', \''.$titulo_registrar_form_laboral.'\', this);')) !!}
                        </th>
                        <th></th>
                        <th></th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>

    </div>
</div>
