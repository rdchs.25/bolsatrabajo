@if(count($lista) == 0)
    <h3 class="text-warning">No se encontraron resultados.</h3>
@else
    {!! $paginacion or '' !!}
    <table id="example1" class="table table-bordered table-striped table-condensed table-hover">

        <thead>
        <tr>
            @foreach($cabecera as $key => $value)
                <th @if((int)$value['numero'] > 1) colspan="{{ $value['numero'] }}" @endif>{!! $value['valor'] !!}</th>
            @endforeach
        </tr>
        </thead>
        <tbody>
        <?php
        $contador = $inicio + 1;
        ?>
        @foreach ($lista as $key => $value)
            <tr>
                <td>{{ $contador }}</td>
                <td>{{ $value->codigouniversitario or '' }}</td>
                <td>{{ $value->nrodocumento }}</td>
                <td>{{ $value->apellidopaterno.' '.$value->apellidomaterno .' '. $value->nombres }}</td>
                <td>{{ $value->telefono.'/'.$value->celular }}</td>
                <td>{{ $value->direccion }}</td>
                <td>{{ $value->distrito !== null ? $value->distrito->provincia->departamento->nombre.'/'. $value->distrito->provincia->nombre.'/'. $value->distrito->nombre: '' }}</td>
                <td>{{ $value->escuela !== null ? $value->escuela->nombre : '' }}</td>
                <td>{{ $value->pais !== null ? $value->pais->nombre : '' }}</td>
                <td><img width="40px" src="{!! Storage::url($value->foto) !!}?img={{str_random(4)}}"></td>
                @if($change)
                    <td>{!! Form::button('<div class="glyphicon glyphicon-pencil"></div> Editar', array('onclick' => 'modal (\''.URL::route($ruta["edit"], array($value->id, 'listar'=>'SI')).'\', \''.$titulo_modificar.'\', this);', 'class' => 'btn btn-xs btn-warning')) !!}</td>
                @endif
                @if($delete)
                    <td>{!! Form::button('<div class="glyphicon glyphicon-remove"></div> Eliminar', array('onclick' => 'modal (\''.URL::route($ruta["delete"], array($value->id, 'SI')).'\', \''.$titulo_eliminar.'\', this);', 'class' => 'btn btn-xs btn-danger')) !!}</td>
                @endif
            </tr>
            
        @endforeach
        </tbody>
    </table>
    {!! $paginacion or '' !!}
@endif




