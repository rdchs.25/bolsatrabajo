<!-- Plugin app -->
<form class="form-horizontal">
    <div class="box-body">
        {!! Form::hidden('index', $index, array('id' => 'index')) !!}
{{--        {!! Form::hidden('editar', $editar, array('id' => 'editar')) !!}--}}
         <div id="divMensajeErrorconocimientoinformatico">
            <div id="conocimientoinformatico-error" class="alert alert-danger" style="display: none">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Por favor corrige los errores: </strong>
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('nombre', 'Nombre:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
            <div class="col-lg-9 col-md-9 col-sm-9">
                {!! Form::text('nombre', $nombre, array('class' => 'form-control input-xs', 'id' => 'nombre', 'placeholder' => 'Ingrese nombre' ,'autofocus')) !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('nivel', 'Nivel:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
            <div class="col-lg-9 col-md-9 col-sm-9">
                {!! Form::select('nivel', $cboNivel, $nivel, array('class' => 'form-control input-xs ', 'id' => 'nivel')) !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('anioexperiencia', 'Años Experiencia:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
            <div class="col-lg-9 col-md-9 col-sm-9">
                {!! Form::number('anioexperiencia', $anioexperiencia, array('class' => 'form-control input-xs', 'id' => 'anioexperiencia', 'placeholder' => 'Ingrese años de experiencia' )) !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('descripcion', 'Descripcion:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
            <div class="col-lg-9 col-md-9 col-sm-9">
                {!! Form::textarea('descripcionsoftware', $descripcionsoftware, array('style' => 'resize: none;', 'rows' => '3','class' => 'form-control input-xs', 'id' => 'descripcionsoftware', 'placeholder' => 'Ingrese drescripción')) !!}
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-12 col-md-12 col-sm-12 text-right">
                {!! Form::button('<i class="fa fa-check fa-lg"></i>'.$boton, array('class' => 'btn btn-success btn-sm', 'id' => $idboton)) !!}
                {!! Form::button('<i class="glyphicon glyphicon-remove"></i> Cancelar', array('class' => 'btn btn-danger btn-sm', 'onclick' => 'cerrarModal();')) !!}
            </div>
        </div>
    </div>
</form>
@routes
<script src="{{asset('app/js/persona/mant.js')}}"></script>


