<div class="row">
    <div class="panel-primary">
        <div class="panel-body">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <br> {!! Form::hidden('contformcomplementaria',  count($listaformcomplementaria) == 0 ? 0:($listaformcomplementaria->last())['id']+1, array('id' => 'contformcomplementaria')) !!}
                <table id="tbl-formacion-complementaria"
                       class="table table-bordered table-striped table-condensed table-hover">
                    <thead style="background: #A9D0F5">
                    <th>Especialidad / Carrera</th>
                    <th>Tipo de Formación Complementaria</th>
                    <th>Centro / Institución</th>
                    <th colspan="2">Opciones</th>
                    <tbody>
                    @foreach ($listaformcomplementaria as $key => $value)
                        <tr class="selected" id="filaformcomplementaria{{$value->id}}">
                            <input type="hidden" name="editarformcomplement{{$value->id}}"
                                   id="editarformcomplement{{$value->id}}"
                                   value="{{$value->id}}">
                            <td><input type="hidden" name="titulo{{$value->id}}"
                                       value="{{$value->titulo}}">{{$value->titulo}}
                            </td>
                            <td><input type="hidden" name="tipoformacion{{$value->id}}"
                                       value="{{$value->tipoformacion}}">
                                {{  $value->tipoformacion }}
                            </td>

                            <td><input type="hidden" name="institucion{{$value->id}}"
                                       value="{{$value->institucion}}">
                                <input type="hidden" name="cantidadhoras{{$value->id}}"
                                       value="{{$value->cantidadhoras}}">
                                <input type="hidden" name="anioiniciocomple{{$value->id}}"
                                       value="{{$value->anioinicio}}">
                                <input type="hidden" name="mesiniciocomple{{$value->id}}"
                                       value="{{$value->mesinicio}}">
                                <input type="hidden" name="mesterminocomple{{$value->id}}"
                                       value="{{$value->mestermino}}">
                                <input type="hidden" name="anioterminocomple{{$value->id}}"
                                       value="{{$value->aniotermino}}">
                                <input type="hidden" name="estudiandoactual{{$value->id}}"
                                       value="{{ $value->estudiandoactualmente }}">
                                {{$value->institucion}}
                            </td>
                            <td>
                                <button onclick="editarformacioncomplementaria('{{$value->id}}');"
                                        class="btn btn-xs btn-warning" type="button">
                                    <div class="glyphicon glyphicon-pencil"></div>
                                    Editar
                                </button>
                            </td>
                            <td>
                                <button onclick="eliminarformacioncomplementaria('{{$value->id}}');"
                                        class="btn btn-xs btn-danger"
                                        type="button">
                                    <div class="glyphicon glyphicon-remove"></div>
                                    Eliminar
                                </button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th></th>
                        <th class="text-center">
                            {!! Form::button('<i class="glyphicon glyphicon-plus"></i>'.$boton_form_complementaria, array('class' => 'btn btn-info waves-effect waves-light m-l-10 btn-md', 'id' => 'btnNuevoformacioncomplementaria', 'onclick' => 'modal(\''.URL::route($ruta["createformacioncomplementaria"], array('NO')).'\', \''.$titulo_registrar_form_complementaria.'\', this);')) !!}
                        </th>
                        <th></th>
                        <th></th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>

    </div>
</div>
