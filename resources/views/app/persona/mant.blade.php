<!-- Plugin app -->

<?php
$date = null;
if ($persona !== NULL) {
    $date = date('d/m/Y', strtotime($persona->fechanacimiento));
}
?>
<div id="divMensajeError{!! $entidad !!}"></div>

{!! Form::model($persona, $formData) !!}
{!! Form::hidden('listar', $listar, array('id' => 'listar')) !!}
{!! Form::hidden('guardareditar', 'NO', array('id' => 'guardareditar')) !!}

<div class="nav-tabs-custom">

    <ul class="nav nav-tabs">
        <li class="active"><a href="#datospersonales" data-toggle="tab">Datos Personales</a></li>
        <li id="discapacidad-tab"><a href="#tab-discapacidad" data-toggle="tab">Discapacidad</a></li>
        <li><a href="#idioma" data-toggle="tab">Idiomas</a></li>
        <li><a href="#conocimientoinformatico" data-toggle="tab">Conocimiento Informático</a></li>
        <li><a href="#formacionlaboral" data-toggle="tab">Formación Laboral</a></li>
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                Otros <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
                <li role="presentation"><a href="#formacionacademica" data-toggle="tab">Formación Académica</a></li>
                <li role="presentation"><a href="#formacioncomplementaria" data-toggle="tab">Formación
                        Complementaria</a></li>
                <li role="presentation"><a href="#preferencialaboral" data-toggle="tab">Preferencia Laboral</a></li>
                <li role="presentation"><a href="#anexos" data-toggle="tab">Anexos</a></li>
            </ul>
        </li>

    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="datospersonales">
            @include('app.persona.tab-datospersonales')

        </div>
        <div class="tab-pane" id="tab-discapacidad">
            @include('app.persona.tab-discapacidad')
        </div>
        <div class="tab-pane" id="idioma">
            @include('app.persona.tab-idioma')

        </div>
        <div class="tab-pane" id="conocimientoinformatico">
            @include('app.persona.tab-conocimientoinformatico')
        </div>
        <div class="tab-pane" id="formacionlaboral">
            @include('app.persona.tab-formacionlaboral')
        </div>

        <div class="tab-pane" id="formacionacademica">
            @include('app.persona.tab-formacionacademica')
        </div>
        <div class="tab-pane" id="formacioncomplementaria">
            @include('app.persona.tab-formacioncomplementaria')
        </div>
        <div class="tab-pane" id="preferencialaboral">
            @include('app.persona.tab-preferencialaboral')
        </div>
        <div class="tab-pane" id="anexos">
            @include('app.persona.tab-anexos')
        </div>

        <!-- /.tab-pane -->
    </div>

</div>
<div class="form-group">
    <div class="col-lg-12 col-md-12 col-sm-12 text-right">
        {!! Form::button('<i class="fa fa-check fa-lg"></i> '.$boton, array('class' => 'btn btn-success btn-sm', 'id' => 'btnGuardar', 'onclick' => 'guardararchivos(\''.$entidad.'\', this)')) !!}
        {!! Form::button('<i class="fa fa-check fa-lg"></i> '.$botoneditar, array('class' => 'btn btn-success btn-sm', 'id' => 'btnGuardar', 'onclick' => 'guardarmodaleditar(\''.$entidad.'\', this)')) !!}
        {!! Form::button('<i class="glyphicon glyphicon-remove"></i> Cancelar', array('class' => 'btn btn-danger btn-sm', 'id' => 'btnCancelar'.$entidad, 'onclick' => 'cerrarModal();')) !!}
    </div>
</div>
{!! Form::close() !!}
@routes
<script src="{{asset('app/js/persona/mant.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        configurarAnchoModal('800');
        $(".chosen-select").chosen({width: "100%"});
        $('#discapacidad-tab').hide();
        $(IDFORMMANTENIMIENTO + '{!! $entidad !!} :input[id="telefono"]').inputmask('Regex', {regex: "[0-9]+-[0-9]+"});
        $(IDFORMMANTENIMIENTO + '{!! $entidad !!} :input[id="celular"]').inputmask('Regex', {regex: "[*]?[#]?[0-9]+-[0-9]+"});
        $(IDFORMMANTENIMIENTO + '{!! $entidad !!} :input[id="nrodocumento"]').inputmask('Regex', {regex: "[*]?[#]?[0-9]+-[0-9]+"});
        $(IDFORMMANTENIMIENTO + '{!! $entidad !!} :input[id="fechanacimiento"]').inputmask("dd/mm/yyyy");

        init(IDFORMMANTENIMIENTO + '{!! $entidad !!}', 'M', '{!! $entidad !!}');
    });

</script>

