<section class="content-header">
    <h1>
        {{$title}}
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Principal</li>
    </ol>
    <hr class="line">
</section>


<div class="row">
    <div class="col-sm-12">
        <div class="card-box table-responsive">

            <div class="row m-b-30">
                <div class="col-sm-12">
                    {!! Form::open(['route' => $ruta["search"], 'method' => 'POST' ,'onsubmit' => 'return false;', 'class' => 'form-inline', 'role' => 'form', 'autocomplete' => 'off', 'id' => 'formBusqueda'.$entidad]) !!}
                    {!! Form::hidden('page', 1, array('id' => 'page')) !!}
                    {!! Form::hidden('accion', 'listar', array('id' => 'accion')) !!}
                    <div class="form-group">
                        {!! Form::label('querysearch', 'Buscar:') !!}
                        {!! Form::text('querysearch', '', array('class' => 'form-control input-xs', 'id' => 'querysearch','placeholder'=>'Buscar...')) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('distrito_id', 'Distrito:') !!}
                        {!! Form::select('distrito_id', $cboDistrito, null, array('class' => 'form-control input-xs', 'id' => 'distrito_id')) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('pais_id', 'País:') !!}
                        {!! Form::select('pais_id', $cboPais, null, array('class' => 'form-control input-xs', 'id' => 'pais_id')) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('escuela_id', 'Escuela:') !!}
                        {!! Form::select('escuela_id', $cboEscuela, null, array('class' => 'form-control input-xs', 'id' => 'escuela_id')) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('tipodocumento_id', 'Tipo Documento:') !!}
                        {!! Form::select('tipodocumento_id', $cboTipodocumento, null, array('class' => 'form-control input-xs', 'id' => 'tipodocumento_id')) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('filas', 'Filas a mostrar:')!!}
                        {!! Form::selectRange('filas', 1, 30, 10, array('class' => 'form-control input-xs', 'onchange' => 'buscar(\''.$entidad.'\')')) !!}
                    </div>
                    {!! Form::button('<i class="glyphicon glyphicon-search"></i> Buscar', array('class' => 'btn btn-success waves-effect waves-light m-l-10 btn-md', 'id' => 'btnBuscar', 'onclick' => 'buscar(\''.$entidad.'\')')) !!}
                    @if($add)
                        {!! Form::button('<i class="glyphicon glyphicon-plus"></i> Nuevo', array('class' => 'btn btn-info waves-effect waves-light m-l-10 btn-md', 'id' => 'btnNuevo', 'onclick' => 'modal (\''.URL::route($ruta["create"], array('listar'=>'SI')).'\', \''.$titulo_registrar.'\', this);')) !!}
                    @endif
                    {!! Form::close() !!}
                </div>
            </div>
            <br><br>
            <div class="box">
                <div class="box-body" id="listado{{ $entidad }}">
                </div>
            </div>
            <table id="datatable" class="table table-striped table-bordered">
            </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        buscar('{{ $entidad }}');
        init(IDFORMBUSQUEDA + '{{ $entidad }}', 'B', '{{ $entidad }}');
        $(IDFORMBUSQUEDA + '{{ $entidad }} :input[id="name"]').keyup(function (e) {
            var key = window.event ? e.keyCode : e.which;
            if (key == '13') {
                buscar('{{ $entidad }}');
            }
        });
    });
</script>

