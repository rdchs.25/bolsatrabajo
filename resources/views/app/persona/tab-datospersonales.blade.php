<div class="form-group">
    {!! Form::label('codigouniversitario', 'Código Universitario (*):', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::text('codigouniversitario', null, array('class' => 'form-control input-xs', 'id' => 'codigouniversitario', 'placeholder' => 'Ingrese Código Universitario' ,'autofocus', 'maxlength' => '11')) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('nombres', 'Nombres Completos:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::text('nombres', null, array('class' => 'form-control input-xs', 'id' => 'nombres', 'placeholder' => 'Ingrese Nombres Completos' )) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('apellidopaterno', 'Apellido Paterno:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::text('apellidopaterno', null, array('class' => 'form-control input-xs', 'id' => 'apellidopaterno', 'placeholder' => 'Ingrese Apellido Paterno' )) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('apellidomaterno', 'Apellido Materno:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::text('apellidomaterno', null, array('class' => 'form-control input-xs', 'id' => 'apellidomaterno', 'placeholder' => 'Ingrese Apellido Materno' )) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('tipodocumento_id', 'Tipo Documento:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-3 col-md-3 col-sm-3 ">
        {!! Form::select('tipodocumento_id', $cboTipodocumento, null, array('class' => 'form-control input-xs', 'id' => 'tipodocumento_id')) !!}
    </div>
    {!! Form::label('nrodocumento', 'Número Documento:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-3 col-md-3 col-sm-3">
        {!! Form::text('nrodocumento', null, array('class' => 'form-control input-xs', 'id' => 'nrodocumento', 'placeholder' => 'Ingrese Número Documento' )) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('estadocivil', 'Estado Civil:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-3 col-md-3 col-sm-3">
        {!! Form::select('estadocivil', $cboEstadocivil, null, array('class' => 'form-control input-xs', 'id' => 'estadocivil')) !!}

    </div>
    {!! Form::label('fechanacimiento', 'Fecha nacimiento:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-3 col-md-3 col-sm-3">
        <div class='input-group input-group-xs' id='divfechanacimiento'>
            {!! Form::text('fechanacimiento', $date, array('class' => 'form-control input-xs', 'id' => 'fechanacimiento', 'placeholder' => 'Ingrese Fecha de Nacimiento')) !!}
            <span class="input-group-btn">
                <button class="btn btn-default calendar">
				    <i class="glyphicon glyphicon-calendar"></i>
                </button>
            </span>
        </div>
    </div>
</div>

<div class="form-group">
    {!! Form::label('email', 'Email:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::email('email', null, array('class' => 'form-control input-xs', 'id' => 'email', 'placeholder' => 'Ingrese Email' )) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('email2', 'Email Secundario:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::email('email2', null, array('class' => 'form-control input-xs', 'id' => 'email2', 'placeholder' => 'Ingrese Email' )) !!}
    </div>
</div>
<div class="form-group">
     {!! Form::label('celular', 'Celular:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-3 col-md-3 col-sm-3">
        {!! Form::text('celular', null, array('class' => 'form-control input-xs', 'id' => 'celular', 'placeholder' => 'Ingrese Celular' )) !!}
    </div>
    {!! Form::label('telefono', 'Teléfono:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-3 col-md-3 col-sm-3">
        {!! Form::text('telefono', null, array('class' => 'form-control input-xs', 'id' => 'telefono', 'placeholder' => 'Cod. ciudad + Teléfono' )) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('sexo', 'Sexo:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-3 col-md-3 col-sm-3">
        {!! Form::select('sexo', $cboSexo, null, array('class' => 'form-control input-xs', 'id' => 'sexo')) !!}
    </div>
    {!! Form::label('pais_id', 'País de Nacimiento:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-3 col-md-3 col-sm-3">
        {!! Form::select('pais_id', $cboPais, null, array('class' => 'form-control input-xs', 'id' => 'pais_id')) !!}

    </div>
</div>
<div class="form-group" id="divubigeo">
    {!! Form::label('departamento_id', 'Ubigeo de Residencia:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::select('departamento_id', $cboDepartamento, isset($departamento)  ? $departamento->id: null, array('style' => 'display: inline; width: 33%;','class' => 'form-control input-xs ', 'id' => 'departamento_id', 'onchange' => 'mostrarProvincias(\''.URL::route('provincia.cboprovincia').'\',\''.$entidad.'\', \'M\')')) !!}
        {!! Form::select('provincia_id', $cboProvincia, isset($provincia) ? $provincia->id : null  , array('style' => 'display: inline; width: 33%;','class' => 'form-control input-xs ', 'id' => 'provincia_id', 'onchange' => 'mostrarDistritos(\''.URL::route('distrito.cbodistrito').'\',\''.$entidad.'\', \'M\')')) !!}
        {!! Form::select('distrito_id', $cboDistrito, null, array('style' => 'display: inline; width: 32%;','class' => 'form-control input-xs', 'id' => 'distrito_id')) !!}

    </div>
</div>
<div class="form-group">
    {!! Form::label('escuela_id', 'Escuela:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::select('escuela_id', $cboEscuela, null, array('class' => 'form-control input-xs', 'id' => 'escuela_id')) !!}

    </div>
</div>
<div class="form-group">
    {!! Form::label('direccion', 'Dirección:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::email('direccion', null, array('class' => 'form-control input-xs', 'id' => 'direccion', 'placeholder' => 'Ingrese Dirección' )) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('facebook', 'Facebook:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::text('facebook', null, array('class' => 'form-control input-xs', 'id' => 'facebook', 'placeholder' => 'Ingrese URL de su Facebook' )) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('linkedin', 'Linkedin:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::text('linkedin', null, array('class' => 'form-control input-xs', 'id' => 'linkedin', 'placeholder' => 'Ingrese URL de su Linkedin' )) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('descripcion', 'Descripcion:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::textarea('descripcion', null, array('style' => 'resize: none;', 'rows' => '3','class' => 'form-control input-xs', 'id' => 'descripcion', 'placeholder' => 'Ingrese drescripción')) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('foto', 'Foto:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::file('foto') !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('privacidad', 'Tipo Privacidad:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9 radio">
        {!! Form::radio('privacidad','privacidadbaja', old('privacidadalta')? old('privacidadalta'):true,array('id'=>'privacidadbaja')) !!}
        Privacidad Baja<br>
        {!! Form::radio('privacidad','privacidadbaja', old('privacidadbaja'),array('id'=>'privacidadalta')) !!}
        Privacidad Alta
    </div>
</div>
<div class="form-group">
    {!! Form::label('discapacidad', '¿Tiene alguna discapacidad?', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9 checkbox">
        {!! Form::checkbox('discapacidad',null, old('discapacidad')) !!}
    </div>
</div>
<div class="form-group checkbox">
    <div class="col-lg-3 col-md-3 col-sm-3 text-right">
        {!! Form::checkbox('noofertasempleo',null, old('noofertasempleo')) !!}
    </div>
    No deseo recibir ofertas de empleo e información Laboral.
</div>
<div class="form-group checkbox">
    <div class="col-lg-3 col-md-3 col-sm-3 text-right">
        {!! Form::checkbox('noofertascurso',null, old('noofertascurso')) !!}
    </div>
    No deseo recibir ofertas de cursos y/o postgrados.
</div>
<div class="form-group checkbox">
    <div class="col-lg-3 col-md-3 col-sm-3 text-right">
        {!! Form::checkbox('noayudar',null, old('noayudar')) !!}
    </div>
    No deseo recibir información de cómo ayudar.
</div>
<div class="form-group checkbox">
    <div class="col-lg-3 col-md-3 col-sm-3 text-right">
        {!! Form::checkbox('nonewsletter',null, old('nonewsletter')) !!}
    </div>
    No deseo suscribirme al newsletter, novedades y comunicados.
</div>
<div class="form-group checkbox">
    <div class="col-lg-3 col-md-3 col-sm-3 text-right">
        {!! Form::checkbox('nocompartircv',null, old('nocompartircv')) !!}
    </div>
    No deseo compartir mi CV con las mejores empresas del mercado.
</div>
<div class="form-group checkbox">
    <div class="col-lg-3 col-md-3 col-sm-3 text-right">
        {!! Form::checkbox('nocomunicacioncomercial',null, old('nocomunicacioncomercial')) !!}
    </div>
    No deseo recibir comunicaciones comerciales de sectores distintos al de empleo.
</div>
<div class="form-group checkbox">
    <div class="col-lg-3 col-md-3 col-sm-3 text-right">
        {!! Form::checkbox('aceptoterminos',null, old('aceptoterminos')) !!}
    </div>
    He leído y acepto los Términos y Condiciones de Servicio y las Políticas de Privacidad.
</div>
<br>
