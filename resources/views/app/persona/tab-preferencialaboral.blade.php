<div class="form-group checkbox">
    <div class="col-lg-3 col-md-3 col-sm-3 text-right">
        {!! Form::checkbox('disponibilidadextranjero',null, isset($preferencialaboral) ? $preferencialaboral->disponibilidadextranjero : null) !!}
    </div>
    Disponibilidad para desarrollar tu actividad profesional fuera de tu país.
</div><br>
<div class="form-group">
    {!! Form::label('jornadalaboral_id', 'Jornada Laboral:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9 ">
        {!! Form::select('jornadalaboral_id', $cboJornadalaboral, isset($preferencialaboral) ? $preferencialaboral->jornadalaboral_id : null, array('class' => 'form-control input-xs', 'id' => 'jornadalaboral_id')) !!}

    </div>
</div>

<div class="form-group">
    {!! Form::label('salariolaboral', 'Expectativa Salarial:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::number('salariolaboral',isset($preferencialaboral) ? $preferencialaboral->salario : null, array('class' => 'form-control input-xs', 'id' => 'salariolaboral', 'placeholder' => 'Ingrese salario' )) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('preferenciamoneda', 'Moneda:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9 ">
        {!! Form::select('preferenciamoneda', $cboMoneda, isset($preferencialaboral) ? $preferencialaboral->moneda : null, array('class' => 'form-control input-xs', 'id' => 'preferenciamoneda')) !!}
    </div>
</div>
<div class="form-group checkbox">
    <div class="col-lg-3 col-md-3 col-sm-3 text-right">
        {!! Form::checkbox('mostrarpreferenciasalario',null, isset($preferencialaboral) ? $preferencialaboral->mostrarpreferenciasalario : null) !!}
    </div>
    Mostrar expectativa salarial en CV.
</div><br>
<div class="form-group">
    {!! Form::label('preferencia_sectorempresa_id', 'Sector Empresa:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9 ">
        {!! Form::select('preferencia_sectorempresa_id', $cboSectorempresa, isset($preferencialaboral) ? $preferencialaboral->sectorempresa_id : null,array('class' => 'form-control input-xs', 'id' => 'preferencia_sectorempresa_id')) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('preferencia_arealaboral_id', 'Área Laboral:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9 ">
        {!! Form::select('preferencia_arealaboral_id', $cboArealaboral, isset($preferencialaboral) ? $preferencialaboral->arealaboral_id : null,array('class' => 'form-control input-xs', 'id' => 'preferencia_arealaboral_id')) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('preferencia_cargolaboral_id', 'Cargo Laboral:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9 ">
        {!! Form::select('preferencia_cargolaboral_id', $cboCargolaboral, isset($preferencialaboral) ? $preferencialaboral->cargolaboral_id : null,array('class' => 'form-control input-xs', 'id' => 'preferencia_cargolaboral_id')) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('preferencia_tipocontrato_id', 'Tipo Contrato:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9 ">
        {!! Form::select('preferencia_tipocontrato_id', $cboTipocontrato, isset($preferencialaboral) ? $preferencialaboral->tipocontrato_id : null,array('class' => 'form-control input-xs', 'id' => 'preferencia_tipocontrato_id')) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('preferencia_departamento_id', 'Departamento:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9 ">
        {!! Form::select('preferencia_departamento_id', $cboDepartamento, isset($preferencialaboral) ? $preferencialaboral->departamento_id : null,array('class' => 'form-control input-xs', 'id' => 'preferencia_departamento_id')) !!}
    </div>
</div>
<div class="form-group checkbox">
    <div class="col-lg-3 col-md-3 col-sm-3 text-right">
        {!! Form::checkbox('cambioresidencia',null, isset($preferencialaboral) ? $preferencialaboral->cambioresidencia : null) !!}
    </div>
    <strong>Disponibilidad para cambio de residencia.</strong>
</div>
<div class="form-group checkbox">
    <div class="col-lg-3 col-md-3 col-sm-3 text-right">
        {!! Form::checkbox('disponibilidadviajar',null, isset($preferencialaboral) ? $preferencialaboral->disponibilidadviajar : null) !!}
    </div>
    <strong>Disponibilidad para viajar.</strong>
</div>
<div class="form-group checkbox">
    <div class="col-lg-3 col-md-3 col-sm-3 text-right">
        {!! Form::checkbox('mostrarcv',null, isset($preferencialaboral) ? $preferencialaboral->mostrarcv : null) !!}
    </div>
    <strong>Deseo mostrar esta sección en el currículum.</strong>
</div>