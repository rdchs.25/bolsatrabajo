<div class="row">
    <div class="panel-primary">
        <div class="panel-body">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <br> {!! Form::hidden('continfcontac',  count($listaconcinfor)==0 ? 0:($listaconcinfor->last())['id'] + 1 , array('id' => 'continfcontac')) !!}
                <table id="tbl-conocimiento-informatico" class="table table-bordered table-striped table-condensed table-hover">
                    <thead style="background: #A9D0F5">
                    <th>Software</th>
                    <th>Nivel</th>
                    <th>Años de Experiencia</th>
                    <th>Descripción</th>
                    <th colspan="2">Opciones</th>
                    <tbody>
                    @foreach ($listaconcinfor as $key => $value)
                        <tr class="selected" id="filaconocinfor{{$value->id}}">
                            <input type="hidden" name="editar{{$value->id}}" id="editar{{$value->id}}"
                                   value="{{$value->id}}">
                            <td><input type="hidden" name="nombre{{$value->id}}"
                                       value="{{$value->nombre}}">{{$value->nombre}}
                            </td>
                            <td><input type="hidden" name="nivel{{$value->id}}"
                                       value="{{$value->nivel}}">{{ ucwords($value->nivel)}}
                            </td>
                            <td><input type="hidden" name="anioexperiencia{{$value->id}}"
                                       value="{{$value->anioexperiencia}}">{{$value->anioexperiencia}}
                            </td>
                            <td><input type="hidden" name="descripcionsoftware{{$value->id}}"
                                       value="{{$value->descripcionsoftware}}">{{$value->descripcionsoftware}}
                            </td>
                            <td>
                                <button onclick="editarconocimientoinformatico('{{$value->id}}');"
                                        class="btn btn-xs btn-warning" type="button">
                                    <div class="glyphicon glyphicon-pencil"></div>
                                    Editar
                                </button>
                            </td>
                            <td>
                                <button onclick="eliminarconocinformatico('{{$value->id}}');" class="btn btn-xs btn-danger"
                                        type="button">
                                    <div class="glyphicon glyphicon-remove"></div>
                                    Eliminar
                                </button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th></th>
                        <th></th>
                        <th class="text-center">
                            {!! Form::button('<i class="glyphicon glyphicon-plus"></i>'.$boton_conoc_informatico, array('class' => 'btn btn-info waves-effect waves-light m-l-10 btn-md', 'id' => 'btnNuevoconocimientosoftware', 'onclick' => 'modal(\''.URL::route($ruta["createconocimientoinformatico"], array('NO')).'\', \''.$titulo_registrar_conoc_informatico.'\', this);')) !!}
                        </th>
                        <th></th>
                        <th></th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
