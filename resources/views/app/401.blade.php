<div class="error-page">
    <h2 class="headline text-yellow"> 404</h2>

    <div class="error-content">
        <h3><i class="fa fa-warning text-yellow"></i> Oops! Usted no tiene permisos para realizar esta acción.</h3>
        <p>
            Este usuario no tiene permiso para realizar este acción.Comuniquese con el administrador del sistema.
            Volver al <a href="/">menú</a> principal.
        </p>

        {{--<form class="search-form">--}}
            {{--<div class="input-group">--}}
                {{--<input type="text" name="search" class="form-control" placeholder="Search">--}}

                {{--<div class="input-group-btn">--}}
                    {{--<button type="submit" name="submit" class="btn btn-warning btn-flat"><i class="fa fa-search"></i>--}}
                    {{--</button>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<!-- /.input-group -->--}}
        {{--</form>--}}

    </div>
    <!-- /.error-content -->
</div>