
<div id="divMensajeError{!! $entidad !!}"></div>
{!! Form::model($alertalaboral, $formData) !!}
	{!! Form::hidden('listar', $listar, array('id' => 'listar')) !!}
	

	<div class="form-group">
		{!! Form::label('escuela_id', 'Escuela:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
		<div class="col-lg-9 col-md-9 col-sm-9">
			{!! Form::select('escuela_id', $cboEscuela, NULL, array('class' => 'form-control input-xs', 'id' => 'escuela_id')) !!}
		</div>
	</div>
	<div class="form-group">
		{!! Form::label('arealaboral_id', 'Area Laboral:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
		<div class="col-lg-9 col-md-9 col-sm-9">
			{!! Form::select('arealaboral_id', $cboArealaboral, NULL, array('class' => 'form-control input-xs', 'id' => 'arealaboral_id')) !!}
		</div>
	</div>
	<div class="form-group">
	    {!! Form::label('cargolaboral_id', 'Cargo Laboral:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
	    <div class="col-lg-9 col-md-9 col-sm-9">
	        {!! Form::select('cargolaboral_id[]', $cboCargolaboral, isset($alertalaboral) ? $alertalaboral->alertalaboralcargos : null, array('data-placeholder'=>'Seleccione...', 'multiple class'=>'chosen-select','class' => 'form-control input-xs', 'id' => 'cargolaboral_id')) !!}
	    </div>
	</div>
	<div class="form-group">
	    {!! Form::label('departamento_id', 'Departamento:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
	    <div class="col-lg-9 col-md-9 col-sm-9">
	        {!! Form::select('departamento_id[]', $cboDepartamento, isset($alertalaboral) ? $alertalaboral->alertalaboraldepartamentos : null, array('data-placeholder'=>'Seleccione...', 'multiple class'=>'chosen-select','class' => 'form-control input-xs', 'id' => 'departamento_id')) !!}
	    </div>
	</div>

	<div class="form-group">
		<div class="col-lg-12 col-md-12 col-sm-12 text-right">
			{!! Form::button('<i class="fa fa-check fa-lg"></i> '.$boton, array('class' => 'btn btn-success btn-sm', 'id' => 'btnGuardar', 'onclick' => 'guardar(\''.$entidad.'\', this)')) !!}
			{!! Form::button('<i class="glyphicon glyphicon-remove"></i> Cancelar', array('class' => 'btn btn-danger btn-sm', 'id' => 'btnCancelar'.$entidad, 'onclick' => 'cerrarModal();')) !!}

			
		</div>
	</div>
{!! Form::close() !!}
<script type="text/javascript">
$(document).ready(function() {

	/*chose multiseleccion*/
   	$(".chosen-select").chosen({width: "100%",max_selected_options: 3});



	configurarAnchoModal('500');
	init(IDFORMMANTENIMIENTO+'{!! $entidad !!}', 'M', '{!! $entidad !!}');
});
</script>