<?php
$icono = 'fa fa-bank';
if ($opcionmenu !== NULL) {
    $icono = $opcionmenu->icon;
}
?>

<div id="divMensajeError{!! $entidad !!}"></div>
{!! Form::model($opcionmenu, $formData) !!}
{!! Form::hidden('listar', $listar, array('id' => 'listar')) !!}
<div class="form-group">
    {!! Form::label('categoriamenu_id', 'Categoria:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::select('categoriamenu_id', $cboCategoriamenu, null, array('class' => 'form-control input-xs chosen-select', 'id' => 'categoriamenu_id')) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('name', 'Nombre:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::text('nombre', null, array('class' => 'form-control input-xs', 'id' => 'nombre', 'placeholder' => 'Ingrese nombre')) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('link', 'Link:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::select('link', $cbolink, null, array('class' => 'form-control input-xs chosen-select', 'id' => 'link')) !!}
        {{--        {!! Form::text('link', null, array('class' => 'form-control input-xs', 'id' => 'link', 'placeholder' => 'Ingrese link')) !!}--}}
    </div>
</div>
<div class="form-group">
    {!! Form::label('orden', 'Orden:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::text('orden', null, array('class' => 'form-control input-xs', 'id' => 'orden', 'placeholder' => 'Ingrese orden')) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('icon', 'Icono:', array('class' => 'col-lg-3 col-md-3 col-sm-3 control-label')) !!}
    <div class="col-lg-9 col-md-9 col-sm-9">
        {!! Form::text('icono', null, array('class' => 'form-control icp icp-auto action-create', 'value'=>'fa-anchor' ,'id' => 'icono', 'placeholder' => 'Ingrese el icono')) !!}
    </div>
</div>
<div class="form-group">
    <div class="col-lg-12 col-md-12 col-sm-12 text-right">
        {!! Form::button('<i class="fa fa-check fa-lg"></i> '.$boton, array('class' => 'btn btn-success btn-sm', 'id' => 'btnGuardar', 'onclick' => 'guardar(\''.$entidad.'\', this)')) !!}
       {!! Form::button('<i class="glyphicon glyphicon-remove"></i> Cancelar', array('class' => 'btn btn-danger btn-sm', 'id' => 'btnCancelar'.$entidad, 'onclick' => 'cerrarModal();')) !!}
    </div>
</div>
{!! Form::close() !!}


<script type="text/javascript">

    $(document).ready(function () {
        $(".chosen-select").chosen({width: "100%"});
        $('.chosen-select').chosen({no_results_text: "Oops, nothing found!"});
        configurarAnchoModal('550');
        init(IDFORMMANTENIMIENTO + '{!! $entidad !!}', 'M', '{!! $entidad !!}');
    });

</script>

<script src="{{asset('template/plugins/iconpicker/js/jquery-migrate-3.0.0.js')}}"></script>
<script src="{{asset('template/plugins/iconpicker/js/fontawesome-iconpicker.js')}}"></script>
<script src="{{asset('app/js/mant.js')}}"></script>

